﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;

namespace Heroes_0._01
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class MyGame : Game
    {
        public GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;
        private string fps = "";

        private bool VisionMode { get; set; }

        public delegate void onUpdate(GameTime gameTime);
        public delegate void onDraw(GameTime gameTime, SpriteBatch spriteBatch);
        public onUpdate updateGame;
        public onUpdate updateInterface;
        public onDraw drawGameObject;
        public onDraw drawLight;
        public onDraw drawBackground;
        public onDraw drawInterface;
        public onDraw drawScaleInterface;

        private RenderTarget2D Frame;
        private BlendState blendState;

        public MyGame()
        {
            IsMouseVisible = true;
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            graphics.IsFullScreen = true;

            VisionMode = true;
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            GameFeature.Font = Content.Load<SpriteFont>("Interface/Font");
            GameFeature.testEffect = Content.Load<SoundEffect>("Songs/Splat3");

            Frame = new RenderTarget2D(GraphicsDevice, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);

            blendState = new BlendState();

            blendState.AlphaBlendFunction = BlendFunction.ReverseSubtract;
            blendState.AlphaDestinationBlend = Blend.One;
            blendState.AlphaSourceBlend = Blend.BlendFactor;
            blendState.BlendFactor = new Color(255, 255, 255, 255);
            blendState.ColorBlendFunction = BlendFunction.Subtract;
            blendState.ColorDestinationBlend = Blend.One;
            blendState.ColorSourceBlend = Blend.BlendFactor;
            blendState.ColorWriteChannels = ColorWriteChannels.All;
            blendState.ColorWriteChannels1 = ColorWriteChannels.All;
            blendState.ColorWriteChannels2 = ColorWriteChannels.All;
            blendState.ColorWriteChannels3 = ColorWriteChannels.All;
            blendState.MultiSampleMask = -1;

            MediaPlayer.Volume = 0.5f;
            MediaPlayer.IsRepeating = true;         
            Camera.Start(this);
            
            Player player = new Player(this);
        }


        protected override void UnloadContent()
        {

        }


        protected override void Update(GameTime gameTime)
        {
            if (gameTime.ElapsedGameTime.Milliseconds != 0)
                fps = $"{1000 / gameTime.ElapsedGameTime.Milliseconds} {1000 / timeDraw}";
            else
                fps = "1000";

            if (!GameFeature.pause && !GameFeature.timeStop) updateGame?.Invoke(gameTime);

            updateInterface?.Invoke(gameTime);

            if (!GameFeature.pause) Camera.update(gameTime);

            InterfaceCore.update(gameTime);

            base.Update(gameTime);
        }

        int timeDraw = 1;
        
        protected override void Draw(GameTime gameTime)
        {
            timeDraw = gameTime.ElapsedGameTime.Milliseconds;

            if (VisionMode)
            {
                #region Игровые объекты с полем видимости

                GraphicsDevice.SetRenderTarget(Frame);

                spriteBatch.Begin(SpriteSortMode.FrontToBack, null, null, DepthStencilState.DepthRead);
                drawBackground?.Invoke(gameTime, spriteBatch);
                spriteBatch.End();

                spriteBatch.Begin(SpriteSortMode.FrontToBack, null, null, DepthStencilState.DepthRead, null, null, Camera.Transform);
                drawGameObject?.Invoke(gameTime, spriteBatch);
                spriteBatch.End();
          
                GraphicsDevice.SetRenderTarget(null);

                spriteBatch.Begin(SpriteSortMode.FrontToBack, null);
                spriteBatch.Draw(Frame, Vector2.Zero, Color.White);
                spriteBatch.End();

                spriteBatch.Begin(SpriteSortMode.FrontToBack, null, null, DepthStencilState.DepthRead, null, null, Camera.Transform);
                drawLight?.Invoke(gameTime, spriteBatch);
                spriteBatch.End();

                spriteBatch.Begin(SpriteSortMode.FrontToBack, blendState);
                spriteBatch.Draw(Frame, Vector2.Zero, Color.White);
                spriteBatch.End();
                #endregion
            }
            else
            {
                #region Просто игровые объекты
                spriteBatch.Begin(SpriteSortMode.FrontToBack, null, null, DepthStencilState.DepthRead);
                drawBackground?.Invoke(gameTime, spriteBatch);
                spriteBatch.End();

                spriteBatch.Begin(SpriteSortMode.FrontToBack, null, null, DepthStencilState.DepthRead, null, null, Camera.Transform);
                drawGameObject?.Invoke(gameTime, spriteBatch);
                spriteBatch.End();
                #endregion
            }

            #region Интерфейс
            spriteBatch.Begin(SpriteSortMode.FrontToBack, null, null, DepthStencilState.DepthRead, null, null, Camera.Transform);
            drawScaleInterface?.Invoke(gameTime, spriteBatch);
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.FrontToBack, null, null, DepthStencilState.DepthRead);
            drawInterface?.Invoke(gameTime, spriteBatch);
            spriteBatch.End();
            #endregion

            //spriteBatch.Begin();
            //spriteBatch.DrawString(GameFeature.Font, $"{fps} { Camera.positionCamera}", Vector2.Zero, Color.White, 0, Vector2.Zero, 0.2f, SpriteEffects.None, 1);
            //spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
