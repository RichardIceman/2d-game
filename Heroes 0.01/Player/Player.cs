﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Player
    {
        private MouseState mouseState;
        private MouseState previouseMouseState;
        private KeyboardState keyboardState;
        private KeyboardState previouseKeyboardState;
        private MyGame game;

        private Map currentMap;
        private Maze maze;
        private WorldMap startMap;
        private BossMap_1 boss_1;
        private BossMap_2 boss_2;
        private BossMap_3 boss_3;

        private Ogr_1 grog;
        private Seller_1 seller;

        private MainMenu mainMenu;
        private PlayerInterface playerInterface;
        private Lose lose;
        private EndScreen end;
        private MenuPause menuPause;

        private Dialogs dialogs;

        public List<AbstractCharacter> listCharactersUnderControll;
        private List<AbstractCharacter> listSelectCharacters;
        private AbstractCharacter targetForAttack;
        private int currentCharacter = 0;

        private Vector2[,] spawnPoints;
        private int currentSpawnPoint = 0;

        private SelectionField selectField;

        private DampingScreen dampingfEffect;

        private Story story;

        private MovePointAnimation iconMove;

        public Player(MyGame game)
        {
            this.game = game;

            //загрузка основных компонентов
            loadGame();

            mainMenu = new MainMenu(game);
            lose = new Lose(game);
            end = new EndScreen(game);
            menuPause = new MenuPause(game);
            selectField = new SelectionField(game, this);

            iconMove = new MovePointAnimation(game);

            mainMenu.Show();

            story = new Story(game);

            dialogs = new Dialogs(game, playerInterface);
            dialogs.CurrentDialog = 0;

            dampingfEffect = new DampingScreen(game);

            ////.......................////
            //playerInterface.Show();    //
            //currentMap.Show();         //
            //mainMenu.Hide();           //
            //GameFeature.pause = false; //
            ///////////////////////////////

            mainMenu.play.mouseLeftButtonDown += new EventHandler((o, e) =>
            {
                if (GameFeature.newStory)
                {
                    GameFeature.newStory = false;
                    dampingfEffect.Play();
                    dampingfEffect.animationEnd += DampingfEffect_storyBegin;
                }
                else
                {
                    dampingfEffect.PlayRevers();
                    playerInterface.Show();
                    mainMenu.Hide();
                    currentMap.Show();
                    GameFeature.pause = false;
                }
            });

            menuPause.mainMenu.mouseLeftButtonDown += new EventHandler((o, e) =>
            {
                dampingfEffect.Play();
                menuPause.Hide();
                dampingfEffect.animationEnd += DampingfEffect_mainMenu;
                GameFeature.pause = true;
            });

            menuPause.restart.mouseLeftButtonDown += Restart_mouseLeftButtonDown;
            lose.restart.mouseLeftButtonDown += nextTry_mouseLeftButtonDown; ;
            end.mainMenu.mouseLeftButtonDown += end_mainMenu_mouseLeftButtonDown;
            updateSelectCharacters(listCharactersUnderControll);
            currentCharacter = 0;

            playerInterface.updateSkillIcons(listSelectCharacters[currentCharacter]);
            playerInterface.hero_1.UpdateTarget(listCharactersUnderControll[0]);
            playerInterface.hero_2.UpdateTarget(listCharactersUnderControll[1]);
            playerInterface.hero_3.UpdateTarget(listCharactersUnderControll[2]);

            playerInterface.hero_1.mouseLeftButtonDown += HeroInfo_mouseLeftButtonDown;
            playerInterface.hero_2.mouseLeftButtonDown += HeroInfo_mouseLeftButtonDown;
            playerInterface.hero_3.mouseLeftButtonDown += HeroInfo_mouseLeftButtonDown;

            playerInterface.nextCharacter.mouseLeftButtonDown += NextCharacter_mouseLeftButtonDown;
            playerInterface.previouseCharacter.mouseLeftButtonDown += PreviouseCharacter_mouseLeftButtonDown;
            playerInterface.iconPause.mouseLeftButtonDown += IconPause_mouseLeftButtonDown;

            dialogs.button_shop.mouseLeftButtonDown += Button_shop_mouseLeftButtonDown;
            dialogs.button_give20.mouseLeftButtonDown += Button_give20_mouseLeftButtonDown;
            dialogs.button_give100.mouseLeftButtonDown += Button_give100_mouseLeftButtonDown;
            dialogs.button_give500.mouseLeftButtonDown += Button_give500_mouseLeftButtonDown;

            dialogs.button_give_1.mouseLeftButtonDown += Button_give_1_mouseLeftButtonDown;
            dialogs.button_give_2.mouseLeftButtonDown += Button_give_2_mouseLeftButtonDown;
            dialogs.button_give_3.mouseLeftButtonDown += Button_give_3_mouseLeftButtonDown;

            dialogs.cancel.mouseLeftButtonDown += Cancel_mouseLeftButtonDown;
            dialogs.endGuard.mouseLeftButtonDown += EndGuard_mouseLeftButtonDown;
            dialogs.endKing.mouseLeftButtonDown += EndKing_mouseLeftButtonDown;
        }

        private void EndKing_mouseLeftButtonDown(object sender, EventArgs e)
        {
            dampingfEffect.Play();
            dampingfEffect.animationEnd += DampingfEffect_animationEndKing;
        }

        private void DampingfEffect_animationEndKing(object sender, EventArgs e)
        {
            dampingfEffect.animationEnd -= DampingfEffect_animationEndKing;
            dialogs.Hide();
            story.currentStory = 3;
            story.Show();
            story.storyEnd += StoryKing_storyEnd;
        }

        private void StoryKing_storyEnd(object sender, EventArgs e)
        {
            story.storyEnd -= StoryKing_storyEnd;
            end.Show();
        }

        private void EndGuard_mouseLeftButtonDown(object sender, EventArgs e)
        {
            dampingfEffect.Play();
            dampingfEffect.animationEnd += DampingfEffect_animationEndGuard;
        }

        private void DampingfEffect_animationEndGuard(object sender, EventArgs e)
        {
            dampingfEffect.animationEnd -= DampingfEffect_animationEndGuard;
            dialogs.Hide();
            story.currentStory = 4;
            story.Show();
            story.storyEnd += StoryGuard_storyEnd;
        }

        private void StoryGuard_storyEnd(object sender, EventArgs e)
        {
            story.storyEnd -= StoryGuard_storyEnd;
            end.Show();
        }

        private void Cancel_mouseLeftButtonDown(object sender, EventArgs e)
        {
            dialogs.CurrentDialog = 9;
            dialogs.Show();
            grog.currentMap.removeCharacterFromMap(grog);
            grog.setPosition(new Vector2(2500, 1500));
            boss_3.addCharacterOnMap(grog);
            grog.Show();
            grog.isAgressive = true;
            grog.isDamageImmune = false;
            GameFeature.endGame = true;
        }

        private void DampingfEffect_storyBegin(object sender, EventArgs e)
        {
            dampingfEffect.animationEnd -= DampingfEffect_storyBegin;
            story.currentStory = 0;
            story.Show();
            story.storyEnd += Begin_storyEnd;
        }

        private void Begin_storyEnd(object sender, EventArgs e)
        {
            story.storyEnd -= Begin_storyEnd;
            dampingfEffect.PlayRevers();
            playerInterface.Show();
            mainMenu.Hide();
            currentMap.Show();
            GameFeature.pause = false;
        }

        private void Button_give_3_mouseLeftButtonDown(object sender, EventArgs e)
        {
            dialogs.Hide();
            dampingfEffect.Play();
            dampingfEffect.animationEnd += DampingfEffect_badEnd;
        }

        private void DampingfEffect_badEnd(object sender, EventArgs e)
        {
            dampingfEffect.animationEnd -= DampingfEffect_badEnd;
            story.currentStory = 2;
            story.Show();
            story.storyEnd += Story_badStoryEnd;
        }

        private void Story_badStoryEnd(object sender, EventArgs e)
        {
            story.storyEnd -= Story_badStoryEnd;
            end.Show();
        }

        private void Button_give_2_mouseLeftButtonDown(object sender, EventArgs e)
        {
            for (int i = 0; i < listCharactersUnderControll.Count; i++)
            {
                for (int j = 0; j < GameFeature.inventorySize; j++)
                    if (listCharactersUnderControll[i].inventory[j] != null &&
                        listCharactersUnderControll[i].inventory[j].name == "Сердце Глыбы")
                    {
                        listCharactersUnderControll[i].inventory[j].Remove();

                        dialogs.Hide();
                        dialogs.CurrentDialog = 6;
                        dialogs.Show();
                        GameFeature.lostHeartGolem = true;
                        return;
                    }
            }
        }

        private void Button_give_1_mouseLeftButtonDown(object sender, EventArgs e)
        {
            for (int i = 0; i < listCharactersUnderControll.Count; i++)
            {
                for (int j = 0; j < GameFeature.inventorySize; j++)
                    if (listCharactersUnderControll[i].inventory[j] != null &&
                        listCharactersUnderControll[i].inventory[j].name == "Сердце Джу")
                    {
                        listCharactersUnderControll[i].inventory[j].Remove();

                        dialogs.Hide();
                        dialogs.CurrentDialog = 3;
                        dialogs.Show();
                        playerInterface.shop.Discount = 0.3f;
                        GameFeature.lostHeartGhost = true;
                        return;
                    }
            }
        }

        private void Button_give500_mouseLeftButtonDown(object sender, EventArgs e)
        {
            if (GameFeature.Money < 500)
            {
                seller.Say("Сначала найдите\n500 золотых!", TypeCharacter.Друг);
                playerInterface.ShowBottomBar();
                dialogs.Hide();
                targetForAttack.lostFocus();
                targetForAttack = null;
                playerInterface.targetInfo.UpdateTarget(targetForAttack);
            }
            else
            {
                GameFeature.Money -= 500;
                dialogs.Hide();
                dialogs.CurrentDialog = 5;
                dialogs.Show();
            }
        }

        private void Button_give100_mouseLeftButtonDown(object sender, EventArgs e)
        {
            if (GameFeature.Money < 100)
            {
                seller.Say("Сначала найдите\n100 золотых!", TypeCharacter.Друг);
                playerInterface.ShowBottomBar();
                dialogs.Hide();
                targetForAttack.lostFocus();
                targetForAttack = null;
                playerInterface.targetInfo.UpdateTarget(targetForAttack);
            }
            else
            {
                GameFeature.Money -= 100;
                dialogs.Hide();
                dialogs.CurrentDialog = 3;
                dialogs.Show();
            }
        }

        private void Button_give20_mouseLeftButtonDown(object sender, EventArgs e)
        {
            if (GameFeature.Money < 20)
            {
                seller.Say("Сначала найдите\n20 золотых!", TypeCharacter.Друг);
                playerInterface.ShowBottomBar();
                dialogs.Hide();
                targetForAttack.lostFocus();
                targetForAttack = null;
                playerInterface.targetInfo.UpdateTarget(targetForAttack);
            }
            else
            {
                GameFeature.Money -= 20; 

                dialogs.Hide();
                dialogs.CurrentDialog = 1;
                dialogs.Show();
            }
          
        }

        private void Button_shop_mouseLeftButtonDown(object sender, EventArgs e)
        {
            GameFeature.InterfaceActive = true;
            dialogs.Hide();
            targetForAttack.lostFocus();
            targetForAttack = null;
            playerInterface.targetInfo.UpdateTarget(targetForAttack);

            playerInterface.ShowBottomBar();
            playerInterface.shop.Show();
        }

        private void nextTry_mouseLeftButtonDown(object sender, EventArgs ex)
        {
            dampingfEffect.PlayRevers();
            lose.Hide();      

            if (boss_1.boss.isAlive)
            {
                boss_1.Restart();
                boss_1.Hide();
                boss_1.boss.died += Boss_1_died;
                boss_1.listPortals[0].activate += boss_1_0_activate;
                boss_1.listPortals[1].activate += boss_1_1_activate;
            }
            if (boss_2.boss.isAlive)
            {
                boss_2.Restart();
                boss_2.Hide();
                boss_2.boss.died += Boss_2_died;
                boss_2.listPortals[0].activate += boss_2_0_activate;
                boss_2.listPortals[1].activate += boss_2_1_activate;
            }
            if (boss_3.boss.isAlive)
            {
                boss_3.Restart();
                boss_3.Hide();
                boss_3.boss.died += Boss_3_died;
                boss_3.listPortals[0].activate += boss_3_0_activate;
            }

            for (int i = 0; i < listCharactersUnderControll.Count; i++)
            {
                listCharactersUnderControll[i].currentMap.removeCharacterFromMap(listCharactersUnderControll[i]);
                listCharactersUnderControll[i].Resurrect();
                listCharactersUnderControll[i].healthBar.Value = listCharactersUnderControll[i].healthBar.MaxValue;
                listCharactersUnderControll[i].manaBar.Value = listCharactersUnderControll[i].manaBar.MaxValue;
                listCharactersUnderControll[i].setPosition(spawnPoints[currentSpawnPoint, i]);
                currentMap.addCharacterOnMap(listCharactersUnderControll[i]);

                if (!listCharactersUnderControll[i].isVisible) listCharactersUnderControll[i].Show();
            }

            updateSelectCharacters(listCharactersUnderControll);

            grog.isAgressive = false;

            targetForAttack = null;
            playerInterface.targetInfo.UpdateTarget(targetForAttack);

            Camera.focusOn(-listSelectCharacters[0].positionOnMap);

            if (!currentMap.isVisible) currentMap.Show();
        }

        private void HeroInfo_mouseLeftButtonDown(object sender, EventArgs e)
        {
            HeroInfo info = sender as HeroInfo;

            if (info.character != null && listSelectCharacters[currentCharacter] == info.character) Camera.focusOn(-info.character.positionOnMap);

            updateSelectCharacters(info.character);
        }

        private void end_mainMenu_mouseLeftButtonDown(object sender, EventArgs e)
        {
            Restart_mouseLeftButtonDown(null, null);
            GameFeature.pause = true;
            GameFeature.newStory = true;
            end.Hide();
            currentMap.Hide();
            mainMenu.Show();
        }

        private void IconPause_mouseLeftButtonDown(object sender, EventArgs e)
        {
            GameFeature.timeStop = !GameFeature.timeStop;
            playerInterface.iconPause.Update();
        }

        private void DampingfEffect_mainMenu(object sender, EventArgs e)
        {
            dampingfEffect.animationEnd -= DampingfEffect_mainMenu;
            playerInterface.Hide();
            currentMap.Hide();
            mainMenu.Show();
        }

        private void PreviouseCharacter_mouseLeftButtonDown(object sender, EventArgs e)
        {
            selectPreviouseHero();
        }

        private void NextCharacter_mouseLeftButtonDown(object sender, EventArgs e)
        {
            selectNextHero();
        }

        private void Restart_mouseLeftButtonDown(object sender, EventArgs e)
        {
            if (mainMenu.isVisible) mainMenu.Hide();
            if (lose.isVisible) lose.Hide();

            menuPause.Hide();

            GameFeature.countTry = GameFeature.maxCountTry;
            GameFeature.score = 0;
            GameFeature.Money = 0;
            GameFeature.lostHeartGhost = false;
            GameFeature.lostHeartGolem = false;
            GameFeature.endGame = false;

            dialogs.CurrentDialog = 0;
            dialogs.canSkip = true;

            playerInterface.shop.Discount = 0;

            for (int i = 0; i < listCharactersUnderControll.Count; i++)
            {
                listCharactersUnderControll[i].Restart();
                listCharactersUnderControll[i].setPosition(spawnPoints[0, i]);
            }

            currentMap.removeListCharctersFromMap(listCharactersUnderControll);

            maze.Restart();
            startMap.Restart();
            boss_1.Restart();
            boss_2.Restart();
            boss_3.Restart();

            boss_1.boss.died += Boss_1_died;
            boss_2.boss.died += Boss_2_died;
            boss_3.boss.died += Boss_3_died;

            maze.Hide();
            startMap.Hide();
            boss_1.Hide();
            boss_2.Hide();
            boss_3.Hide();

            Vector2[] points = maze.getSecondPortalSpawnPoint();
            spawnPoints[5, 0] = points[0];
            spawnPoints[5, 1] = points[1];
            spawnPoints[5, 2] = points[2];

            points = maze.getThirdPortalSpawnPoint();
            spawnPoints[6, 0] = points[0];
            spawnPoints[6, 1] = points[1];
            spawnPoints[6, 2] = points[2];

            seller = new Seller_1(game, new Vector2(900, 1200));
            grog = new Ogr_1(game, new Vector2(900, 1400));

            grog.died += Grog_died;

            playerInterface.shop.setSeller(seller);

            startMap.addCharacterOnMap(seller);
            startMap.addCharacterOnMap(grog);

            currentMap = startMap;

            grog.isAgressive = false;

            currentMap.addListCharactersOnMap(listCharactersUnderControll);
            currentMap.Show();

            currentSpawnPoint = 0;

            targetForAttack = null;
            playerInterface.targetInfo.UpdateTarget(targetForAttack);

            updateSelectCharacters(listCharactersUnderControll);

            dampingfEffect.PlayRevers();

            Camera.focusOn(new Vector2(-game.GraphicsDevice.Viewport.Width / 2,
                                       -game.GraphicsDevice.Viewport.Height / 2));
            loadPortals();
        }

        private void Boss_2_died(object sender, EventArgs e)
        {
            boss_2.boss.died -= Boss_2_died;
            dialogs.CurrentDialog = 4;

            Vector2[] newPos = maze.getSecondSellerPoint();

            seller.setPosition(newPos[0]);
            grog.setPosition(newPos[1]);

            grog.isAgressive = false;
        }

        private void Boss_1_died(object sender, EventArgs e)
        {
            boss_1.boss.died -= Boss_1_died;
            dialogs.CurrentDialog = 2;

            Vector2[] newPos = maze.getFirstSellerPoint();

            seller.currentMap.removeCharacterFromMap(seller);
            grog.currentMap.removeCharacterFromMap(grog);

            seller.setPosition(newPos[0]);
            grog.setPosition(newPos[1]);

            grog.isAgressive = false;

            maze.addCharacterOnMap(seller);
            maze.addCharacterOnMap(grog);
        }

        private void loadGame()
        {
            playerInterface = new PlayerInterface(game);

            spawnPoints = new Vector2[7, 3]
            {
                {new Vector2(500,200), new Vector2(800, 200), new Vector2(500, 400) },//старт
                {new Vector2(4000,800), new Vector2(3800, 800), new Vector2(3800, 700) },//TrainingMap правый
                {new Vector2(3500,2600), new Vector2(3300, 2600), new Vector2(3300, 2500) },//TrainingMap нижний
                {new Vector2(1000,600), new Vector2(1000, 400), new Vector2(700, 600) },//босс вход
                {new Vector2(600,600), new Vector2(600, 300), new Vector2(300, 600) },//лабиринт вход
                {Vector2.Zero, Vector2.Zero, Vector2.Zero}, //лабирит 1 портал
                {Vector2.Zero, Vector2.Zero, Vector2.Zero}  //лабирит 2 портал
            };

            listSelectCharacters = new List<AbstractCharacter>();

            listCharactersUnderControll = new List<AbstractCharacter>(){
                new Tank(game, Vector2.Zero),
                new Archer(game, Vector2.Zero),
                new Healer(game, Vector2.Zero)
            };

            maze = new Maze(game, mouseLeftButtonDown);
            startMap = new WorldMap(game, mouseLeftButtonDown);
            boss_1 = new BossMap_1(game, mouseLeftButtonDown);
            boss_2 = new BossMap_2(game, mouseLeftButtonDown);
            boss_3 = new BossMap_3(game, mouseLeftButtonDown);

            boss_1.boss.died += Boss_1_died;
            boss_2.boss.died += Boss_2_died;
            boss_3.boss.died += Boss_3_died;

            grog = new Ogr_1(game, new Vector2(900, 1400));
            seller = new Seller_1(game, new Vector2(900, 1200));

            grog.died += Grog_died;

            playerInterface.shop.setSeller(seller);

            startMap.addCharacterOnMap(grog);
            startMap.addCharacterOnMap(seller);

            Vector2[] points = maze.getSecondPortalSpawnPoint();
            spawnPoints[5, 0] = points[0];
            spawnPoints[5, 1] = points[1];
            spawnPoints[5, 2] = points[2];

            points = maze.getThirdPortalSpawnPoint();
            spawnPoints[6, 0] = points[0];
            spawnPoints[6, 1] = points[1];
            spawnPoints[6, 2] = points[2];

            maze.Hide();
            startMap.Hide();
            boss_1.Hide();
            boss_2.Hide();
            boss_3.Hide();

            maze.mouseRightButtonDown += mouseRightButtonDown;
            startMap.mouseRightButtonDown += mouseRightButtonDown;
            boss_1.mouseRightButtonDown += mouseRightButtonDown;
            boss_2.mouseRightButtonDown += mouseRightButtonDown;
            boss_3.mouseRightButtonDown += mouseRightButtonDown;
              
            loadPortals();

            currentSpawnPoint = 0;

            for (int i = 0; i < listCharactersUnderControll.Count; i++)
            {
                listCharactersUnderControll[i].setPosition(spawnPoints[currentSpawnPoint, i]);
                listCharactersUnderControll[i].mouseLeftButtonDown += mouseLeftButtonDown;
                listCharactersUnderControll[i].died += checkAllDied;
            }

            currentMap = startMap;

            currentMap.addListCharactersOnMap(listCharactersUnderControll);

            playerInterface.shop.setCustomers(listCharactersUnderControll);

            playerInterface.iconAttack.mouseLeftButtonDown += Attack;
            game.updateInterface += update;
        }

        private void Grog_died(object sender, EventArgs e)
        {
            playerInterface.HideBottomBar();
            dialogs.CurrentDialog = 11;
            dialogs.Show();
        }

        private void Boss_3_died(object sender, EventArgs e)
        {
            boss_3.listPortals[0].isActive = false;

            for (int i = 0; i < listCharactersUnderControll.Count; i++)
                for (int j = 0; j < GameFeature.inventorySize; j++)
                    if (listCharactersUnderControll[i].inventory[j] != null)
                    {
                        if (listCharactersUnderControll[i].inventory[j].name == "Сердце Джу") GameFeature.lostHeartGhost = false;
                        else if (listCharactersUnderControll[i].inventory[j].name == "Сердце Глыбы") GameFeature.lostHeartGolem = false;
                    }

            dialogs.CurrentDialog = 7;
            dialogs.Show();
            playerInterface.HideBottomBar();

            for (int i = 0; i < listCharactersUnderControll.Count; i++)
            {
                if (!listCharactersUnderControll[i].isAlive) listCharactersUnderControll[i].Resurrect();

                listCharactersUnderControll[i].healthBar.Value = listCharactersUnderControll[i].healthBar.MaxValue;
                listCharactersUnderControll[i].manaBar.Value = listCharactersUnderControll[i].manaBar.MaxValue;
            }

            seller.currentMap.removeCharacterFromMap(seller);
            seller.setPosition(new Vector2(4300, 600));
            boss_3.addCharacterOnMap(seller);
            seller.Show();
        }

        private void loadPortals()
        {
            startMap.listPortals[0].isActive = false;
            startMap.listPortals[0].activate += startMap_0_activate;
            startMap.listPortals[1].activate += startMap_1_activate;

            maze.listPortals[0].activate += maze_0_activate;
            maze.listPortals[1].activate += maze_1_activate;
            maze.listPortals[2].activate += maze_2_activate;

            boss_1.listPortals[0].activate += boss_1_0_activate;
            boss_1.listPortals[1].activate += boss_1_1_activate;

            boss_2.listPortals[0].activate += boss_2_0_activate;
            boss_2.listPortals[1].activate += boss_2_1_activate;

            boss_3.listPortals[0].activate += boss_3_0_activate;
        }

        private void boss_3_0_activate(object sender, EventArgs e)
        {
            dampingfEffect.Play();
            dampingfEffect.animationEnd += teleportOnMazePortal_3;
        }

        private void boss_2_1_activate(object sender, EventArgs e)
        {
            dampingfEffect.Play();
            dampingfEffect.animationEnd += teleportOnMazePortal_2;
            maze.openWall();
        }

        private void boss_2_0_activate(object sender, EventArgs e)
        {
            dampingfEffect.Play();
            dampingfEffect.animationEnd += teleportOnMazePortal_2;
        }

        private void boss_1_1_activate(object sender, EventArgs e)
        {
            dampingfEffect.Play();
            startMap.listPortals[0].isActive = true;
            dampingfEffect.animationEnd += teleportOnMazePortal_1;
        }

        private void boss_1_0_activate(object sender, EventArgs e)
        {
            dampingfEffect.Play();
            dampingfEffect.animationEnd += teleportOnTrainingPortal_2;
        }

        private void maze_2_activate(object sender, EventArgs e)
        {
            dampingfEffect.Play();
            dampingfEffect.animationEnd += teleportOnBoss_3;
        }

        private void maze_1_activate(object sender, EventArgs e)
        {
            dampingfEffect.Play();
            dampingfEffect.animationEnd += teleportOnBoss_2;
        }

        private void maze_0_activate(object sender, EventArgs e)
        {
            dampingfEffect.Play();
            dampingfEffect.animationEnd += teleportOnTrainingPortal_1;
        }

        private void startMap_1_activate(object sender, EventArgs e)
        {
            dampingfEffect.Play();
            dampingfEffect.animationEnd += teleportOnBoss_1;
        }

        private void startMap_0_activate(object sender, EventArgs e)
        {
            dampingfEffect.Play();
            dampingfEffect.animationEnd += teleportOnMazePortal_1;
        }

        private void teleportOnMazePortal_1(object o, EventArgs e)
        {
            teleportOn(maze, 4, teleportOnMazePortal_1);
        }

        private void teleportOnMazePortal_2(object ox, EventArgs ex)
        {
            teleportOn(maze, 5, teleportOnMazePortal_2);

            if (boss_2.boss.isAlive)
            {
                boss_2.Restart();

                boss_2.boss.died += Boss_2_died;

                boss_2.listPortals[0].activate += boss_2_0_activate;
                boss_2.listPortals[1].activate += boss_2_1_activate;
            }
        }

        private void teleportOnMazePortal_3(object ox, EventArgs ex)
        {
            teleportOn(maze, 6, teleportOnMazePortal_3);
            if (boss_3.boss.isAlive)
            {
                for (int i = 0; i < listCharactersUnderControll.Count; i++)
                   if (listCharactersUnderControll[i].isAlive)
                        listCharactersUnderControll[i].ScaleVision = listCharactersUnderControll[i].defaultScaleVision;

                boss_3.Restart();

                boss_3.boss.died += Boss_3_died;

                boss_3.listPortals[0].activate += boss_3_0_activate;
            }
        }

        private void teleportOnTrainingPortal_1(object o, EventArgs e)
        {
            teleportOn(startMap, 1, teleportOnTrainingPortal_1);
        }

        private void teleportOnTrainingPortal_2(object ox, EventArgs ex)
        {
            teleportOn(startMap, 2, teleportOnTrainingPortal_2);

            if (boss_1.boss.isAlive)
            {
                boss_1.Restart();
                boss_1.Hide();

                boss_1.boss.died += Boss_1_died;

                boss_1.listPortals[0].activate += boss_1_0_activate;
                boss_1.listPortals[1].activate += boss_1_1_activate;
            }
        }

        private void teleportOnBoss_1(object o, EventArgs e)
        {
            teleportOn(boss_1, 3, teleportOnBoss_1);
        }

        private void teleportOnBoss_2(object o, EventArgs e)
        {
            teleportOn(boss_2, 3, teleportOnBoss_2);
        }

        private void teleportOnBoss_3(object o, EventArgs e)
        {
            teleportOn(boss_3, 3, teleportOnBoss_3);
        }

        private void teleportOn(Map map, int spawnPointIndex, EventHandler eventHandler)
        {
            if (targetForAttack != null)
            {
                targetForAttack.lostFocus();
                targetForAttack = null;
            }

            dampingfEffect.animationEnd -= eventHandler;

            currentMap.removeListCharctersFromMap(listCharactersUnderControll);
            currentMap.Hide();

            playerInterface.targetInfo.UpdateTarget(targetForAttack);

            currentSpawnPoint = spawnPointIndex;

            for (int i = 0; i < listCharactersUnderControll.Count; i++)
                if (listCharactersUnderControll[i].isAlive)
                    listCharactersUnderControll[i].setPosition(spawnPoints[currentSpawnPoint, i]);

            for (int i = 0; i < listCharactersUnderControll.Count; i++)
                if (listCharactersUnderControll[i].isAlive)
                {
                    Camera.focusOn(-listCharactersUnderControll[i].positionOnMap);
                    break;
                }

            currentMap = map;

            currentMap.Show();
            currentMap.addListCharactersOnMap(listCharactersUnderControll);

            dampingfEffect.PlayRevers();
        }

        private void checkAllDied(object o, EventArgs e)
        {
            bool allDie = true;

            for (int i = 0; i < listCharactersUnderControll.Count; i++)
            {
                if (listCharactersUnderControll[i].isAlive)
                {
                    allDie = false;
                    break;
                }
            }

            if (allDie)
            {
                dampingfEffect.Play();
                dampingfEffect.animationEnd += DampingfEffect_animationEnd;
            }
        }

        private void DampingfEffect_animationEnd(object sender, EventArgs e)
        {
            dampingfEffect.animationEnd -= DampingfEffect_animationEnd;

            currentMap.Hide();
            GameFeature.countTry--;

            if (dialogs.isVisible)
            {
                dialogs.Hide();
                playerInterface.ShowBottomBar();
                if (!dialogs.dialogEnd)
                    dialogs.currentPhrase = 0;
            }
            if (GameFeature.endGame)
            {
                story.currentStory = 1;
                story.Show();
                story.storyEnd += Story_deadStoryEnd;
            }
            else
            {
                if (GameFeature.countTry <= 0)
                {
                    end.Show();
                }
                else
                {
                    lose.Show();
                }
            }
        }

        private void Story_deadStoryEnd(object sender, EventArgs e)
        {
            story.storyEnd -= Story_deadStoryEnd;
            end.Show();
        }

        private void mouseLeftButtonDown(object o, EventArgs e)
        {
            if (!GameFeature.InterfaceActive) return;

            AbstractCharacter newTarget = (AbstractCharacter)o;
            switch (newTarget.typeCharacter)
            {
                case TypeCharacter.Герой:
                    clearListSelectCharacter();
                    newTarget.getFocus();
                    listSelectCharacters.Add(newTarget);
                    currentCharacter = 0;
                    playerInterface.updateSkillIcons(listSelectCharacters[currentCharacter]);
                    playerInterface.nextCharacter.isVisible = false;
                    playerInterface.previouseCharacter.isVisible = false;

                    playerInterface.nextIsVisible = false;
                    break;
                case TypeCharacter.Враг:
                    if (newTarget == targetForAttack)
                    {
                        Attack(null, null);
                        break;
                    }
                    targetForAttack?.lostFocus();
                    if (targetForAttack != null) targetForAttack.died -= onDie;
                    targetForAttack = newTarget;
                    playerInterface.targetInfo.UpdateTarget(targetForAttack);
                    targetForAttack.getFocus();
                    targetForAttack.died += onDie;
                    break;
                case TypeCharacter.Друг:
                    if (newTarget == targetForAttack)
                    {
                        Vector2 shift = new Vector2(-100, 50);

                        bool onPosition = false;

                        for (int i = 0; i < listSelectCharacters.Count; i++)
                        {
                            Vector2 distance = newTarget.positionOnMap - listSelectCharacters[i].positionOnMap;

                            if (!GameFeature.inEllipse(distance, playerInterface.shop.maxRange))
                            {
                                listSelectCharacters[i].moveTo((newTarget.positionOnMap + shift).ToPoint());
                               
                            }
                            else
                            {
                                onPosition = true;
                            }
                            shift.X += 100;
                        }

                        if (onPosition)
                        {
                            if (!dialogs.isVisible)
                            {
                                playerInterface.HideBottomBar();
                                dialogs.Show();
                            }
                        }
                        break;
                    }

                    targetForAttack?.lostFocus();
                    targetForAttack = newTarget;
                    playerInterface.targetInfo.UpdateTarget(targetForAttack);
                    targetForAttack.getFocus();
                    break;
            }
        }

        private void onDie(object o, EventArgs e)
        {
            AbstractCharacter character = (AbstractCharacter)o;
            character.lostFocus();
            character.died -= onDie;
            character.mouseLeftButtonDown -= mouseLeftButtonDown;

            targetForAttack = null;
            playerInterface.targetInfo.UpdateTarget(null);
        }

        private void mouseRightButtonDown(object o, EventArgs e)
        {
            if (!GameFeature.InterfaceActive) return;

            iconMove.Play(Camera.scalePositionMouse.ToPoint());

            if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
            {
                listSelectCharacters[currentCharacter].moveTo(Camera.scalePositionMouse.ToPoint());
            }
            else if (listSelectCharacters.Count > 1)
            {
                int radius = 100;
                double angle = -30;
                double step = 2 * Math.PI / listSelectCharacters.Count;

                for (int i = 0; i < listSelectCharacters.Count; ++i)
                {
                    Point newPosition = new Point((int)(Camera.scalePositionMouse.X + radius * Math.Cos(angle) * GameFeature.scaleEllipse_X),
                                                  (int)(Camera.scalePositionMouse.Y + radius * Math.Sin(angle) * GameFeature.scaleEllipse_Y));
                    angle += step;
                    listSelectCharacters[i].targetForAttack = null;
                    listSelectCharacters[i].moveTo(newPosition);
                }
            }
            else if (listSelectCharacters.Count == 1)
            {
                listSelectCharacters[0].moveTo(Camera.scalePositionMouse.ToPoint());
                listSelectCharacters[0].targetForAttack = null;
            }
        }



        private void update(GameTime gameTime)
        {
            previouseMouseState = mouseState;
            mouseState = Mouse.GetState();

            previouseKeyboardState = keyboardState;
            keyboardState = Keyboard.GetState();

            #region selectField
            if (!GameFeature.pause)
            {
                if (previouseMouseState.LeftButton == ButtonState.Released && mouseState.LeftButton == ButtonState.Pressed && currentMap.isCaptureMouse)
                {
                    selectField.Begin();
                }

                if (previouseMouseState.LeftButton == ButtonState.Pressed && mouseState.LeftButton == ButtonState.Released)
                {
                    if (selectField.End())
                    {
                        updateSelectCharacters(selectField.listSelectTargets);
                    }
                }
            }
            #endregion

            #region Keyboard

            if (keyboardState.IsKeyDown(Keys.Space) && previouseKeyboardState.IsKeyUp(Keys.Space))
                IconPause_mouseLeftButtonDown(null, null);

            if (previouseKeyboardState.IsKeyUp(Keys.Escape) && keyboardState.IsKeyDown(Keys.Escape))
            {
                bool isCancel = false;

                for (int i = 0; i < listCharactersUnderControll.Count; i++)
                {
                    for (int j = 0; j < GameFeature.countSkills; j++)
                        if (listCharactersUnderControll[i].skills[i].inProcess)
                        {
                            listCharactersUnderControll[i].skills[i].Cancel();
                            isCancel = true;
                            break;
                        }
                    if (isCancel) break;
                }
                if (!isCancel)
                {
                    if (playerInterface.shop.isVisible)
                    {
                        playerInterface.shop.Hide();
                    }
                    else if (story.isVisible)
                    {
                        story.Skip();
                    }
                    else if (dialogs.isVisible)
                    {
                        if (dialogs.canSkip)
                        {
                            if (dialogs.dialogEnd)
                            {
                                dialogs.Hide();
                                playerInterface.ShowBottomBar();
                                targetForAttack?.lostFocus();
                                targetForAttack = null;
                                playerInterface.targetInfo.UpdateTarget(targetForAttack);
                            }
                            else
                            {
                                dialogs.Skip();
                            }
                        }
                    }
                    else if (playerInterface.inventory.isVisible)
                    {
                        playerInterface.inventory.Hide();
                    }
                    else if (targetForAttack != null)
                    {
                        targetForAttack.lostFocus();
                        targetForAttack = null;
                        playerInterface.targetInfo.UpdateTarget(targetForAttack);
                    }
                    else if (!lose.isVisible && !end.isVisible && !mainMenu.isVisible)
                    {
                        if (menuPause.isVisible)
                        {
                            menuPause.Hide();
                        }
                        else
                        {
                            menuPause.Show();
                        }
                    }
                }
            }

            if (previouseKeyboardState.IsKeyUp(Keys.A) && keyboardState.IsKeyDown(Keys.A))
            {
                Attack(null, null);
            }

            if (previouseKeyboardState.IsKeyUp(Keys.S) && keyboardState.IsKeyDown(Keys.S))
            {
                playerInterface.Tome_mouseLeftButtonDown(null, null);
            }

            if (previouseKeyboardState.IsKeyUp(Keys.I) && keyboardState.IsKeyDown(Keys.I))
            {
                playerInterface.InventoryIcon_mouseLeftButtonDown(null, null);
            }

            if (previouseKeyboardState.IsKeyUp(Keys.F1) && keyboardState.IsKeyDown(Keys.F1))
            {
                if (listSelectCharacters[currentCharacter] == listCharactersUnderControll[0]) Camera.focusOn(-listSelectCharacters[currentCharacter].positionOnMap);

                updateSelectCharacters(listCharactersUnderControll[0]);
               
            }

            if (previouseKeyboardState.IsKeyUp(Keys.F2) && keyboardState.IsKeyDown(Keys.F2))
            {
                if (listSelectCharacters[currentCharacter] == listCharactersUnderControll[1]) Camera.focusOn(-listSelectCharacters[currentCharacter].positionOnMap);

                updateSelectCharacters(listCharactersUnderControll[1]);
            }

            if (previouseKeyboardState.IsKeyUp(Keys.F3) && keyboardState.IsKeyDown(Keys.F3))
            {
                if (listSelectCharacters[currentCharacter] == listCharactersUnderControll[2]) Camera.focusOn(-listSelectCharacters[currentCharacter].positionOnMap);

                updateSelectCharacters(listCharactersUnderControll[2]);
            }

            if (previouseKeyboardState.IsKeyUp(Keys.F4) && keyboardState.IsKeyDown(Keys.F4))
            {
                updateSelectCharacters(listCharactersUnderControll);
            }

            if (previouseKeyboardState.IsKeyUp(Keys.D1) && keyboardState.IsKeyDown(Keys.D1))
            {
                playerInterface.potionIcons[0].Use(null, null);
            }

            if (previouseKeyboardState.IsKeyUp(Keys.D2) && keyboardState.IsKeyDown(Keys.D2))
            {
                playerInterface.potionIcons[1].Use(null, null);
            }

            if (previouseKeyboardState.IsKeyUp(Keys.D3) && keyboardState.IsKeyDown(Keys.D3))
            {
                playerInterface.potionIcons[2].Use(null, null);
            }

            if (previouseKeyboardState.IsKeyUp(Keys.H) && keyboardState.IsKeyDown(Keys.H) && keyboardState.IsKeyDown(Keys.LeftControl))
            {
                if (playerInterface.isVisible)
                    playerInterface.Hide();
                else
                    playerInterface.Show();
            }

            if (previouseKeyboardState.IsKeyUp(Keys.Q) && keyboardState.IsKeyDown(Keys.Q))
            {
                if (keyboardState.IsKeyDown(Keys.LeftShift))
                    playerInterface.skillIcons[0].upgradeSkill();
                else
                    playerInterface.skillIcons[0].Cast(null, null);
            }
            if (previouseKeyboardState.IsKeyUp(Keys.W) && keyboardState.IsKeyDown(Keys.W))
            {
                if (keyboardState.IsKeyDown(Keys.LeftShift))
                    playerInterface.skillIcons[1].upgradeSkill();
                else
                    playerInterface.skillIcons[1].Cast(null, null);
            }
            if (previouseKeyboardState.IsKeyUp(Keys.E) && keyboardState.IsKeyDown(Keys.E))
            {
                if (keyboardState.IsKeyDown(Keys.LeftShift))
                    playerInterface.skillIcons[2].upgradeSkill();
                else
                    playerInterface.skillIcons[2].Cast(null, null);
            }

            if (previouseKeyboardState.IsKeyUp(Keys.Tab) && keyboardState.IsKeyDown(Keys.Tab))
            {
                if (keyboardState.IsKeyDown(Keys.LeftShift))
                    selectPreviouseHero();
                else
                    selectNextHero();
            }
            #endregion
        }

        private void selectNextHero()
        {
            if (listSelectCharacters.Count > 1)
            {
                int previouseCharacter = currentCharacter;
                currentCharacter = (currentCharacter + 1) % listSelectCharacters.Count;
                int nextCharacter = (currentCharacter + 1) % listSelectCharacters.Count;
              
                playerInterface.updateSkillIcons(listSelectCharacters[currentCharacter]);
                playerInterface.nextCharacter.update(listSelectCharacters[nextCharacter]);
                playerInterface.previouseCharacter.update(listSelectCharacters[previouseCharacter]);
                Camera.scrollTo(-listSelectCharacters[currentCharacter].position);
            }
        }

        private void selectPreviouseHero()
        {
            if (listSelectCharacters.Count > 1)
            {
                int nextCharacter = currentCharacter;
                currentCharacter = (currentCharacter - 1) % listSelectCharacters.Count;
                if (currentCharacter < 0) currentCharacter = listSelectCharacters.Count - 1;
                int previouseCharacter = (currentCharacter - 1) % listSelectCharacters.Count;
                if (previouseCharacter < 0) previouseCharacter = listSelectCharacters.Count - 1;

                playerInterface.updateSkillIcons(listSelectCharacters[currentCharacter]);
                playerInterface.nextCharacter.update(listSelectCharacters[nextCharacter]);
                playerInterface.previouseCharacter.update(listSelectCharacters[previouseCharacter]);
                Camera.scrollTo(-listSelectCharacters[currentCharacter].position);
            }
        }

        private AbstractCharacter findClosestTarget()
        {
            AbstractCharacter target = null;
            float minRange = 1000;

            for (int i = 0; i < currentMap.listEnemyOnMap.Count; i++)
            {
                Vector2 distance = listSelectCharacters[currentCharacter].positionOnMap - currentMap.listEnemyOnMap[i].positionOnMap;
                float range = (float)Math.Sqrt(distance.X * distance.X + distance.Y * distance.Y);

                if (range <= minRange && currentMap.listEnemyOnMap[i].isAlive && currentMap.listEnemyOnMap[i].isVisible && (currentMap.listEnemyOnMap[i] as AbstractEnemy).isAgressive)
                {
                    minRange = range;
                    target = currentMap.listEnemyOnMap[i];
                }
            }

            return target;
        }

        private void Attack(object o, EventArgs e)
        {
            if (targetForAttack == null) //ближайший противник
            {
                targetForAttack?.lostFocus();
                if (targetForAttack != null) targetForAttack.died -= onDie;

                targetForAttack = findClosestTarget();

                if (targetForAttack != null)
                {
                    targetForAttack.getFocus();
                    targetForAttack.died += onDie;
                }

                playerInterface.targetInfo.UpdateTarget(targetForAttack);
                return;
            }

            if (targetForAttack.typeCharacter == TypeCharacter.Враг && (targetForAttack as AbstractEnemy).isAgressive || Keyboard.GetState().IsKeyDown(Keys.LeftControl))
            {
                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
                {
                    listSelectCharacters[currentCharacter].Attack(targetForAttack);
                    return;
                }

                for (int i = 0; i < listSelectCharacters.Count; ++i)
                {
                    listSelectCharacters[i].Attack(targetForAttack);
                }
            }
        }

        public void updateSelectCharacters(List<AbstractCharacter> newListTargets)
        {
            clearListSelectCharacter();

            for (int i = 0; i < newListTargets.Count; ++i)
                if (newListTargets[i].isAlive)
                {
                    newListTargets[i].getFocus();
                    listSelectCharacters.Add(newListTargets[i]);
                }
            currentCharacter = 0;
            playerInterface.updateSkillIcons(listSelectCharacters[currentCharacter]);
            if (listSelectCharacters.Count > 1)
            {
                playerInterface.nextIsVisible = true;

                playerInterface.nextCharacter.update(listSelectCharacters[1]);
             
                int previouseCharacter = listSelectCharacters.Count - 1;
                playerInterface.previouseCharacter.update(listSelectCharacters[previouseCharacter]);

                if (playerInterface.isVisible)
                {
                    playerInterface.previouseCharacter.isVisible = true;
                    playerInterface.nextCharacter.isVisible = true;
                }
            }
            else
            {
                playerInterface.nextCharacter.isVisible = false;
                playerInterface.previouseCharacter.isVisible = false;

                playerInterface.nextIsVisible = false;
            }
        }

        public void updateSelectCharacters(AbstractCharacter newTarget)
        {
            clearListSelectCharacter();

            currentCharacter = 0;

            playerInterface.nextCharacter.isVisible = false;
            playerInterface.previouseCharacter.isVisible = false;

            playerInterface.nextIsVisible = false;

            newTarget.getFocus();
            listSelectCharacters.Add(newTarget);
            playerInterface.updateSkillIcons(newTarget);
        }

        private void clearListSelectCharacter()
        {
            for (int i = 0; i < listSelectCharacters.Count; ++i)
            {
                listSelectCharacters[i].lostFocus();
            }
            listSelectCharacters.Clear();
        }
    }
}
