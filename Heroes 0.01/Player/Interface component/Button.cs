﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Button : InteractiveObject
    {
        private Vector2 positionTxt;
        private string text;
        private float scaleTxt;
        private Vector2 borders = new Vector2(20, -80);
        public Color fontColor = Color.White;

        private bool isactive = true;
        public bool isActive
        {
            get { return isactive; }
            set
            {
                isactive = value;
                if (isactive) fontColor = Color.White;
                else fontColor = Color.Gray;
            }
        }

        public Button(MyGame game,Point position, string text, float scaleTxt, float zIndex) : base (game)
        {
            this.game = game;
            this.text = text;
            this.scaleTxt = scaleTxt;
            this.zIndex = zIndex;

            isInterface = true;

            drawField.Location = position;

            Vector2 sizeTxt = GameFeature.Font.MeasureString(text) * scaleTxt;
            borders = borders * scaleTxt;

            drawField.Size = (sizeTxt + borders).ToPoint();

            positionTxt.X = drawField.X + (drawField.Width - sizeTxt.X) / 2;
            positionTxt.Y = drawField.Y + (drawField.Height - sizeTxt.Y) / 2;

            mouseEnter += Button_mouseEnter;

            mouseLeave += Button_mouseLeave;
        }

        private void Button_mouseLeave(object sender, EventArgs e)
        {
            if (isactive) fontColor = Color.White;
        }

        private void Button_mouseEnter(object sender, EventArgs e)
        {
            if (isactive) fontColor = Color.Yellow;
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(GameFeature.Font, text, positionTxt, fontColor, 0, Vector2.Zero, scaleTxt, SpriteEffects.None, zIndex);
        }
    }
}
