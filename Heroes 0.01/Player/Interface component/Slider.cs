﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Heroes_0._01
{
    class Slider : InteractiveObject
    {
        public event EventHandler valueChange;

        private Rectangle line;
        private Rectangle runner;
        private Texture2D texture;
        private Vector2 positionText;
        private float value;
        public float Value { get { return value; } }
        private string valueTxt = "0%";
        private float scaleTxt = 0.4f;

        public Slider(MyGame game, Point position, Point size, float zIndex) : base(game)
        {
            this.zIndex = zIndex;
            isInterface = true;
            line.Size = size;
            line.Location = position;

            scaleTxt *= Camera.scaleWnd;

            Vector2 sizeText = GameFeature.Font.MeasureString("100%") * scaleTxt;

            positionText.X = line.X + line.Width + (int)(20 * Camera.scaleWnd);
            positionText.Y = line.Y + (line.Height - sizeText.Y) / 2;

            runner.Width = line.Height;
            runner.Height = line.Height * 3;
            runner.X = line.X + (line.Width - runner.Width) / 2;
            runner.Y = line.Y + (line.Height - runner.Height) / 2;

            texture =  game.Content.Load<Texture2D>("Characters/Shades/white");

            value = runner.X - line.X;
            value /= line.Width - runner.Width;
            valueTxt = ((int)(value * 100)).ToString() + "%";

            drawField.X = line.X;
            drawField.Y = runner.Y;
            drawField.Height = runner.Height;
            drawField.Width = line.Width;

            game.updateInterface += update;
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, line, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, zIndex);
            spriteBatch.Draw(texture, runner, null, Color.Yellow, 0, Vector2.Zero, SpriteEffects.None, zIndex + GameFeature.zIndexStep);
            spriteBatch.DrawString(GameFeature.Font, valueTxt, positionText, Color.Yellow, 0, Vector2.Zero, scaleTxt, SpriteEffects.None, zIndex);
        }

        private void update(GameTime gameTime)
        {
            if (isCaptureMouse)
            {
                MouseState mouseState = Mouse.GetState();
                if (mouseState.LeftButton == ButtonState.Pressed)
                {
                    runner.X = mouseState.X;
                    if (runner.X < drawField.X) runner.X = drawField.X;
                    else if (runner.X + runner.Width > drawField.X + drawField.Width) runner.X = drawField.X + drawField.Width - runner.Width;

                    value = runner.X - line.X;
                    value /= line.Width - runner.Width;
                    valueTxt = ((int)(value * 100)).ToString() + "%";

                    valueChange?.Invoke(this, new EventArgs());
                }
            }
        }
    }
}
