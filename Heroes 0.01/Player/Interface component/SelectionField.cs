﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class SelectionField
    {
        private MyGame game;
        private Player player;

        private Rectangle selectField;
        private Texture2D textureSelect;
        public Vector2 startPosition;

        private Color color = Color.White * 0.3f;

        private bool isBegining = false;

        public List<AbstractCharacter> listSelectTargets;

        public SelectionField(MyGame game, Player player)
        {
            this.game = game;
            this.player = player;

            Camera.selectionField = this;
            selectField = new Rectangle(0, 0, 1, 1);
            textureSelect = game.Content.Load<Texture2D>("Characters/Shades/select");
            listSelectTargets = new List<AbstractCharacter>();
        }

        public void Begin()
        {
            isBegining = true;
            startPosition = Camera.scalePositionMouse;
            listSelectTargets.Clear();
            selectField.Width = 1;
            selectField.Height = 1;
            game.updateInterface += update;
            game.drawScaleInterface += draw;
        }

        public bool End()
        {
            game.updateInterface -= update;
            game.drawScaleInterface -= draw;
            if (listSelectTargets.Count > 0 && (selectField.Width > 4 || selectField.Height > 4) && isBegining)
            {
                isBegining = false;
                return true;
            }
            return false;
        }

        private void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(textureSelect, selectField, null, color, 0, Vector2.Zero, SpriteEffects.None, 0.5f);
        }

        private void update(GameTime gameTime)
        {
            Point mouseState = Camera.scalePositionMouse.ToPoint();

            int width = (int)(mouseState.X - startPosition.X);
            int height = (int)(mouseState.Y - startPosition.Y);

            if (width > 0)
            {
                selectField.X = (int)startPosition.X;
                selectField.Width = width;
            }
            else if (width < 0)
            {
                selectField.X = mouseState.X;
                selectField.Width = -width;
            }

            if (height > 0)
            {
                selectField.Y = (int)startPosition.Y;
                selectField.Height = height;
            }
            else if (height < 0)
            {
                selectField.Y = mouseState.Y;
                selectField.Height = -height;
            }
            listSelectTargets.Clear();
            if (selectField.Width > 4 && selectField.Height > 4)
                for (int i = 0; i < player.listCharactersUnderControll.Count; i++)
                {
                    if (player.listCharactersUnderControll[i].isAlive &&
                        selectField.Intersects(player.listCharactersUnderControll[i].drawField))
                    {
                        listSelectTargets.Add(player.listCharactersUnderControll[i]);
                        player.listCharactersUnderControll[i].mouseOver(null, null);
                    }
                    else
                    {
                        player.listCharactersUnderControll[i].lostMouseOver(null, null);
                    }
                }
        }      
    }
}
