﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Heroes_0._01
{
    public class Label
    {
        private MyGame game;
        private Vector2 position;
        public Vector2 Position { get { return position; } set { position = value; } }
        private string text;
        public string Text { get { return text; } set { text = value; } }
        private float scale;
        private Color color;
        private float zIndex;

        private bool isvisible = true;
        public bool isVisible
        {
            get
            {
                return isvisible;
            }
            set
            {
                if (value != isvisible)
                {
                    isvisible = value;

                    if (value)
                        game.drawInterface += draw;
                    else
                        game.drawInterface -= draw;


                }
            }
        }

        public Label(MyGame game, string text, Vector2 position, float scale, Color color, float zIndex)
        {
            this.game = game;
            this.text = text;
            this.position = position;
            this.scale = scale;
            this.color = color;
            this.zIndex = zIndex;

            game.drawInterface += draw;
        }

        protected void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(GameFeature.Font, text, position, color, 0, Vector2.Zero, scale, SpriteEffects.None, zIndex);
        }
    }
}
