﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Icon : InteractiveObject
    {
        protected Texture2D texture;
        protected Texture2D defaultTexture;
        protected Texture2D selectTexture;
        protected int width = (int)(100 * Camera.scaleWnd);
        protected int height = (int)(100 * Camera.scaleWnd);
        protected int shiftKey = 0;

        protected float fontLetterScale = 0.12f * Camera.scaleWnd;
        protected string Key;
        protected Vector2 positionLetter;

        public Icon(MyGame game, Point position, string Key) : base (game)
        {
            this.Key = Key;
            isInterface = true;
            drawField.Location = position;

            mouseEnter += Icon_mouseEnter;
            mouseLeave += Icon_mouseLeave;
        }

        protected virtual void Icon_mouseLeave(object sender, EventArgs e)
        {
            texture = defaultTexture;
        }

        protected virtual void Icon_mouseEnter(object sender, EventArgs e)
        {
            texture = selectTexture;
        }

        protected void loadIcon(string defaultTXT, string selectTXT)
        {
            defaultTexture = game.Content.Load<Texture2D>(defaultTXT);
            selectTexture = game.Content.Load<Texture2D>(selectTXT);
            drawField.Width = width;
            drawField.Height = height;
            texture = defaultTexture;
            zIndex = 0.9f + GameFeature.zIndexStep;

            Vector2 sizeLetter = GameFeature.Font.MeasureString(Key) * fontLetterScale;

            positionLetter.X = drawField.X + (drawField.Width - sizeLetter.X) / 2;
            positionLetter.Y = drawField.Y + drawField.Height - shiftKey;
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, drawField, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, zIndex);
            spriteBatch.DrawString(GameFeature.Font, Key, positionLetter, Color.White, 0, Vector2.Zero, fontLetterScale, SpriteEffects.None, zIndex);
        }
    }
}
