﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Heroes_0._01
{
    public class InventoryCell : InteractiveObject
    {
        private Texture2D texture;

        public AbstractItem item = null;

        private ItemType type;

        protected Color color;
        protected Color defaultColor = Color.Transparent;
        protected Color selectColor = Color.LightGray * 0.3f;

        private float zIndexIcon;
        private float zIndexFont;
        private float zIndexDescription;

        public Rectangle iconRect;

        public bool isSelected = false;

        private string count;
        private Vector2 positionText;

        private float fontScale = 0.15f;

        private Description description;

        public int Index;

        private bool isactive = true;
        public bool isActive
        {
            get { return isactive; }
            set
            {
                if (value != isactive)
                {
                    isactive = value;
                    if (isactive) color = defaultColor;
                    else color = Color.Black * 0.7f;
                }
            }
        }

        private bool isdrag = false;
        public bool isDrag
        {
            get { return isdrag; }
            set
            {
                if (value != isdrag)
                {
                    isdrag = value;
                    if (isdrag)
                    {
                        game.updateInterface += Drag;
                        zIndexIcon = zIndex + 3 * GameFeature.zIndexStep;
                        description.isVisible = false;
                    }
                    else
                    {
                        game.updateInterface -= Drag;
                        iconRect.X = drawField.X;
                        iconRect.Y = drawField.Y;
                        updateCount();
                        zIndexIcon = zIndex + GameFeature.zIndexStep;
                    }
                }
            }
        }

        public InventoryCell(MyGame game, Point position, float zIndex, Point size, int Index, ItemType type) : base(game)
        {
            isInterface = true;

            this.type = type;
            this.Index = Index;
            this.zIndex = zIndex;
            zIndexIcon = zIndex + GameFeature.zIndexStep;
            zIndexFont = zIndex + 4 * GameFeature.zIndexStep;

            zIndexDescription = zIndexFont + GameFeature.zIndexStep;

            texture = game.Content.Load<Texture2D>("Characters/Shades/white");

            description = new Description(game, true);

            drawField = new Rectangle(position.X,
                                      position.Y,
                                      size.X,
                                      size.Y);

            fontScale *= Camera.scaleWnd;

            iconRect = drawField;

            mouseEnter += InventoryCell_mouseEnter;
            mouseLeave += InventoryCell_mouseLeave;

            color = defaultColor;
        }

        private void InventoryCell_mouseLeave(object sender, EventArgs e)
        {
            if (!isSelected && isActive)
            {
                color = defaultColor;
                description.isVisible = false;
            }
        }

        private void InventoryCell_mouseEnter(object sender, EventArgs e)
        {
            if (!isSelected && isActive)
            {
                color = selectColor;
                if (item != null)
                {
                    description.isVisible = true;
                }
            }
        }

        public virtual bool updateItem(AbstractItem item)
        {
            if (!isActive) return false;

            if (item != null)
            {
                if (type == ItemType.Любой || item.type == type)
                {
                    this.item = item;

                    if (item.type == ItemType.Зелье)
                    {
                        updateCount();
                    }

                    defaultColor = Color.DimGray * 0.9f;
                    selectColor = Color.LightGray * 0.9f;
                    color = defaultColor;

                    description.updateDescription(item.name, item.description + $"\nЦена: {item.Cost * item.count}", 0.15f, drawField, zIndexDescription);
                    return true;
                }
                return false;
            }
            defaultColor = Color.Transparent;
            selectColor = Color.LightGray * 0.3f;
            color = defaultColor;
            this.item = null;
            return true;
        }

        public void updateCount()
        {
            if (item != null)
            {
                count = item.count.ToString();
                Vector2 sizeTxt = GameFeature.Font.MeasureString(count) * fontScale;
                positionText.X = iconRect.X + iconRect.Width - sizeTxt.X;
                positionText.Y = iconRect.Y + iconRect.Height - sizeTxt.Y;
            }
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, drawField, null, color, 0, Vector2.Zero, SpriteEffects.None, zIndex);
            if (item != null)
            {
                spriteBatch.Draw(item.icon, iconRect, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, zIndexIcon);
                if (item.type == ItemType.Зелье)
                    spriteBatch.DrawString(GameFeature.Font, count, positionText, Color.White, 0, Vector2.Zero, fontScale, SpriteEffects.None, zIndexFont);
            }
        }

        private void Drag(GameTime gameTime)
        {
            if (item != null && isdrag)
            {
                iconRect.X = Mouse.GetState().X - iconRect.Width / 2;
                iconRect.Y = Mouse.GetState().Y - iconRect.Height / 2;
                if (item.type == ItemType.Зелье) updateCount();
            }
        }
    }
}
