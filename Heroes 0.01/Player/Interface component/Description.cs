﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Description
    {
        private MyGame game;

        private Texture2D descBackground;
        private Rectangle descriptionRect;
        private Vector2 positionName;
        private Vector2 positionDescription;
        public string name;
        public string description;
        private int borderThickness = 10;
        private float fontScale;

        private float zIndexBackground;
        private float zIndexFont;

        public bool isInterface = false;

        private bool isvisible = false;
        public bool isVisible
        {
            get { return isvisible; }
            set
            {
                if (isvisible != value)
                {
                    isvisible = value;
                    if (isvisible)
                    {
                        if (isInterface) game.drawInterface += drawDescription;
                        else game.drawScaleInterface += drawDescription;
                    }
                    else
                    {
                        if (isInterface) game.drawInterface -= drawDescription;
                        else game.drawScaleInterface -= drawDescription;
                    }
                }
            }
        }


        public Description(MyGame game, bool isInterface)
        {
            this.game = game;
            this.isInterface = isInterface;
            descBackground = game.Content.Load<Texture2D>("Interface/iconback");
        }

        private void drawDescription(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(descBackground, descriptionRect, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, zIndexBackground);
            spriteBatch.DrawString(GameFeature.Font, name, positionName, Color.Yellow, 0, Vector2.Zero, fontScale, SpriteEffects.None, zIndexFont);
            spriteBatch.DrawString(GameFeature.Font, description, positionDescription, Color.White, 0, Vector2.Zero, fontScale, SpriteEffects.None, zIndexFont);
        }

        public void updateDescription(string name, string description, float fontScale, Rectangle drawField, float zIndex)
        {
            if (isInterface) fontScale *= Camera.scaleWnd;

            this.name = name;
            this.description = description;
            this.fontScale = fontScale;

            zIndexBackground = zIndex;
            zIndexFont = zIndex + GameFeature.zIndexStep;

            Vector2 sizeName = GameFeature.Font.MeasureString(name) * fontScale;
            Vector2 sizeTxt = GameFeature.Font.MeasureString(description) * fontScale;

            float width = sizeName.Length() > sizeTxt.Length()? sizeName.X : sizeTxt.X;

            if (isInterface)
            {
                descriptionRect.Height = (int)sizeName.Y + (int)sizeTxt.Y + (int)(borderThickness * Camera.scaleWnd) * 2;
                descriptionRect.Width = (int)width + (int)(borderThickness * Camera.scaleWnd) * 2;
                descriptionRect.X = drawField.X + (drawField.Width - descriptionRect.Width) / 2;
                descriptionRect.Y = drawField.Y - descriptionRect.Height;
                if (descriptionRect.Y < 0) descriptionRect.Y = drawField.Y + drawField.Height;
                positionName.X = descriptionRect.X + (descriptionRect.Width - sizeName.X) / 2;
                positionName.Y = descriptionRect.Y + (int)(borderThickness * Camera.scaleWnd);
                positionDescription.Y = positionName.Y + sizeName.Y;
                positionDescription.X = descriptionRect.X + (int)(borderThickness * Camera.scaleWnd);
            }
            else
            {
                descriptionRect.Height = (int)sizeName.Y + (int)sizeTxt.Y + borderThickness* 2;
                descriptionRect.Width = (int)width + borderThickness * 2;
                descriptionRect.X = drawField.X + (drawField.Width - descriptionRect.Width) / 2;
                descriptionRect.Y = drawField.Y - descriptionRect.Height;
                positionName.X = descriptionRect.X + (descriptionRect.Width - sizeName.X) / 2;
                positionName.Y = descriptionRect.Y + borderThickness;
                positionDescription.Y = positionName.Y + sizeName.Y;
                positionDescription.X = descriptionRect.X + borderThickness;
            }
        }
    }
}
