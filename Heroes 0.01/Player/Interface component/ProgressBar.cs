﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class ProgressBar
    {
        public event EventHandler valueChange;

        private Texture2D texture;
        private Rectangle front;
        private Rectangle background;

        private bool isvisible = false;
        public bool isVisible
        {
            get
            {
                return isvisible;
            }
            set
            {
                if (value != isvisible)
                {
                    if (isInterface)
                    {
                        if (value)
                        {
                            game.drawInterface -= draw;
                            game.drawInterface += draw;
                        }
                        else
                            game.drawInterface -= draw;
                    }
                    else
                    {
                        if (value)
                        {
                            game.drawGameObject -= draw;
                            game.drawGameObject += draw;
                        }
                        else
                            game.drawGameObject -= draw;
                    }

                    isvisible = value;
                }
            }
        }

        private int x;
        public int X
        {
            get
            {
                return x;
            }
            set
            {
                x = value;
                background.X = value;
                front.X = value;
            }
        }

        private int y;
        public int Y
        {
            get
            {
                return y;
            }
            set
            {
                y = value;
                background.Y = value;
                front.Y = value;
            }
        }

        private int height;
        public int Height
        {
            get
            {
                return height;
            }
            set
            {
                height = value;
                background.Height = value;
                front.Height = value;
            }
        }
        private int width;
        public int Width
        {
            get
            {
                return width;
            }
            set
            {
                width = value;
                background.Width = value;
                update();
            }
        }

        private float _value;
        public float Value
        {
            get
            {
                return _value;
            }
            set
            {

                if (value > maxValue) _value = maxValue;
                else if (value < minValue) _value = minValue;
                else _value = value;
                update();

            }
        }

        private int maxValue = 1;
        public int MaxValue
        {
            get
            {
                return maxValue;
            }
            set
            {
                if (value > 0)
                {
                    maxValue = value;
                    if (_value > maxValue) _value = maxValue;
                    update();
                }
            }
        }
        private int minValue = 0;
        public Color color;
        public Color backColor = Color.White * 0.3f;

        protected MyGame game;

        public float zIndex;

        protected bool isInterface;

        public ProgressBar(MyGame game, int width, int height, bool isInterface)
        {
            this.game = game;
            this.width = width;
            this.height = height;
            this.isInterface = isInterface;
            background = new Rectangle(0, 0, width, height);
            front = new Rectangle(0, 0, width, height);

            texture = game.Content.Load<Texture2D>("Characters/Shades/white");
            color = Color.White;
        }

        public void Remove()
        {
            isVisible = false;
            game = null;
        }


        private void update()
        {
            front.Width = (int)(width * Value / maxValue);
            valueChange?.Invoke(this, null);
        }

        protected void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, background, null, backColor, 0, Vector2.Zero, SpriteEffects.None, zIndex);
            spriteBatch.Draw(texture, front, null, color, 0, Vector2.Zero, SpriteEffects.None, zIndex + GameFeature.zIndexStep);
        }
    }
}
