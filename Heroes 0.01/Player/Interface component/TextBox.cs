﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Heroes_0._01
{
    public class TextBox : InteractiveObject
    {
        public string Text = "";

        private string text = "";
        private float fontScale;

        private KeyboardState previouseKeyboardState;
        private KeyboardState keyboardState;

        private Vector2 position;
        private int time;
        private int delay = 500;
        private bool flagСarriage = false;

        public TextBox(MyGame game, Vector2 position, float fontScale) : base(game)
        {
            this.position = position;
            this.fontScale = fontScale;
            isInterface = true;
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(GameFeature.Font, text, position, Color.White, 0, Vector2.Zero, fontScale, SpriteEffects.None, zIndex);
        }

        private void Update(GameTime gameTime)
        {
            previouseKeyboardState = keyboardState;
            keyboardState = Keyboard.GetState();

            if (previouseKeyboardState.IsKeyUp(Keys.Back) && keyboardState.IsKeyDown(Keys.Back) && Text.Length > 0)
                Text = Text.Substring(0, Text.Length - 1);

            if (Text.Length < 10)
            {
                if (previouseKeyboardState.IsKeyUp(Keys.Space) && keyboardState.IsKeyDown(Keys.Space))
                    Text += "_";
                if (previouseKeyboardState.IsKeyUp(Keys.NumPad0) && keyboardState.IsKeyDown(Keys.NumPad0) || previouseKeyboardState.IsKeyUp(Keys.D0) && keyboardState.IsKeyDown(Keys.D0))
                    Text += "0";
                if (previouseKeyboardState.IsKeyUp(Keys.NumPad1) && keyboardState.IsKeyDown(Keys.NumPad1) || previouseKeyboardState.IsKeyUp(Keys.D1) && keyboardState.IsKeyDown(Keys.D1))
                    Text += "1";
                if (previouseKeyboardState.IsKeyUp(Keys.NumPad2) && keyboardState.IsKeyDown(Keys.NumPad2) || previouseKeyboardState.IsKeyUp(Keys.D2) && keyboardState.IsKeyDown(Keys.D2))
                    Text += "2";
                if (previouseKeyboardState.IsKeyUp(Keys.NumPad3) && keyboardState.IsKeyDown(Keys.NumPad3) || previouseKeyboardState.IsKeyUp(Keys.D3) && keyboardState.IsKeyDown(Keys.D3))
                    Text += "3";
                if (previouseKeyboardState.IsKeyUp(Keys.NumPad4) && keyboardState.IsKeyDown(Keys.NumPad4) || previouseKeyboardState.IsKeyUp(Keys.D4) && keyboardState.IsKeyDown(Keys.D4))
                    Text += "4";
                if (previouseKeyboardState.IsKeyUp(Keys.NumPad5) && keyboardState.IsKeyDown(Keys.NumPad5) || previouseKeyboardState.IsKeyUp(Keys.D5) && keyboardState.IsKeyDown(Keys.D5))
                    Text += "5";
                if (previouseKeyboardState.IsKeyUp(Keys.NumPad6) && keyboardState.IsKeyDown(Keys.NumPad6) || previouseKeyboardState.IsKeyUp(Keys.D6) && keyboardState.IsKeyDown(Keys.D6))
                    Text += "6";
                if (previouseKeyboardState.IsKeyUp(Keys.NumPad7) && keyboardState.IsKeyDown(Keys.NumPad7) || previouseKeyboardState.IsKeyUp(Keys.D7) && keyboardState.IsKeyDown(Keys.D7))
                    Text += "7";
                if (previouseKeyboardState.IsKeyUp(Keys.NumPad8) && keyboardState.IsKeyDown(Keys.NumPad8) || previouseKeyboardState.IsKeyUp(Keys.D8) && keyboardState.IsKeyDown(Keys.D8))
                    Text += "8";
                if (previouseKeyboardState.IsKeyUp(Keys.NumPad9) && keyboardState.IsKeyDown(Keys.NumPad9) || previouseKeyboardState.IsKeyUp(Keys.D9) && keyboardState.IsKeyDown(Keys.D9))
                    Text += "9";
                if (keyboardState.CapsLock)
                {
                    if (previouseKeyboardState.IsKeyUp(Keys.Q) && keyboardState.IsKeyDown(Keys.Q))
                        Text += "Й";
                    if (previouseKeyboardState.IsKeyUp(Keys.W) && keyboardState.IsKeyDown(Keys.W))
                        Text += "Ц";
                    if (previouseKeyboardState.IsKeyUp(Keys.E) && keyboardState.IsKeyDown(Keys.E))
                        Text += "У";
                    if (previouseKeyboardState.IsKeyUp(Keys.R) && keyboardState.IsKeyDown(Keys.R))
                        Text += "К";
                    if (previouseKeyboardState.IsKeyUp(Keys.T) && keyboardState.IsKeyDown(Keys.T))
                        Text += "Е";
                    if (previouseKeyboardState.IsKeyUp(Keys.Y) && keyboardState.IsKeyDown(Keys.Y))
                        Text += "Н";
                    if (previouseKeyboardState.IsKeyUp(Keys.U) && keyboardState.IsKeyDown(Keys.U))
                        Text += "Г";
                    if (previouseKeyboardState.IsKeyUp(Keys.I) && keyboardState.IsKeyDown(Keys.I))
                        Text += "Ш";
                    if (previouseKeyboardState.IsKeyUp(Keys.O) && keyboardState.IsKeyDown(Keys.O))
                        Text += "Щ";
                    if (previouseKeyboardState.IsKeyUp(Keys.P) && keyboardState.IsKeyDown(Keys.P))
                        Text += "З";
                    if (previouseKeyboardState.IsKeyUp(Keys.OemOpenBrackets) && keyboardState.IsKeyDown(Keys.OemOpenBrackets))
                        Text += "Х";
                    if (previouseKeyboardState.IsKeyUp(Keys.OemCloseBrackets) && keyboardState.IsKeyDown(Keys.OemCloseBrackets))
                        Text += "Ъ";
                    if (previouseKeyboardState.IsKeyUp(Keys.A) && keyboardState.IsKeyDown(Keys.A))
                        Text += "Ф";
                    if (previouseKeyboardState.IsKeyUp(Keys.S) && keyboardState.IsKeyDown(Keys.S))
                        Text += "Ы";
                    if (previouseKeyboardState.IsKeyUp(Keys.D) && keyboardState.IsKeyDown(Keys.D))
                        Text += "В";
                    if (previouseKeyboardState.IsKeyUp(Keys.F) && keyboardState.IsKeyDown(Keys.F))
                        Text += "А";
                    if (previouseKeyboardState.IsKeyUp(Keys.G) && keyboardState.IsKeyDown(Keys.G))
                        Text += "П";
                    if (previouseKeyboardState.IsKeyUp(Keys.H) && keyboardState.IsKeyDown(Keys.H))
                        Text += "Р";
                    if (previouseKeyboardState.IsKeyUp(Keys.J) && keyboardState.IsKeyDown(Keys.J))
                        Text += "О";
                    if (previouseKeyboardState.IsKeyUp(Keys.K) && keyboardState.IsKeyDown(Keys.K))
                        Text += "Л";
                    if (previouseKeyboardState.IsKeyUp(Keys.L) && keyboardState.IsKeyDown(Keys.L))
                        Text += "Д";
                    if (previouseKeyboardState.IsKeyUp(Keys.OemSemicolon) && keyboardState.IsKeyDown(Keys.OemSemicolon))
                        Text += "Ж";
                    if (previouseKeyboardState.IsKeyUp(Keys.OemQuotes) && keyboardState.IsKeyDown(Keys.OemQuotes))
                        Text += "Э";
                    if (previouseKeyboardState.IsKeyUp(Keys.Z) && keyboardState.IsKeyDown(Keys.Z))
                        Text += "Я";
                    if (previouseKeyboardState.IsKeyUp(Keys.X) && keyboardState.IsKeyDown(Keys.X))
                        Text += "Ч";
                    if (previouseKeyboardState.IsKeyUp(Keys.C) && keyboardState.IsKeyDown(Keys.C))
                        Text += "С";
                    if (previouseKeyboardState.IsKeyUp(Keys.V) && keyboardState.IsKeyDown(Keys.V))
                        Text += "М";
                    if (previouseKeyboardState.IsKeyUp(Keys.B) && keyboardState.IsKeyDown(Keys.B))
                        Text += "И";
                    if (previouseKeyboardState.IsKeyUp(Keys.N) && keyboardState.IsKeyDown(Keys.N))
                        Text += "Т";
                    if (previouseKeyboardState.IsKeyUp(Keys.M) && keyboardState.IsKeyDown(Keys.M))
                        Text += "Ь";
                    if (previouseKeyboardState.IsKeyUp(Keys.OemComma) && keyboardState.IsKeyDown(Keys.OemComma))
                        Text += "Б";
                    if (previouseKeyboardState.IsKeyUp(Keys.OemPeriod) && keyboardState.IsKeyDown(Keys.OemPeriod))
                        Text += "Ю";
                }
                else
                {
                    if (previouseKeyboardState.IsKeyUp(Keys.Q) && keyboardState.IsKeyDown(Keys.Q))
                        Text += "й";
                    if (previouseKeyboardState.IsKeyUp(Keys.W) && keyboardState.IsKeyDown(Keys.W))
                        Text += "ц";
                    if (previouseKeyboardState.IsKeyUp(Keys.E) && keyboardState.IsKeyDown(Keys.E))
                        Text += "у";
                    if (previouseKeyboardState.IsKeyUp(Keys.R) && keyboardState.IsKeyDown(Keys.R))
                        Text += "к";
                    if (previouseKeyboardState.IsKeyUp(Keys.T) && keyboardState.IsKeyDown(Keys.T))
                        Text += "е";
                    if (previouseKeyboardState.IsKeyUp(Keys.Y) && keyboardState.IsKeyDown(Keys.Y))
                        Text += "н";
                    if (previouseKeyboardState.IsKeyUp(Keys.U) && keyboardState.IsKeyDown(Keys.U))
                        Text += "г";
                    if (previouseKeyboardState.IsKeyUp(Keys.I) && keyboardState.IsKeyDown(Keys.I))
                        Text += "ш";
                    if (previouseKeyboardState.IsKeyUp(Keys.O) && keyboardState.IsKeyDown(Keys.O))
                        Text += "щ";
                    if (previouseKeyboardState.IsKeyUp(Keys.P) && keyboardState.IsKeyDown(Keys.P))
                        Text += "з";
                    if (previouseKeyboardState.IsKeyUp(Keys.OemOpenBrackets) && keyboardState.IsKeyDown(Keys.OemOpenBrackets))
                        Text += "х";
                    if (previouseKeyboardState.IsKeyUp(Keys.OemCloseBrackets) && keyboardState.IsKeyDown(Keys.OemCloseBrackets))
                        Text += "ъ";
                    if (previouseKeyboardState.IsKeyUp(Keys.A) && keyboardState.IsKeyDown(Keys.A))
                        Text += "ф";
                    if (previouseKeyboardState.IsKeyUp(Keys.S) && keyboardState.IsKeyDown(Keys.S))
                        Text += "ы";
                    if (previouseKeyboardState.IsKeyUp(Keys.D) && keyboardState.IsKeyDown(Keys.D))
                        Text += "в";
                    if (previouseKeyboardState.IsKeyUp(Keys.F) && keyboardState.IsKeyDown(Keys.F))
                        Text += "а";
                    if (previouseKeyboardState.IsKeyUp(Keys.G) && keyboardState.IsKeyDown(Keys.G))
                        Text += "п";
                    if (previouseKeyboardState.IsKeyUp(Keys.H) && keyboardState.IsKeyDown(Keys.H))
                        Text += "р";
                    if (previouseKeyboardState.IsKeyUp(Keys.J) && keyboardState.IsKeyDown(Keys.J))
                        Text += "о";
                    if (previouseKeyboardState.IsKeyUp(Keys.K) && keyboardState.IsKeyDown(Keys.K))
                        Text += "л";
                    if (previouseKeyboardState.IsKeyUp(Keys.L) && keyboardState.IsKeyDown(Keys.L))
                        Text += "д";
                    if (previouseKeyboardState.IsKeyUp(Keys.OemSemicolon) && keyboardState.IsKeyDown(Keys.OemSemicolon))
                        Text += "ж";
                    if (previouseKeyboardState.IsKeyUp(Keys.OemQuotes) && keyboardState.IsKeyDown(Keys.OemQuotes))
                        Text += "э";
                    if (previouseKeyboardState.IsKeyUp(Keys.Z) && keyboardState.IsKeyDown(Keys.Z))
                        Text += "я";
                    if (previouseKeyboardState.IsKeyUp(Keys.X) && keyboardState.IsKeyDown(Keys.X))
                        Text += "ч";
                    if (previouseKeyboardState.IsKeyUp(Keys.C) && keyboardState.IsKeyDown(Keys.C))
                        Text += "с";
                    if (previouseKeyboardState.IsKeyUp(Keys.V) && keyboardState.IsKeyDown(Keys.V))
                        Text += "м";
                    if (previouseKeyboardState.IsKeyUp(Keys.B) && keyboardState.IsKeyDown(Keys.B))
                        Text += "и";
                    if (previouseKeyboardState.IsKeyUp(Keys.N) && keyboardState.IsKeyDown(Keys.N))
                        Text += "т";
                    if (previouseKeyboardState.IsKeyUp(Keys.M) && keyboardState.IsKeyDown(Keys.M))
                        Text += "ь";
                    if (previouseKeyboardState.IsKeyUp(Keys.OemComma) && keyboardState.IsKeyDown(Keys.OemComma))
                        Text += "б";
                    if (previouseKeyboardState.IsKeyUp(Keys.OemPeriod) && keyboardState.IsKeyDown(Keys.OemPeriod))
                        Text += "ю";
                }
            }

            time += gameTime.ElapsedGameTime.Milliseconds;

            if (time >= delay)
            {
                time -= delay;
                flagСarriage = !flagСarriage;
            }

            if (flagСarriage)
                text = Text + "|";
            else
                text = Text;
        }

        public void Show()
        {
            isVisible = true;
            game.updateInterface -= Update;
            game.updateInterface += Update;
        }

        public void Hide()
        {
            isVisible = false;
            game.updateInterface -= Update;
        }
    }
}
