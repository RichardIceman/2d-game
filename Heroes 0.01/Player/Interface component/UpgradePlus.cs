﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Heroes_0._01
{
    public class UpgradePlus : InteractiveObject
    {
        private Texture2D texture;
        private Texture2D defaultTxt;
        private Texture2D selectTxt;
        public int Height { get { return drawField.Height; } }

        public UpgradePlus(MyGame game, int height, int width, Point position, float zIndex) : base(game)
        {
            this.zIndex = zIndex;
            drawField.Location = position;
            drawField.Width = width;
            drawField.Height = height;
            isInterface = true;
            defaultTxt = game.Content.Load<Texture2D>("Interface/upgrade");
            selectTxt = game.Content.Load<Texture2D>("Interface/upgradeselect");
            texture = defaultTxt;

            mouseEnter += UpgradePlus_mouseEnter;
            mouseLeave += UpgradePlus_mouseLeave;
        }

        private void UpgradePlus_mouseLeave(object sender, EventArgs e)
        {
            texture = defaultTxt;
        }

        private void UpgradePlus_mouseEnter(object sender, EventArgs e)
        {
            texture = selectTxt;
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, drawField, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, zIndex);
        }
    }
}
