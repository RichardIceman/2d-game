﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class MovePointAnimation
    {
        private Texture2D texture;
        private Rectangle rect;


        private Vector2 size;
        private Vector2 shift = new Vector2(3 * GameFeature.scaleEllipse_X, 3 * GameFeature.scaleEllipse_Y);
        private Vector2 position;

        private MyGame game;

        private Vector2 maxSize = new Vector2(50 * GameFeature.scaleEllipse_X, 50 * GameFeature.scaleEllipse_Y);

        private float zIndex = GameFeature.zIndexStep;

        public MovePointAnimation(MyGame game)
        {
            this.game = game;

            texture = game.Content.Load<Texture2D>("Interface/circle");
        }

        private void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, rect, null, Color.Lime, 0, Vector2.Zero, SpriteEffects.None, GameFeature.zIndexStep);
        }

        private void update(GameTime gameTime)
        {
            size += shift;
            position -= shift / 2;

            rect.Location = position.ToPoint();
            rect.Size = size.ToPoint();

            if (size.X >= maxSize.X &&
                size.Y >= maxSize.Y)
            {
                game.updateGame -= update;
                game.drawGameObject -= draw;
            }
        }

        public void Play(Point _position)
        {
            game.updateGame -= update;
            game.drawGameObject -= draw;

            game.updateGame += update;
            game.drawGameObject += draw;

            size.X = 10 * GameFeature.scaleEllipse_X;
            size.Y = 10 * GameFeature.scaleEllipse_Y;

            position = _position.ToVector2() - size / 2;

            rect.Location = position.ToPoint();
            rect.Size = size.ToPoint();
        }
    }
}
