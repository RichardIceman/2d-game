﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Dialogs
    {
        private MyGame game;
        private PlayerInterface playInt;

        private Texture2D texture;
        private Texture2D icon;
        private Rectangle rect;
        private Rectangle headRect;

        private Color color;

        private Texture2D[] icons;

        private string text;
        private string currentText;
        private string skipStr = "Esc";

        public bool canSkip = true;

        private Vector2 positionText;
        private Vector2 positionSkipStr;

        private int currentDialog = 0;
        public int CurrentDialog
        {
            get { return currentDialog; }
            set
            {
                currentDialog = value;
                dialogEnd = false;
                currentPhrase = 0;
            }
        }
        public int currentPhrase = 0;

        public bool dialogEnd = false;

        public bool isVisible = false;

        private List<string>[] dialogTexts;
        private List<int>[] phraseIcons;
        private List<int>[] phraseDuration;

        public Button button_shop;
        public Button button_give20;
        public Button button_give100;
        public Button button_give500;
        public Button button_give_1;
        public Button button_give_2;
        public Button button_give_3;

        private Button button_repeat;
        public Button cancel;
        public Button endKing;
        public Button endGuard;

        private float zIndexBack;
        private float zIndexText;

        private float sizeFont = 0.2f * Camera.scaleWnd;
        private float sizeSkip = 0.25f * Camera.scaleWnd;

        private int currentSimbol = 0;

        private int time;
        private int duration = 5000;

        private float a = 0;
        private float step_a = (float)Math.PI / 36;

        private Color skipColor = Color.Yellow;

        public Dialogs(MyGame game, PlayerInterface playInt)
        {
            this.game = game;
            this.playInt = playInt;

            zIndexBack = 0.91f;

            zIndexText = zIndexBack + GameFeature.zIndexStep;

            texture = game.Content.Load<Texture2D>("Characters/Shades/gradient_1");
            rect = new Rectangle(0, (int)(Camera.sizeWnd.Y * 0.8f), (int)Camera.sizeWnd.X, (int)(Camera.sizeWnd.Y * 0.2f));
            positionText.Y = rect.Y + 80 * Camera.scaleWnd;

            Vector2 sizeTxt = GameFeature.Font.MeasureString(skipStr) * sizeFont;

            positionSkipStr.X = Camera.sizeWnd.X - sizeTxt.X - 50 * Camera.scaleWnd;
            positionSkipStr.Y = Camera.sizeWnd.Y - sizeTxt.Y - 50 * Camera.scaleWnd;

            headRect = new Rectangle(0, (int)(rect.Y + 40 * Camera.scaleWnd), (int)(140 * Camera.scaleWnd), (int)(140 * Camera.scaleWnd));

            color = Color.Black * 0.8f;

            ////////
            string text = "-> Покажи свои товары <-";
            Point position;
            sizeTxt = GameFeature.Font.MeasureString(text) * sizeFont;

            position.Y = rect.Y + (int)(80 * Camera.scaleWnd);
            position.X = (int)(Camera.sizeWnd.X - sizeTxt.X) / 2;

            button_shop = new Button(game, position, text, sizeFont, zIndexText);

            position.Y += (int)sizeTxt.Y;

            ////////

            text = "-> Вот твои 20 золотых. <-";

            sizeTxt = GameFeature.Font.MeasureString(text) * sizeFont;

            position.X = (int)(Camera.sizeWnd.X - sizeTxt.X) / 2;

            button_give20 = new Button(game, position, text, sizeFont, zIndexText);

            ///////

            text = "-> Вот твои 100 золотых. <-";
            sizeTxt = GameFeature.Font.MeasureString(text) * sizeFont;

            position.X = (int)(Camera.sizeWnd.X - sizeTxt.X) / 2;

            button_give100 = new Button(game, position, text, sizeFont, zIndexText);

            ///////

            button_give500 = new Button(game, position, "-> Вот твои 500 золотых. <-", sizeFont, zIndexText);

            //////

            text = "-> Стать хранителями артефакта <-";
            sizeTxt = GameFeature.Font.MeasureString(text) * sizeFont;

            position.X = (int)(Camera.sizeWnd.X - sizeTxt.X) / 2;

            endGuard = new Button(game, position, text, sizeFont, zIndexText);

            ///////

            text = "-> Повторить информацию о боссе <-";
            sizeTxt = GameFeature.Font.MeasureString(text) * sizeFont;

            position.X = (int)(Camera.sizeWnd.X - sizeTxt.X) / 2;
            button_repeat = new Button(game, position, text, sizeFont, zIndexText);
            button_repeat.mouseLeftButtonDown += Button_repeat_mouseLeftButtonDown;
            position.Y += (int)sizeTxt.Y;

            /////
            position.Y -= (int)sizeTxt.Y;
            text = "-> Лучше умереть, чем отдать их тебе <-";
            sizeTxt = GameFeature.Font.MeasureString(text) * sizeFont;

            position.X = (int)(Camera.sizeWnd.X - sizeTxt.X) / 2;
            cancel = new Button(game, position, text, sizeFont, zIndexText);
            position.Y += (int)sizeTxt.Y;
            /////

            text = "-> Отдать сердце Джу <-";
            sizeTxt = GameFeature.Font.MeasureString(text) * sizeFont;

            position.X = (int)(Camera.sizeWnd.X - sizeTxt.X) / 2;

            button_give_1 = new Button(game, position, text, sizeFont, zIndexText);

            /////

            text = "-> Отдать артефакт королю <-";
            sizeTxt = GameFeature.Font.MeasureString(text) * sizeFont;

            position.X = (int)(Camera.sizeWnd.X - sizeTxt.X) / 2;

            endKing = new Button(game, position, text, sizeFont, zIndexText);

            /////

            text = "-> Отдать сердце Глыбы <-";
            sizeTxt = GameFeature.Font.MeasureString(text) * sizeFont;

            position.X = (int)(Camera.sizeWnd.X - sizeTxt.X) / 2;

            button_give_2 = new Button(game, position, text, sizeFont, zIndexText);

            /////

            text = "-> Отдать оставшиеся сердца <-";
            sizeTxt = GameFeature.Font.MeasureString(text) * sizeFont;

            position.X = (int)(Camera.sizeWnd.X - sizeTxt.X) / 2;

            button_give_3 = new Button(game, position, text, sizeFont, zIndexText);

            /////

            icons = new Texture2D[6];
            phraseIcons = new List<int>[12];
            phraseDuration = new List<int>[12];
            dialogTexts = new List<string>[12];



            icons[0] = game.Content.Load<Texture2D>("Characters/Tank/TankHead");
            icons[1] = game.Content.Load<Texture2D>("Characters/Archer/ArcherHead");
            icons[2] = game.Content.Load<Texture2D>("Characters/Healer/HealerHead");
            icons[3] = game.Content.Load<Texture2D>("Characters/Sellers/seller_1_head");
            icons[4] = game.Content.Load<Texture2D>("Characters/Ogr_1/ogr_1_head");
            icons[5] = game.Content.Load<Texture2D>("Characters/Demon/demonHead");

            dialogTexts[0] = new List<string>(new string[]
            {
                "Приветствую вас, охотники за сокровищами!\nЯ - Флавий Эфти, здешний торговец.\nА это мой охранник Горг.",
                "Гы",
                "Почему ты здесь торгуешь?",
                "Да потому что, тут золотая жила!!! До вас много\nгероев здесь побывало и каждому нужно снаряжение.",
                "А что вас сюда привело? Стоп, не говорите.\nВы похожи на рыцарей Анатель Лино, я прав?",
                "Да.",
                "Ха, конечно, я всегда прав.\nСлышал дела в вашем королевстве очень плохи.\nЗначит вы здесь ищете тот самый «давно затерянный артефакт».",
                "Мне вас жаль, большинство приходивших сюда за ним\nуходили с пустыми руками или вовсе пропадали.",
                "Знаете, я могу вам кое чем помочь. Но это будет\nне бесплатно. Для начала принесите мне 20 золота и я укажу\nвам верный путь к тому самому «давно затерянному артефакту»."
            });

            phraseIcons[0] = new List<int>(new int[]
            {
                3,
                4,
                0,
                3,
                3,
                1,
                3,
                3,
                3
            });

            phraseDuration[0] = new List<int>(new int[]
            {
                8000,
                1000,
                3000,
                8000,
                8000,
                1000,
                9000,
                7000,
                10000
            });

            dialogTexts[1] = new List<string>(new string[]
           {
                "Ха, а вот и ваша информация! Ваш противник - это Темный дух Джу.\nПопасть к нему можно через портал, который находится\nв нижней части Вызженых земель.",
                "Будьте осторожнее, на всём протяжении боя Джу будет призывать призраков\nсебе в подмогу. Как только его здоровье опуститься ниже 30%, Джу наложит\nна себя непробиваемый барьер и у вас будет ровно минута, чтобы его снять.",
                "Иначе дух полностью восстановится. Для снятия барьера, каждому из вас\nнужно будет встать на свою печать. И если вы всё сделаете правильно,\nэто снимет барьер, а у босса останется 1 хп. Так, что просто добейте его!!!"
           });

            phraseIcons[1] = new List<int>(new int[]
            {
                3,
                3,
                3
            });

            phraseDuration[1] = new List<int>(new int[]
            {
                15000,
                12000,
                12000
            });

            dialogTexts[2] = new List<string>(new string[]
            {
                "Вы вернулись! Я удивлен. Ты был прав Грог, держи свои 10 золотых.",
                "Гы",
                "Погодите-ка, а вы не находили там блестяшку? Интересная, знаете предлагаю\nсделку. Я могу с вами поделится информацией за эту безделушку и\nсделаю скидку 30% на все товары в моем магазине.",
                "Ну или платите 100 золотых, все в ваших руках!"
            });

            phraseIcons[2] = new List<int>(new int[]
            {
                3,
                4,
                3,
                3
            });

            phraseDuration[2] = new List<int>(new int[]
            {
                6000,
                1000,
                10000,
                4000
            });

            dialogTexts[3] = new List<string>(new string[]
            {
                "Итак, к делу! Ваш противник - это Глыба. Попасть к нему\nможно через портал, который находится где то тут.\nНо остерегайтесь его магии льда!",
                "Благо, область, куда будет нанесен удар, подсвечивается красным.\nНо и это еще не все! Когда у Глыбы останется 30% здоровья,\nон призовет своих сыновей и получит неуязвимость.",
                "Для снятия неуязвимости потребуется уничтожить всех его сыновей!.",
                "И да, чуть не забыл, у голема есть часть того самого\n«давно затерянного артефакта», это рукоять меча\nпервого эльфийского короля. Удачи вам, друзья!"
            });

            phraseIcons[3] = new List<int>(new int[]
            {
                3,
                3,
                3,
                3
            });

            phraseDuration[3] = new List<int>(new int[]
            {
               10000,
               12000,
               7000,
               10000
            });

            dialogTexts[4] = new List<string>(new string[]
            {
                "Грог, гляди кто вернулся! Отдавай мои 20 золотых.",
                "Гы",
                "Ого, вы нашли рукоять... и... а это... вы не находили там никаких камней? Грог, как\nдумаешь может сказать им, где находится лезвие и как победить...\nвзамен на этот камешек? ДА, стоп, НЕТ! Ааа чуть не проболтался.",
                "Ну так что, с вас 500 золота, и я вам говорю особенности\nбосса. Но, если вы отдадите мне блестяшку, информации\nбудет больше!"
            });

            phraseIcons[4] = new List<int>(new int[]
            {
                3,
                4,
                3,
                3
            });

            phraseDuration[4] = new List<int>(new int[]
            {
               4000,
               1000,
               10000,
               13000
            });

            dialogTexts[5] = new List<string>(new string[]
            {
                "Ну ладно, деньги так деньги... Ваш противник это - Харон.\nОгнедышащее существо. В соих атаках схож с Глыбой. Все так же\nостерегайтесь красных зон!",
                "Переодически он извергает пламя прямо перед собой. И он всегда\nобращает свое внимание на самую дальнюю цель! Когда его здоровье\nбудет приближаться к 50%...",
                "Хотя нет, это все."
            });

            phraseIcons[5] = new List<int>(new int[]
            {
                3,
                3,
                3
            });

            phraseDuration[5] = new List<int>(new int[]
            {
               10000,
               10000,
               2000
            });

            dialogTexts[6] = new List<string>(new string[]
            {
                "Моя прелесть... Ах да, ваш противник это - Харон.\nОгнедышащее существо. В соих атаках схож с Глыбой.\nВсе так же остерегайтесь красных зон!",
                "Переодически он извергает пламя прямо перед собой. И всегда\nобращает свое внимание на самую дальнюю цель!\nКогда его здоровье будет приближаться к 50%...",
                "Стойте на 11 часов от центра поля битвы. Эта атака будет\nсмещаться по часовой стрелке, пока не пройдет\nполный круг.",
                "На 10% здоровья он впадет в ярость и нанесет свою самую\nсмертоносную атаку. Советую использовать барьер\nзащищающий от урона! Удачи, друзья!"
            });

            phraseIcons[6] = new List<int>(new int[]
            {
                3,
                3,
                3,
                3
            });

            phraseDuration[6] = new List<int>(new int[]
            {
               10000,
               10000,
               10000,
               10000
            });

            dialogTexts[7] = new List<string>(new string[]
            {
                "Свобода...\nНо я должен сделать хотябы что-то хорошее напоследок...\nЯ полностью востанавливаю ваши силы, герои Анатель Лино."
            });

            phraseIcons[7] = new List<int>(new int[]
            {
                5
            });

            phraseDuration[7] = new List<int>(new int[]
            {
               12000
            });

            dialogTexts[8] = new List<string>(new string[]
            {
                "Надо же, вы справились со всеми стражами артефакта.\nЯ впечатлен, но как видите, у меня не хватает, частей артефакта,\nне той зубочистки «Меч первого эльфийского короля», а Амулета мертвых.",
                "Предлагаю сделку, вы мне отдаете эти «блестяшки»,\nа я вас отпущу живыми."
            });

            phraseIcons[8] = new List<int>(new int[]
            {
                3,
                3
            });

            phraseDuration[8] = new List<int>(new int[]
            {
               12000,
               5000
            });

            dialogTexts[9] = new List<string>(new string[]
            {
                "Нет? Ну что ж, Грог убей их, только не отрывай\nим руки и ноги, у меня на них есть планы",
                "Гы"
            });

            phraseIcons[9] = new List<int>(new int[]
            {
                3,
                4
            });

            phraseDuration[9] = new List<int>(new int[]
            {
               5000,
               1000
            });

            dialogTexts[10] = new List<string>(new string[]
            {
                "И что же нам теперь делать с этим артефактом?"
            });

            phraseIcons[10] = new List<int>(new int[]
            {
                0
            });

            phraseDuration[10] = new List<int>(new int[]
            {
               5000
            });

            dialogTexts[11] = new List<string>(new string[]
            {
                "НЕТ, ВЫ не остановите меня. Я должен стать владыкой мертвых!!!\nВ следующий раз я точно завладею этим ожерельем."
            });

            phraseIcons[11] = new List<int>(new int[]
            {
                3
            });

            phraseDuration[11] = new List<int>(new int[]
            {
               8000
            });
        }

        private void Button_repeat_mouseLeftButtonDown(object sender, EventArgs e)
        {
            dialogEnd = false;

            button_shop.isVisible = false;
            button_repeat.isVisible = false;
            button_give_2.isVisible = false;

            updatePhrase();
            currentPhrase = 1;
        }

        

        private void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, rect, null, color, 0, Vector2.Zero, SpriteEffects.None, zIndexBack);
            if (!dialogEnd) spriteBatch.Draw(icon, headRect, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, zIndexText);

            spriteBatch.DrawString(GameFeature.Font, currentText, positionText, Color.White, 0, Vector2.Zero, sizeFont, SpriteEffects.None, zIndexText);
            spriteBatch.DrawString(GameFeature.Font, skipStr, positionSkipStr, skipColor, 0, Vector2.Zero, sizeSkip, SpriteEffects.None, zIndexText);
        }

        private void update(GameTime gameTime)
        {
            a = (a + step_a) % (float)Math.PI;

            skipColor = Color.Yellow * (float)Math.Abs(Math.Sin(a));

            if (!isVisible || dialogEnd) return;

            time += gameTime.ElapsedGameTime.Milliseconds;

            if (time >= duration)
            {
                Skip();
                return;
            }     

            if (currentText != text && !dialogEnd)
            {
                currentText += text[currentSimbol];
                currentSimbol++;
            }
        }

        public void Show()
        {
            Hide();

            currentSimbol = 0;

            game.updateInterface += update;
            game.drawInterface += draw;

            isVisible = true;

            GameFeature.InterfaceActive = false;

            a = 0;

            Skip();
        }

        private void updatePhrase()
        {
            time = 0;

            duration = phraseDuration[CurrentDialog][currentPhrase];

            icon = icons[phraseIcons[CurrentDialog][currentPhrase]];
            currentSimbol = 0;
            currentText = "";
            text = dialogTexts[CurrentDialog][currentPhrase];

            Vector2 sizeTxt = GameFeature.Font.MeasureString(text) * sizeFont;
            positionText.X = (rect.Width - sizeTxt.X) / 2;

            headRect.X = (int)positionText.X - headRect.Width;
        }

        public void Hide()
        {
            game.updateInterface -= update;
            game.drawInterface -= draw;

            button_shop.isVisible = false;
            button_give20.isVisible = false;
            button_give100.isVisible = false;
            button_give500.isVisible = false;
            button_give_1.isVisible = false;
            button_give_2.isVisible = false;
            button_give_3.isVisible = false;
            button_repeat.isVisible = false;
            cancel.isVisible = false;

            endGuard.isVisible = false;
            endKing.isVisible = false;

            GameFeature.InterfaceActive = true;
            isVisible = false;
        }

        public void Skip()
        {
            if (currentPhrase < dialogTexts[CurrentDialog].Count && !dialogEnd)
            {
                updatePhrase();
                currentPhrase++;
            }
            else
            {
                dialogEnd = true;
                currentSimbol = 0;
                currentPhrase = 0;
                currentText = "";

                switch (CurrentDialog)
                {
                    case 0:
                        button_shop.isVisible = true;
                        button_give20.isVisible = true;                    
                        break;
                    case 1:
                    case 3:
                    case 6:
                        button_shop.isVisible = true;
                        button_repeat.isVisible = true;
                        break;
                    case 2:
                        button_shop.isVisible = true;
                        button_give100.isVisible = true;
                        if (!GameFeature.lostHeartGhost) button_give_1.isVisible = true;
                        break;
                    case 4:
                        button_shop.isVisible = true;
                        button_give500.isVisible = true;
                        if (!GameFeature.lostHeartGolem) button_give_2.isVisible = true;
                        break;
                    case 5:
                        button_shop.isVisible = true;
                        button_give_2.isVisible = true;
                        button_repeat.isVisible = true;
                        break;
                    case 7:
                        if (GameFeature.lostHeartGhost || GameFeature.lostHeartGolem)
                        {
                            Hide();
                            playInt.ShowBottomBar();
                            CurrentDialog = 8;
                        }
                        else
                        {
                            CurrentDialog = 10;
                            Show();
                        }
                        break;
                    case 8:
                        cancel.isVisible = true;
                        button_give_3.isVisible = true;
                        break;
                    case 9:
                        Hide();
                        playInt.ShowBottomBar();
                        break;
                    case 10:
                        canSkip = false;
                        endGuard.isVisible = true;
                        endKing.isVisible = true;
                        break;
                    case 11:
                        CurrentDialog = 10;
                        Show();
                        break;
                }
            }
        }
    }
}
