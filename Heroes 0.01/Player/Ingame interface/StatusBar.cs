﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class StatusBar : ProgressBar
    {
        private Label labelName;
        private Label labelText;
        private Label labelRegen;
        private float fontScale;
        public string Text
        {
            set
            {
                labelText.Position = new Vector2(X + (Width - GameFeature.Font.MeasureString(value).X * fontScale) / 2, Y);
                labelText.Text = value;
            }
        }

        public StatusBar(MyGame game, Vector2 position, int width, int height, string name, float fontScale, Color color, Color colorFont, float zIndex) : base(game, width, height, true)
        {
            X = (int)position.X;
            Y = (int)position.Y;
            this.zIndex = zIndex;
            this.color = color;
            this.fontScale = fontScale;
            labelName = new Label(game, name, new Vector2(X + 5, Y), fontScale, colorFont, zIndex + 2 * GameFeature.zIndexStep);
            labelText = new Label(game, "", new Vector2(X + 5, Y), fontScale, colorFont, zIndex + 2 * GameFeature.zIndexStep);
            labelRegen = new Label(game, "", new Vector2(X + 5, Y), fontScale, colorFont * 0.8f, zIndex + 2 * GameFeature.zIndexStep);
        }

        public void Show()
        {
            isVisible = true;

            labelName.isVisible = true;
            labelRegen.isVisible = true;
            labelText.isVisible = true;
        }

        public void Hide()
        {
            isVisible = false;

            labelName.isVisible = false;
            labelRegen.isVisible = false;
            labelText.isVisible = false;
        }

        public void Update(int maxValue, float value, int regen)
        {
            MaxValue = maxValue;
            Value = value;

            string text = $"{ (int)value } / { maxValue }";
            Vector2 size = GameFeature.Font.MeasureString(text);
            Vector2 newPos = new Vector2(X + (Width - size.X * fontScale) / 2, Y);
            labelText.Text = text;
            labelText.Position = newPos;

            if (regen > 0)
            {
                text = $"+{regen}";
                size = GameFeature.Font.MeasureString(text);
                newPos = new Vector2(X + Width - size.X * fontScale - 5, Y);

                labelRegen.Text = text;
                labelRegen.Position = newPos;
            }
            else labelRegen.Text = "";
        }
    }
}
