﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace Heroes_0._01
{
    public class Inventory : InteractiveObject
    {
        private AbstractCharacter character;

        private Texture2D background;
        private Texture2D inventGrid;
        private Texture2D equipeGrid;
        private Texture2D equipeIcon;

        private Rectangle invGridRect;
        private Rectangle equipeGridRect;
        private Rectangle equipeIconRect;

        private Color color = Color.White * 0.95f;

        private float zIndexGrid;
        private float zIndexIconGrid;
        private float zIndexIcon;

        private string info = "";
        private string lvl = "";
        private string characterInfo = "";
        private string characterName = "";
        private Vector2 positionLvl;
        private Vector2 positionInfo;
        private Vector2 positionCharacterInfo;
        private Vector2 positionCharacterName;
        private Rectangle infoRect_1;
        private Rectangle infoRect_2;
        private float fontScale;

        private int maxCell;
        private InventoryCell[] grid;
        private InventoryCell selectedCell = null;

        private MouseState mouseState;
        private MouseState previouseMouseState;

        private PotionIcon[] potionIcons;

        private SoundEffect effect;

        public Inventory(MyGame game, PotionIcon[] potionIcons) : base(game)
        {
            this.potionIcons = potionIcons;

            isInterface = true;
            zIndex = 0.91f;

            zIndexIconGrid = zIndex + GameFeature.zIndexStep;
            zIndexIcon = zIndex + 2 * GameFeature.zIndexStep;
            zIndexGrid = zIndex + 4 * GameFeature.zIndexStep;

            fontScale = Camera.scaleWnd * 0.15f;

            maxCell = GameFeature.inventorySize + GameFeature.equipementSize + GameFeature.countPotions;

            background = game.Content.Load<Texture2D>("Interface/inventory2");
            inventGrid = game.Content.Load<Texture2D>("Interface/inventoryGrid");
            equipeGrid = game.Content.Load<Texture2D>("Interface/ecvipGrid");
            equipeIcon = game.Content.Load<Texture2D>("Interface/ecvipIcons");
            effect = game.Content.Load<SoundEffect>("Songs/cloth-heavy");

            drawField = new Rectangle((int)(Camera.sizeWnd.X - 730 * Camera.scaleWnd) / 2,
                                     (int)(150 * Camera.scaleWnd),
                                     (int)(780 * Camera.scaleWnd),
                                     (int)(600 * Camera.scaleWnd));

            invGridRect = new Rectangle(drawField.X + (int)(150 * Camera.scaleWnd),
                                        drawField.Y + (int)(30 * Camera.scaleWnd),
                                        (int)(228 * Camera.scaleWnd),
                                        (int)(336 * Camera.scaleWnd));
            equipeGridRect = new Rectangle(drawField.X + (int)(423 * Camera.scaleWnd),
                                           drawField.Y + (int)(30 * Camera.scaleWnd),
                                           (int)(198 * Camera.scaleWnd),
                                           (int)(372 * Camera.scaleWnd));
            equipeIconRect = new Rectangle(drawField.X + (int)(423 * Camera.scaleWnd),
                                           drawField.Y + (int)(30 * Camera.scaleWnd),
                                           (int)(198 * Camera.scaleWnd),
                                           (int)(372 * Camera.scaleWnd));


            infoRect_1 = new Rectangle((int)(drawField.X + 120 * Camera.scaleWnd), (int)(drawField.Y + 390 * Camera.scaleWnd), (int)(280 * Camera.scaleWnd), (int)(150 * Camera.scaleWnd));
            infoRect_2 = new Rectangle((int)(drawField.X + 420 * Camera.scaleWnd), (int)(drawField.Y + 415 * Camera.scaleWnd), (int)(200 * Camera.scaleWnd), (int)(120 * Camera.scaleWnd));

            positionLvl = new Vector2(drawField.X + 200 * Camera.scaleWnd, infoRect_1.Y);
            positionInfo = new Vector2(infoRect_1.X + 15 * Camera.scaleWnd, infoRect_1.Y + 25 * Camera.scaleWnd);
            positionCharacterName = new Vector2(drawField.X + 480 * Camera.scaleWnd, infoRect_2.Y);
            positionCharacterInfo = new Vector2(infoRect_2.X + 10 * Camera.scaleWnd, infoRect_2.Y + 25 * Camera.scaleWnd);

            grid = new InventoryCell[maxCell];

            int index;
            Point size = new Point((int)(54 * Camera.scaleWnd), (int)(54 * Camera.scaleWnd));
            for (int i = 0; i < 6; i++)
                for (int j = 0; j < 4; j++)
                {
                    index = i * 4 + j;
                    grid[index] = new InventoryCell(game,
                                                    new Point(invGridRect.X + (int)(6 * Camera.scaleWnd) + size.X * j,
                                                              invGridRect.Y + (int)(6 * Camera.scaleWnd) + size.Y * i),
                                                    zIndexIcon,
                                                    size,
                                                    index,
                                                    ItemType.Любой);

                    grid[index].mouseLeftButtonDown += Inventory_mouseLeftButtonDown;
                    grid[index].mouseRightButtonDown += Inventory_mouseRightButtonDown;
                }

            index = GameFeature.inventorySize;

            grid[index] = new InventoryCell(game,
                                         new Point(equipeGridRect.X + (int)(71 * Camera.scaleWnd),
                                                   equipeGridRect.Y + (int)(7 * Camera.scaleWnd)),
                                         zIndexIcon,
                                         size,
                                         index++, //увеличение индекса
                                         ItemType.Шлем);

            grid[index] = new InventoryCell(game,
                                         new Point(equipeGridRect.X + (int)(71 * Camera.scaleWnd),
                                                   equipeGridRect.Y + (int)(80 * Camera.scaleWnd)),
                                         zIndexIcon,
                                         size,
                                         index++,
                                         ItemType.Доспех);

            grid[index] = new InventoryCell(game,
                                         new Point(equipeGridRect.X + (int)(71 * Camera.scaleWnd),
                                                   equipeGridRect.Y + (int)(148 * Camera.scaleWnd)),
                                         zIndexIcon,
                                         size,
                                         index++,
                                         ItemType.Пояс);


            grid[index] = new InventoryCell(game,
                                         new Point(equipeGridRect.X + (int)(71 * Camera.scaleWnd),
                                                   equipeGridRect.Y + (int)(216 * Camera.scaleWnd)),
                                         zIndexIcon,
                                         size,
                                         index++,
                                         ItemType.Ботинки);

            grid[index] = new InventoryCell(game,
                                         new Point(equipeGridRect.X + (int)(137 * Camera.scaleWnd),
                                                   equipeGridRect.Y + (int)(44 * Camera.scaleWnd)),
                                         zIndexIcon,
                                         size,
                                         index++,
                                         ItemType.Ожерелье);

            grid[index] = new InventoryCell(game,
                                         new Point(equipeGridRect.X + (int)(5 * Camera.scaleWnd),
                                                   equipeGridRect.Y + (int)(112 * Camera.scaleWnd)),
                                         zIndexIcon,
                                         size,
                                         index++,
                                         ItemType.ОсновноеОружие);

            grid[index] = new InventoryCell(game,
                                         new Point(equipeGridRect.X + (int)(137 * Camera.scaleWnd),
                                                   equipeGridRect.Y + (int)(112 * Camera.scaleWnd)),
                                         zIndexIcon,
                                         size,
                                         index++,
                                         ItemType.ВторостепенноеОружие);
             
            grid[index] = new InventoryCell(game,
                                         new Point(equipeGridRect.X + (int)(17 * Camera.scaleWnd),
                                                   equipeGridRect.Y + (int)(310 * Camera.scaleWnd)),
                                         zIndexIcon,
                                         size,
                                         index++,
                                         ItemType.Зелье);

            grid[index] = new InventoryCell(game,
                                         new Point(equipeGridRect.X + (int)(71 * Camera.scaleWnd),
                                                   equipeGridRect.Y + (int)(310 * Camera.scaleWnd)),
                                         zIndexIcon,
                                         size,
                                         index++,
                                         ItemType.Зелье);

            grid[index] = new InventoryCell(game,
                                         new Point(equipeGridRect.X + (int)(125 * Camera.scaleWnd),
                                                   equipeGridRect.Y + (int)(310 * Camera.scaleWnd)),
                                         zIndexIcon,
                                         size,
                                         index++,
                                         ItemType.Зелье);

            for (int i = GameFeature.inventorySize; i < maxCell; i++)
            {
                grid[i].mouseLeftButtonDown += Inventory_mouseLeftButtonDown;
                grid[i].mouseRightButtonDown += Inventory_mouseRightButtonDown;
            }
            
            game.updateInterface += update;
        }

        private void update(GameTime gameTime)
        {
            previouseMouseState = mouseState;
            mouseState = Mouse.GetState();

            if (selectedCell != null && previouseMouseState.LeftButton == ButtonState.Pressed && mouseState.LeftButton == ButtonState.Released)
            {

                if (selectedCell.iconRect.Intersects(drawField))
                {
                    InventoryCell newCell = null;
                    int maxIntersect = 0;

                    for (int i = 0; i < maxCell; i++)
                    {
                        if (selectedCell.iconRect.Intersects(grid[i].drawField))
                        {
                            Point distance = new Point(selectedCell.iconRect.X - grid[i].drawField.X,
                                                       selectedCell.iconRect.Y - grid[i].drawField.Y);

                            Point intersectField = Point.Zero;

                            if (distance.X >= 0)
                                intersectField.X = grid[i].drawField.X + grid[i].drawField.Width - selectedCell.iconRect.X;
                            else
                                intersectField.X = selectedCell.iconRect.X + selectedCell.iconRect.Width - grid[i].drawField.X;

                            if (distance.Y >= 0)
                                intersectField.Y = grid[i].drawField.Y + grid[i].drawField.Height - selectedCell.iconRect.Y;
                            else
                                intersectField.Y = selectedCell.iconRect.Y + selectedCell.iconRect.Height - grid[i].drawField.Y;

                            if (intersectField.X * intersectField.Y > maxIntersect)
                            {
                                maxIntersect = intersectField.X * intersectField.Y;
                                newCell = grid[i];
                            }
                        }
                    }

                    if (newCell != null && newCell != selectedCell)
                    {
                        AbstractItem tmpItem = newCell.item;
                        AbstractItem selectedItem = selectedCell.item;

                        if (selectedItem != null) effect.Play();

                        if (tmpItem != null && selectedItem!= null &&
                            tmpItem.type == ItemType.Зелье && selectedItem.type == ItemType.Зелье &&
                            (tmpItem as StackableItem).stackType == (selectedItem as StackableItem).stackType &&
                            tmpItem.itemLvl == selectedItem.itemLvl)
                        {
                            if (tmpItem.count + selectedItem.count > tmpItem.maxCount)
                            {
                                selectedItem.count = selectedItem.count + tmpItem.count - tmpItem.maxCount;
                                tmpItem.count = tmpItem.maxCount;
                                character.onInventoryChanged();
                            }
                            else
                            {
                                tmpItem.count += selectedItem.count;
                                character.inventory[selectedCell.Index] = null;
                                character.onInventoryChanged();
                            }
                        }
                        else if (newCell.updateItem(selectedItem) && selectedCell.updateItem(tmpItem))
                        {
                            character.inventory[newCell.Index] = selectedItem;
                            character.inventory[selectedCell.Index] = tmpItem;
                           
                            if (newCell.Index >= GameFeature.inventorySize && newCell.Index < GameFeature.inventorySize + 7 ||
                                selectedCell.Index >= GameFeature.inventorySize && selectedCell.Index < GameFeature.inventorySize + 7)
                                character.recalculateStats();

                            character.onInventoryChanged();
                        }
                        //else //без понятия зачем написал этот код, но вдруг вспомню
                        //{
                        //    newCell.updateItem(tmpItem);
                        //    selectedCell.updateItem(selectedItem);
                        //}
                    }
                }
                else // за пределами окна инвентаря, дропаем
                {
                    dropItem(selectedCell.item);

                    character.inventory[selectedCell.Index] = null;
                    selectedCell.updateItem(null);
                    if (selectedCell.Index >= GameFeature.inventorySize && 
                        selectedCell.Index < GameFeature.inventorySize + GameFeature.equipementSize)
                        character.recalculateStats();//если дропнули экипированную шмотку
                    character.onInventoryChanged();
                }

                selectedCell.isDrag = false;
                selectedCell = null;
            }
        }

        private void dropItem(AbstractItem item)
        {
            Point position = new Point((int)character.positionOnMap.X - GameFeature.rand.Next(character.shade.Width) - 15,
                           (int)character.positionOnMap.Y + character.shade.Height);

            for (int i = 0; i < character.currentMap.listBorders.Count; i++)
                if (position.X >= character.currentMap.listBorders[i].X &&
                    position.X <= character.currentMap.listBorders[i].X + character.currentMap.listBorders[i].Width &&
                    position.Y >= character.currentMap.listBorders[i].Y &&
                    position.Y <= character.currentMap.listBorders[i].Y + character.currentMap.listBorders[i].Height)
                {
                    position.Y = (int)character.positionOnMap.Y - character.shade.Height;
                }

            effect.Play();
            item.Drop(character.currentMap, position);
        }

        private void Inventory_mouseRightButtonDown(object sender, EventArgs e)
        {
            InventoryCell cell = sender as InventoryCell;

            if (cell.item != null)
            {
                switch (cell.item.type)
                {
                    case ItemType.Зелье:
                        StackableItem potion = cell.item as StackableItem;

                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) && potion.count > 1)//разделяем пополам
                        {
                            StackableItem newPotion = potion.getNewItem();

                            newPotion.count = potion.count / 2;

                            potion.count -= newPotion.count;

                           

                            bool find = false;
                            for (int i = 0; i < 24; i++)
                                if (character.inventory[i] == null)
                                {
                                    find = true;
                                    character.inventory[i] = newPotion;
                                    effect.Play();

                                    break;
                                }
                                               
                            if (!find)
                            {
                                character.Say("Недостаточно\nместа.", TypeCharacter.Герой);
                                dropItem(newPotion);
                            }
                            character.onInventoryChanged();
                        }
                        else //просто используем
                            potion.Use();
                        break;

                    case ItemType.Артефакт:
                    case ItemType.Любой:
                        //ни че не делаем
                        break;

                    default: //экипировка
                        if (cell.Index < GameFeature.inventorySize) //нажали на инвентарь
                        {
                            AbstractEquipment equipItem = cell.item as AbstractEquipment;

                            character.inventory[cell.Index] = equipItem.Equipe(character);

                            character.recalculateStats();
                            character.onInventoryChanged();
                        }
                        else //нажали на экипировку
                        {
                            for (int i = 0; i < GameFeature.inventorySize; i++)
                                if (character.inventory[i] == null)
                                {
                                    character.inventory[i] = cell.item;
                                    character.inventory[cell.Index] = null;

                                    character.recalculateStats();
                                    character.onInventoryChanged();
                                    break;
                                }
                        }

                        effect.Play();
                        break;
                }
            }
        }

        private void Inventory_mouseLeftButtonDown(object sender, EventArgs e)
        {
            selectedCell = sender as InventoryCell;
            selectedCell.isDrag = true;
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background, drawField, null, color, 0, Vector2.Zero, SpriteEffects.None, zIndex);
            spriteBatch.Draw(inventGrid, invGridRect, null, color, 0, Vector2.Zero, SpriteEffects.None, zIndexGrid);
            spriteBatch.Draw(equipeGrid, equipeGridRect, null, color, 0, Vector2.Zero, SpriteEffects.None, zIndexGrid);
            spriteBatch.Draw(equipeIcon, equipeIconRect, null, color, 0, Vector2.Zero, SpriteEffects.None, zIndexIconGrid);
            spriteBatch.DrawString(GameFeature.Font, info, positionInfo, Color.White, 0, Vector2.Zero, fontScale, SpriteEffects.None, zIndexGrid);
            spriteBatch.DrawString(GameFeature.Font, lvl, positionLvl, Color.Yellow, 0, Vector2.Zero, fontScale, SpriteEffects.None, zIndexGrid);
            spriteBatch.DrawString(GameFeature.Font, characterName, positionCharacterName, Color.Yellow, 0, Vector2.Zero, fontScale, SpriteEffects.None, zIndexGrid);
            spriteBatch.DrawString(GameFeature.Font, characterInfo, positionCharacterInfo, Color.White, 0, Vector2.Zero, fontScale, SpriteEffects.None, zIndexGrid);
        }

        public void updateItems(AbstractCharacter character)
        {
            if (character != null)
            {
                character.inventoryChanged -= Character_inventoryChanged;
                character.statsChanged -= Character_statsChanged;
            }

            if (selectedCell != null) selectedCell.isDrag = false;
            selectedCell = null;            

            this.character = character;

            characterName = character.name;
            characterInfo = character.info;

            Character_inventoryChanged(null, null);
            Character_statsChanged(null, null);

            Vector2 size = GameFeature.Font.MeasureString(lvl) * fontScale;
            positionLvl.X = infoRect_1.X + (infoRect_1.Width - size.X) / 2;
            size = GameFeature.Font.MeasureString(characterName) * fontScale;
            positionCharacterName.X = infoRect_2.X + (infoRect_2.Width - size.X) / 2;

            character.inventoryChanged += Character_inventoryChanged;
            character.statsChanged += Character_statsChanged;
        }

        private void Character_statsChanged(object sender, EventArgs e)
        {
            lvl = $"Уровень: {character.Lvl}";
            info = $"Здоровье: {character.healthBar.MaxValue}\nМана: {character.manaBar.MaxValue}\nАтака: {character.attack}\nЗащита: {character.defense}\nСкорость: {(int)character.moveSpeed}";
        }

        private void Character_inventoryChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < maxCell - GameFeature.countPotions; i++)
                grid[i].updateItem(character.inventory[i]);


            AbstractBelt belt = character.inventory[26] as AbstractBelt;

            for (int i = 0; i < GameFeature.countPotions; i++)
            {
                int index = GameFeature.inventorySize + GameFeature.equipementSize + i;

                if (belt != null && belt.Size > i)
                {
                    grid[index].isActive = true;
                    grid[index].updateItem(character.inventory[index]);
                    potionIcons[i].updatePotion(character.inventory[index] as StackableItem);
                }
                else if (character.inventory[index] != null)
                {
                    int newIndex = -1;

                    for (int j = 0; j < GameFeature.inventorySize; j++)
                        if (character.inventory[j] == null)
                        {
                            newIndex = j;
                            break;
                        }

                    if (newIndex == -1) dropItem(character.inventory[index]);
                    else character.inventory[newIndex] = character.inventory[index];

                    potionIcons[i].updatePotion(null);
                    grid[index].updateItem(null);
                    grid[index].isActive = false;
                    character.inventory[index] = null;
                    character.onInventoryChanged();
                }
                else grid[index].isActive = false;
            }

        }

        public void Show()
        {
            isVisible = true;

            for (int i = 0; i < maxCell; i++)
                grid[i].isVisible = true;
        }

        public void Hide()
        {
            isVisible = false;
            for (int i = 0; i < maxCell; i++)
                grid[i].isVisible = false;
        }
    }
}
