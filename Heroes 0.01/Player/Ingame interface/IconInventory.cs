﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Heroes_0._01
{
    public class IconInventory : Icon
    {
        public IconInventory(MyGame game, Point position, string Key) : base(game, position, Key)
        {
            width = (int)(90 * Camera.scaleWnd);
            height = (int)(90 * Camera.scaleWnd);
            shiftKey = (int)(9 * Camera.scaleWnd);
            zIndex = 0.9f + 2 * GameFeature.zIndexStep;
            loadIcon("Interface/backpack", "Interface/backpack_select");
        }
    }
}
