﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Heroes_0._01
{
    public class CharacterHead : Icon
    {
        public bool isOpen = false;

        private string text = "";
        private Vector2 positionTxt;
        public float zIndexFont;
        private float fontScale = 0.18f;
        private AbstractCharacter character;

        private IconBright effect;

        public CharacterHead(MyGame game, Point position, string Key) : base(game, position, Key)
        {
            width = (int)(80 * Camera.scaleWnd);
            height = (int)(80 * Camera.scaleWnd);
            shiftKey = (int)(5 * Camera.scaleWnd);
            loadIcon("Characters/Tank/TankHead", "Characters/Tank/selectTankHead");
            zIndex = 0.9f + 2 * GameFeature.zIndexStep;
            zIndexFont = zIndex + GameFeature.zIndexStep;
            effect = new IconBright(game, zIndex - GameFeature.zIndexStep, new Point(position.X - (int)(70 * Camera.scaleWnd), position.Y - (int)(70 * Camera.scaleWnd)), new Point((int)(220 * Camera.scaleWnd), (int)(220 * Camera.scaleWnd)));
            fontScale *= Camera.scaleWnd;
        }

        private void drawText(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(GameFeature.Font, text, positionTxt, Color.Yellow, 0, Vector2.Zero, fontScale, SpriteEffects.None, zIndexFont);
        }

        public void updateCharacter(AbstractCharacter character)
        {
            if (this.character != null) this.character.skillPointChange -= Character_skillPointChange;
            this.character = character;

            defaultTexture = character.iconHead;
            selectTexture = character.iconSelectHead;
            texture = defaultTexture;

            if (isVisible && character.SkillPoint > 0) effect.Play();
            else effect.Stop();

            updateText();
            updatePosition();
            character.skillPointChange += Character_skillPointChange;
        }

        private void Character_skillPointChange(object sender, EventArgs e)
        {
            updateText();
            if (isVisible && character.SkillPoint > 0) effect.Play();
            else effect.Stop();
        }

        private void updateText()
        {
            if (character.SkillPoint > 0)
            {
                text = character.SkillPoint.ToString();
                updatePosition();
            }
            else
            {
                text = "";
            }
        }

        private void updatePosition()
        {
            Vector2 sizeTxt = GameFeature.Font.MeasureString(text) * fontScale;
            positionTxt.X = drawField.X + drawField.Width - sizeTxt.X/2;
            positionTxt.Y = drawField.Y;
        }

        public void Show()
        {
            game.drawInterface -= drawText;
            game.drawInterface += drawText;
            isVisible = true;
            if (character != null && character.SkillPoint > 0) effect.Play();
        }

        public void Hide()
        {
            game.drawInterface -= drawText;
            isVisible = false;
            effect.Stop();
        }
    }
}
