﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Heroes_0._01
{
    public class SkillIcon : Icon
    {
        private UpgradePlus upgrade;
        private UpgradeEffect effect;
        private Description description;
        private Rectangle iconField;
        private Texture2D icon;

        public AbstractSkill skill;
        private float zIndexIcon;
        private float zIndexCooldown;
        private float zIndexDescription;

        private float descriptionFontSize = 0.17f;

        private float fontCooldownScale = 0.3f * Camera.scaleWnd;
        private Vector2 positionCooldown;
        private string cooldown = "";
        private Color colorCooldown = Color.DimGray * 0.9f;

        public SkillIcon(MyGame game, Point position, string Key) : base(game, position, Key)
        {
            width = (int)(60 * Camera.scaleWnd);
            height = (int)(60 * Camera.scaleWnd);
            loadIcon("Interface/iconback", "Interface/iconbackselect");
            zIndex = 0.9f + 2 * GameFeature.zIndexStep;
            icon = game.Content.Load<Texture2D>("Interface/iconback");
            description = new Description(game, true);
            zIndexIcon = zIndex - GameFeature.zIndexStep;
            zIndexCooldown = zIndex + 2 * GameFeature.zIndexStep;

            zIndexDescription = 0.95f;

            iconField.Width = drawField.Width - (int)(4 * Camera.scaleWnd);
            iconField.Height = drawField.Height - (int)(4 * Camera.scaleWnd);
            iconField.X = drawField.X + (int)(2 * Camera.scaleWnd);
            iconField.Y = drawField.Y + (int)(2 * Camera.scaleWnd);

            int Height = (int)(drawField.Height * 0.25f);
            upgrade = new UpgradePlus(game, Height, drawField.Width, new Point(drawField.X, drawField.Y - Height), zIndex);
            upgrade.mouseEnter += Upgrade_mouseEnter;
            upgrade.mouseLeave += Upgrade_mouseLeave;
            upgrade.mouseLeftButtonDown += Upgrade_mouseLeftButtonDown;


            mouseLeftButtonDown += Cast;
            effect = new UpgradeEffect(game, zIndexCooldown + GameFeature.zIndexStep, new Point(position.X - (int)(70 * Camera.scaleWnd), position.Y - (int)(70 * Camera.scaleWnd)));
        }

        public void ShowUpgrade()
        {
            if (skill.maxLvl > skill.Lvl && !upgrade.isVisible) upgrade.isVisible = true;
        }

        public void HideUpgrade()
        {
            upgrade.isVisible = false;
        }

        private void Upgrade_mouseLeftButtonDown(object sender, EventArgs e)
        {
            upgradeSkill();
        }

        public void upgradeSkill()
        {
            if (upgrade.isVisible && skill.Upgrade())
            {
                effect.Play();
                zIndexIcon = zIndex + GameFeature.zIndexStep;
                if (skill.Lvl >= skill.maxLvl)
                {
                    upgrade.isVisible = false;
                    return;
                }
                description.updateDescription(skill.nextLvlName, skill.nextLvlDescription, descriptionFontSize, upgrade.drawField, zIndexDescription);
            }
        }

        private void Upgrade_mouseLeave(object sender, EventArgs e)
        {
            description.isVisible = false;
        }

        private void Upgrade_mouseEnter(object sender, EventArgs e)
        {
            description.updateDescription(skill.nextLvlName, skill.nextLvlDescription, descriptionFontSize, upgrade.drawField, zIndexDescription);
            description.isVisible = true;
        }

        protected override void Icon_mouseEnter(object sender, EventArgs e)
        {
            if (skill != null && skill.Lvl > 0)
            {
                description.updateDescription(skill.name, skill.description, descriptionFontSize, drawField, zIndexDescription);
                description.isVisible = true;
            }
            if (skill != null && skill.typeSkill == TypeSkill.Active) texture = selectTexture;
        }

        protected override void Icon_mouseLeave(object sender, EventArgs e)
        {
            description.isVisible = false;
            texture = defaultTexture;
        }

        private void drawIcon(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(icon, iconField, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, zIndexIcon);
        }

        public void updateSkill(AbstractSkill skill)
        {
            if (this.skill != null)
            {
                this.skill.isCast -= Skill_isCast;
                this.skill.Cancel();
            }
            game.updateGame -= updateCooldown;
            game.drawInterface -= drawCooldown;
            upgrade.isVisible = false;
            this.skill = skill;
            icon = skill.icon;
            if (skill.Lvl == 0) zIndexIcon = zIndex - GameFeature.zIndexStep;
            else zIndexIcon = zIndex + GameFeature.zIndexStep;

            description.updateDescription(skill.name, skill.description, descriptionFontSize, drawField, zIndexDescription);

            if (skill.currentCooldown != 0)
            {
                game.updateGame += updateCooldown;
                game.drawInterface += drawCooldown;
            }

            skill.isCast += Skill_isCast;
        }

        private void Skill_isCast(object sender, EventArgs e)
        {
            game.updateGame += updateCooldown;
            game.drawInterface += drawCooldown;
            cooldown = skill.cooldown.ToString();
            updateCooldown(null);
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, drawField, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, zIndex);
            if (skill != null && skill.typeSkill == TypeSkill.Active) spriteBatch.DrawString(GameFeature.Font, Key, positionLetter, Color.White, 0, Vector2.Zero, fontLetterScale, SpriteEffects.None, zIndex);
        }

        private void drawCooldown(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(GameFeature.Font, cooldown, positionCooldown, colorCooldown, 0, Vector2.Zero, fontCooldownScale, SpriteEffects.None, zIndexCooldown);         
        }

        private void updateCooldown(GameTime gameTime)
        {
            if (skill.currentCooldown == 0)
            {
                game.updateGame -= updateCooldown;
                game.drawInterface -= drawCooldown;
                cooldown = skill.cooldown.ToString();
                return;
            }
            cooldown = skill.currentCooldown.ToString();
            Vector2 size = GameFeature.Font.MeasureString(cooldown) * fontCooldownScale;
            positionCooldown.X = iconField.X + (iconField.Width - size.X) / 2;
            positionCooldown.Y = iconField.Y + (iconField.Height - size.Y) / 2;
        }

        public void Cast(object o, EventArgs e)
        {
            skill?.Cast();
        }

        public void Show()
        {
            isVisible = true;
            game.drawInterface -= drawIcon;
            game.drawInterface += drawIcon;
            if (skill != null && skill.currentCooldown > 0)
            {
                game.updateGame -= updateCooldown;
                game.drawInterface -= drawCooldown;

                game.updateGame += updateCooldown;
                game.drawInterface += drawCooldown;
            }
        }

        public void Hide()
        {
            isVisible = false;
            game.drawInterface -= drawIcon;
            game.updateGame -= updateCooldown;
            game.drawInterface -= drawCooldown;
        }
    }
}
