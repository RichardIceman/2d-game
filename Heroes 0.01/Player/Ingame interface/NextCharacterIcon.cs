﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Heroes_0._01
{
    public class NextCharacterIcon : Icon
    {
        private Texture2D arrowTexture;
        protected Rectangle arrowRect;
        private Vector2 origin;
        protected float angle;
        protected SpriteEffects spriteEffect = SpriteEffects.None;

        public NextCharacterIcon(MyGame game, Point position, string Key) : base(game, position, Key)
        {
            width = (int)(40 * Camera.scaleWnd);
            height = (int)(40 * Camera.scaleWnd);
            shiftKey = (int)(5 * Camera.scaleWnd);
            zIndex = 0.9f + 2 * GameFeature.zIndexStep;
            arrowRect = new Rectangle(position.X - (int)(35 * Camera.scaleWnd), position.Y - (int)(30 * Camera.scaleWnd), (int)(25 * Camera.scaleWnd), (int)(50 * Camera.scaleWnd));
            origin = new Vector2(20 * Camera.scaleWnd, 40 * Camera.scaleWnd);
            angle = -(float)Math.PI / 7;
            arrowTexture = game.Content.Load<Texture2D>("Interface/next");
            
            loadIcon("Characters/Tank/TankHead", "Characters/Tank/selectTankHead");
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(arrowTexture, arrowRect, null, Color.Yellow, angle, origin, spriteEffect, zIndex);
            base.draw(gameTime, spriteBatch);
        }

        public void update(AbstractCharacter character)
        {
            defaultTexture = character.iconHead;
            selectTexture = character.iconSelectHead;
            texture = defaultTexture;
        }
    }
}
