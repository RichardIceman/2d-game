﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;

namespace Heroes_0._01
{
    public class HeroInfo : TargetInfo
    {
        public IconBright effect;

        private Vector2 positionTxt;
        private string key;
        private float fontSize = 0.12f * Camera.scaleWnd;
        private float zIndexFont;

        private int time;
        private int duration = 500;
        private float prevHP = 0;

        public HeroInfo(MyGame game, Point position, string key) : base(game, position)
        {
            healthBar.color = Color.LimeGreen;

            this.key = key;

            Vector2 sizeTxt = GameFeature.Font.MeasureString(key) * fontSize;
            positionTxt = new Vector2(iconRect.X + (iconRect.Width - sizeTxt.X) / 2, iconRect.Y + iconRect.Height);

            zIndexFont = zIndex + 4 * GameFeature.zIndexStep;
            effect = new IconBright(game, zIndex + 2 * GameFeature.zIndexStep, new Point(position.X - (int)(110 * Camera.scaleWnd), position.Y - (int)(80 * Camera.scaleWnd)), new Point((int)(220 * Camera.scaleWnd), (int)(220 * Camera.scaleWnd)));

            mouseEnter += HeroInfo_mouseEnter;
            mouseLeave += HeroInfo_mouseLeave;

            effect.Play();
        }

        protected override void HealthBar_valueChange(object sender, EventArgs e)
        {
            base.HealthBar_valueChange(sender, e);
            if (prevHP > healthBar.Value)
            {
                time = 0;
                game.updateInterface -= update;
                game.updateInterface += update;
            }
            prevHP = healthBar.Value;
        }

        protected void update(GameTime gameTime)
        {
            time += gameTime.ElapsedGameTime.Milliseconds;

            if (time >= duration)
            {
                game.updateInterface -= update;
                healthBar.color = Color.LimeGreen;
                return;
            }

            healthBar.color = new Color(GameFeature.rand.Next(150, 255), 0, 0);
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(GameFeature.Font, key, positionTxt, Color.White, 0, Vector2.Zero, fontSize, SpriteEffects.None, zIndexFont);
            base.draw(gameTime, spriteBatch);
        }

        private void HeroInfo_mouseLeave(object sender, EventArgs e)
        {
            if (character != null)
                iconTexture = character.iconHead;
        }

        private void HeroInfo_mouseEnter(object sender, EventArgs e)
        {
            if (character != null)
                iconTexture = character.iconSelectHead;
        }

        public override void UpdateTarget(AbstractCharacter character)
        {
            if (this.character != null) this.character.skillPointChange -= Character_skillPointChange;
            base.UpdateTarget(character);
            if (character != null)
            {
                character.skillPointChange += Character_skillPointChange;

                prevHP = character.healthBar.Value;

                if (isVisible && character.SkillPoint > 0) effect.Play();
                else effect.Stop();
            }
        }

        private void Character_skillPointChange(object sender, EventArgs e)
        {
            if (isVisible && character.SkillPoint > 0) effect.Play();
            else effect.Stop();
        }

        public override void Hide()
        {
            effect.Stop();
            base.Hide();
        }

        public override void Show()
        {
            if (character != null && character.SkillPoint > 0) effect.Play();
            else effect.Stop();
            base.Show();
        }
    }
}
