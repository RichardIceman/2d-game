﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace Heroes_0._01
{
    public class Shop : InteractiveObject
    {
        private Texture2D backTexture;
        private Texture2D gridTexture;

        public int maxRange = 200;

        public List<AbstractCharacter> listCustomers;
        public AbstractNPC seller;

        private Rectangle gridRectangle;

        private Rectangle hero_1_rect;
        private Rectangle hero_2_rect;
        private Rectangle hero_3_rect;

        private Rectangle seller_rect;
        private Rectangle sell_rect;
        private Rectangle buy_rect;

        private Rectangle head_1;
        private Rectangle head_2;
        private Rectangle head_3;
        private Rectangle head_4;

        private Texture2D head_1_txt;
        private Texture2D head_2_txt;
        private Texture2D head_3_txt;
        private Texture2D head_4_txt;
        private Texture2D rectTxt;

        private float zIndexGrid;
        private float zIndexIcon;
        private float zIndexHead;

        private float fontScale = 0.2f * Camera.scaleWnd;
        private float sellBuyInfoScale = 0.18f * Camera.scaleWnd;

        private InventoryCell character_cell;
        private InventoryCell seller_cell;

        private InventoryCell[] grid_characters;
        private InventoryCell[] grid_seller;
        private InventoryCell sellerMoney;
        private InventoryCell charatcersMoney;

        private MouseState previouseMouseState;
        private MouseState mouseState;

        private Label labelSellInfo;
        private Label labelSellCount;

        private Label labelBuyInfo;
        private Label labelBuyCount;

        private Label labelDiscount;

        private Button buttonCalculate;
        private Button buttonCancel;
        private Button buttonBuy;

        private Color backColor = Color.White * 0.95f;
        private Color colorRect = Color.Black * 0.5f;

        private SoundEffect effect;

        private int sellCost = 0;
        private int buyCost = 0;

        private float discount = 0;
        public float Discount
        {
            get { return discount; }
            set
            {
                discount = value;
                labelDiscount.Text = $"Скидка: {(int)(Discount * 100)}%";
            }
        }

        public Shop(MyGame game) : base(game)
        {
            backTexture = game.Content.Load<Texture2D>("Interface/shopBack");
            gridTexture = game.Content.Load<Texture2D>("Interface/shop");
            rectTxt = game.Content.Load<Texture2D>("Characters/Shades/white");

            effect = game.Content.Load<SoundEffect>("Songs/cloth-heavy");

            isInterface = true;

            zIndex = 0.92f;
            zIndexIcon = zIndex + GameFeature.zIndexStep;
            zIndexGrid = zIndex + GameFeature.zIndexStep * 3;
            zIndexHead = zIndex + GameFeature.zIndexStep * 4;

            gridRectangle = new Rectangle(0, 0, (int)(gridTexture.Width * Camera.scaleWnd), (int)(gridTexture.Height * Camera.scaleWnd));
            drawField = new Rectangle(0, 0, gridRectangle.Width, gridRectangle.Height);

            gridRectangle.X = (int)(Camera.sizeWnd.X - gridRectangle.Width) / 2;
            gridRectangle.Y = (int)(40 * Camera.scaleWnd);
            drawField.Location = gridRectangle.Location;

            grid_characters = new InventoryCell[84];
            //0 - 23 hero_1
            //24 - 47 hero_2
            //48 - 71 hero_3
            //72 - 83 на продажу

            grid_seller = new InventoryCell[36];
            //0 - 23 seller
            //24 - 35 на покупку


            int index;
            Point size = new Point((int)(54 * Camera.scaleWnd), (int)(54 * Camera.scaleWnd));
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 6; j++)
                {
                    index = i * 6 + j;
                    grid_characters[index] = new InventoryCell(game, 
                                                               new Point(drawField.X + (int)(36 * Camera.scaleWnd) + size.X * j,
                                                                         drawField.Y + (int)(40 * Camera.scaleWnd) + size.Y * i),
                                                               zIndexIcon,
                                                               size,
                                                               index,
                                                               ItemType.Любой);


                    grid_characters[24 + index] = new InventoryCell(game,
                                                                    new Point(drawField.X + (int)(36 * Camera.scaleWnd) + size.X * j,
                                                                              drawField.Y + (int)(279 * Camera.scaleWnd) + size.Y * i),
                                                                    zIndexIcon,
                                                                    size,
                                                                    24 + index,
                                                                    ItemType.Любой);


                    grid_characters[48 + index] = new InventoryCell(game,
                                                                    new Point(drawField.X + (int)(36 * Camera.scaleWnd) + size.X * j,
                                                                              drawField.Y + (int)(518 * Camera.scaleWnd) + size.Y * i),
                                                                    zIndexIcon,
                                                                    size,
                                                                    48 + index,
                                                                    ItemType.Любой);

                    grid_characters[index].mouseLeftButtonDown += grid_character_mouseLeftButtonDown;
                    grid_characters[24 + index].mouseLeftButtonDown += grid_character_mouseLeftButtonDown;
                    grid_characters[48 + index].mouseLeftButtonDown += grid_character_mouseLeftButtonDown;

                    grid_characters[index].mouseRightButtonDown += grid_character_mouseRightButtonDown;
                    grid_characters[24 + index].mouseRightButtonDown += grid_character_mouseRightButtonDown;
                    grid_characters[48 + index].mouseRightButtonDown += grid_character_mouseRightButtonDown;
                }

            for (int i = 0; i < 6; i++)
                for (int j = 0; j < 4; j++)
                {
                    index = i * 4 + j;
                    grid_seller[index] = new InventoryCell(game,
                                                           new Point(drawField.X + (int)(709 * Camera.scaleWnd) + size.X * j,
                                                                     drawField.Y + (int)(40 * Camera.scaleWnd) + size.Y * i),
                                                           zIndexIcon,
                                                           size,
                                                           index,
                                                           ItemType.Любой);

                    grid_seller[index].mouseLeftButtonDown += grid_seller_mouseLeftButtonDown;
                    grid_seller[index].mouseRightButtonDown += grid_seller_mouseRightButtonDown;
                }

            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 4; j++)
                {
                    index = i * 4 + j;
                    grid_characters[72 + index] = new InventoryCell(game,
                                                                    new Point(drawField.X + (int)(425 * Camera.scaleWnd) + size.X * j,
                                                                              drawField.Y + (int)(522 * Camera.scaleWnd) + size.Y * i),
                                                                    zIndexIcon,
                                                                    size,
                                                                    72 + index,
                                                                    ItemType.Любой);

                    grid_seller[24 + index] = new InventoryCell(game,
                                                                new Point(drawField.X + (int)(425 * Camera.scaleWnd) + size.X * j,
                                                                          drawField.Y + (int)(224 * Camera.scaleWnd) + size.Y * i),
                                                                zIndexIcon,
                                                                size,
                                                                24 + index,
                                                                ItemType.Любой);

                    grid_characters[72 + index].mouseLeftButtonDown += grid_character_mouseLeftButtonDown;
                    grid_characters[72 + index].mouseRightButtonDown += sellGrid_mouseRightButtonDown;
                    grid_seller[24 + index].mouseLeftButtonDown += grid_seller_mouseLeftButtonDown;
                    grid_seller[24 + index].mouseRightButtonDown += buyGrid_mouseRightButtonDown;
                }

            sellerMoney = new InventoryCell(game,
                                            new Point(drawField.X + (int)(425 * Camera.scaleWnd) + size.X * 3,
                                                      drawField.Y + (int)(224 * Camera.scaleWnd) + size.Y * 3),
                                            zIndexIcon,
                                            size,
                                            0,
                                            ItemType.Любой);

            charatcersMoney = new InventoryCell(game,
                                                new Point(drawField.X + (int)(425 * Camera.scaleWnd) + size.X * 3,
                                                          drawField.Y + (int)(522 * Camera.scaleWnd) + size.Y * 3),
                                                zIndexIcon,
                                                size,
                                                0,
                                                ItemType.Любой);

            sellerMoney.mouseLeftButtonDown += SellerMoney_mouseLeftButtonDown;
            charatcersMoney.mouseLeftButtonDown += CharatcersMoney_mouseLeftButtonDown;

            sellerMoney.mouseRightButtonDown += SellerMoney_mouseRightButtonDown;
            charatcersMoney.mouseRightButtonDown += CharatcersMoney_mouseRightButtonDown;

            hero_1_rect = new Rectangle(drawField.X + (int)(36 * Camera.scaleWnd),
                                        drawField.Y + (int)(40 * Camera.scaleWnd),
                                        size.X * 6,
                                        size.Y * 4);

            hero_2_rect = new Rectangle(drawField.X + (int)(36 * Camera.scaleWnd),
                                        drawField.Y + (int)(279 * Camera.scaleWnd),
                                        size.X * 6,
                                        size.Y * 4);

            hero_3_rect = new Rectangle(drawField.X + (int)(36 * Camera.scaleWnd),
                                        drawField.Y + (int)(518 * Camera.scaleWnd),
                                        size.X * 6,
                                        size.Y * 4);

            seller_rect = new Rectangle(drawField.X + (int)(709 * Camera.scaleWnd),
                                        drawField.Y + (int)(40 * Camera.scaleWnd),
                                        size.X * 4,
                                        size.Y * 6);

            sell_rect = new Rectangle(drawField.X + (int)(425 * Camera.scaleWnd),
                                      drawField.Y + (int)(224 * Camera.scaleWnd),
                                      size.X * 4,
                                      size.Y * 3);

            buy_rect = new Rectangle(drawField.X + (int)(425 * Camera.scaleWnd),
                                     drawField.Y + (int)(522 * Camera.scaleWnd),
                                     size.X * 4,
                                     size.Y * 3);

            labelBuyCount = new Label(game,
                                      "Итого: 0",
                                      new Vector2(drawField.X + 425 * Camera.scaleWnd,
                                                  drawField.Y + 405 * Camera.scaleWnd),
                                      sellBuyInfoScale,
                                      Color.White,
                                      zIndexGrid);
            labelBuyCount.isVisible = false;

            labelSellCount = new Label(game,
                                       "Итого: 0",
                                       new Vector2(drawField.X + 425 * Camera.scaleWnd,
                                                   drawField.Y + 703 * Camera.scaleWnd),
                                       sellBuyInfoScale,
                                       Color.White,
                                       zIndexGrid);
            labelSellCount.isVisible = false;

            Vector2 sizeTxt = GameFeature.Font.MeasureString("Товары торговца") * fontScale;

            labelBuyInfo = new Label(game,
                                     "Товары торговца",
                                     new Vector2(drawField.X + 425 * Camera.scaleWnd + (buy_rect.Width - sizeTxt.X) / 2,
                                                 drawField.Y + 180 * Camera.scaleWnd),
                                     fontScale,
                                     Color.White,
                                     zIndexGrid);
            labelBuyInfo.isVisible = false;

            sizeTxt = GameFeature.Font.MeasureString("Ваши товары") * fontScale;

            labelSellInfo = new Label(game,
                                      "Ваши товары",
                                      new Vector2(drawField.X + 425 * Camera.scaleWnd + (buy_rect.Width - sizeTxt.X) / 2,
                                                  drawField.Y + 478 * Camera.scaleWnd),
                                      fontScale,
                                      Color.White,
                                      zIndexGrid);
            labelSellInfo.isVisible = false;

            sizeTxt = GameFeature.Font.MeasureString("Скидка: 0%") * fontScale;

            labelDiscount = new Label(game,
                                      "Скидка: 0%",
                                      new Vector2(seller_rect.X + (seller_rect.Width - sizeTxt.X) / 2,
                                                  seller_rect.Y + seller_rect.Height + 60 * Camera.scaleWnd),
                                      fontScale,
                                      Color.Yellow,
                                      zIndexGrid);
            labelDiscount.isVisible = false;

            sizeTxt = GameFeature.Font.MeasureString("Обменять") * fontScale;

            buttonBuy = new Button(game,
                                   new Point(seller_rect.X + (int)(seller_rect.Width - sizeTxt.X)/2,
                                             seller_rect.Y + seller_rect.Height + (int)(200 * Camera.scaleWnd)),
                                   "Обменять",
                                   fontScale,
                                   zIndexGrid);
            buttonBuy.isActive = false;

            sizeTxt = GameFeature.Font.MeasureString("Сбалансировать") * fontScale;

            buttonCalculate = new Button(game,
                                         new Point(seller_rect.X + (int)(seller_rect.Width - sizeTxt.X) / 2,
                                                   seller_rect.Y + seller_rect.Height + (int)(250 * Camera.scaleWnd)),
                                         "Сбалансировать",
                                          fontScale,
                                          zIndexGrid);

            sizeTxt = GameFeature.Font.MeasureString("Закрыть") * fontScale;

            buttonCancel = new Button(game,
                                      new Point(seller_rect.X + (int)(seller_rect.Width - sizeTxt.X) / 2,
                                                seller_rect.Y + seller_rect.Height + (int)(300 * Camera.scaleWnd)),
                                      "Закрыть",
                                      fontScale,
                                      zIndexGrid);


            buttonCancel.mouseLeftButtonDown += ButtonCancel_mouseLeftButtonDown;
            buttonCalculate.mouseLeftButtonDown += ButtonCalculate_mouseLeftButtonDown;
            buttonBuy.mouseLeftButtonDown += ButtonBuy_mouseLeftButtonDown;


            head_1 = new Rectangle(drawField.X - (int)(80 * Camera.scaleWnd),
                                   hero_1_rect.Y,
                                   (int)(100 * Camera.scaleWnd),
                                   (int)(100 * Camera.scaleWnd));

            head_2 = new Rectangle(drawField.X - (int)(80 * Camera.scaleWnd),
                                  hero_2_rect.Y,
                                  (int)(100 * Camera.scaleWnd),
                                  (int)(100 * Camera.scaleWnd));
            head_3 = new Rectangle(drawField.X - (int)(80 * Camera.scaleWnd),
                                  hero_3_rect.Y,
                                  (int)(100 * Camera.scaleWnd),
                                  (int)(100 * Camera.scaleWnd));
            head_4 = new Rectangle(drawField.X + drawField.Width - (int)(20 * Camera.scaleWnd),
                                  seller_rect.Y,
                                  (int)(100 * Camera.scaleWnd),
                                  (int)(100 * Camera.scaleWnd));
        }

        private void CharatcersMoney_mouseRightButtonDown(object sender, EventArgs e)
        {
         
            GameFeature.Money += charatcersMoney.item.count;
            charatcersMoney.updateItem(null);
            effect.Play();
            recalculateSellCost();
        }

        private void SellerMoney_mouseRightButtonDown(object sender, EventArgs e)
        {
            effect.Play();
            sellerMoney.updateItem(null);
            recalculateBuyCost();
        }

        private void CharatcersMoney_mouseLeftButtonDown(object sender, EventArgs e)
        {
            charatcersMoney.isDrag = true;
        }

        private void SellerMoney_mouseLeftButtonDown(object sender, EventArgs e)
        {
            sellerMoney.isDrag = true;
        }

        private void ButtonBuy_mouseLeftButtonDown(object sender, EventArgs e)
        {
            if (buttonBuy.isActive)
            {
                int j = 0;
                int i = 0;

                for (; i < 12; i++)//за 1 проход по инвентарям героев распределяем покупки
                {
                    bool find = false;
                    if (grid_seller[24 + i].item != null)
                    {
                        for (; j < 72; j++)
                            if (listCustomers[j / 24].inventory[j % 24] == null)
                            {
                                find = true;

                                listCustomers[j / 24].inventory[j % 24] = grid_seller[24 + i].item;
                                grid_seller[24 + i].item.character = listCustomers[j / 24];                           
                                break;
                            }

                        if (!find)
                        {
                            dropItem(grid_seller[24 + i].item);
                        }

                        grid_seller[24 + i].updateItem(null);
                    }
                }

                i = 0;
                j = 0;

                for (; i < 12; i++)//за 1 проход по инвентарю продавца распределяем проданное
                {
                    bool find = false;
                    if (grid_characters[72 + i].item != null)
                    {
                        if (grid_characters[72 + i].item.type != ItemType.Артефакт)
                        {
                            for (; j < 24; j++)
                                if (seller.inventory[j] == null)
                                {
                                    find = true;

                                    seller.inventory[j] = grid_characters[72 + i].item;
                                    grid_characters[72 + i].item.character = seller;
                                    break;
                                }

                            if (!find)
                            {
                                seller.extraInventory.Add(grid_characters[72 + i].item);
                                grid_characters[72 + i].item.character = seller;
                            }
                        }
                        else //продаем артефакт
                        {
                            switch(grid_characters[72 + i].item.name)
                            {
                                case "Сердце Джу:":
                                    GameFeature.lostHeartGhost = true;
                                    break;
                                case "Сердце Глыбы:":
                                    GameFeature.lostHeartGolem = true;
                                    break;
                            }                   
                        }
                        grid_characters[72 + i].updateItem(null);
                    }
                }

                charatcersMoney.updateItem(null);

                if (sellerMoney.item != null)
                    GameFeature.Money += sellerMoney.item.count;

                sellerMoney.updateItem(null);

                recalculateBuyCost();
                recalculateSellCost();

                updateCustomers();
                updateSeller();
            }
        }
            
        

        private void ButtonCalculate_mouseLeftButtonDown(object sender, EventArgs e)
        {
            if (sellCost != buyCost) 
            {
                int countMoney = sellCost - buyCost;

                if (countMoney > 0) //торговец
                {
                    if (sellerMoney.item != null)
                    {
                        sellerMoney.item.count += countMoney;
                        sellerMoney.updateItem(sellerMoney.item);
                    }
                    else
                    {
                        Money money = new Money(game);
                        money.count = countMoney;
                        sellerMoney.updateItem(money);
                    }

                    recalculateBuyCost();
                }
                else //герои
                {
                    if (charatcersMoney.item != null)
                    {
                        if (GameFeature.Money > 0 && GameFeature.Money + countMoney < 0)
                            charatcersMoney.item.count -= countMoney;
                        else
                        {
                            charatcersMoney.item.count += GameFeature.Money;
                            GameFeature.Money = 0;
                        }
                        charatcersMoney.updateItem(charatcersMoney.item);
                    }
                    else if (GameFeature.Money > 0)
                    {
                        Money money = new Money(game);

                        if (GameFeature.Money + countMoney < 0)
                        {
                            money.count = GameFeature.Money;
                            GameFeature.Money = 0;
                        }
                        else
                        {
                            money.count = -countMoney;
                            GameFeature.Money += countMoney;
                        }                       
                       
                        charatcersMoney.updateItem(money);
                    }

                    recalculateSellCost();
                }
            }
        }

        private void sellGrid_mouseRightButtonDown(object sender, EventArgs e)
        {
            InventoryCell cell = sender as InventoryCell;

            if (cell.item != null)
            {
                if (cell.item.type == ItemType.Зелье && Keyboard.GetState().IsKeyDown(Keys.LeftShift) && cell.item.count > 1) //разделяем при Shift
                {
                    InventoryCell newCell = null;
                    for (int i = 72; i<84; i++)
                        if (grid_characters[i].item == null)
                        {
                            newCell = grid_characters[i];
                            break;
                        }
                    if (newCell != null)
                    {
                        StackableItem potion = cell.item as StackableItem;
                        StackableItem newPotion = potion.getNewItem();


                        newPotion.count = potion.count / 2;
                        potion.count -= newPotion.count;

                        cell.updateItem(potion);
                        newCell.updateItem(newPotion);
                        effect.Play();
                    }
                }
                else
                {
                    bool find = false;

                    for (int i = 0; i < 24; i++) //пытаемся вернуть владельцу
                    {
                        if (cell.item.character.inventory[i] == null)
                        {
                            find = true;
                            cell.item.character.inventory[i] = cell.item;
                            cell.item.character.onInventoryChanged();
                            break;
                        }
                    }

                    if (!find) //пытаемся вернуть хоть кому нибудь
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            if (listCustomers[i] == cell.item.character) continue; //скипаем владельца

                            for (int j = 0; j < 24; j++)
                                if (listCustomers[i].inventory[j] == null)
                                {
                                    find = true;
                                    listCustomers[i].inventory[j] = cell.item;
                                    listCustomers[i].onInventoryChanged();
                                    break;
                                }

                            if (find) break;
                        }

                        if (!find) //дропаем
                            dropItem(cell.item);
                    }

                    cell.updateItem(null);
                    effect.Play();
                }
                recalculateSellCost();
            }
        }

        private void buyGrid_mouseRightButtonDown(object sender, EventArgs e)
        {
            InventoryCell cell = sender as InventoryCell;

            if (cell.item != null)
            {
                if (cell.item.type == ItemType.Зелье && Keyboard.GetState().IsKeyDown(Keys.LeftShift) && cell.item.count > 1) //разделяем при Shift
                {
                    for (int i = 24; i < 36; i++)
                        if (grid_seller[i].item == null)
                        {
                            StackableItem potion = cell.item as StackableItem;
                            StackableItem newPotion = potion.getNewItem();

                            newPotion.count = potion.count / 2;
                            potion.count -= newPotion.count;

                            cell.updateItem(potion);
                            grid_seller[i].updateItem(newPotion);
                            effect.Play();
                            break;
                        }
                }
                else
                {
                    bool find = false;

                    for (int i = 0; i < 24; i++) //пытаемся вернуть владельцу
                    {
                        if (seller.inventory[i] == null)
                        {
                            find = true;
                            seller.inventory[i] = cell.item;
                            seller.onInventoryChanged();
                            break;
                        }
                    }

                    if (!find)
                    {
                        seller.extraInventory.Add(cell.item);
                        cell.item.character = seller;
                    }

                    cell.updateItem(null);
                    effect.Play();
                }
                recalculateBuyCost();
            }
        }

        private void dropItem(AbstractItem item)
        {
            Point position = seller.positionOnMap.ToPoint();
            position.Y += 100;
            position.X += GameFeature.rand.Next(200) - 100;

            item.Drop(seller.currentMap, position);
        }

        private void grid_character_mouseRightButtonDown(object sender, EventArgs e)
        {
            InventoryCell cell = sender as InventoryCell;

            if (cell.item != null)
            {
                if (cell.item.type == ItemType.Зелье && Keyboard.GetState().IsKeyDown(Keys.LeftShift) && cell.item.count > 1) //при зажатом Shift делим пополам
                {
                    StackableItem potion = cell.item as StackableItem;
                    StackableItem newPotion = potion.getNewItem();

                    newPotion.count = potion.count / 2;
                    potion.count -= newPotion.count;

                    effect.Play();

                    bool find = false;
                    AbstractCharacter character = listCustomers[cell.Index / 24];

                    for (int i = 0; i < 24; i++)
                          if (character.inventory[i]==null)
                        {
                            find = true;
                            character.inventory[i] = newPotion;
                            break;
                        }

                    if (!find)
                    {
                        character.Say("Недостаточно\nместа.", TypeCharacter.Герой);
                        dropItem(newPotion);
                    }

                    character.onInventoryChanged();
                }
                else //просто переносим
                {
                    for (int i = 72; i < 84; i++)
                        if (grid_characters[i].item == null)
                        {
                            effect.Play();

                            listCustomers[cell.Index / 24].inventory[cell.Index % 24] = null;
                            grid_characters[i].updateItem(cell.item);

                            listCustomers[cell.Index / 24].onInventoryChanged();

                            recalculateSellCost();
                            break;
                        }
                }
            }
        }

        private void grid_seller_mouseRightButtonDown(object sender, EventArgs e)
        {
            InventoryCell cell = sender as InventoryCell;

            if (cell.item != null)
            {
                if (cell.item.type == ItemType.Зелье && Keyboard.GetState().IsKeyDown(Keys.LeftShift) && cell.item.count > 1) //при зажатом Shift делим пополам
                {                  
                    for (int i = 0; i < 24; i++)
                        if (seller.inventory[i] == null)
                        {
                            StackableItem potion = cell.item as StackableItem;
                            StackableItem newPotion = potion.getNewItem();

                            newPotion.count = potion.count / 2;
                            potion.count -= newPotion.count;

                            seller.inventory[i] = newPotion;
                            seller.onInventoryChanged();

                            effect.Play();
                            break;
                        }
                }
                else //просто переносим
                {
                    for (int i = 24; i < 36; i++)
                        if (grid_seller[i].item == null)
                        {
                            effect.Play();

                            seller.inventory[cell.Index % 24] = null;
                            grid_seller[i].updateItem(cell.item);
                            cell.updateItem(null);

                            listCustomers[cell.Index / 24].onInventoryChanged();
                            recalculateBuyCost();
                            break;
                        }
                }
            }
        }

        private void ButtonCancel_mouseLeftButtonDown(object sender, EventArgs e)
        {
            Hide();
        }

        private void grid_seller_mouseLeftButtonDown(object sender, EventArgs e)
        {
            seller_cell = sender as InventoryCell;
            seller_cell.isDrag = true;
        }

        private void grid_character_mouseLeftButtonDown(object sender, EventArgs e)
        {
            character_cell = sender as InventoryCell;
            character_cell.isDrag = true;
        }

        private void drawRect_1(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(rectTxt, hero_1_rect, null, colorRect, 0, Vector2.Zero, SpriteEffects.None, zIndexGrid);
        }

        private void drawRect_2(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(rectTxt, hero_2_rect, null, colorRect, 0, Vector2.Zero, SpriteEffects.None, zIndexGrid);
        }

        private void drawRect_3(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(rectTxt, hero_3_rect, null, colorRect, 0, Vector2.Zero, SpriteEffects.None, zIndexGrid);
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(backTexture, drawField, null, backColor, 0, Vector2.Zero, SpriteEffects.None, zIndex);
            spriteBatch.Draw(gridTexture, gridRectangle, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, zIndexGrid);

            spriteBatch.Draw(head_1_txt, head_1, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, zIndexGrid);
            spriteBatch.Draw(head_2_txt, head_2, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, zIndexGrid);
            spriteBatch.Draw(head_3_txt, head_3, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, zIndexGrid);
            spriteBatch.Draw(head_4_txt, head_4, null, Color.White, 0, Vector2.Zero, SpriteEffects.FlipHorizontally, zIndexGrid);
        }

        private void update(GameTime gameTime)
        {
            previouseMouseState = mouseState;
            mouseState = Mouse.GetState();

            if (previouseMouseState.LeftButton == ButtonState.Pressed && mouseState.LeftButton == ButtonState.Released)
            {
                #region dragNdrop customers
                if (character_cell != null)
                {
                    InventoryCell newCell = null;
                    int maxIntersect = 0;

                    for (int i = 0; i < 84; i++)
                    {
                        if (character_cell.iconRect.Intersects(grid_characters[i].drawField) && grid_characters[i].isActive)
                        {
                            Point distance = new Point(character_cell.iconRect.X - grid_characters[i].drawField.X,
                                                       character_cell.iconRect.Y - grid_characters[i].drawField.Y);

                            Point intersectField = Point.Zero;

                            if (distance.X >= 0)
                                intersectField.X = grid_characters[i].drawField.X + grid_characters[i].drawField.Width - character_cell.iconRect.X;
                            else
                                intersectField.X = character_cell.iconRect.X + character_cell.iconRect.Width - grid_characters[i].drawField.X;

                            if (distance.Y >= 0)
                                intersectField.Y = grid_characters[i].drawField.Y + grid_characters[i].drawField.Height - character_cell.iconRect.Y;
                            else
                                intersectField.Y = character_cell.iconRect.Y + character_cell.iconRect.Height - grid_characters[i].drawField.Y;

                            if (intersectField.X * intersectField.Y > maxIntersect)
                            {
                                maxIntersect = intersectField.X * intersectField.Y;
                                newCell = grid_characters[i];
                            }
                        }
                    }

                    if (newCell != null && newCell != character_cell)
                    {
                        if (character_cell.item != null) effect.Play();

                        if (character_cell.item != null && newCell.item != null &&
                            character_cell.item.type == ItemType.Зелье && newCell.item.type == ItemType.Зелье &&
                            (character_cell.item as StackableItem).stackType == (newCell.item as StackableItem).stackType) //стакаем одинаковые зелья
                        {
                            if (newCell.item.count + character_cell.item.count > newCell.item.maxCount)
                            {
                                character_cell.item.count += newCell.item.count - newCell.item.maxCount;
                                newCell.item.count = newCell.item.maxCount;

                                newCell.updateItem(newCell.item);
                                character_cell.updateItem(character_cell.item);
                            }
                            else
                            {
                                newCell.item.count += character_cell.item.count;
                                newCell.updateItem(newCell.item);

                                if (character_cell.Index < 72)
                                {
                                    listCustomers[character_cell.Index / 24].inventory[character_cell.Index % 24] = null;
                                    listCustomers[character_cell.Index / 24].onInventoryChanged();
                                }
                                else character_cell.updateItem(null);

                            }
                            recalculateSellCost();
                        }
                        else if (character_cell.Index < 72 && newCell.Index < 72) //перенос предмета между персонажами
                        {
                            listCustomers[newCell.Index / 24].inventory[newCell.Index % 24] = character_cell.item;
                            if (character_cell.item != null) character_cell.item.character = listCustomers[newCell.Index / 24];

                            listCustomers[character_cell.Index / 24].inventory[character_cell.Index % 24] = newCell.item;
                            if (newCell.item != null) newCell.item.character = listCustomers[character_cell.Index / 24];

                            listCustomers[character_cell.Index / 24].onInventoryChanged();

                            if (character_cell.Index / 24 - newCell.Index / 24 != 0)
                                listCustomers[newCell.Index / 24].onInventoryChanged();
                        }
                        else if (character_cell.Index >= 72 && newCell.Index < 72) //перенос между прилавком и персонажем
                        {
                            listCustomers[newCell.Index / 24].inventory[newCell.Index % 24] = character_cell.item;
                            if (character_cell.item != null) character_cell.item.character = listCustomers[newCell.Index / 24];

                            character_cell.updateItem(newCell.item);

                            listCustomers[newCell.Index / 24].onInventoryChanged();

                            recalculateSellCost();
                        }
                        else if (character_cell.Index < 72 && newCell.Index >= 72) //перенос между персонажем и прилавком
                        {

                            listCustomers[character_cell.Index / 24].inventory[character_cell.Index % 24] = newCell.item;
                            if (newCell.item != null) newCell.item.character = listCustomers[character_cell.Index / 24];

                            newCell.updateItem(character_cell.item);

                            listCustomers[character_cell.Index / 24].onInventoryChanged();

                            recalculateSellCost();

                        }
                        else //перенос только на прилавке
                        {
                            AbstractItem tmpItem = character_cell.item;
                            character_cell.updateItem(newCell.item);
                            newCell.updateItem(tmpItem);
                        }
                    }

                    character_cell.isDrag = false;
                    character_cell = null;
                }
                #endregion

                #region dragNdrop seller
                if (seller_cell != null)
                {
                    InventoryCell newCell = null;
                    int maxIntersect = 0;

                    for (int i = 0; i < 36; i++)
                    {
                        if (seller_cell.iconRect.Intersects(grid_seller[i].drawField))
                        {
                            Point distance = new Point(seller_cell.iconRect.X - grid_seller[i].drawField.X,
                                                       seller_cell.iconRect.Y - grid_seller[i].drawField.Y);

                            Point intersectField = Point.Zero;

                            if (distance.X >= 0)
                                intersectField.X = grid_seller[i].drawField.X + grid_seller[i].drawField.Width - seller_cell.iconRect.X;
                            else
                                intersectField.X = seller_cell.iconRect.X + seller_cell.iconRect.Width - grid_seller[i].drawField.X;

                            if (distance.Y >= 0)
                                intersectField.Y = grid_seller[i].drawField.Y + grid_seller[i].drawField.Height - seller_cell.iconRect.Y;
                            else
                                intersectField.Y = seller_cell.iconRect.Y + seller_cell.iconRect.Height - grid_seller[i].drawField.Y;

                            if (intersectField.X * intersectField.Y > maxIntersect)
                            {
                                maxIntersect = intersectField.X * intersectField.Y;
                                newCell = grid_seller[i];
                            }
                        }
                    }
             
                    
                    if (newCell != null && seller_cell != newCell)
                    {
                        if (seller_cell.item != null) effect.Play();

                        if (seller_cell.item != null && newCell.item != null &&
                            seller_cell.item.type == ItemType.Зелье && newCell.item.type == ItemType.Зелье &&
                            (seller_cell.item as StackableItem).stackType == (newCell.item as StackableItem).stackType) //стакаем одинаковые зелья
                        {
                            if (newCell.item.count + seller_cell.item.count > newCell.item.maxCount)
                            {
                                seller_cell.item.count += newCell.item.count - newCell.item.maxCount;
                                newCell.item.count = newCell.item.maxCount;

                                newCell.updateItem(newCell.item);
                                seller_cell.updateItem(seller_cell.item);
                            }
                            else
                            {
                                newCell.item.count += seller_cell.item.count;
                                newCell.updateItem(newCell.item);

                                if (seller_cell.Index < 24)
                                {
                                    seller.inventory[seller_cell.Index % 24] = null;
                                    seller.onInventoryChanged();
                                }
                                else seller_cell.updateItem(null);

                            }
                            recalculateSellCost();
                        }
                        else if (seller_cell.Index < 24 && newCell.Index < 24) //перенос предмета у торговца
                        {
                            seller.inventory[newCell.Index % 24] = seller_cell.item;
                            if (seller_cell.item != null) seller_cell.item.character = seller;

                            seller.inventory[seller_cell.Index % 24] = newCell.item;
                            if (newCell.item != null) newCell.item.character = seller;

                            seller.onInventoryChanged();
                        }
                        else if (seller_cell.Index >= 24 && newCell.Index < 24) //перенос между прилавком и торговцем
                        {
                            seller.inventory[newCell.Index % 24] = seller_cell.item;
                            if (seller_cell.item != null) seller_cell.item.character = seller;

                            seller_cell.updateItem(newCell.item);

                            seller.onInventoryChanged();

                            recalculateBuyCost();
                        }
                        else if (seller_cell.Index < 24 && newCell.Index >= 24) //перенос между торговцем и прилавком
                        {
                                seller.inventory[seller_cell.Index % 24] = newCell.item;
                                if (newCell.item != null) newCell.item.character = seller;

                                newCell.updateItem(seller_cell.item);

                                seller.onInventoryChanged();

                                recalculateBuyCost();
                        }
                        else //перенос только на прилавке
                        {
                            AbstractItem tmpItem = seller_cell.item;
                            seller_cell.updateItem(newCell.item);
                            newCell.updateItem(tmpItem);
                        }
                    }

                    seller_cell.isDrag = false;
                    seller_cell = null;
                }
                #endregion

                if (sellerMoney.isDrag)
                {
                    if (sellerMoney.iconRect.Intersects(seller_rect))
                    {
                        effect.Play();
                        sellerMoney.updateItem(null);
                        recalculateBuyCost();
                    }
                    sellerMoney.isDrag = false;
                }

                if (charatcersMoney.isDrag)
                {                  
                    if (charatcersMoney.iconRect.Intersects(hero_1_rect) && maxRange >= Vector2.Distance(seller.positionOnMap, listCustomers[0].positionOnMap) ||
                        charatcersMoney.iconRect.Intersects(hero_2_rect) && maxRange >= Vector2.Distance(seller.positionOnMap, listCustomers[1].positionOnMap) ||
                        charatcersMoney.iconRect.Intersects(hero_3_rect) && maxRange >= Vector2.Distance(seller.positionOnMap, listCustomers[2].positionOnMap))
                    {
                        effect.Play();
                        GameFeature.Money += charatcersMoney.item.count;
                        charatcersMoney.updateItem(null);
                        recalculateSellCost();
                    }
                    charatcersMoney.isDrag = false;
                }
            }
        }

        private void updateSeller()
        {
            for (int i = 0; i < 24; i++)
            {
                if (seller.inventory[i] == null && seller.extraInventory.Count > 0)
                {
                    seller.inventory[i] = seller.extraInventory[0];
                    seller.extraInventory.RemoveAt(0);
                }
                grid_seller[i].updateItem(seller.inventory[i]);
            }
        }

        private void updateCustomers()
        {
            bool someoneInRange = false;

            for (int i = 0; i < 3; i++)
            {
                float distance = Vector2.Distance(seller.positionOnMap, listCustomers[i].positionOnMap);

                if (distance <= maxRange)
                {
                    someoneInRange = true;
                    for (int j = 0; j < 24; j++)
                    {
                        grid_characters[i * 24 + j].isActive = true;
                        grid_characters[i * 24 + j].updateItem(listCustomers[i].inventory[j]);
                    }
                }
                else
                {
                    for (int j = 0; j < 24; j++)
                    {
                        grid_characters[i * 24 + j].updateItem(null);
                        grid_characters[i * 24 + j].isActive = false;
                    }
                }
            }

            if (!someoneInRange) Hide();
        }

        private void recalculateSellCost()
        {
            sellCost = 0;
            for (int i = 72; i < 84; i++)
                if (grid_characters[i].item != null)
                    sellCost += grid_characters[i].item.count * grid_characters[i].item.Cost;

            if (charatcersMoney.item != null) sellCost += charatcersMoney.item.count;

            labelSellCount.Text = $"Итого: {sellCost}";

            if (sellCost < buyCost || (sellCost == 0 && buyCost == 0)) buttonBuy.isActive = false;
            else buttonBuy.isActive = true;
        }

        private void recalculateBuyCost()
        {
            buyCost = 0;

            for (int i = 24; i < 36; i++)
                if (grid_seller[i].item != null)
                    buyCost += grid_seller[i].item.count * grid_seller[i].item.Cost;

            buyCost = (int)(buyCost * (1 - Discount));

            if (sellerMoney.item != null) buyCost += sellerMoney.item.count;

            labelBuyCount.Text = $"Итого: {buyCost}";

            if (sellCost < buyCost || (sellCost == 0 && buyCost == 0)) buttonBuy.isActive = false;
            else buttonBuy.isActive = true;
        }

        public void setCustomers(List<AbstractCharacter> listCharacters)
        {
            listCustomers = listCharacters;

            head_1_txt = listCustomers[0].iconHead;
            head_2_txt = listCustomers[1].iconHead;
            head_3_txt = listCustomers[2].iconHead;
        }

        public void setSeller(AbstractNPC seller)
        {
            if (this.seller != null) this.seller.inventoryChanged -= Seller_inventoryChanged;

            this.seller = seller;

            seller.inventoryChanged += Seller_inventoryChanged;

            head_4_txt = seller.iconHead;
        }

        private void Seller_inventoryChanged(object sender, EventArgs e)
        {
            updateSeller();
        }

        private void Hero_positionChanged(object sender, EventArgs e)
        {
            updateCustomers();
        }

        private void Hero_1_inventoryChanged(object sender, EventArgs e)
        {
            float distance = Vector2.Distance(seller.positionOnMap, listCustomers[0].positionOnMap);

            if (distance <= maxRange)
                for (int i = 0; i < 24; i++)
                    grid_characters[i].updateItem(listCustomers[0].inventory[i]);
        }

        private void Hero_2_inventoryChanged(object sender, EventArgs e)
        {
            float distance = Vector2.Distance(seller.positionOnMap, listCustomers[1].positionOnMap);

            if (distance <= maxRange)
                for (int i = 0; i < 24; i++)
                    grid_characters[24 + i].updateItem(listCustomers[1].inventory[i]);
        }

        private void Hero_3_inventoryChanged(object sender, EventArgs e)
        {
            float distance = Vector2.Distance(seller.positionOnMap, listCustomers[2].positionOnMap);

            if (distance <= maxRange)
                for (int i = 0; i < 24; i++)
                    grid_characters[48 + i].updateItem(listCustomers[2].inventory[i]);
        }

        public void Show()
        {
            updateCustomers();
            updateSeller();
            isVisible = true;

            listCustomers[0].inventoryChanged += Hero_1_inventoryChanged;
            listCustomers[1].inventoryChanged += Hero_2_inventoryChanged;
            listCustomers[2].inventoryChanged += Hero_3_inventoryChanged;

            listCustomers[0].positionChanged += Hero_positionChanged;
            listCustomers[1].positionChanged += Hero_positionChanged;
            listCustomers[2].positionChanged += Hero_positionChanged;

            labelBuyCount.isVisible = true;
            labelBuyInfo.isVisible = true;

            labelSellCount.isVisible = true;
            labelSellInfo.isVisible = true;

            labelDiscount.isVisible = true;

            buttonBuy.isVisible = true;
            buttonCalculate.isVisible = true;
            buttonCancel.isVisible = true;

            game.updateInterface -= update;
            game.updateInterface += update;

            sellerMoney.isVisible = true;

            charatcersMoney.isVisible = true;

            for (int i = 0; i < 84; i++)
            {
                grid_characters[i].isVisible = true;

                if (i < 36)
                {
                    grid_seller[i].isVisible = true;
                }
            }
        }

        public void Hide()
        {
            isVisible = false;
            game.updateInterface -= update;

            listCustomers[0].inventoryChanged -= Hero_1_inventoryChanged;
            listCustomers[1].inventoryChanged -= Hero_2_inventoryChanged;
            listCustomers[2].inventoryChanged -= Hero_3_inventoryChanged;

            listCustomers[0].positionChanged -= Hero_positionChanged;
            listCustomers[1].positionChanged -= Hero_positionChanged;
            listCustomers[2].positionChanged -= Hero_positionChanged;

            labelBuyCount.isVisible = false;
            labelBuyInfo.isVisible = false;

            labelSellCount.isVisible = false;
            labelSellInfo.isVisible = false;

            labelDiscount.isVisible = false;

            buttonBuy.isVisible = false;
            buttonCalculate.isVisible = false;
            buttonCancel.isVisible = false;

            sellerMoney.updateItem(null);
            sellerMoney.isVisible = false;

            if (charatcersMoney.item != null)
            {
                GameFeature.Money += charatcersMoney.item.count;
                charatcersMoney.updateItem(null);
            }
            charatcersMoney.isVisible = false;

            int index = 0;

            for (int i = 0; i < 84; i++)
            {          
                if (i >= 72 && grid_characters[i].item != null) //если на прилавке остались товары
                {
                    dropItem(grid_characters[i].item);

                    grid_characters[i].updateItem(null);
                }

                grid_characters[i].isVisible = false;

                if (i < 36)
                {
                    if (i >= 24)
                    {
                        bool find = false;
                        if (grid_seller[i].item != null)
                        {
                            for (; index < 24; index++)
                                if (seller.inventory[index] == null)
                                {
                                    find = true;
                                    seller.inventory[index] = grid_seller[i].item;
                                    break;
                                }

                            if (!find)
                            {
                                seller.extraInventory.Add(grid_seller[i].item);
                            }
                            grid_seller[i].updateItem(null);
                        }
                    }
                              
                    grid_seller[i].isVisible = false;
                }
            }

            recalculateBuyCost();
            recalculateSellCost();
        }
    }
}
