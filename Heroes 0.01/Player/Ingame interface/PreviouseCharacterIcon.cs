﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Heroes_0._01
{
    public class PreviouseCharacterIcon : NextCharacterIcon
    {
        public PreviouseCharacterIcon(MyGame game, Point position, string Key) : base(game, position, Key)
        {
            arrowRect = new Rectangle(position.X - (int)(15 * Camera.scaleWnd), position.Y + (int)(45 * Camera.scaleWnd), (int)(25 * Camera.scaleWnd), (int)(50 * Camera.scaleWnd));
            spriteEffect = SpriteEffects.FlipVertically;
            angle = (float)Math.PI / 7;
        }
    }
}
