﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Heroes_0._01
{
    public class Story : InteractiveObject
    {
        public event EventHandler storyEnd;

        private Texture2D texture;
        private string currentText;

        private Vector2 positionText;

        private string[] story;

        private float zIndexTxt;
        private float fontScale = 0.3f * Camera.scaleWnd;
        private float skipScale = 0.25f * Camera.scaleWnd;

        private string skipStr = "Esc";
        private float angle = 0;
        private float angleStep = (float)Math.PI / 36;
        private Vector2 positionSkip;
        private Color color = Color.Yellow;

        private int time;
        private int maxTime = 40000;

        public int currentStory = 0;
        public int currentSymbol = 0;

        public Story(MyGame game) : base(game)
        {
            texture = game.Content.Load<Texture2D>("Characters/Shades/white");
            zIndex = 0.99f;
            zIndexTxt = zIndex + GameFeature.zIndexStep;

            isInterface = true;

            drawField.Size = Camera.sizeWnd.ToPoint();

            story = new string[]
            {
                "Королевство эльфов Анатель Лино\n13 год века Ворона\n\n         Древнее королевство эльфов Анатель Лино стоит на пороге краха.\nКрестьяне, рабы и вассалы восстали против своих господ. Соседние\nкоролевства так же оказывают давление на Анатель Лино, поддерживая\nвосставших и подсылая шпионов во дворец. Отчаявшийся король эльфов\nМератэль отправляет трех лучших защитников Анатель Лино на поиски\nдавно затерянного артефакта. Уже никто не помнит, как он выглядит,\nно о его мощи слагают легенды. Теперь трем героям придётся отправиться\nв опасный путь и найти артефакт, или королевство Анатель Лино будет\nуничтожено...",
                "Отважные герои Анатель Лино погибли в схватке с Грогом.\nНо победа лжеторговца длилась не долго. Амулет мертвых сломил\nволю и взял под контроль Флавия. И теперь ни одно королевство\nне выстоит перед натиском нежити. Все живое станет мертвым.\n\nНаступает новая эра - эра тьмы.",
                "Отважные герои Анатель Лино отдали оставшиеся части артефакта.\nНо победа лжеторговца длилась не долго. Амулет мертвых сломил\nволю и взял под контроль Флавия. И теперь ни одно королевство\nне выстоит перед натиском нежити. Все живое станет мертвым.\n\nНаступает новая эра - эра тьмы.",
                "Отважные герои Анатель Лино исполнили приказ своего короля.\nОни нашли артефакт. Король Мератэль подавил восстание и захватил\nсоседние королевства. Но Амулет мертвых сломил волю и взял под\nконтроль правителя Анатель Лино. И теперь ни одно королевство\nне выстоит перед натиском нежити. Все живое станет мертвым.\n\nНаступает новая эра - эра тьмы.",
                "Отважные герои нашли артефакт, но не исполнили приказ короля.\nПоняв всю силу и опасность Амулета мертвых, защитники Анатель\nЛино стали стражами артефакта, поделив между собой его части.\nКоролевство эльфов Анатель Лино пало, но весь остальной мир\nпродолжает жить."
            };

            Vector2 sizeTxt = GameFeature.Font.MeasureString(story[currentStory]) * fontScale;

            positionText.Y = (Camera.sizeWnd.Y - sizeTxt.Y) / 2;
            positionText.X = (Camera.sizeWnd.X - sizeTxt.X) / 2;

            sizeTxt = GameFeature.Font.MeasureString(skipStr) * fontScale;
            positionSkip = Camera.sizeWnd - sizeTxt - new Vector2(50, 50);
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, drawField, null, Color.Black, 0, Vector2.Zero, SpriteEffects.None, zIndex);
            spriteBatch.DrawString(GameFeature.Font, currentText, positionText, Color.White, 0, Vector2.Zero, fontScale, SpriteEffects.None, zIndexTxt);
            spriteBatch.DrawString(GameFeature.Font, skipStr, positionSkip, color, 0, Vector2.Zero, skipScale, SpriteEffects.None, zIndexTxt);
        }

        private void update(GameTime gameTime)
        {
            time += gameTime.ElapsedGameTime.Milliseconds;

            if (time >= maxTime)
            {
                Hide();
                storyEnd?.Invoke(this, null);
            }

            if (currentSymbol < story[currentStory].Length)
            {
                currentText += story[currentStory][currentSymbol];
                currentSymbol++;
            }

            angle += angleStep;
            color = Color.Yellow * (float)Math.Abs(Math.Cos(angle));
        }

        public void Show()
        {
            Vector2 sizeTxt = GameFeature.Font.MeasureString(story[currentStory]) * fontScale;

            positionText.Y = (Camera.sizeWnd.Y - sizeTxt.Y) / 2;
            positionText.X = (Camera.sizeWnd.X - sizeTxt.X) / 2;

            isVisible = true;
            time = 0;
            angle = 0;
            currentSymbol = 0;
            currentText = "";
            game.updateInterface -= update;
            game.updateInterface += update;
        }

        public void Hide()
        {
            isVisible = false;
            game.updateInterface -= update;
        }

        public void Skip()
        {
            Hide();
            storyEnd?.Invoke(this, null);
        }
    }
}
