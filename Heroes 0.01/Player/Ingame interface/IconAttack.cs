﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Heroes_0._01
{
    public class IconAttack : Icon
    {
        private IconBright effect;
        private AbstractCharacter character;

        public IconAttack(MyGame game, Point position, string Key) : base(game, position, Key)
        {
            width = (int)(70 * Camera.scaleWnd);
            height = (int)(70 * Camera.scaleWnd);
            shiftKey = (int)(10 * Camera.scaleWnd);
            loadIcon("Interface/woodSword", "Interface/woodSword_select");
            zIndex = 0.9f + 2 * GameFeature.zIndexStep;
            effect = new IconBright(game, zIndex - GameFeature.zIndexStep, new Point(position.X - (int)(65 * Camera.scaleWnd), position.Y - (int)(60 * Camera.scaleWnd)), new Point((int)(200 * Camera.scaleWnd), (int)(200 * Camera.scaleWnd)));

            mouseRightButtonDown += IconAttack_mouseRightButtonDown;
        }

        private void IconAttack_mouseRightButtonDown(object sender, EventArgs e)
        {
            if (character == null) return;

            character.isAutoattack = !character.isAutoattack;
            if (character.isAutoattack) effect.Play();
            else effect.Stop();
        }

        public void update(AbstractCharacter character)
        {
            this.character = character;

            if (isVisible && character.isAutoattack) effect.Play();
            else effect.Stop();
        }

        public void Show()
        {
            isVisible = true;
            if (character != null && character.isAutoattack) effect.Play();
            else effect.Stop();
        }

        public void Hide()
        {
            isVisible = false;
            effect.Stop();
        }
    }
}
