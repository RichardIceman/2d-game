﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class TargetInfo : InteractiveObject
    {
        private string info;

        private Vector2 positionInfo;
        protected StatusBar healthBar;

        protected Rectangle iconRect;
        private Rectangle textureRect;

        private Texture2D texture;
        private Texture2D backTexture;
        protected Texture2D iconTexture;

        private float zIndexFont;
        private float zIndexIcon;
        private Color fontColor = Color.Yellow;
        private float fontScale = 0.17f * Camera.scaleWnd;

        public AbstractCharacter character;

        public TargetInfo(MyGame game, Point position) : base (game)
        {
            this.game = game;

            isInterface = true;

            zIndex = 0.9f;
            zIndexFont = 0.9f + GameFeature.zIndexStep;
            zIndexIcon = 0.9f + 3 * GameFeature.zIndexStep;

            backTexture = game.Content.Load<Texture2D>("Characters/Shades/gradient");
            texture = game.Content.Load<Texture2D>("Interface/part06");

            drawField = new Rectangle(0, 0, (int)(320 * Camera.scaleWnd), (int)(60 * Camera.scaleWnd));
            iconRect = new Rectangle(0, 0, (int)(80 * Camera.scaleWnd), (int)(80 * Camera.scaleWnd));
            textureRect = new Rectangle(0, 0, (int)(100 * Camera.scaleWnd), (int)(100 * Camera.scaleWnd));

            drawField.Location = position;

            iconRect.X = drawField.X - iconRect.Width / 2;
            iconRect.Y = drawField.Y - (iconRect.Height - drawField.Height) / 2;

            textureRect.X = drawField.X - textureRect.Width / 2;
            textureRect.Y = drawField.Y - (textureRect.Height - drawField.Height) / 2; ;

            positionInfo.Y = drawField.Y;

            healthBar = new StatusBar(game,
                                     new Vector2(drawField.X + (drawField.Width - 270 * Camera.scaleWnd) / 2 + 10 * Camera.scaleWnd,
                                                 drawField.Y + drawField.Height - 28 * Camera.scaleWnd),
                                     (int)(250 * Camera.scaleWnd),
                                     (int)(20 * Camera.scaleWnd),
                                     "ОЗ",
                                     0.12f * Camera.scaleWnd,
                                     Color.OrangeRed,
                                     Color.White,
                                     zIndexFont);
            healthBar.Hide();
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(backTexture, drawField, null, Color.Black * 0.5f, 0, Vector2.Zero, SpriteEffects.None, zIndex);
            spriteBatch.Draw(texture, textureRect, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, zIndexFont);
            spriteBatch.Draw(iconTexture, iconRect, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, zIndexIcon);
            spriteBatch.DrawString(GameFeature.Font, info, positionInfo, Color.Yellow, 0, Vector2.Zero, fontScale, SpriteEffects.None, zIndexFont);
        }

        public virtual void UpdateTarget(AbstractCharacter character)
        {
            Hide();
            if (this.character != null) this.character.healthBar.valueChange -= HealthBar_valueChange;

            this.character = character;

            if (character == null) return;

            info = $"{character.name}";

            Vector2 sizeTxt = GameFeature.Font.MeasureString(info) * fontScale;

            positionInfo.X = drawField.X + (drawField.Width - sizeTxt.X) / 2 - 10 * Camera.scaleWnd;

            iconTexture = character.iconHead;       

            healthBar.Update(character.MaxHealthPoint, character.healthBar.Value, 0);

            character.healthBar.valueChange += HealthBar_valueChange;
            Show();
        }

        protected virtual void HealthBar_valueChange(object sender, EventArgs e)
        {
            healthBar.Update(character.MaxHealthPoint, character.healthBar.Value, 0);
        }

        public virtual void Show()
        {
            isVisible = true;
            healthBar.Show();
        }

        public virtual void Hide()
        {
            healthBar.Hide();
            isVisible = false;
        }
    }

    
}
