﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Heroes_0._01
{
    public class IconPause : Icon
    {
        private Texture2D pause;
        private Texture2D pauseSelected;
        private Texture2D play;
        private Texture2D playSelected;

        private Rectangle pauseRect;

        private float zIndexPause = 0.5f + GameFeature.zIndexStep;

        private PauseEffect effect;

        public IconPause(MyGame game, Point position, string Key) : base(game, position, Key)
        {
            width = (int)(90 * Camera.scaleWnd);
            height = (int)(90 * Camera.scaleWnd);
            shiftKey = (int)(9 * Camera.scaleWnd);
            zIndex = 0.9f + 2 * GameFeature.zIndexStep;
            loadIcon("Interface/pause", "Interface/pause_selected");

            pause = game.Content.Load<Texture2D>("Interface/pause");
            pauseSelected = game.Content.Load<Texture2D>("Interface/pause_selected");

            play = game.Content.Load<Texture2D>("Interface/play");
            playSelected = game.Content.Load<Texture2D>("Interface/play_select");

            pauseRect = new Rectangle((int)(Camera.sizeWnd.X - 150 * Camera.scaleWnd) / 2,
                                      (int)(Camera.sizeWnd.Y - 150 * Camera.scaleWnd) / 2,
                                      (int)(150 * Camera.scaleWnd),
                                      (int)(150 * Camera.scaleWnd));

            effect = new PauseEffect(game);
        }

        public void Update()
        {
            if (GameFeature.timeStop)
            {
                defaultTexture = play;
                selectTexture = playSelected;
                effect.Play();

                game.drawInterface -= drawPause;
                game.drawInterface += drawPause;
            }
            else
            {
                defaultTexture = pause;
                selectTexture = pauseSelected;
                effect.Stop();
                game.drawInterface -= drawPause;
            }

            texture = defaultTexture;
        }

        private void drawPause(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(pause, pauseRect, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, zIndexPause);
        } 

        public void Show()
        {
            isVisible = true;

            if (GameFeature.timeStop)
            {
                game.drawInterface -= drawPause;
                game.drawInterface += drawPause;
            }
        }

        public void Hide()
        {
            isVisible = false;
            game.drawInterface -= drawPause;
        }
    }
}
