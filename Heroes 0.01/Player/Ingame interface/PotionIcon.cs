﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Heroes_0._01
{
    public class PotionIcon : Icon
    {
        private Description description;
        private Rectangle iconField;
        private Texture2D icon;

        public StackableItem potion = null;
        private float zIndexIcon;

        public PotionIcon(MyGame game, Point position, string Key) : base(game, position, Key)
        {
            width = (int)(30 * Camera.scaleWnd);
            height = (int)(30 * Camera.scaleWnd);
            shiftKey = (int)(2 * Camera.scaleWnd);
            loadIcon("Interface/iconback", "Interface/iconbackselect");
            zIndex = 0.9f + 2 * GameFeature.zIndexStep;
            icon = game.Content.Load<Texture2D>("Interface/iconback");
            description = new Description(game, true);
            zIndexIcon = zIndex + GameFeature.zIndexStep;
            iconField.Width = drawField.Width - (int)(2 * Camera.scaleWnd);
            iconField.Height = drawField.Height - (int)(2 * Camera.scaleWnd);
            iconField.X = drawField.X + (int)(Camera.scaleWnd);
            iconField.Y = drawField.Y + (int)(Camera.scaleWnd);

            fontLetterScale = 0.09f * Camera.scaleWnd;

            mouseLeftButtonDown += Use;
        }

        protected override void Icon_mouseEnter(object sender, EventArgs e)
        {
            if (potion != null)
            {
                description.updateDescription(potion.name, potion.description, 0.17f, drawField, zIndexIcon);
                description.isVisible = true;
            }
            texture = selectTexture;
        }

        protected override void Icon_mouseLeave(object sender, EventArgs e)
        {
            description.isVisible = false;
            texture = defaultTexture;
        }

        private void drawIcon(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(icon, iconField, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, zIndexIcon);
        }

        public void updatePotion(StackableItem potion)
        {
            game.drawInterface -= drawIcon;
            this.potion = potion;

            if (potion != null)
            {
                icon = potion.icon;
                if (isVisible) game.drawInterface += drawIcon;
                description.updateDescription(potion.name, potion.description, 0.17f, drawField, zIndexIcon);
            }         
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, drawField, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, zIndex);
            if (potion != null) spriteBatch.DrawString(GameFeature.Font, Key, positionLetter, Color.White, 0, Vector2.Zero, fontLetterScale, SpriteEffects.None, zIndex);
        }

        public void Use(object o, EventArgs e)
        {
            potion?.Use();
        }

        public void Show()
        {
            isVisible = true;

            if (potion != null)
            {
                icon = potion.icon;
                game.drawInterface += drawIcon;
            }
        }

        public void Hide()
        {
            isVisible = false;
            game.drawInterface -= drawIcon;
        }
    }
}
