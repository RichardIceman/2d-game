﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class GoldWidget
    {
        private MyGame game;

        private string info;
        private Vector2 positionInfo;

        private Color color = Color.White;
        private float zIndex;
        private float fontSize = 0.25f * Camera.scaleWnd;

        private Point standartSize;
        private Point standartPosition;

        private float radius = 40;

        private float angle;
        private float angleStep = (float)(Math.PI / 18);

        private int currentMoney = 0;
        private int step = 1;

        private Texture2D texture;
        private Rectangle rect;

        private UpgradeEffect effect;

        public GoldWidget(MyGame _game)
        {
            game = _game;

            texture = game.Content.Load<Texture2D>("Items/мешочек-с-золотом-png-2");

            zIndex = 0.93f;

            rect = new Rectangle((int)(Camera.sizeWnd.X - 200 * Camera.scaleWnd),
                                 (int)(50 * Camera.scaleWnd),
                                 (int)(50 * Camera.scaleWnd),
                                 (int)(50 * Camera.scaleWnd));

            standartSize = rect.Size;
            standartPosition = rect.Location;

            Vector2 sizeTxt = GameFeature.Font.MeasureString("0") * fontSize;

            positionInfo = new Vector2();

            positionInfo.X = rect.X + rect.Width;
            positionInfo.Y = rect.Y + (rect.Height - sizeTxt.Y) / 2;

            Point position = new Point(rect.X - (int)(75 * Camera.scaleWnd), rect.Y - (int)(75 * Camera.scaleWnd));

            info = $"{GameFeature.Money}";
            GameFeature.moneyChange += moneyChange;

            effect = new UpgradeEffect(game, zIndex - GameFeature.zIndexStep * 2, position);
        }

        private void moneyChange(object sender, EventArgs e)
        {
            int dif = GameFeature.Money - currentMoney;

            if (dif != 0)
            {
                step = Math.Abs(dif / 83);

                if (step < 3) step = Math.Abs(dif / 7);

                if (step == 0) step = 1;

                if (dif < 0) step = -step;
                else effect.Play();

                angle = (float)Math.PI * 3 / 2;

                game.updateInterface -= update;
                game.updateInterface += update;
            }
        }

        private void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, rect, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, zIndex);
            spriteBatch.DrawString(GameFeature.Font, info, positionInfo, color, 0, Vector2.Zero, fontSize, SpriteEffects.None, zIndex);
        }

        private void update(GameTime gameTime)
        {
            if (currentMoney != GameFeature.Money)
            {
                if (currentMoney + step >= GameFeature.Money && step > 0 ||
                    currentMoney + step <= GameFeature.Money && step < 0)
                {
                    currentMoney = GameFeature.Money;
                    info = $"{currentMoney}";
                }
                else
                {
                    currentMoney += step;
                    info = $"{currentMoney}";
                }
            }

            if (angle != (float)Math.PI / 2)
            {
                angle -= angleStep;

                Point newSize = new Point(standartSize.X - (int)(radius * Math.Cos(angle)), standartSize.Y - (int)(radius * Math.Cos(angle)));

                rect.X -= (newSize.X - rect.Width) / 2;
                rect.Y -= (newSize.Y - rect.Height) / 2;

                rect.Size = newSize;

                if (angle < Math.PI / 2)
                {
                    rect.Size = standartSize;
                    rect.Location = standartPosition;
                    angle = (float)Math.PI / 2;
                }
            }
            
            if (angle == (float)Math.PI / 2 && currentMoney == GameFeature.Money)
             game.updateInterface -= update;
        }

        public void Show()
        {
            game.drawInterface -= draw;
            game.drawInterface += draw;
        }

        public void Hide()
        {
            game.drawInterface -= draw;
            effect.Stop();
        }
    }
}
