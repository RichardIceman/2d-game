﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class PlayerInterface : InteractiveObject
    {
        private Texture2D texture;
        private AbstractCharacter character;

        public IconAttack iconAttack;

        public SkillIcon[] skillIcons;

        public PotionIcon[] potionIcons;

        private StatusBar HP;
        private StatusBar MP;
        private StatusBar XP;
        private Label lvl;

        private float fontScale = 0.12f;
        private float zIndexIcon;

        private CharacterHead characterHead;
        public NextCharacterIcon nextCharacter;
        public PreviouseCharacterIcon previouseCharacter;

        private IconInventory inventoryIcon;
        public Inventory inventory;

        public Shop shop;

        public IconPause iconPause;

        public TargetInfo targetInfo;

        private GoldWidget goldWidget;

        public HeroInfo hero_1;
        public HeroInfo hero_2;
        public HeroInfo hero_3;

        public bool nextIsVisible = false;

        public PlayerInterface(MyGame game) : base(game)
        {
            this.game = game;
            isInterface = true;
            fontScale *= Camera.scaleWnd;
            drawField.Width = (int)(800 * Camera.scaleWnd);
            drawField.Height = (int)(230 * Camera.scaleWnd);
            zIndex = 0.89f;
            zIndexIcon = zIndex + GameFeature.zIndexStep;
            drawField.Y = game.graphics.PreferredBackBufferHeight - (int)(250 * Camera.scaleWnd);
            drawField.X = (int)(Camera.sizeWnd.X - drawField.Width) / 2;
            texture = game.Content.Load<Texture2D>("Interface/menu1");

            iconAttack = new IconAttack(game, new Point(drawField.X + (int)(125 * Camera.scaleWnd), drawField.Y + (int)(55 * Camera.scaleWnd)), "A");

            skillIcons = new SkillIcon[GameFeature.countSkills];
            skillIcons[0] = new SkillIcon(game, new Point(drawField.X + (int)(475 * Camera.scaleWnd), drawField.Y + (int)(62 * Camera.scaleWnd)), "Q");
            skillIcons[1] = new SkillIcon(game, new Point(drawField.X + (int)(545 * Camera.scaleWnd), drawField.Y + (int)(62 * Camera.scaleWnd)), "W");
            skillIcons[2] = new SkillIcon(game, new Point(drawField.X + (int)(615 * Camera.scaleWnd), drawField.Y + (int)(62 * Camera.scaleWnd)), "E");

            potionIcons = new PotionIcon[GameFeature.countPotions];
            potionIcons[0] = new PotionIcon(game, new Point(drawField.X + (int)(290 * Camera.scaleWnd), drawField.Y + (int)(39 * Camera.scaleWnd)), "1");
            potionIcons[1] = new PotionIcon(game, new Point(drawField.X + (int)(335 * Camera.scaleWnd), drawField.Y + (int)(39 * Camera.scaleWnd)), "2");
            potionIcons[2] = new PotionIcon(game, new Point(drawField.X + (int)(380 * Camera.scaleWnd), drawField.Y + (int)(39 * Camera.scaleWnd)), "3");

            characterHead = new CharacterHead(game, new Point(drawField.X + (int)(25 * Camera.scaleWnd), drawField.Y + (int)(55 * Camera.scaleWnd)), "S");
            characterHead.mouseLeftButtonDown += Tome_mouseLeftButtonDown;

            nextCharacter = new NextCharacterIcon(game, new Point(drawField.X + (int)(43 * Camera.scaleWnd), drawField.Y + (int)(150 * Camera.scaleWnd)), "Tab");
            previouseCharacter = new PreviouseCharacterIcon(game, new Point(drawField.X + (int)(43 * Camera.scaleWnd), drawField.Y - (int)(5 * Camera.scaleWnd)), "Shift + Tab");

            inventoryIcon = new IconInventory(game, new Point(drawField.X + (int)(670 * Camera.scaleWnd), drawField.Y + (int)(45 * Camera.scaleWnd)), "I");
            inventoryIcon.mouseLeftButtonDown += InventoryIcon_mouseLeftButtonDown;
            inventory = new Inventory(game, potionIcons);

            shop = new Shop(game);

            goldWidget = new GoldWidget(game);

            iconPause = new IconPause(game, new Point((int)(Camera.sizeWnd.X * (1 - Camera.indent) - 90 * Camera.scaleWnd), (int)(Camera.sizeWnd.Y - 150 * Camera.scaleWnd)), "Space");

            targetInfo = new TargetInfo(game, new Point((int)(Camera.sizeWnd.X - (int)(300 * Camera.scaleWnd) + (int)(80 * Camera.scaleWnd) / 2) / 2, (int)(50 * Camera.scaleWnd)));

            hero_1 = new HeroInfo(game, new Point((int)(130 * Camera.scaleWnd) / 2, (int)(400 * Camera.scaleWnd)), "F1");
            hero_2 = new HeroInfo(game, new Point((int)(130 * Camera.scaleWnd) / 2, (int)(500 * Camera.scaleWnd)), "F2");
            hero_3 = new HeroInfo(game, new Point((int)(130 * Camera.scaleWnd) / 2, (int)(600 * Camera.scaleWnd)), "F3");

            HP = new StatusBar(game, new Vector2(drawField.X + (int)(200 * Camera.scaleWnd), drawField.Y + (int)(81 * Camera.scaleWnd)), (int)(265 * Camera.scaleWnd), (int)(19 * Camera.scaleWnd), "ОЗ", fontScale, Color.LimeGreen, Color.White, zIndexIcon);
            MP = new StatusBar(game, new Vector2(drawField.X + (int)(200 * Camera.scaleWnd), drawField.Y + (int)(103 * Camera.scaleWnd)), (int)(265 * Camera.scaleWnd), (int)(19 * Camera.scaleWnd), "ОМ", fontScale, Color.Blue, Color.White, zIndexIcon);
            XP = new StatusBar(game, new Vector2(drawField.X + (int)(110 * Camera.scaleWnd), drawField.Y + (int)(190 * Camera.scaleWnd)), (int)(570 * Camera.scaleWnd), (int)(20 * Camera.scaleWnd), "Опыт", fontScale, Color.Yellow, Color.Black, zIndexIcon);

            string text = "Уровень 1";
            Vector2 size = GameFeature.Font.MeasureString(text) * fontScale;
            Vector2 position = new Vector2(XP.X + (XP.Width - size.X) / 2, XP.Y - size.Y - 5 * Camera.scaleWnd);
            lvl = new Label(game, text, position, 0.15f * Camera.scaleWnd, Color.White, zIndexIcon);
        }

        public void InventoryIcon_mouseLeftButtonDown(object sender, EventArgs e)
        {
            if (!isVisible || shop.isVisible) return;
            if (inventory.isVisible) inventory.Hide();
            else inventory.Show();
        }

        public void Tome_mouseLeftButtonDown(object sender, EventArgs e)
        {
            characterHead.isOpen = !characterHead.isOpen;

            if (isVisible && characterHead.isOpen)
            {
                for (int i = 0; i < GameFeature.countSkills; i++)
                    skillIcons[i].ShowUpgrade();
            }
            else
            {
                for (int i = 0; i < GameFeature.countSkills; i++)
                    skillIcons[i].HideUpgrade();
            }
        }

        public void updateSkillIcons(AbstractCharacter character)
        {
            if (this.character != null)
            {
                this.character.healthBar.valueChange -= updateHP;
                this.character.manaBar.valueChange -= updateMP;
                this.character.expBar.valueChange -= updateXP;
                this.character.skillPointChange -= skillPointChange;
            }

            this.character = character;
            for (int i = 0; i < GameFeature.countSkills; i++)
                skillIcons[i].updateSkill(character.skills[i]);

            characterHead.updateCharacter(character);
            inventory.updateItems(character);          

            iconAttack.update(character);

            updateHP(null, null);
            updateMP(null, null);
            updateXP(null, null);
            skillPointChange(null, null);

            character.healthBar.valueChange += updateHP;
            character.manaBar.valueChange += updateMP;
            character.expBar.valueChange += updateXP;
            character.skillPointChange += skillPointChange;
        }

        private void skillPointChange(object sender, EventArgs e)
        {
            if (character.SkillPoint > 0 && isVisible)
            {
                for (int i = 0; i < GameFeature.countSkills; i++)
                    skillIcons[i].ShowUpgrade();

                characterHead.isOpen = true;
            }
            else
            {
                for (int i = 0; i < GameFeature.countSkills; i++)
                    skillIcons[i].HideUpgrade();

                characterHead.isOpen = false;
            }
        }

        private void updateHP(object o, EventArgs e)
        {
            HP.Update(character.healthBar.MaxValue, character.healthBar.Value, character.healthRegen);
        }

        private void updateMP(object o, EventArgs e)
        {
            MP.Update(character.manaBar.MaxValue, character.manaBar.Value, character.manaRegen);
        }

        private void updateXP(object o, EventArgs e)
        {
            XP.Update(character.expBar.MaxValue, character.expBar.Value, -1);
            string text = "Уровень " + character.Lvl.ToString();
            Vector2 size = GameFeature.Font.MeasureString(text) * fontScale;
            Vector2 position = new Vector2(XP.X + (XP.Width - size.X) / 2, XP.Y - size.Y - 5 * Camera.scaleWnd);
            lvl.Text = text;
            lvl.Position = position;
        }

        public void Show()
        {
            if (!isVisible)
            {
                isVisible = true;
                iconAttack.Show();
                characterHead.Show();
                if (nextIsVisible)
                {
                    nextCharacter.isVisible = true;
                    previouseCharacter.isVisible = true;
                }
                inventoryIcon.isVisible = true;
                iconPause.Show();
                hero_1.Show();
                hero_2.Show();
                hero_3.Show();

                goldWidget.Show();

                HP.Show();
                MP.Show();
                XP.Show();

                if (characterHead.isOpen)
                {
                    for (int i = 0; i < GameFeature.countSkills; i++)
                        skillIcons[i].ShowUpgrade();
                }

                lvl.isVisible = true;

                for (int i = 0; i < GameFeature.countSkills; i++)
                    skillIcons[i].Show();


                for (int i = 0; i < GameFeature.countPotions; i++)
                    potionIcons[i].Show();
            }
        }

        public void ShowBottomBar()
        {
            isVisible = true;
            iconAttack.Show();
            characterHead.Show();
            nextCharacter.isVisible = true;
            previouseCharacter.isVisible = true;
            inventoryIcon.isVisible = true;
            iconPause.Show();

            HP.Show();
            MP.Show();
            XP.Show();

            lvl.isVisible = true;

            for (int i = 0; i < GameFeature.countSkills; i++)
                skillIcons[i].Show();

            for (int i = 0; i < GameFeature.countPotions; i++)
                potionIcons[i].Show();
        }

        public void HideBottomBar()
        {
            isVisible = false;
            iconAttack.Hide();
            characterHead.Hide();
            nextCharacter.isVisible = false;
            previouseCharacter.isVisible = false;
            inventoryIcon.isVisible = false;
            iconPause.Hide();

            HP.Hide();
            MP.Hide();
            XP.Hide();

            for (int i = 0; i < GameFeature.countSkills; i++)
                skillIcons[i].HideUpgrade();

            lvl.isVisible = false;

            for (int i = 0; i < GameFeature.countSkills; i++)
                skillIcons[i].Hide();

            for (int i = 0; i < GameFeature.countPotions; i++)
                potionIcons[i].Hide();
        }

        public void Hide()
        {
            if (isVisible)
            {
                nextIsVisible = nextCharacter.isVisible;

                isVisible = false;
                iconAttack.Hide();
                characterHead.Hide();
                nextCharacter.isVisible = false;
                previouseCharacter.isVisible = false;
                inventoryIcon.isVisible = false;
                iconPause.Hide();
                hero_1.Hide();
                hero_2.Hide();
                hero_3.Hide();

                shop.Hide();

                if (inventory.isVisible) inventory.Hide();

                goldWidget.Hide();

                HP.Hide();
                MP.Hide();
                XP.Hide();

                for (int i = 0; i < GameFeature.countSkills; i++)
                    skillIcons[i].HideUpgrade();

                lvl.isVisible = false;

                for (int i = 0; i < GameFeature.countSkills; i++)
                    skillIcons[i].Hide();

                for (int i = 0; i < GameFeature.countPotions; i++)
                    potionIcons[i].Hide();
            }
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, drawField, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, zIndex);
        }
    }
}
