﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Heroes_0._01
{
    class MenuPause : InteractiveObject
    {
        private Texture2D background;
        private string text = "ПАУЗА";
        private Vector2 positionTxt;
        private Color fontColor = Color.Yellow;
        private float scaleTxt = 0.6f * Camera.scaleWnd;
        private float zIndexFont;

        public Button mainMenu;
        private Button resume;
        public Button restart;

        public MenuPause(MyGame game) : base(game)
        {
            isInterface = true;
            drawField.Size = Camera.sizeWnd.ToPoint();
            background = game.Content.Load<Texture2D>("Characters/Shades/select");

            Vector2 sizeTxt = GameFeature.Font.MeasureString(text) * scaleTxt;

            positionTxt.X = drawField.Location.X + (drawField.Size.X - sizeTxt.X) / 2;
            positionTxt.Y = Camera.sizeWnd.Y / 10;

            zIndex = 0.94f;

            Vector2 positionButton;
            sizeTxt = GameFeature.Font.MeasureString("Продолжить") * scaleTxt;
            positionButton.X = drawField.Location.X + (drawField.Size.X - sizeTxt.X) / 2;
            positionButton.Y = Camera.sizeWnd.Y / 2 - Camera.sizeWnd.Y / 8;
            resume = new Button(game, positionButton.ToPoint(), "Продолжить", scaleTxt, zIndex + GameFeature.zIndexStep);

            resume.mouseLeftButtonDown += new EventHandler((o, e) =>
            {
                Hide();
                GameFeature.pause = false;
            });

            sizeTxt = GameFeature.Font.MeasureString("Начать заново") * scaleTxt;
            positionButton.X = drawField.Location.X + (drawField.Size.X - sizeTxt.X) / 2;
            positionButton.Y = Camera.sizeWnd.Y / 2;
            restart = new Button(game, positionButton.ToPoint(), "Начать заново", scaleTxt, zIndex + GameFeature.zIndexStep);


            sizeTxt = GameFeature.Font.MeasureString("Главное меню") * scaleTxt;
            positionButton.X = drawField.Location.X + (drawField.Size.X - sizeTxt.X) / 2;
            positionButton.Y = Camera.sizeWnd.Y / 2 + Camera.sizeWnd.Y / 8;
            mainMenu = new Button(game, positionButton.ToPoint(), "Главное меню", scaleTxt, zIndex + GameFeature.zIndexStep);


            zIndexFont = zIndex + GameFeature.zIndexStep;
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background, drawField, null, Color.Black * 0.5f, 0, Vector2.Zero, SpriteEffects.None, zIndex);
            spriteBatch.DrawString(GameFeature.Font, text, positionTxt, fontColor, 0, Vector2.Zero, scaleTxt, SpriteEffects.None, zIndexFont);
        }

        public void Show()
        {
            if (!isVisible)
            {
                GameFeature.pause = true;
                isVisible = true;
                mainMenu.isVisible = true;
                resume.isVisible = true; 
                restart.isVisible = true;
            }
        }

        public void Hide()
        {
            if (isVisible)
            {
                GameFeature.pause = false;
                isVisible = false;
                mainMenu.isVisible = false;
                resume.isVisible = false;
                restart.isVisible = false;
            }
        }
    }
}
