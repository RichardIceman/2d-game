﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Lose : InteractiveObject
    {
        private Texture2D background;
        private string text;
        private Vector2 positionTxt;
        private Color fontColor = Color.Yellow;
        private float scoreScale = 0.5f * Camera.scaleWnd;
        private float restartScale = 0.6f * Camera.scaleWnd;
        private float fontScale = 0.8f * Camera.scaleWnd;
        private float zIndexFont;
        private Vector2 shiftTxt;
        private Label scoreInfo;
        public Button restart;

        public Lose(MyGame game) : base(game)
        {
            isInterface = true;

            background = game.Content.Load<Texture2D>("Characters/Shades/select");

            drawField.Size = Camera.sizeWnd.ToPoint();
            shiftTxt = new Vector2(0, Camera.sizeWnd.Y / 6);

            zIndex = 0.94f;
            zIndexFont = zIndex + GameFeature.zIndexStep;

            text = "Осталось попыток: 0";
            Vector2 sizeTxt = GameFeature.Font.MeasureString(text) * scoreScale;
            positionTxt = drawField.Location.ToVector2() + (drawField.Size.ToVector2() - sizeTxt - shiftTxt) / 2;
            scoreInfo = new Label(game, text, positionTxt, scoreScale, Color.White, zIndexFont);
            scoreInfo.isVisible = false;

            text = "Продолжить";
            sizeTxt = GameFeature.Font.MeasureString(text) * restartScale;
            positionTxt = drawField.Location.ToVector2() + (drawField.Size.ToVector2() - sizeTxt + shiftTxt) / 2;
            restart = new Button(game, positionTxt.ToPoint(), text, restartScale, zIndexFont);

            text = "Все герои погибли";
            sizeTxt = GameFeature.Font.MeasureString(text) * fontScale;
            positionTxt = drawField.Location.ToVector2() + (drawField.Size.ToVector2() - sizeTxt - 2 * shiftTxt) / 2;
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background, drawField, null, Color.Black, 0, Vector2.Zero, SpriteEffects.None, zIndex);
            spriteBatch.DrawString(GameFeature.Font, text, positionTxt, fontColor, 0, Vector2.Zero, fontScale, SpriteEffects.None, zIndexFont);
        }

        public void updateInfo()
        {
            string info = $"Осталось попыток: {GameFeature.countTry}";

            Vector2 newPosition = new Vector2((Camera.sizeWnd.X - GameFeature.Font.MeasureString(info).X * scoreScale) / 2, scoreInfo.Position.Y);

            scoreInfo.Text = info;
            scoreInfo.Position = newPosition;
        }

        public void Show()
        {
            updateInfo();
            isVisible = true;
            scoreInfo.isVisible = true;
            restart.isVisible = true;
        }

        public void Hide()
        {
            isVisible = false;
            scoreInfo.isVisible = false;
            restart.isVisible = false;
        }
    }
}
