﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using System.IO;

namespace Heroes_0._01
{
    public class MainMenu : InteractiveObject
    {
        private Song song;
        private Texture2D background;
        public Button play;
        private Button settings;
        private Button scoreTable;
        private Button exit;
        private Button settingsBack;
        private Button scoreTableBack;
        private Slider volumeSongs;
        private Label labelVolumeSongs;
        private Slider volumeEffects;
        private Label labelVolumeEffects;
        private Label labelScoreTable;
        private Label labelNameTable;
        private float tableScale = 0.5f * Camera.scaleWnd;

        public MainMenu(MyGame game) : base(game)
        {
            isInterface = true;
            background = game.Content.Load<Texture2D>("Interface/Hellstrom_final");
            song = game.Content.Load<Song>("Songs/It Came From the Deep");
            MediaPlayer.Play(song);

            drawField.Size = Camera.sizeWnd.ToPoint();
            zIndex = 0.95f - GameFeature.zIndexStep;

            #region main
            float scale = 0.6f * Camera.scaleWnd;
            Vector2 sizeTxt = GameFeature.Font.MeasureString("Играть") * scale;
            Point position = new Point((int)(Camera.sizeWnd.X - sizeTxt.X) / 2, (int)(400 * Camera.scaleWnd));

            play = new Button(game, position, "Играть", scale, zIndex + GameFeature.zIndexStep);

            sizeTxt = GameFeature.Font.MeasureString("Таблица результатов") * scale;
            position.X = (int)(Camera.sizeWnd.X - sizeTxt.X) / 2;
            position.Y = (int)(500 * Camera.scaleWnd);

            scoreTable = new Button(game, position, "Таблица результатов", scale, zIndex + GameFeature.zIndexStep);
            scoreTable.mouseLeftButtonDown += new EventHandler((o, e) =>
            {
                settings.isVisible = false;
                play.isVisible = false;
                scoreTable.isVisible = false;
                exit.isVisible = false;

                updateTable();

                labelScoreTable.isVisible = true;
                labelNameTable.isVisible = true;
                scoreTableBack.isVisible = true;
            });

            sizeTxt = GameFeature.Font.MeasureString("Настройки") * scale;
            position.X = (int)(Camera.sizeWnd.X - sizeTxt.X) / 2;
            position.Y = (int)(600 * Camera.scaleWnd);

            settings = new Button(game, position, "Настройки", scale, zIndex + GameFeature.zIndexStep);

            settings.mouseLeftButtonDown += new EventHandler((o, e) =>
            {
                settings.isVisible = false;
                play.isVisible = false;
                scoreTable.isVisible = false;
                exit.isVisible = false;

                labelVolumeSongs.isVisible = true;
                volumeSongs.isVisible = true;
                volumeEffects.isVisible = true;
                labelVolumeEffects.isVisible = true;
                settingsBack.isVisible = true;
            });

            sizeTxt = GameFeature.Font.MeasureString("Выход") * scale;
            position.X = (int)(Camera.sizeWnd.X - sizeTxt.X) / 2;
            position.Y = (int)(700 * Camera.scaleWnd);
            exit = new Button(game, position, "Выход", scale, zIndex + GameFeature.zIndexStep);
            exit.mouseLeftButtonDown += new EventHandler((o, e) =>
            {
                game.Exit();
            });       
            #endregion

        

            #region settings

            Vector2 sizeSlider = new Vector2((int)(300 * Camera.scaleWnd), (int)(10 * Camera.scaleWnd));
            position = ((Camera.sizeWnd - sizeSlider) / 2).ToPoint();
            position.Y -= (int)(100 * Camera.scaleWnd);
            position.X += (int)(200 * Camera.scaleWnd);

            volumeSongs = new Slider(game, position, sizeSlider.ToPoint(), zIndex + GameFeature.zIndexStep);
            volumeSongs.isVisible = true;
            volumeSongs.valueChange += new EventHandler((o, e) => 
            {
                MediaPlayer.Volume = volumeSongs.Value;
            });


            position.Y += (int)(100 * Camera.scaleWnd);
            volumeEffects = new Slider(game, position, sizeSlider.ToPoint(), zIndex + GameFeature.zIndexStep);
            volumeEffects.valueChange += new EventHandler((o, e) =>
            {
                SoundEffect.MasterVolume = volumeEffects.Value;
            });

            volumeEffects.mouseLeftButtonUp += new EventHandler((o, e) =>
            {
                GameFeature.testEffect.Play();
            });

            position.Y -= (int)(100 * Camera.scaleWnd);

            scale = 0.4f * Camera.scaleWnd;
            position.X -= (int)(640 * Camera.scaleWnd);
            position.Y += (int)((sizeSlider.Y - GameFeature.Font.MeasureString("Громкость музыки").Y * scale) / 2);
            labelVolumeSongs = new Label(game, "Громкость музыки", position.ToVector2(), scale, Color.White, zIndex + GameFeature.zIndexStep);

            position.Y += (int)(100 * Camera.scaleWnd);
            labelVolumeEffects = new Label(game, "Громкость эффектов", position.ToVector2(), scale, Color.White, zIndex + GameFeature.zIndexStep);


            scale = 0.6f * Camera.scaleWnd;
            sizeTxt = GameFeature.Font.MeasureString("Назад") * scale;
            position = ((Camera.sizeWnd - sizeTxt) / 2).ToPoint();
            position.Y += (int)(200 * Camera.scaleWnd);
            settingsBack = new Button(game, position, "Назад", scale, zIndex + GameFeature.zIndexStep);

            settingsBack.mouseLeftButtonDown += new EventHandler((o, e) =>
            {
                labelVolumeSongs.isVisible = false;
                volumeSongs.isVisible = false;
                volumeEffects.isVisible = false;
                labelVolumeEffects.isVisible = false;
                settingsBack.isVisible = false;

                settings.isVisible = true;
                scoreTable.isVisible = true;
                play.isVisible = true;
                exit.isVisible = true;
            });

            labelVolumeSongs.isVisible = false;
            volumeSongs.isVisible = false;
            volumeEffects.isVisible = false;
            labelVolumeEffects.isVisible = false;
            settingsBack.isVisible = false;
            #endregion

            #region scoreTable

            labelScoreTable = new Label(game, "", Vector2.Zero, tableScale, Color.White, zIndex + GameFeature.zIndexStep);
            labelNameTable = new Label(game, "", Vector2.Zero, tableScale, Color.White, zIndex + GameFeature.zIndexStep);

            scoreTableBack = new Button(game, position, "Назад", scale, zIndex + GameFeature.zIndexStep);

            scoreTableBack.mouseLeftButtonDown += new EventHandler((o, e) => 
            {
                scoreTableBack.isVisible = false;
                labelScoreTable.isVisible = false;
                labelNameTable.isVisible = false;

                play.isVisible = true;
                settings.isVisible = true;
                exit.isVisible = true;
                scoreTable.isVisible = true;
            });

            scoreTableBack.isVisible = false;
            labelScoreTable.isVisible = false;
            labelNameTable.isVisible = false;
            #endregion
        }

        private void updateTable()
        {
            FileStream stream = new FileStream("scoreDB.txt", FileMode.OpenOrCreate);

            using (StreamReader sr = new StreamReader(stream))
            {
                string text = sr.ReadToEnd();

                try
                {
                    string names = "";
                    string scores = "";

                    string[] data = text.Split('\n');

                    for (int i = 0; i< data.Count(); i++)
                    {
                        string[] userData = data[i].Split(' ');

                        names += userData[0];
                        scores += userData[1];

                        if (i != data.Count() - 1)
                        {
                            names += "\n";
                            scores += "\n";
                        }
                    }
                    Vector2 sizeNames = GameFeature.Font.MeasureString(names) * tableScale;
                    Vector2 sizeScores = GameFeature.Font.MeasureString(scores) * tableScale;

                    labelNameTable.Text = names;
                    labelNameTable.Position = new Vector2((Camera.sizeWnd.X - sizeNames.X - sizeScores.X - 100 * Camera.scaleWnd) / 2, 228 * Camera.scaleWnd);

                    labelScoreTable.Text = scores;
                    labelScoreTable.Position = new Vector2(labelNameTable.Position.X + sizeNames.X + 100 * Camera.scaleWnd, 228 * Camera.scaleWnd);

                }
                catch
                {
                    stream.Close();
                    stream = new FileStream("scoreDB.txt", FileMode.Truncate);
                    labelScoreTable.Text = "";
                    labelNameTable.Text = "";
                }
            }

            stream.Close();
        }

        public void Hide()
        {
            if (isVisible)
            {
                isVisible = false;
                play.isVisible = false;
                settings.isVisible = false;
                exit.isVisible = false;
                scoreTable.isVisible = false;

                scoreTableBack.isVisible = false;
                labelScoreTable.isVisible = false;
                labelNameTable.isVisible = false;

                labelVolumeSongs.isVisible = false;
                volumeSongs.isVisible = false;
                volumeEffects.isVisible = false;
                labelVolumeEffects.isVisible = false;
                settingsBack.isVisible = false;
            }
        }

        public void Show()
        {     
            if (!isVisible)
            {
                MediaPlayer.Play(song);
                isVisible = true;
                play.isVisible = true;
                settings.isVisible = true;
                exit.isVisible = true;
                scoreTable.isVisible = true;
            }
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background, drawField, null, Color.Gray, 0, Vector2.Zero, SpriteEffects.None, zIndex);
        }
    }
}
