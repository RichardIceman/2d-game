﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class EndScreen : InteractiveObject
    {
        private Texture2D background;
        private string text;
        private string nameTable;
        private string scoreTable;
        private Vector2 positionTxt;
        private Vector2 positionNameTable;
        private Vector2 positionScoreTable;
        private Color fontColor = Color.Yellow;
        private float scoreScale = 0.5f * Camera.scaleWnd;
        private float restartScale = 0.6f * Camera.scaleWnd;
        private float fontScale = 0.8f * Camera.scaleWnd;
        private float zIndexFont;
        private Vector2 shiftTxt;
        private Label scoreInfo;
        private Label nameInfo;
        public Button mainMenu;
        private TextBox textBox;

        private List<string> listName;
        private List<int> listScore;

        public EndScreen(MyGame game) : base(game)
        {
            isInterface = true;

            background = game.Content.Load<Texture2D>("Characters/Shades/select");

            drawField.Size = Camera.sizeWnd.ToPoint();
            shiftTxt = new Vector2(0, Camera.sizeWnd.Y / 6);

            zIndex = 0.94f;
            zIndexFont = zIndex + GameFeature.zIndexStep;

            text = "Продолжить";
            Vector2 sizeTxt = GameFeature.Font.MeasureString(text) * restartScale;
            positionTxt.X = drawField.X + (drawField.Width - sizeTxt.X) / 2;
            positionTxt.Y = drawField.Y + drawField.Height - sizeTxt.Y - 50 * Camera.scaleWnd;
            mainMenu = new Button(game, positionTxt.ToPoint(), text, restartScale, zIndexFont);
            mainMenu.mouseLeftButtonDown += MainMenu_mouseLeftButtonDown;
            mainMenu.isVisible = false;

            text = "Имя: ";
            sizeTxt = GameFeature.Font.MeasureString(text + "абвгдежзийкл") * scoreScale;
            positionTxt.X = drawField.X + (drawField.Width - sizeTxt.X) / 2;
            positionTxt.Y -= sizeTxt.Y + 50 * Camera.scaleWnd;
            nameInfo = new Label(game, text, positionTxt, scoreScale, Color.Yellow, zIndexFont);
            nameInfo.isVisible = false;

            sizeTxt = GameFeature.Font.MeasureString(text) * scoreScale;
            positionTxt.X += sizeTxt.X;
            textBox = new TextBox(game, positionTxt, scoreScale);
            textBox.zIndex = 1;
            textBox.Text = "Игрок_1";

            text = "Счет: ";
            sizeTxt = GameFeature.Font.MeasureString(text) * scoreScale;
            positionTxt.X -= sizeTxt.X;
            positionTxt.Y -= sizeTxt.Y;
            scoreInfo = new Label(game, text, positionTxt, scoreScale, Color.Yellow, zIndexFont);
            scoreInfo.isVisible = false;           
           
            text = "Конец игры";
            sizeTxt = GameFeature.Font.MeasureString(text) * fontScale;
            positionTxt.X = drawField.X + (drawField.Width - sizeTxt.X) / 2;
            positionTxt.Y = drawField.Y + 50 * Camera.scaleWnd;

            nameTable = "|\n|\n|\n|\n|";
            positionNameTable.X = positionTxt.X - 100 * Camera.scaleWnd;
            positionNameTable.Y = positionTxt.Y + sizeTxt.Y + 50 * Camera.scaleWnd;
            positionScoreTable.Y = positionNameTable.Y;

            listName = new List<string>();
            listScore = new List<int>();
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background, drawField, null, Color.Black, 0, Vector2.Zero, SpriteEffects.None, zIndex);
            spriteBatch.DrawString(GameFeature.Font, text, positionTxt, fontColor, 0, Vector2.Zero, fontScale, SpriteEffects.None, zIndexFont);
            spriteBatch.DrawString(GameFeature.Font, nameTable, positionNameTable, Color.White, 0, Vector2.Zero, scoreScale, SpriteEffects.None, zIndexFont);
            spriteBatch.DrawString(GameFeature.Font, scoreTable, positionScoreTable, Color.White, 0, Vector2.Zero, scoreScale, SpriteEffects.None, zIndexFont);
        }

        private void MainMenu_mouseLeftButtonDown(object sender, EventArgs e)
        {
            if (listScore.Count > 0)
            {
                for (int i = 0; i < listScore.Count; i++)
                {
                    if (listScore[i] <= GameFeature.score)
                    {
                        listScore.Insert(i, GameFeature.score);
                        listName.Insert(i, textBox.Text);
                        break;
                    }
                }
            }
            else
            {
                listScore.Add(GameFeature.score);
                listName.Add(textBox.Text);
            }

            if (listScore.Count > 5)
            {
                listScore.RemoveRange(5, listScore.Count - 5);
                listName.RemoveRange(5, listScore.Count - 5);
            }

            FileStream stream = new FileStream("scoreDB.txt", FileMode.OpenOrCreate);
            using (StreamWriter sw = new StreamWriter(stream))
            {
                for (int i = 0; i < listScore.Count; i++)
                {
                    sw.Write($"{listName[i]} {listScore[i]}");
                    if (i != listScore.Count - 1) sw.Write("\n");
                }
            }
            stream.Close();
        }

        public void updateInfo()
        {
            nameTable = "";
            scoreTable = "";

            scoreInfo.Text = $"Счет: {GameFeature.score}";

            listScore.Clear();
            listName.Clear();

            FileStream stream = new FileStream("scoreDB.txt", FileMode.OpenOrCreate);
            using (StreamReader sr = new StreamReader(stream))
            {
                string myDB = sr.ReadToEnd();
                if (myDB.Length > 0)
                {
                    try
                    {
                        string[] data = myDB.Split('\n');
                        for (int i = 0; i < data.Count(); i++)
                        {
                            string[] userData = data[i].Split(' ');

                            listName.Add(userData[0]);
                            listScore.Add(int.Parse(userData[1]));

                            nameTable += userData[0];
                            scoreTable += userData[1];

                            if (i != data.Count() - 1)
                            {
                                nameTable += "\n";
                                scoreTable += "\n";
                            }
                        }

                        Vector2 sizeName = GameFeature.Font.MeasureString(nameTable) * scoreScale;
                        Vector2 sizeScore = GameFeature.Font.MeasureString(scoreTable) * scoreScale;
                        positionNameTable.X = (Camera.sizeWnd.X - sizeName.X - sizeScore.X - 100 * Camera.scaleWnd ) / 2;
                        positionScoreTable.X = positionNameTable.X + sizeName.X + 100 * Camera.scaleWnd;
                    }
                    catch //поврежден обнуляем
                    {
                        listName.Clear();
                        listScore.Clear();
                        nameTable = "";
                        scoreTable = "";

                        stream.Close();
                        stream = new FileStream("scoreDB.txt", FileMode.Truncate);
                    }
                }
            }

            stream.Close();
        }

        public void Show()
        {
            updateInfo();
            isVisible = true;
            scoreInfo.isVisible = true;
            mainMenu.isVisible = true;
            nameInfo.isVisible = true;
            textBox.Show();
        }

        public void Hide()
        {
            isVisible = false;
            scoreInfo.isVisible = false;
            mainMenu.isVisible = false;
            nameInfo.isVisible = false;
            textBox.Hide();
        }
    }
}
