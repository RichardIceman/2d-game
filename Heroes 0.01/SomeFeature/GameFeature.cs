﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public static class GameFeature
    {
        public static event EventHandler moneyChange;

        public static bool InterfaceActive = true;

        public static float scaleEllipse_X = 1f;
        public static float scaleEllipse_Y = 0.7f;
        public static float zIndexStep = 0.00001f;
        public static Random rand = new Random((int)(DateTime.Now.Ticks));
        public static SpriteFont Font;
        public static SoundEffect testEffect;
        public static string testStr = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦШЩЬЫЪЭЮЯ\nабвгдежзийклмнопрстуфхцшщьыъэюя\n1234567890\n!?&#@%";
        public static bool pause = true;
        public static bool timeStop = false;
        public static int score = 0;
        public static int maxCountTry = 3;
        public static int countTry = maxCountTry;

        public static bool lostHeartGhost = false;
        public static bool lostHeartGolem = false;

        public static bool newStory = true;
        public static bool endGame = false;

        private static int money = 0;
        public static int Money
        {
            get { return money; }
            set
            {
                money = value;
                moneyChange?.Invoke(null, null);
            }
        }

        public static int inventorySize = 24;
        public static int equipementSize = 7;
        public static int countPotions = 3;
        public static int countSkills = 3;

        public static List<int> lvlTable = new List<int>
        {
            50, 150, 300, 500, 900, 1500, 2500, 3500, 5000
        };

        public static float getZIndex(float y)
        {
            return 0.9f - 0.9f / y;
        }     

        public static bool inEllipse(Vector2 distance, float maxRange)
        {
            return Math.Pow(distance.X / (maxRange * scaleEllipse_X), 2) + Math.Pow(distance.Y / (maxRange * scaleEllipse_Y), 2) <= 1;
        }

        public static float radiusEllipse(Vector2 distance, float maxRange)
        {
            return (float)(Math.Pow(distance.X / (maxRange * scaleEllipse_X), 2) + Math.Pow(distance.Y / (maxRange * scaleEllipse_Y), 2));
        }

        public static int calculateDamage(int attack, int defense)
        {
            int damage = attack - defense;
            return damage > 0 ? damage : 1;
        }

        public static Vector2 getPointInCircle()
        {
            Vector2 point;

            double radius =  rand.Next(0, 10000)/10000f;

            double angle = rand.Next(0, 10000) / 10000f * 2 * Math.PI;

            point.X = (float)(radius * Math.Cos(angle));
            point.Y = (float)(radius * Math.Sin(angle));

            return point;
        }
    }
}
