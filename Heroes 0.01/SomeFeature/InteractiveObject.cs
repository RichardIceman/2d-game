﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public abstract class InteractiveObject
    {
        public MyGame game;

        public event EventHandler mouseLeftButtonUp;
        public event EventHandler mouseLeftButtonDown;
        public event EventHandler mouseRightButtonDown;
        public event EventHandler mouseEnter;
        public event EventHandler mouseLeave;

        public Rectangle drawField;
        public float zIndex = 0;

        public bool isInterface = false;

        public bool isCaptureMouse = false;

        private bool isvisible = false;
        public bool isVisible
        {
            get
            {
                return isvisible;
            }
            set
            {
                if (value != isvisible)
                {
                    if (value)
                    {
                        if (isInterface)
                        {
                            game.drawInterface -= draw;
                            game.drawInterface += draw;
                        }                            
                        else
                        {
                            game.drawGameObject -= draw;
                            game.drawGameObject += draw;
                        } 

                       if (!InterfaceCore.listObjects.Contains(this)) InterfaceCore.listObjects.Add(this);
                    }
                    else
                    {
                        if (isInterface)
                            game.drawInterface -= draw;
                        else 
                            game.drawGameObject -= draw;

                        InterfaceCore.listObjects.Remove(this);
                    }

                    isvisible = value;
                }
            }
        }

        public InteractiveObject(MyGame game)
        {
            this.game = game;
        }

        public virtual void Remove()
        {
            InterfaceCore.listObjects.Remove(this);
            isVisible = false;          
        }

        protected abstract void draw(GameTime gameTime, SpriteBatch spriteBatch);

        public void onMouseLeftButtonUp()
        {
            mouseLeftButtonUp?.Invoke(this, null);
        }

        public void onMouseLeftButtonDown()
        {
            mouseLeftButtonDown?.Invoke(this, null);
        }

        public void onMouseRightButtonDown()
        {
            mouseRightButtonDown?.Invoke(this, null);
        }

        public void onMouseEnter()
        {
            if (!isCaptureMouse)
            {
                mouseEnter?.Invoke(this, null);
                isCaptureMouse = true;
            }
        }

        public void onMouseLeave()
        {
            if (isCaptureMouse)
            {
                isCaptureMouse = false;
                mouseLeave?.Invoke(this, null);
            }
        }
    }
}
