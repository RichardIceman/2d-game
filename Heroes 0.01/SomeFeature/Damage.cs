﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Damage
    {
        public AbstractCharacter character;
        public int damage;

        public Damage(AbstractCharacter character, int damage)
        {
            this.character = character;
            this.damage = damage;
        }
    }
}
