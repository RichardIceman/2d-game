﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public static class Camera
    {
        public static float indent = 0.05f;
        private static int scrollSpeed = 40;
        private static float zoom = 0.5f;
        private static float zoomStep = 0.05f;
        private static float maxZoom = 1f;
        private static float minZoom = 0.4f;

        private static bool isScroling = false;
        private static Vector2 shift;
        private static Vector3 newPosition;

        public static Vector3 positionCamera;
        private static Vector3 origin;
        public static Matrix Transform;
        public static Vector2 scalePositionMouse
        {
            get
            {
                return Vector2.Transform(Mouse.GetState().Position.ToVector2(), Matrix.Invert(Transform));
            }
        }
        private static MyGame game;

        public static Map map;

        public static float scaleWnd;
        public static Vector2 sizeWnd;
        public static Vector2 zoomSizeWnd
        {
            get
            {
                return sizeWnd / zoom;
            }
        }

        public static SelectionField selectionField;

        private static MouseState mouseState = new MouseState();
        private static MouseState previouseMouseState;

        //для тряски камеры
        private static Vector3 originalPos;
        private static int timer;
        private static bool isShaking;
        private static float duration;
        private static float power;

        public static void Start(MyGame _game)
        {
            game = _game;

            positionCamera = new Vector3(-game.GraphicsDevice.Viewport.Width / 2,
                                         -game.GraphicsDevice.Viewport.Height / 2, 
                                         0);

            sizeWnd = new Vector2(game.graphics.PreferredBackBufferWidth, game.graphics.PreferredBackBufferHeight);

            scaleWnd =  sizeWnd.Y / 1080;

            maxZoom *= scaleWnd;
            zoom *= scaleWnd;
            minZoom *= scaleWnd;

            scrollSpeed = (int)(scrollSpeed * scaleWnd);

            if (scaleWnd > 1) scaleWnd = 1;

            origin = new Vector3(game.GraphicsDevice.Viewport.Width / 2,
                                 game.GraphicsDevice.Viewport.Height / 2,
                                 0);
            
            updateCamera();
        }

        public static void updateCamera()
        {
            Matrix translation = Matrix.CreateTranslation(positionCamera);
            Matrix scale = Matrix.CreateScale(new Vector3(zoom, zoom, 1));
            Matrix orgn = Matrix.CreateTranslation(origin);
            
            Transform = translation * scale * orgn;
        }

        public static void scrollTo(Vector2 _newPosition)
        {
            newPosition = new Vector3( _newPosition.X, _newPosition.Y, 0);

            if (Math.Abs(positionCamera.X - newPosition.X) > Math.Abs(positionCamera.Y - newPosition.Y))
            {
                shift.X = scrollSpeed * (newPosition.X - positionCamera.X) / Math.Abs(positionCamera.X - newPosition.X);
                shift.Y = scrollSpeed * (newPosition.Y - positionCamera.Y) / Math.Abs(positionCamera.X - newPosition.X);
            }
            else
            {
                shift.X = scrollSpeed * (newPosition.X - positionCamera.X) / Math.Abs(positionCamera.Y - newPosition.Y);
                shift.Y = scrollSpeed * (newPosition.Y - positionCamera.Y) / Math.Abs(positionCamera.Y - newPosition.Y);
            }

            isScroling = true;
        }

        public static void focusOn(Vector2 _newPosition)
        {
            positionCamera = new Vector3(_newPosition.X, _newPosition.Y, 0);
            originalPos = positionCamera;

            updateCamera();
        }

        public static void update(GameTime gameTime)
        {
            previouseMouseState = mouseState;
            mouseState = Mouse.GetState();
            int delta = mouseState.ScrollWheelValue - previouseMouseState.ScrollWheelValue;

            if (delta > 0)
            {
                zoom += zoomStep;
                if (zoom > maxZoom) zoom = maxZoom;
                updateCamera();
            }
            else if (delta < 0)
            {
                zoom -= zoomStep;
                if (zoom < minZoom) zoom = minZoom;
                updateCamera();
            }

            if (isScroling)
            {
                if (positionCamera.X + scrollSpeed / 2 >= newPosition.X && positionCamera.X - scrollSpeed / 2 <= newPosition.X &&
                    positionCamera.Y + scrollSpeed / 2 >= newPosition.Y && positionCamera.Y - scrollSpeed / 2 <= newPosition.Y)
                {
                    isScroling = false;
                    positionCamera = newPosition;
                    originalPos = positionCamera;
                    updateCamera();
                }
                else
                {
                    positionCamera.X += shift.X;
                    positionCamera.Y += shift.Y;
                    originalPos = positionCamera;
                    updateCamera();
                }
            }

            #region scrolling
            if (mouseState.X >= sizeWnd.X * (1 - indent) &&
                mouseState.X <= sizeWnd.X &&
                positionCamera.X >=-map.drawField.Width)
            {
                positionCamera.X-=scrollSpeed;
                originalPos = positionCamera;
                isScroling = false;
                updateCamera();
            }
            else if (mouseState.X >= 0 &&
                     mouseState.X <= sizeWnd.X * indent &&
                     positionCamera.X <= -game.GraphicsDevice.Viewport.Width / 2)
            {
                isScroling = false;
                positionCamera.X += scrollSpeed;
                originalPos = positionCamera;
                updateCamera();
            }

            if (mouseState.Y >= sizeWnd.Y * (1 - indent) &&
                mouseState.Y <= sizeWnd.Y &&
                positionCamera.Y >= -map.drawField.Height)
            {
                isScroling = false;
                positionCamera.Y -= scrollSpeed;
                originalPos = positionCamera;
                updateCamera();
            }
            else if (mouseState.Y >= 0 &&
                     mouseState.Y <= sizeWnd.Y * indent &&
                     positionCamera.Y <= -game.GraphicsDevice.Viewport.Height / 2)
            {
                isScroling = false;
                positionCamera.Y += scrollSpeed;
                originalPos = positionCamera;
                updateCamera();
            }

            #endregion

            if (isShaking)
            {
                if (timer < duration)
                {
                    timer += gameTime.ElapsedGameTime.Milliseconds;
                    float percentComplete = timer / duration;
                    if (percentComplete > 1) percentComplete = 1;

                    Vector2 rnd = GameFeature.getPointInCircle() * power * (1f - percentComplete);

                    positionCamera = originalPos + new Vector3(rnd.X, rnd.Y, 0);

                    if (isScroling) newPosition += new Vector3(rnd.X, rnd.Y, 0);

                    updateCamera();
                }
                else isShaking = false;
            }
        }

        public static void Shake(float _duration, float _power)
        {
            if (!isShaking) originalPos = positionCamera;

            power = _power;
            duration = _duration * 1000;

            isShaking = true;        
            timer = 0;
        }
    }
}
