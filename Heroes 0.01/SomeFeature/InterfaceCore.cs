﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public static class InterfaceCore
    {
        public static List<InteractiveObject> listObjects = new List<InteractiveObject>();
        public static InteractiveObject map;

        private static MouseState mouseState = new MouseState();
        private static MouseState previouseMouseState;

        private static InteractiveObject gameObject = null;
        private static InteractiveObject previouseTarget = null;
        private static float maxZIndex = 0;

        public static Rectangle mouseRect = new Rectangle(0, 0, 1, 1);
        private static Rectangle scaleMouseRect = new Rectangle(0, 0, 1, 1);

        public static void update(GameTime gameTime)
        {
            previouseMouseState = mouseState;
            mouseState = Mouse.GetState();

            mouseRect.Location = mouseState.Position;
            scaleMouseRect.Location = Camera.scalePositionMouse.ToPoint();

            maxZIndex = 0;

            previouseTarget?.onMouseLeave();
            previouseTarget = gameObject;

            for (int i = 0; i < listObjects.Count; i++)
            {
                if (listObjects[i].isVisible &&
                    maxZIndex <= listObjects[i].zIndex &&
                   (mouseRect.Intersects(listObjects[i].drawField) && listObjects[i].isInterface || scaleMouseRect.Intersects(listObjects[i].drawField) && !listObjects[i].isInterface))
                {
                    gameObject = listObjects[i];
                    maxZIndex = gameObject.zIndex;
                }
            }

            if (mouseState.LeftButton == ButtonState.Pressed && previouseMouseState.LeftButton == ButtonState.Released)
            {
                gameObject?.onMouseLeftButtonDown();
            }

            if (mouseState.LeftButton == ButtonState.Released && previouseMouseState.LeftButton == ButtonState.Pressed)
            {
                gameObject?.onMouseLeftButtonUp();
            }

            if (mouseState.RightButton == ButtonState.Pressed && previouseMouseState.RightButton == ButtonState.Released)
            {
                gameObject?.onMouseRightButtonDown();
                if (gameObject != null && !gameObject.isInterface && gameObject != map)
                    map?.onMouseRightButtonDown();
            }

            gameObject?.onMouseEnter();
        }
    }
}
