﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 
namespace Heroes_0._01
{
    public class MagePassive : AbstractSkill
    {

        public MagePassive(AbstractCharacter character) : base(character)
        {
            typeSkill = TypeSkill.Passive;
            icon = character.game.Content.Load<Texture2D>("SkillIcon/magePassive");
            UpdateDiscription();
        }

        protected override void Effect() { }

        public override void UpdateDiscription()
        {
            name = $"Концентрация Ур. { Lvl }";
            description = $"За счет сильной\nконцентрации, жрец\nможет восполнять ману.\nCкорость: {Lvl} ОМ / сек\nПовышение маны: { Lvl * 20 } ОМ";
            nextLvlName = $"Концентрация Ур. { Lvl + 1 }";
            nextLvlDescription = $"За счет сильной\nконцентрации, жрец\nможет восполнять ману.\nCкорость: {(Lvl + 1)} ОМ / сек\nПовышение маны: { (Lvl + 1) * 20 } ОМ";
        }

        public override void updadeSkill()
        {
            ManaRegen = Lvl;
            ManaPoint = 20 * Lvl;
        }
    }
}
