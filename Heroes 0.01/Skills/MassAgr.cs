﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class MassAgr : AbstractSkill
    {
        private int skillPower = 0;
        private int mult = 15;

        public MassAgr(AbstractCharacter character) : base(character)
        {
            typeSkill = TypeSkill.Active;
            rangeCast = 500;
            icon = character.game.Content.Load<Texture2D>("SkillIcon/massAgr");
            UpdateDiscription();
        }

        public override void UpdateDiscription()
        {
            name = $"Провокация Ур. { Lvl }";
            description = $"Танк провоцирует врагов,\nзаставляя их переключить\nсвою атаку на него.\nРадиус действия: {rangeCast}\nМощность: {character.attack * Lvl * mult}\nСтоимость: {5 * Lvl} ОМ\nПерезарядка: {6 - Lvl} сек";
            nextLvlName = $"Провокация Ур. { Lvl + 1 }";
            nextLvlDescription = $"Танк провоцирует врагов,\nзаставляя их переключить\nсвою атаку на него.\nРадиус действия: {rangeCast}\nМощность: {character.attack * (Lvl + 1) * mult}\nСтоимость: {5 * (Lvl + 1)} ОМ\nПерезарядка: {6 - (Lvl + 1)} сек";
        }

        protected override void Effect()
        {
            character.animAttack.Play();

            new Agr(character.game, character, sideAnimation.Back, SpriteEffects.None).Play(); 

            for (int i = 0; i < character.currentMap.listEnemyOnMap.Count; i++)
            {
                Vector2 distance = character.positionOnMap - character.currentMap.listEnemyOnMap[i].positionOnMap;
                if (character.currentMap.listEnemyOnMap[i].isAlive &&
                    GameFeature.inEllipse(distance, rangeCast))
                {
                    AbstractEnemy enemy = character.currentMap.listEnemyOnMap[i] as AbstractEnemy;
                    enemy.updateListDmg(character, skillPower);
                    new Agr(character.game, enemy, sideAnimation.Back, SpriteEffects.None).Play();
                }
            }   
        }

        public override void updadeSkill()
        {
            cooldown = 6 - Lvl;
            manaCost = 5 * Lvl;
            skillPower = character.attack * Lvl * mult;
        }
    }
}
