﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class RainOfArrows : AbstractSkill
    {
        private MouseState mouseState;
        private MouseState previouseMouseState;

        private RainOfArrowsAnimation animation;

        private Vector2 position;

        private Texture2D selectTexture;
        private Rectangle selectionRect;

        private float zIndex = GameFeature.zIndexStep;
        private Color color = Color.Lime * 0.3f;

        private int duration;
        private int maxDuration = 5000;

        private int timeDelayClick;
        private int time;
        private int delayDamage = 500;

        private int diametrSelect = 600;

        private SpriteEffects flip;

        public RainOfArrows(AbstractCharacter character) : base(character)
        {
            typeSkill = TypeSkill.Active;
            rangeCast = 600;

            icon = character.game.Content.Load<Texture2D>("SkillIcon/rainArrows");
            selectTexture = character.game.Content.Load<Texture2D>("Characters/Shades/light");
            selectionRect = new Rectangle(0, 0, (int)(diametrSelect * GameFeature.scaleEllipse_X), (int)(diametrSelect * GameFeature.scaleEllipse_Y));

            animation = new RainOfArrowsAnimation(character.game);

            UpdateDiscription();
        }

        public override void UpdateDiscription()
        {
            name = $"Ливень стрел Ур. { Lvl }";
            description = $"Лучник выпускает в\nвоздух тучу стрел,\nнанося переодический\nурон всем, кто попал\nв область действия\nРадиус применения: {rangeCast}\nРадиус действия: {diametrSelect / 2}\nУрон: {character.attack * Lvl * 2} / сек\nДлительность: 5 сек\nСтоимость: {2 + 6 * Lvl} ОМ\nПерезарядка: {30 - 5 * Lvl} сек";
            nextLvlName = $"Ливень стрел Ур. { Lvl + 1 }";
            nextLvlDescription = $"Лучник выпускает в\nвоздух тучу стрел,\nнанося переодический\nурон всем, кто попал\nв область действия\nРадиус применения: {rangeCast}\nРадиус действия: {diametrSelect / 2}\nУрон: {character.attack * (Lvl + 1) * 2} / сек\nДлительность: 5 сек\nСтоимость: {2 + 6 * (Lvl + 1)} ОМ\nПерезарядка: {30 - 5 * (Lvl + 1)} сек";
        }

        protected override void Effect()
        {
            character.game.updateInterface -= update;
            character.game.drawGameObject -= draw;

            timeDelayClick = 0;
            duration = 0;

            updatePositionSelectField();

            character.game.updateInterface += update;
            character.game.drawGameObject += draw;
        }

        public override void updadeSkill()
        {
            if (Lvl == 0) return;
            Attack = character.attack * Lvl;
            cooldown = 30 - 5 * Lvl;
            manaCost = 6 * Lvl + 2;
            animation.delaySpawn = 200 / Lvl;
            delayDamage = 500 / Lvl;
        }

        private void updatePositionSelectField()
        {
            position = Camera.scalePositionMouse;

            Vector2 distance = position - character.positionOnMap;

            if (!GameFeature.inEllipse(distance, rangeCast))
            {
                double radius = Math.Sqrt(distance.X * distance.X + distance.Y * distance.Y);


                position.X = character.positionOnMap.X + diametrSelect * GameFeature.scaleEllipse_X * (float)(distance.X / radius);
                position.Y = character.positionOnMap.Y + diametrSelect * GameFeature.scaleEllipse_Y * (float)(distance.Y / radius);
            }

            selectionRect.X = (int)(position.X - selectionRect.Width / 2);
            selectionRect.Y = (int)(position.Y - selectionRect.Height / 2);
        }

        private void update(GameTime gameTime)
        {
            previouseMouseState = mouseState;
            mouseState = Mouse.GetState();

            updatePositionSelectField();


            timeDelayClick += gameTime.ElapsedGameTime.Milliseconds;

            if (timeDelayClick > 50 && previouseMouseState.LeftButton == ButtonState.Released && mouseState.LeftButton == ButtonState.Pressed && inProcess)

                finalyCast();
        }

        private void timerEffect(GameTime gameTime)
        {
            duration += gameTime.ElapsedGameTime.Milliseconds;
            if (duration >= maxDuration)
            {
                character.game.updateGame -= timerEffect;
                animation.Stop();
                return;
            }

            time += gameTime.ElapsedGameTime.Milliseconds;
            if (time >= delayDamage)
            {
                time -= delayDamage;

                for (int i = 0; i < character.currentMap.listEnemyOnMap.Count; i++)
                {
                    Vector2 distance = position - character.currentMap.listEnemyOnMap[i].positionOnMap;

                    if (GameFeature.inEllipse(distance, diametrSelect * 3 / 7))
                    {
                        new ArrowHitEffect(character.game, character.currentMap.listEnemyOnMap[i], sideAnimation.Front, flip);
                        character.currentMap.listEnemyOnMap[i].getDamage(character);
                    }
                }
            }
        }

        private void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(selectTexture, selectionRect, null, color, 0, Vector2.Zero, SpriteEffects.None, zIndex);
        }

        public override bool Cancel()
        {
            if (inProcess)
            {
                inProcess = false;
                animation.Stop();
                character.game.updateInterface -= update;
                character.game.drawGameObject -= draw;
                return true;
            }
            return false;
        }

        protected override void tryCast()
        {
            Effect();
            inProcess = true;
        }

        private void finalyCast()
        {
            AbstractWeapon weapon = character.inventory[29] as AbstractWeapon;
            AbstractSecondWeapon secondWeapom = character.inventory[30] as AbstractSecondWeapon;

            if (weapon != null && weapon.typeAttack == TypeAttack.Лук)
            {
                if (secondWeapom != null && secondWeapom.secondWeaponType == SecondWeaponType.Колчан)
                {

                    if (!character.characterVisibile) character.characterVisibile = true;

                    float distance_x = position.X - character.positionOnMap.X;

                    character.shift.X = distance_x >= 0 ? 1 : -1;

                    character.game.updateGame += timerEffect;

                    character.animAttack.Play();

                    canCast = false;

                    inProcess = false;

                    character.game.updateInterface -= update;
                    character.game.drawGameObject -= draw;

                    flip = character.shift.X >= 0 ? SpriteEffects.FlipHorizontally : SpriteEffects.None;

                    animation.Play(position, flip, (int)(diametrSelect * 0.6f));

                    character.manaBar.Value -= manaCost;
                    currentCooldown = cooldown;
                    character.game.updateGame += Cooldown;
                    onCast();
                }
                else character.Say("Мне нужны стрелы!", TypeCharacter.Герой);
            }
            else character.Say("Мне нужен лук!", TypeCharacter.Герой);
        }
    }
}
