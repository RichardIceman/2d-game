﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public enum TypeSkill
    {
        Active,
        Passive
    }

    public abstract class AbstractSkill
    {
        public event EventHandler isCast;

        public Texture2D icon;
        protected AbstractCharacter character;
        public TypeSkill typeSkill;

        public int maxLvl = 3;
        private int lvl;
        public int Lvl
        {
            get { return lvl; }
            set
            {
                if (value >= 0 && value <= maxLvl)
                {
                    lvl = value;
                    updadeSkill();
                    UpdateDiscription();
                    if (typeSkill == TypeSkill.Passive) character?.recalculateStats();
                }
            }
        }
        public string name;
        public string description;
        public string nextLvlDescription;
        public string nextLvlName;

        public int HealthPoint = 0;
        public int ManaPoint = 0;
        public int Attack = 0;
        public int Defense = 0;
        public int HealPower = 0;
        public int ManaRegen = 0;
        public int HealthRegen = 0;
        public int MoveSpeed = 0;

        protected int manaCost = 0;
        public int cooldown = 0;
        public int currentCooldown;
        private int currentTime;

        public bool canCast = true;

        public bool inProcess = false;

        protected int rangeCast;

        public AbstractSkill(AbstractCharacter character)
        {
            this.character = character;
            character.statsChanged += Character_statsChanged;
        }

        private void Character_statsChanged(object sender, EventArgs e)
        {
            UpdateDiscription();
        }

        protected abstract void Effect();
        public abstract void UpdateDiscription();
        public abstract void updadeSkill();

        public virtual bool Cancel() { return false; }

        public bool Upgrade()
        {
            if (character.SkillPoint > 0 && Lvl < maxLvl)
            {
                character.SkillPoint--;
                Lvl++;
                return true;
            }
            return false;
        }

        protected void Cooldown(GameTime gameTime)
        {
            currentTime += gameTime.ElapsedGameTime.Milliseconds;
            if (currentTime >= 1000)
            {
                currentTime -= 1000;
                currentCooldown--;
                if (currentCooldown == 0)
                {
                    character.game.updateGame -= Cooldown;
                    canCast = true;
                }
            }
        }

        public bool Cast()
        {
            if (character.isAlive)
            {
                if (lvl > 0)
                {
                    if (typeSkill == TypeSkill.Active)
                    {
                        if (canCast)
                        {
                            if (character.manaBar.Value >= manaCost)
                            {
                                tryCast();
                                return true;
                            }
                            else character.Say("Недостаточно\nманы.", TypeCharacter.Герой);
                        }
                        else character.Say("Еще не время.", TypeCharacter.Герой);
                    }
                    else character.Say("Это пассивная\nспособность.", TypeCharacter.Герой);
                }
                else character.Say("Я еще не изучил\nэту способность.", TypeCharacter.Герой);
            }
            return false;
        }

        protected virtual void tryCast()
        {
            if (!character.characterVisibile) character.characterVisibile = true;
            Effect();
            canCast = false;
            character.manaBar.Value -= manaCost;
            currentCooldown = cooldown;
            character.game.updateGame += Cooldown;
            onCast();
        }

        protected void onCast()
        {
            isCast?.Invoke(this, null);
        }


        public virtual void Remove()
        {
            Cancel();
            character = null;
        }
    }
}
