﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class MassProtection : AbstractSkill
    {
        private int maxTime;
        private int time;
        private List<AbstractCharacter> charactersUnderEffect;
        private List<AbstractEffect> effects;

        public MassProtection(AbstractCharacter character) : base(character)
        {
            typeSkill = TypeSkill.Active;
            rangeCast = 500;
            icon = character.game.Content.Load<Texture2D>("SkillIcon/protection");
            charactersUnderEffect = new List<AbstractCharacter>();
            effects = new List<AbstractEffect>();
            UpdateDiscription();
        }

        public override void UpdateDiscription()
        {
            name = $"Защита веры Ур. { Lvl }";
            description = $"Танк накладывает на\nближайших союзников\nбарьер, защищающий\nих от урона.\nРадиус действия: { rangeCast }\nВремя действия: {Lvl * 5} сек\nСтоимость: {20 + 10 * Lvl} ОМ\nПерезарядка: {55 - Lvl * 5} сек";
            nextLvlName = $"Защита веры Ур. { Lvl + 1 }";
            nextLvlDescription = $"Танк накладывает на\nближайших союзников\nбарьер, защищающий\nих от урона.\nРадиус действия: { rangeCast }\nВремя действия: {(Lvl + 1) * 5} сек\nСтоимость: {20 + 10 * (Lvl + 1)} ОМ\nПерезарядка: {55 - (Lvl + 1) * 5} сек";
        }

        private void timer(GameTime gameTime)
        {
            time += gameTime.ElapsedGameTime.Milliseconds;
            if (time >= maxTime)
            {
                time = 0;
                character.game.updateGame -= timer;
                for (int i = 0; i < charactersUnderEffect.Count; i++)
                {
                    charactersUnderEffect[i].isDamageImmune = false;
                    effects[i].Remove();
                }
                charactersUnderEffect.Clear();
                effects.Clear();
            }
        }

        protected override void Effect()
        {
            effects.Clear();
            character.animAttack.Play();
            for (int i = 0; i < character.currentMap.listHeroOnMap.Count; i++)
            {
                Vector2 distance = character.positionOnMap - character.currentMap.listHeroOnMap[i].positionOnMap;
                if (character.currentMap.listHeroOnMap[i].isAlive &&
                    GameFeature.inEllipse(distance, rangeCast))
                {
                    Protection effect = new Protection(character.game, character.currentMap.listHeroOnMap[i], sideAnimation.Front, SpriteEffects.None);
                    effect.Play();
                    
                    effects.Add(effect);
                    character.currentMap.listHeroOnMap[i].isDamageImmune = true;
                    charactersUnderEffect.Add(character.currentMap.listHeroOnMap[i]);
                }
            }
            character.game.updateGame += timer;
        }

        public override void updadeSkill()
        {
            cooldown = 55 - Lvl * 5;
            manaCost = 20 + 10 * Lvl;
            maxTime = Lvl * 5000;
        }

        public override void Remove()
        {
            charactersUnderEffect.Clear();
            effects.Clear();
            base.Remove();
        }
    }
}
