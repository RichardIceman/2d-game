﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class MassHeal : AbstractSkill
    {

        public MassHeal(AbstractCharacter character) : base(character)
        {
            typeSkill = TypeSkill.Active;
            rangeCast = 400;
            cooldown = 6;
            manaCost = 5;
            icon = character.game.Content.Load<Texture2D>("SkillIcon/massHeal");
            UpdateDiscription();
        }

        public override void UpdateDiscription()
        {
            name = $"Исцеление Ур. { Lvl }";
            description = $"Восстанавливает союзным\nюнитам { HealPower + character.healPower } здоровья.\nРадиус действия: { rangeCast }\nСтоимость: { 20 * Lvl } ОМ\nПерезарядка: { 6 - Lvl } сек";
            nextLvlName = $"Исцеление Ур. { Lvl + 1 }";
            nextLvlDescription = $"Восстанавливает союзным\nюнитам { HealPower + (Lvl + 1) * 5 + character.healPower } здоровья.\nРадиус действия: { rangeCast }\nСтоимость: { 20 * (Lvl + 1) } ОМ\nПерезарядка: { 6 - (Lvl + 1) } сек";
        }

        protected override void Effect()
        {
            character.animAttack.Play();
            for (int i = 0; i < character.currentMap.listHeroOnMap.Count; i++)
            {
                Vector2 distance = character.positionOnMap - character.currentMap.listHeroOnMap[i].positionOnMap;
                if (character.currentMap.listHeroOnMap[i].isAlive && GameFeature.inEllipse(distance, rangeCast))
                {
                    new Heal(character.game, character.currentMap.listHeroOnMap[i], sideAnimation.Front, SpriteEffects.None).Play();
                    character.currentMap.listHeroOnMap[i].healthBar.Value += HealPower + character.healPower;
                }
            }   
        }

        public override void updadeSkill()
        {
            cooldown = 6 - Lvl;
            manaCost = 20 * Lvl;
            HealPower = 10 * Lvl;
        }
    }
}
