﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class MassResurrection : AbstractSkill
    {

        public MassResurrection(AbstractCharacter character) : base(character)
        {
            typeSkill = TypeSkill.Active;
            rangeCast = 500;
            icon = character.game.Content.Load<Texture2D>("SkillIcon/resurrection");
            UpdateDiscription();
        }

        public override void UpdateDiscription()
        {
            name = $"Воскрешение Ур. { Lvl }";
            description = $"Воскрешает ближайших\nсоюзных юнитов\nвосстанавливая им {Lvl * 20} %\nздоровья.\nРадиус действия: { rangeCast }\nСтоимость: { 30 * Lvl } ОМ\nПерезарядка: { 70 - Lvl * 10 } сек";
            nextLvlName = $"Воскрешение Ур. { Lvl + 1 }";
            nextLvlDescription = $"Воскрешает ближайших\nсоюзных юнитов\nвосстанавливая им {(Lvl + 1) * 20} %\nздоровья.\nРадиус действия: { rangeCast }\nСтоимость: { 30 * (Lvl + 1) } ОМ\nПерезарядка: { 70 - (Lvl + 1) * 10 } сек";
        }

        protected override void Effect()
        {
            character.animAttack.Play();
            character.Say("Герои не умирают!", TypeCharacter.Герой);
            for (int i = 0; i < character.currentMap.listHeroOnMap.Count; i++)
            {
                Vector2 distance = character.positionOnMap - character.currentMap.listHeroOnMap[i].positionOnMap;
                if (character.currentMap.listHeroOnMap[i] != character &&
                    !character.currentMap.listHeroOnMap[i].isAlive &&
                    GameFeature.inEllipse(distance, rangeCast))
                {
                    new Wings(character.game, character.currentMap.listHeroOnMap[i], sideAnimation.Back, SpriteEffects.None).Play();
                    character.currentMap.listHeroOnMap[i].healthBar.Value = character.currentMap.listHeroOnMap[i].healthBar.MaxValue * Lvl / 5;
                    character.currentMap.listHeroOnMap[i].Resurrect();
                }
            }   
        }

        public override void updadeSkill()
        {
            cooldown = 70 - Lvl * 10;
            manaCost = 30 * Lvl;
        }
    }
}
