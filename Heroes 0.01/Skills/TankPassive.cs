﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class TankPassive : AbstractSkill
    {

        public TankPassive(AbstractCharacter character) : base(character)
        {
            typeSkill = TypeSkill.Passive;
            icon = character.game.Content.Load<Texture2D>("SkillIcon/tankPassive");
            UpdateDiscription();
        }

        protected override void Effect() { }

        public override void UpdateDiscription()
        {
            name = $"Боевой дух Ур. { Lvl }";
            description = $"Повышая свой боевой\nдух танк начинает\nлучше сопротивляться\nурону и регенерировать\nздоровье.\nПовышение защиты: {Lvl}\nСкорость: {Lvl} ОЗ / сек";
            nextLvlName = $"Боевой дух Ур. { Lvl + 1 }";
            nextLvlDescription = $"Повышая свой боевой\nдух танк начинает\nлучше сопротивляться\nурону и регенерировать\nздоровье.\nПовышение защиты: {Lvl + 1}\nСкорость: {(Lvl + 1)} ОЗ / сек";
        }

        public override void updadeSkill()
        {
            HealthRegen = Lvl;
            Defense = Lvl;
        }
    }
}
