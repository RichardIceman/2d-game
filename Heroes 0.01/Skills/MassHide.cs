﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class MassHide : AbstractSkill
    {
        private int maxTime;
        private int time;
        private List<AbstractCharacter> charactersUnderEffect;

        public MassHide(AbstractCharacter character) : base(character)
        {
            typeSkill = TypeSkill.Active;
            rangeCast = 500;
            icon = character.game.Content.Load<Texture2D>("SkillIcon/massHide");
            charactersUnderEffect = new List<AbstractCharacter>();
            UpdateDiscription();
        }

        public override void UpdateDiscription()
        {
            name = $"Невидимость Ур. { Lvl }";
            description = $"Лучник взрывает дымовую\nбомбу, делая всех, кто\nпопал под ее действие,\nвременно невидимыми.\nРадиус действия: { rangeCast }\nВремя действия: {10 + Lvl * 10} сек\nСтоимость: {20 + 10 * Lvl} ОМ\nПерезарядка: {55 - Lvl * 5} сек";
            nextLvlName = $"Невидимость Ур. { Lvl + 1 }";
            nextLvlDescription = $"Лучник взрывает дымовую\nбомбу, делая всех, кто\nпопал под ее действие,\nвременно невидимыми.\nРадиус действия: { rangeCast }\nВремя действия: {10 + (Lvl + 1) * 10} сек\nСтоимость: {20 + 10 * (Lvl + 1)} ОМ\nПерезарядка: {55 - (Lvl + 1) * 5} сек";
        }

        private void timer(GameTime gameTime)
        {
            time += gameTime.ElapsedGameTime.Milliseconds;
            if (time >= maxTime)
            {
                time = 0;
                character.game.updateGame -= timer;
                for (int i = 0; i < charactersUnderEffect.Count; i++)
                {
                    charactersUnderEffect[i].characterVisibile = true;
                }
                charactersUnderEffect.Clear();
            }
        }

        protected override void Effect()
        {
            character.animAttack.Play();
            for (int i = 0; i < character.currentMap.listHeroOnMap.Count; i++)
            {
                Vector2 distance = character.positionOnMap - character.currentMap.listHeroOnMap[i].positionOnMap;
                if (character.currentMap.listHeroOnMap[i].isAlive &&
                    GameFeature.inEllipse(distance, rangeCast))
                {
                    character.currentMap.listHeroOnMap[i].characterVisibile = false;
                    new Smoke(character.game, character.currentMap.listHeroOnMap[i], sideAnimation.Front, SpriteEffects.None).Play();
                    charactersUnderEffect.Add(character.currentMap.listHeroOnMap[i]);
                }
            }
            character.game.updateGame += timer;
        }

        public override void updadeSkill()
        {
            cooldown = 55 - Lvl * 5;
            manaCost = 20 + 10 * Lvl;
            maxTime = 10000 + Lvl * 10000;
        }
    }
}
