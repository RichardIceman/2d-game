﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class ArcherPassive : AbstractSkill
    {

        public ArcherPassive(AbstractCharacter character) : base(character)
        {
            typeSkill = TypeSkill.Passive;
            icon = character.game.Content.Load<Texture2D>("SkillIcon/archerAttack");
            UpdateDiscription();
        }

        protected override void Effect() { }

        public override void UpdateDiscription()
        {
            name = $"Улучшенные\nстрелы Ур. { Lvl }";
            description = $"Лучник затачивает\nнаконечники стрел,\nчто увеличивает его\nурон на { Lvl * 2 } единиц";
            nextLvlName = $"Улучшенные\nстрелы Ур. { Lvl + 1 }";
            nextLvlDescription = $"Лучник затачивает\nнаконечники стрел,\nчто увеличивает его\nурон на {(Lvl + 1) * 2} единиц";
        }

        public override void updadeSkill()
        {
            Attack = 2 * Lvl;
        }
    }
}
