﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class BossMap_1 : BossMap
    {
        private Stamp stamp_1;
        private Stamp stamp_2;
        private Stamp stamp_3;

        private int duration = 60000;

        public BossMap_1(MyGame game, EventHandler mouseLeftButtonDown) : base(game, mouseLeftButtonDown) { }

        protected override void loadMap()
        {
            base.loadMap();
            listPortals.Add(new Portal(game, this, new Point(450, 250), SpriteEffects.None, "Выжженная земля", 1));
            listPortals.Add(new Portal(game, this, new Point(4150, 600), SpriteEffects.FlipHorizontally, "Великий лабиринт", 1));

            stamp_1 = new Stamp(game, new Point(2500, 900), this);
            stamp_2 = new Stamp(game, new Point(1700, 1700), this);
            stamp_3 = new Stamp(game, new Point(3300, 1700), this);

            listPortals[1].isActive = false;

            boss = new GhostBoss(game, new Vector2(2600, 1400));

            effect = new BossProtection(game, boss, sideAnimation.Front, SpriteEffects.None);
            effect.color = Color.DeepPink;

            addCharacterOnMap(boss);

            boss.died += Boss_died;
        }

        public override void Restart()
        {
            stamp_1.Hide();
            stamp_2.Hide();
            stamp_3.Hide();
            base.Restart();
        }

        public override void Hide()
        {
            stamp_1.Hide();
            stamp_2.Hide();
            stamp_3.Hide();
            base.Hide();
        }

        public override void Show()
        {
            if (boss.isAlive && inFaze)
            {
                stamp_1.Show();
                stamp_2.Show();
                stamp_3.Show();
            }
            base.Show();
        }

        private void Boss_died(object sender, EventArgs e)
        {
            boss.died -= Boss_died;
            listPortals[1].isActive = true;
        }

        protected override void faza(GameTime gameTime)
        {
            if (boss.healthBar.Value / boss.MaxHealthPoint <= 0.3f && !inFaze)
            {
                boss.isDamageImmune = true;

                boss.Say("Глупцы, вам\nне победить!", TypeCharacter.Враг);

                effect.Play();

                ShowTimer();
                time = duration;
                updateTimer();
  
                if (listHeroOnMap.Count > 0) listHeroOnMap[0].Say("Каждый встаньте\nв свой круг!", TypeCharacter.Герой);
                if (listHeroOnMap.Count > 1) listHeroOnMap[1].Say("Быстрее! Нужно\nналожить печать!", TypeCharacter.Герой);
                if (listHeroOnMap.Count > 2) listHeroOnMap[2].Say("Если не успеем, он\nвосстановиться!", TypeCharacter.Герой);

                stamp_1.Show();
                stamp_2.Show();
                stamp_3.Show();

                inFaze = true;
            }

            if (inFaze)
            {
                //норм исход

                int countActiveStamp = 0;
                int countAliveHero = 0;

                if (stamp_1.isActive) countActiveStamp++;
                if (stamp_2.isActive) countActiveStamp++;
                if (stamp_3.isActive) countActiveStamp++;

                for (int i = 0; i < listHeroOnMap.Count; i++)
                    if (listHeroOnMap[i].isAlive)
                        countAliveHero++;

                if (countActiveStamp == countAliveHero)
                {
                    effect.Stop();
                  
                    boss.Say("Невозможно...", TypeCharacter.Враг);

                    if (listHeroOnMap.Count > 0) listHeroOnMap[0].Say("Нанесем последний\nудар!!!", TypeCharacter.Герой);
                    if (listHeroOnMap.Count > 1) listHeroOnMap[1].Say("Нанесем последний\nудар!!!", TypeCharacter.Герой);
                    if (listHeroOnMap.Count > 2) listHeroOnMap[2].Say("Нанесем последний\nудар!!!", TypeCharacter.Герой);

                    boss.healthBar.Value = 1;

                    boss.isDamageImmune = false;

                    inFaze = false;

                    HideTimer();

                    game.updateGame -= faza;
                }

                //время фазы
                time -= gameTime.ElapsedGameTime.Milliseconds;
                updateTimer();

                if (time <= 0)
                {
                    effect.Stop();
                    stamp_1.Hide();
                    stamp_2.Hide();
                    stamp_3.Hide();
                    HideTimer();

                    boss.isDamageImmune = false;

                    boss.Say("Вам меня не\nодолеть!!!", TypeCharacter.Враг);

                    boss.healthBar.Value = boss.MaxHealthPoint;

                    inFaze = false;
                }
            }
        }
    }
}
