﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public abstract class BossMap : Map
    {
        public AbstractEnemy boss;

        protected BossProtection effect;
        protected bool inFaze = false;

        protected string time_str = "";
        protected int time = 0;
        private float fontSize = 0.25f * Camera.scaleWnd;
        private Vector2 positionTimer;
        private float zIndexTimer = 1;

        public BossMap(MyGame game, EventHandler mouseLeftButtonDown) : base(game, mouseLeftButtonDown)
        {
        }

        protected override void loadContent()
        {
            textureFon = game.Content.Load<Texture2D>("map/backgroundHD");
            song = game.Content.Load<Song>("Songs/ConfrontBattle");
            textureMap = game.Content.Load<Texture2D>("map/bossMap");

            sizeMap.X = textureMap.Width;
            sizeMap.Y = textureMap.Height;

            scaleMap = 2.5f;
        }

        public override void Hide()
        {
            game.updateGame -= faza;
            HideTimer();
            base.Hide();
        }

        public override void Restart()
        {
            inFaze = false;
            time = 0;
            effect.Stop();
            base.Restart();
        }

        protected void ShowTimer()
        {
            positionTimer = new Vector2(0, 110 * Camera.scaleWnd);

            game.drawInterface -= drawTimer;
            game.drawInterface += drawTimer;
        }

        protected void HideTimer()
        {
            game.drawInterface -= drawTimer;
        }

        protected void updateTimer()
        {
            int min = time / 60000;
            int sec = (time / 1000) % 60;
            int msec = time % 1000;

            time_str = min > 9 ? $"{min}" : $"0{min}";
            time_str += sec > 9 ? $" : {sec} : " : $" : 0{sec} : ";
            time_str += msec > 99 ? $"{msec}" : msec > 9 ? $"0{msec}" : $"00{msec}";

            Vector2 sizeTxt = GameFeature.Font.MeasureString(time_str) * fontSize;

            positionTimer.X = (Camera.sizeWnd.X - sizeTxt.X) / 2;
        }

        public override void Show()
        {
            if (boss.isAlive)
            {
                game.updateGame -= faza;
                game.updateGame += faza;
                if (inFaze) ShowTimer();
            }
            base.Show();
        }

        private void drawTimer(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(GameFeature.Font, time_str, positionTimer, Color.White, 0, Vector2.Zero, fontSize, SpriteEffects.None, zIndexTimer);
        }

        protected abstract void faza(GameTime gameTime);

        protected override void loadMap()
        {
            listBorders.AddRange(new List<Rectangle>
            {
               new Rectangle(0,200,300,800),
               new Rectangle(300,150,150,300),
               new Rectangle(450,50,100,300),
               new Rectangle(550,0,420,300),
               new Rectangle(970,70,60,300),
               new Rectangle(1030,170,150,300),
               new Rectangle(1180,250,150,300),
               new Rectangle(1330,350,150,300),
               new Rectangle(1480,400,100,300),
               new Rectangle(1580,450,200,300),
               new Rectangle(1780,530,70,300),
               new Rectangle(1850,600,2100,300),
               new Rectangle(3350,900,300,100),
               new Rectangle(3950,400,740,300),

               //низ
               new Rectangle(300,720,140,300),
               new Rectangle(440,830,260,300),
               new Rectangle(700,900,100,300),
               new Rectangle(800,950,80,300),
               new Rectangle(880,1000,100,300),
               new Rectangle(980,1050,100,300),
               new Rectangle(1080,1150,70,300),
               new Rectangle(1150,1200,200,900),
               new Rectangle(1350,1900,100,300),
               new Rectangle(1450,2050,200,300),
               new Rectangle(1650,2150,200,300),
               new Rectangle(1850,2250,1600,300),
               new Rectangle(3450,2100,170,300),
               new Rectangle(3620,1950,160,400),
               new Rectangle(3780,1400,240,850),
               new Rectangle(4020,1300,140,300),
               new Rectangle(4160,1200,400,300),
               new Rectangle(4560,1150,130,300),
               new Rectangle(4690,400,200,900)
            });
        }
    }
}
