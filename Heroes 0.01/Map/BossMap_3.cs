﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class BossMap_3 : BossMap
    {
        private List<Meteorit> listMeterorits;

        private int duration = 7000;

        private int delay = 25;

        private int time_1;

        private bool faza_1 = false;
        private bool canPizza = true;
        private int countPizza = 0;
        private int maxPizza = 9;

        private Pizza pizza;

        private Vector2 spawnPoint = new Vector2(2600, 2400);
        private Vector2 size =  new Vector2(1400, 1200);

        public BossMap_3(MyGame game, EventHandler mouseLeftButtonDown) : base(game, mouseLeftButtonDown)
        {
        }

        protected override void faza(GameTime gameTime)
        {
            if (boss.healthBar.Value <= boss.healthBar.MaxValue * 0.5f && countPizza < maxPizza && canPizza)
            {
                if (!faza_1)
                {
                    boss.Say("Тьма, поглоти...\nВСЕ!!!", TypeCharacter.Враг);
                    faza_1 = true;
                    boss.setPosition(new Vector2(2400, 1200));
                    boss.isDamageImmune = true;
                    boss.canMove = false;
                    effect.Play();
                }

                if (boss.canMove)
                {
                    boss.setPosition(new Vector2(2400, 1200));
                    boss.canMove = false;
                }

                pizza = new Pizza(game, boss, SpriteEffects.None, boss.positionOnMap, (float)Math.PI / 4 * countPizza);
                pizza.end += Animation_animationEnd;
                canPizza = false;
                countPizza++;
            }

            if (boss.healthBar.Value <= boss.healthBar.MaxValue * 0.1f && time_1 <= duration)
            {
                if (!inFaze)
                {
                    boss.Say("Это конец...", TypeCharacter.Враг);
                    listMeterorits.Clear();
                    inFaze = true;
                    time_1 = 0;
                    time = 0;
                }

                time += gameTime.ElapsedGameTime.Milliseconds;
                time_1 += gameTime.ElapsedGameTime.Milliseconds;

                if (time >= delay)
                {
                    time -= delay;

                    Vector2 rnd = GameFeature.getPointInCircle();

                    Vector2 position = spawnPoint + size * rnd;

                    position.X *= GameFeature.scaleEllipse_X;
                    position.Y *= GameFeature.scaleEllipse_Y;

                    listMeterorits.Add(new Meteorit(game, boss, SpriteEffects.None, position));
                }
            }
        }

        private void Animation_animationEnd(object sender, EventArgs e)
        {
            Pizza pizza = sender as Pizza;
            pizza.end -= Animation_animationEnd;
            canPizza = true;

            if (countPizza >= maxPizza && boss.isDamageImmune)
            {
                boss.isDamageImmune = false;
                boss.canMove = true;
                effect.Stop();
            }
        }

        protected override void loadMap()
        {
            base.loadMap();

            listPortals.Add(new Portal(game, this, new Point(450, 250), SpriteEffects.None, "Великий лабиринт", 1));

            boss = new DemonBoss(game, new Vector2(2400, 1200));

            effect = new BossProtection(game, boss, sideAnimation.Front, SpriteEffects.None);

            listMeterorits = new List<Meteorit>();

            addCharacterOnMap(boss);
        }

        public override void Restart()
        {
            for (int i = 0; i < listMeterorits.Count; i++)
            {
                listMeterorits[i].animation?.Remove();
                listMeterorits[i].Remove();
            }

            if (pizza != null) pizza.Remove();
            effect.Stop();
            countPizza = 0;
            canPizza = true;
            time_1 = 0;

            listMeterorits.Clear();

            base.Restart();
        }
    }
}
