﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

namespace Heroes_0._01
{
    public class Maze : Map
    {
        private MazeCell[,] grid;
        private int countRow = 9;
        private int countColumn = 9;

        private Stack<Point> previousePosition;
        private Point currentPosition;

        private int currentCount = 0;
        private int countVisited = 1;
        private int maxCount = 0;
        private Point[] mazePath;

        public MazeCell firstPortalCell;
        private MazeCell nextFirstPortalCell;
        private int wall_1;

       // Cell test;

        public Maze(MyGame game, EventHandler mouseLeftButtonDown) : base(game, mouseLeftButtonDown)
        {
        }

        protected override void loadContent()
        {
            textureFon = game.Content.Load<Texture2D>("Characters/Shades/white");
            colorBackground = Color.Black;
            song = game.Content.Load<Song>("Songs/12 - Descent Into Madness");
            previousePosition = new Stack<Point>();

            scaleMap = 5;
            sizeMap = new Point(MazeCell.SizeCell.X * (countColumn), MazeCell.SizeCell.Y * (countRow));
        }

        protected override void loadMap()
        {
            previousePosition.Clear();

            grid = new MazeCell[countRow, countColumn];
            for (int i = 0; i < countRow; i++)
                for (int j = 0; j < countColumn; j++)
                    grid[i, j] = new MazeCell(game, new Point(j * MazeCell.SizeCell.X * (int)scaleMap, i * MazeCell.SizeCell.Y * (int)scaleMap), this, (int)scaleMap);

            generateMaze();

            //test = new Cell(game, Point.Zero, this, (int)scaleMap);
            //test.TopBorder = true;
            //test.LeftBorder = false;
            //test.RightBorder = false;
            //test.BotBorder = false;
        }

        public void removeMaze()
        {
            for (int i = 0; i < countRow; i++)
                for (int j = 0; j < countColumn; j++)
                    grid[i, j].Remove();
        }

        public void generateMaze()
        {
            previousePosition.Clear();
            currentPosition = new Point(0, 0);
            maxCount = 0;
            currentCount = 0;
            mazePath = new Point[countRow * countColumn];
            grid[0, 0].isVisited = true;
            grid[0, 0].Number = 0;
            countVisited = 1;

            while (countVisited != countRow * countColumn)
            {
                if (currentPosition.X > 0 && !grid[currentPosition.X - 1, currentPosition.Y].isVisited ||
                    currentPosition.X < countRow - 1 && !grid[currentPosition.X + 1, currentPosition.Y].isVisited ||
                    currentPosition.Y > 0 && !grid[currentPosition.X, currentPosition.Y - 1].isVisited ||
                    currentPosition.Y < countColumn - 1 && !grid[currentPosition.X, currentPosition.Y + 1].isVisited)
                {

                    switch (GameFeature.rand.Next(4))
                    {
                        case 0: //right
                            if (currentPosition.Y < countColumn - 1 && !grid[currentPosition.X, currentPosition.Y + 1].isVisited)
                            {
                                grid[currentPosition.X, currentPosition.Y].RightBorder = false;
                                previousePosition.Push(currentPosition);
                                currentPosition.Y++;
                                grid[currentPosition.X, currentPosition.Y].LeftBorder = false;

                                stepForward(currentPosition);
                            }
                            break;
                        case 1: //bot
                            if (currentPosition.X < countRow - 1 && !grid[currentPosition.X + 1, currentPosition.Y].isVisited)
                            {
                                grid[currentPosition.X, currentPosition.Y].BotBorder = false;
                                previousePosition.Push(currentPosition);
                                currentPosition.X++;
                                grid[currentPosition.X, currentPosition.Y].TopBorder = false;

                                stepForward(currentPosition);
                            }
                            break;
                        case 2://left
                            if (currentPosition.Y > 0 && !grid[currentPosition.X, currentPosition.Y - 1].isVisited)
                            {
                                grid[currentPosition.X, currentPosition.Y].LeftBorder = false;
                                previousePosition.Push(currentPosition);
                                currentPosition.Y--;
                                grid[currentPosition.X, currentPosition.Y].RightBorder = false;

                                stepForward(currentPosition);
                            }
                            break;
                        case 3: //top
                            if (currentPosition.X > 0 && !grid[currentPosition.X - 1, currentPosition.Y].isVisited)
                            {
                                grid[currentPosition.X, currentPosition.Y].TopBorder = false;
                                previousePosition.Push(currentPosition);
                                currentPosition.X--;
                                grid[currentPosition.X, currentPosition.Y].BotBorder = false;

                                stepForward(currentPosition);
                            }
                            break;
                    }
                }
                else if (previousePosition.Count > 0 && currentPosition != previousePosition.Peek())
                {
                    stepBack(currentPosition);
                    currentPosition = previousePosition.Pop();
                }
            }
            //for (int i = maxCount; i > 0; i--)
            //    grid[mazePath[i].X, mazePath[i].Y].color = new Color(255, 0, 0);

            int step = maxCount / 2;

            grid[mazePath[maxCount / 4].X, mazePath[maxCount / 4].Y].canSpawn = false;
            grid[mazePath[maxCount * 3 / 4].X, mazePath[maxCount * 3 / 4].Y].canSpawn = false;

            for (int i = 0; i < countRow; i++)
                for (int j = 0; j < countColumn; j++)
                    if (i > 1 || j > 1) grid[i, j].generateMob(maxCount);

            Point position = grid[0, 0].getPortalPosition(0);
            listPortals.Add(new Portal(game, this, position, SpriteEffects.None, "Выжженная земля", 1));

            firstPortalCell = grid[mazePath[ step].X, mazePath[step].Y];
            nextFirstPortalCell = grid[mazePath[step - 1].X, mazePath[step - 1].Y];
            wall_1 = closeWallAndGeneratePortal(step);
            position = grid[mazePath[step].X, mazePath[step].Y].getPortalPosition(wall_1);
            listPortals.Add(new Portal(game, this, position, wall_1 == 0 ? SpriteEffects.None : SpriteEffects.FlipHorizontally, "Покои босса", 1));

            int wall_2 = 0;
            if (mazePath[0].Y > mazePath[1].Y) wall_2 = 1;
            grid[mazePath[0].X, mazePath[0].Y].color = new Color(255, 255, 255);
            position = grid[mazePath[0].X, mazePath[0].Y].getPortalPosition(wall_2);
            listPortals.Add(new Portal(game, this, position, wall_2 == 0 ? SpriteEffects.None : SpriteEffects.FlipHorizontally, "Покои босса", 3));
        }

        public Vector2[] getThirdPortalSpawnPoint()
        {
            Vector2[] points = new Vector2[3];

            Point position = grid[mazePath[0].X, mazePath[0].Y].position;

            points[0].X = position.X + 600;
            points[0].Y = position.Y + 600;

            points[1].X = position.X + 600;
            points[1].Y = position.Y + 300;

            points[2].X = position.X + 300;
            points[2].Y = position.Y + 600;

            return points;
        }

        public Vector2[] getSecondPortalSpawnPoint()
        {
            Vector2[] points = new Vector2[3];

            Point position = grid[mazePath[maxCount/2].X, mazePath[maxCount/2].Y].position;

            points[0].X = position.X + 600;
            points[0].Y = position.Y + 600;

            points[1].X = position.X + 600;
            points[1].Y = position.Y + 300;

            points[2].X = position.X + 300;
            points[2].Y = position.Y + 600;

            return points;
        }

        public Vector2[] getFirstSellerPoint()
        {
            Vector2[] point = new Vector2[2];

            Point position = grid[mazePath[maxCount * 3 / 4].X, mazePath[maxCount * 3 / 4].Y].position;

            point[0].X = position.X + 300;
            point[0].Y = position.Y + 300;

            point[1].X = position.X + 300;
            point[1].Y = position.Y + 500;

            return point;
        }

        public Vector2[] getSecondSellerPoint()
        {
            Vector2[] point = new Vector2[2];

            Point position = grid[mazePath[maxCount / 4].X, mazePath[maxCount / 4].Y].position;

            point[0].X = position.X + 300;
            point[0].Y = position.Y + 300;

            point[1].X = position.X + 300;
            point[1].Y = position.Y + 500;

            return point;
        }


        public void openWall()
        {
            switch (wall_1)
                {
                    case 0:
                        firstPortalCell.LeftBorder = false;
                        nextFirstPortalCell.RightBorder = false;
                        break;
                    case 1:
                        firstPortalCell.BotBorder = false;
                        nextFirstPortalCell.TopBorder = false;
                        break;
                    case 2:
                        firstPortalCell.TopBorder = false;
                        nextFirstPortalCell.BotBorder = false;
                        break;
                    case 3:
                        firstPortalCell.RightBorder = false;
                        nextFirstPortalCell.LeftBorder = false;
                        break;
                }
        }

        private int closeWallAndGeneratePortal(int index)
        {
            MazeCell currentCell = grid[mazePath[index].X, mazePath[index].Y];
            MazeCell nextCell = grid[mazePath[index - 1].X, mazePath[index - 1].Y];

            if (mazePath[index - 1].X > mazePath[index].X)
            {
                currentCell.BotBorder = true;
                nextCell.TopBorder = true;
                return 1;
            }
            else if (mazePath[index - 1].X < mazePath[index].X)
            {
                currentCell.TopBorder = true;
                nextCell.BotBorder = true;
                return 2;
            }
            else if (mazePath[index - 1].Y > mazePath[index].Y)
            {
                currentCell.RightBorder = true;
                nextCell.LeftBorder = true;
                return 3;
            }
            else if (mazePath[index - 1].Y < mazePath[index].Y)
            {
                currentCell.LeftBorder = true;
                nextCell.RightBorder = true;
                return 0;
            }
            return 0;
        }

        private void stepForward(Point currentPosition)
        {
            currentCount++;
            countVisited++;
            grid[currentPosition.X, currentPosition.Y].isVisited = true;
            grid[currentPosition.X, currentPosition.Y].Number = currentCount;
            if (currentCount > maxCount)
            {
                maxCount = currentCount;
                mazePath[0] = currentPosition;
                previousePosition.CopyTo(mazePath, 1);
            }
        }
        private void stepBack(Point currentPosition)
        {
            currentCount--;
        }

        private bool allVisited()
        {
            for (int i = 0; i < countRow; i++)
                for (int j = 0; j < countColumn; j++)
                    if (!grid[i, j].isVisited) return false;

            return true;
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            game.drawGameObject -= draw;
        }

        public override void Restart()
        {
            removeMaze();
            base.Restart();
        }

        public override void Show()
        {
            base.Show();
            for (int i = 0; i < countRow; i++)
                for (int j = 0; j < countColumn; j++)
                    grid[i, j].isVisible = true;
        }

        public override void Hide()
        {
            base.Hide();
            for (int i = 0; i < countRow; i++)
                for (int j = 0; j < countColumn; j++)
                    grid[i, j].isVisible = false;
        }
    }
}
