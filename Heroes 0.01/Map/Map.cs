﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public abstract class Map : InteractiveObject
    {
        public List<AbstractCharacter> listHeroOnMap;
        public List<AbstractCharacter> listEnemyOnMap;
        public List<AbstractCharacter> listFriendOnMap;
        public List<Portal> listPortals;

        private EventHandler leftButtonDown;

        public List<Rectangle> listBorders;
        public List<AbstractItem> listItems;
        protected Color colorBackground = Color.White;

        protected Song song;

        protected Texture2D textureMap;
        protected Texture2D textureFon;
        protected Rectangle rectFon;

        private Vector2 position;

        public float scaleMap = 1;
        protected Point sizeMap;

        public Map(MyGame game, EventHandler mouseLeftButtonDown) : base(game)
        {
            this.game = game;

            leftButtonDown = mouseLeftButtonDown;

            Camera.map = this;
            InterfaceCore.map = this;

            position = Vector2.Zero;

            listItems = new List<AbstractItem>();
            listEnemyOnMap = new List<AbstractCharacter>();
            listFriendOnMap = new List<AbstractCharacter>();
            listHeroOnMap = new List<AbstractCharacter>();
            listPortals = new List<Portal>();
            listBorders = new List<Rectangle>();

            loadContent();
            loadMap();

            rectFon = new Rectangle(0, 0, game.Window.ClientBounds.Width, game.Window.ClientBounds.Height);
            drawField = new Rectangle((int)position.X, (int)position.Y, (int)(sizeMap.X * scaleMap), (int)(sizeMap.Y * scaleMap));
        }

        public virtual void Show()
        {
            MediaPlayer.Play(song);
            game.drawBackground -= drawBackground;
            game.drawBackground += drawBackground;
            isVisible = true;

            Camera.map = this;
            InterfaceCore.map = this;

            for (int i = 0; i < listHeroOnMap.Count; i++)
                listHeroOnMap[i].Show();

            for (int i = 0; i < listFriendOnMap.Count; i++)
                listFriendOnMap[i].Show();

            for (int i = 0; i < listEnemyOnMap.Count; i++)
                listEnemyOnMap[i].Show();

            for (int i = 0; i < listPortals.Count; i++)
                listPortals[i].Show();

            for (int i = 0; i < listItems.Count; i++)
                listItems[i].isVisible = true;

        }

        public virtual void Hide()
        {
            MediaPlayer.Stop();
            game.drawBackground -= drawBackground;

            isVisible = false;

            for (int i = 0; i < listHeroOnMap.Count; i++)
                listHeroOnMap[i].Hide();

            for (int i = 0; i < listFriendOnMap.Count; i++)
                listFriendOnMap[i].Hide();

            for (int i = 0; i < listEnemyOnMap.Count; i++)
                listEnemyOnMap[i].Hide();

            for (int i = 0; i < listPortals.Count; i++)
                listPortals[i].Hide();

            for (int i = 0; i < listItems.Count; i++)
            {
                listItems[i].isVisible = false;
            }
        }

        protected abstract void loadContent();

        protected abstract void loadMap();

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(textureMap, drawField, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, float.Epsilon);
        }
       
        protected virtual void drawBackground(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(textureFon, rectFon, null, colorBackground, 0, Vector2.Zero, SpriteEffects.None, 0);
        }

        public void addListCharactersOnMap(List<AbstractCharacter> listCharacters)
        {
            for (int i = 0; i < listCharacters.Count; ++i)
                addCharacterOnMap(listCharacters[i]);
        }

        public void addCharacterOnMap(AbstractCharacter character)
        {
            switch (character.typeCharacter)
            {
                case TypeCharacter.Друг:
                    if (!listFriendOnMap.Contains(character))
                    {
                        listFriendOnMap.Add(character);
                        character.mouseLeftButtonDown += leftButtonDown;
                        character.currentMap = this;
                    }
                    break;

                case TypeCharacter.Враг:
                    if (!listEnemyOnMap.Contains(character))
                    {
                        listEnemyOnMap.Add(character);
                        character.mouseLeftButtonDown += leftButtonDown;
                        character.died += onEnemyDie;
                        character.currentMap = this;
                    }
                    break;

                case TypeCharacter.Герой:
                    if (!listHeroOnMap.Contains(character) && character.isAlive)
                    {
                        listHeroOnMap.Add(character);
                        character.currentMap = this;
                    }
                    break;
            }
        }

        public void removeListCharctersFromMap(List<AbstractCharacter> listCharacters)
        {
            for (int i = 0; i < listCharacters.Count; ++i)
                removeCharacterFromMap(listCharacters[i]);
        }

        public void removeCharacterFromMap(AbstractCharacter character)
        {
            switch (character.typeCharacter)
            {
                case TypeCharacter.Друг:
                    listFriendOnMap.Remove(character);
                    character.mouseLeftButtonDown -= leftButtonDown;
                    character.currentMap = null;
                    break;

                case TypeCharacter.Враг:
                    listEnemyOnMap.Remove(character);
                    character.died -= onEnemyDie;
                    character.mouseLeftButtonDown -= leftButtonDown;
                    character.currentMap = null;
                    break;
                case TypeCharacter.Герой:
                    if (character.isAlive)
                    {
                        listHeroOnMap.Remove(character);
                        character.currentMap = null;
                    }
                    break;
            }
        }

        public void removeEnemyFromMap()
        {
            for (int i = listEnemyOnMap.Count - 1; i >= 0; i--)
            {
                listEnemyOnMap[i].Remove();
                removeCharacterFromMap(listEnemyOnMap[i]);
            }

            listEnemyOnMap.Clear();
        }

        public void removeNPCFromMap()
        {
            for (int i = listFriendOnMap.Count - 1; i >= 0; i--)
            {
                listFriendOnMap[i].Remove();
                removeCharacterFromMap(listFriendOnMap[i]);
            }

            listFriendOnMap.Clear();
        }

        public void removeItemsFromMap()
        {
            for (int i = listItems.Count - 1; i >= 0; i--)
                listItems[i].Remove();

            listItems.Clear();
        }

        public void removePortalsFromMap()
        {
            for (int i = 0; i < listPortals.Count; i++)
                listPortals[i].Remove();
            listPortals.Clear();
        }

        public virtual void Restart()
        {
            removeEnemyFromMap();
            removePortalsFromMap();
            removeItemsFromMap();
            removeNPCFromMap();
            loadMap();
        }

        private void onEnemyDie(object o, EventArgs e)
        {
            AbstractCharacter character = o as AbstractCharacter;
            character.died -= onEnemyDie;
            listEnemyOnMap.Remove(character);
        }

        protected Texture2D textureBorder;
        protected void borderVisible()
        {
            textureBorder = game.Content.Load<Texture2D>("Characters/Shades/select");
            game.drawGameObject += drawBorder;
        }

        protected void drawBorder(GameTime gameTime, SpriteBatch spriteBatch)
        {
            for (int i = 0; i < listBorders.Count; ++i)
                spriteBatch.Draw(textureBorder, listBorders[i], null, Color.White * 0.2f, 0, Vector2.Zero, SpriteEffects.None, 1);
        }

    }
}
