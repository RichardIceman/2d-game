﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class BossMap_2 : BossMap
    {
        private List<AbstractCharacter> listGuardians;
        private List<AbstractAnimations> listConnections;

        private int radiusSpawn = 400;
        private int countGuardians = 5;

        private Vector2 bossSpawn = new Vector2(2600, 1350);

        public BossMap_2(MyGame game, EventHandler mouseLeftButtonDown) : base(game, mouseLeftButtonDown) { }

        protected override void faza(GameTime gameTime)
        {
           if (boss.healthBar.Value <= boss.healthBar.MaxValue * 0.3f && !inFaze)
            {
                inFaze = true;

                new IceWallEffect(game, zIndex + GameFeature.zIndexStep, (boss.positionOnMap - new Vector2(150, 0)).ToPoint(), SpriteEffects.None).Play();
                boss.canMove = false;
                boss.setPosition(bossSpawn);
                new IceWallEffect(game, zIndex + GameFeature.zIndexStep, (boss.positionOnMap - new Vector2(150, 0)).ToPoint(), SpriteEffects.None).Play();

                effect.Play();

                boss.Say("Восстаньте, мои\nподданые!!!", TypeCharacter.Враг);

                boss.isDamageImmune = true;

                float stepAngle = 2 * (float)Math.PI / countGuardians;
                float angle = 0;

                for (int i = 0; i < countGuardians; i++)
                {
                    Vector2 position = bossSpawn + new Vector2(radiusSpawn * (float)Math.Cos(angle), radiusSpawn * (float)Math.Sin(angle));
                    angle += stepAngle;

                    AbstractCharacter newGuardian = new GolemGuardian(game, position);
                    AbstractAnimations newConnection = new ConnectionAnimation(game, newGuardian, boss);

                    newGuardian.died += NewGuardian_died;

                    position = newGuardian.positionOnMap;
                    position.X -= 150;

                    new IceWallEffect(game, newGuardian.zIndex + GameFeature.zIndexStep, position.ToPoint(), SpriteEffects.None).Play();

                    newConnection.Play();

                    addCharacterOnMap(newGuardian);

                    listGuardians.Add(newGuardian);
                    listConnections.Add(newConnection);
                }
            }
        }

        private void NewGuardian_died(object sender, EventArgs e)
        {
            AbstractCharacter guardian = sender as AbstractCharacter;

            guardian.died -= NewGuardian_died;

            int index = listGuardians.IndexOf(guardian);
            listConnections[index].Remove();
            listConnections.RemoveAt(index);
            listGuardians.RemoveAt(index);

            if (listGuardians.Count == 0)
            {
                effect.Stop();
                boss.isDamageImmune = false;
                boss.canMove = true;
            }
        }

        protected override void loadMap()
        {
            base.loadMap();

            listPortals.Add(new Portal(game, this, new Point(450, 250), SpriteEffects.None, "Великий лабиринт", 1));
            listPortals.Add(new Portal(game, this, new Point(4150, 600), SpriteEffects.FlipHorizontally, "Великий лабиринт", 1));

            listPortals[1].isActive = false;

            boss = new GolemBoss(game, bossSpawn);

            effect = new BossProtection(game, boss, sideAnimation.Front, SpriteEffects.None);
            effect.color = Color.Blue;

            listGuardians = new List<AbstractCharacter>();
            listConnections = new List<AbstractAnimations>();

            addCharacterOnMap(boss);

            boss.died += Boss_died;
        }

        private void Boss_died(object sender, EventArgs e)
        {
            boss.died -= Boss_died;
            listPortals[1].isActive = true;
        }

        public override void Restart()
        {
            for (int i = 0; i < listConnections.Count; i++)
                listConnections[i].Remove();

            listConnections.Clear();
            listGuardians.Clear();
            base.Restart();
        }
    }
}
