﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Stamp
    {
        private MyGame game;

        private Map map;

        private Texture2D light;

        private Rectangle lightRect;

        private int diametrLight = 1000;

        private int time;
        private int delay = 70;

        private Random rand;

        private Rectangle stampField;

        private StampAnimation animation;

        private Point position;

        public bool isActive;

        public Stamp(MyGame game, Point position, Map map)
        {
            this.game = game;

            this.map = map;

            this.position = position;

            light = game.Content.Load<Texture2D>("Characters/Shades/noir");

            rand = new Random();

            animation = new StampAnimation(game, position);

            stampField = new Rectangle(position.X + 100, position.Y + 200, 100, 70);

            lightRect = new Rectangle(position.X - diametrLight / 2,
                                      position.Y - diametrLight * 4 / 15,
                                      (int)(diametrLight * GameFeature.scaleEllipse_X),
                                      (int)(diametrLight * GameFeature.scaleEllipse_Y));

            game.drawLight += drawLight;
            game.updateGame += update;
        }

        private void drawLight(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(light, lightRect, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 1);
        }

        private void update(GameTime gameTime)
        {
            isActive = false;

            if (map != null)
            {
                for (int i = 0; i < map.listHeroOnMap.Count; i++)
                    if (map.listHeroOnMap[i].shade.Intersects(stampField))
                    {
                        isActive = true;
                        break;
                    }
            }

            time += gameTime.ElapsedGameTime.Milliseconds;
            if (time >= delay)
            {
                time -= delay;

                int rnd_x = 10 - rand.Next(20);
                int rnd_y = 10 - rand.Next(20);

                lightRect = new Rectangle(position.X - diametrLight / 3 + rnd_x,
                                      position.Y - diametrLight * 2 / 15 + rnd_y,
                                      (int)(diametrLight * GameFeature.scaleEllipse_X + rnd_x * 2),
                                      (int)(diametrLight * GameFeature.scaleEllipse_Y + rnd_y * 2));
            }
        }

        public void Show()
        {
            Hide();
            animation.isVisible = true;
            game.drawLight += drawLight;
            game.updateGame += update;
        }

        public void Hide()
        {
            if (animation != null) animation.isVisible = false;
            game.drawLight -= drawLight;
            game.updateGame -= update;
        }

        public void Remove()
        {
            Hide();
            animation.isVisible = false;
            game = null;
            map = null;
            animation.Remove();
            animation = null;
        }
    }
}
