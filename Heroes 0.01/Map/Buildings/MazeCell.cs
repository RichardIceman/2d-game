﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class MazeCell
    {
        private Map map;
        private MyGame game;
        public bool isVisited { get; set; }
        private Point wallShift = new Point(28, 27);
        public static Point SizeCell = new Point(216, 197);
        private Point scaleSizeCell;
        private int scaleCell;
        private float botZindex = 0.98f;

        public bool canSpawn = true;

        public Point position;

        private bool isvisible = false;
        public bool isVisible
        {
            get { return isvisible; }
            set
            {
                if (isvisible != value)
                {
                    isvisible = value;
                    if (isvisible)
                        game.drawGameObject += draw;
                    else
                        game.drawGameObject -= draw;
                }
            }
        }


        private int number = 0;
        public int Number
        {
            get { return number; }
            set
            {
                number = value;
            }
        }

        private bool left = true;
        public bool LeftBorder
        {
            get { return left; }
            set
            {
                left = value;
                if (value && !map.listBorders.Contains(W))
                {
                    map.listBorders.Remove(NW);
                    NW.Height = leftBorderRect_3.Height + leftBorderRect_4.Height;
                    map.listBorders.Add(NW);
                    map.listBorders.Add(W);
                }
                else if (!value)
                {
                    map.listBorders.Remove(NW);
                    NW.Height = leftBorderRect_3.Height + leftBorderRect_4.Height + wall2[0,0].Height * 2;
                    map.listBorders.Add(NW);
                    map.listBorders.Remove(W);
                }
            }
        }
        private bool top = false;
        public bool TopBorder
        {
            get { return top; }
            set
            {
                top = value;
                if (value && !map.listBorders.Contains(N))
                {
                    N.Height = leftBorderRect_3.Height + leftBorderRect_4.Height + wall1[0, 0].Height * 2;
                    map.listBorders.Add(N);
                }
                else if (!value)
                {            
                    map.listBorders.Remove(N);
                    N.Height = leftBorderRect_3.Height + leftBorderRect_4.Height;
                }
            }
        }
        private bool right = true;
        public bool RightBorder
        {
            get { return right; }
            set
            {
                right = value;
                if (value && !map.listBorders.Contains(E))
                {
                    map.listBorders.Remove(NE);
                    NE.Height = leftBorderRect_3.Height + leftBorderRect_4.Height;
                    map.listBorders.Add(NE);
                    map.listBorders.Add(E);
                }
                else if (!value)
                {
                    map.listBorders.Remove(NE);
                    NE.Height = leftBorderRect_3.Height + leftBorderRect_4.Height + wall2[0, 0].Height * 2;
                    map.listBorders.Add(NE);
                    map.listBorders.Remove(E);
                }
            }
        }
        private bool bot = true;
        public bool BotBorder
        {
            get { return bot; }
            set
            {
                bot = value;
                if (value && !map.listBorders.Contains(SBorder))
                    map.listBorders.Add(SBorder);
                else if (!value)
                    map.listBorders.Remove(SBorder);
            }
        }

        private Rectangle cellField;
        private Texture2D whiteTxt;

        public List<Rectangle> borders;
        private Texture2D texture;
        private Rectangle textureLeftBorder_1;
        private Rectangle textureLeftBorder_2;
        private Rectangle textureRightBorder_1;
        private Rectangle textureRightBorder_2;
        private Rectangle textureBottomBorder_1;
        private Rectangle textureBottomBorder_2;
        private Rectangle textureTopBorder;
        private Rectangle textureFloor_1;
        private Rectangle textureFloor_2;
        private Rectangle textureFloor_3;
        private Rectangle textureWall;

        private Rectangle leftBorderRect_1;
        private Rectangle leftBorderRect_2;
        private Rectangle leftBorderRect_3;
        private Rectangle leftBorderRect_4;
        private Rectangle leftBorderRect_5;
        private Rectangle leftBorderRect_6;

        private Rectangle topBorderRect_1;
        private Rectangle topBorderRect_2;
        private Rectangle topBorderRect_3;
        private Rectangle topBorderRect_4;
        private Rectangle topBorderRect_5;
        private Rectangle topBorderRect_6;

        private Rectangle rightBorderRect_1;
        private Rectangle rightBorderRect_2;
        private Rectangle rightBorderRect_3;
        private Rectangle rightBorderRect_4;
        private Rectangle rightBorderRect_5;
        private Rectangle rightBorderRect_6;

        private Rectangle bottomBorderRect_1;
        private Rectangle bottomBorderRect_2;
        private Rectangle bottomBorderRect_3;
        private Rectangle bottomBorderRect_4;
        private Rectangle bottomBorderRect_5;
        private Rectangle bottomBorderRect_6;


        private Rectangle[,] wall1;
        private Rectangle[,] wall2;
        private Rectangle[,] wall3;
        private Rectangle[,] floor1;
        private Rectangle[,] floor2;
        private Rectangle[] floor3;
        private Rectangle[] floor4;
        private Rectangle[] floor5;

        private Rectangle NW;
        private Rectangle NE;
        private Rectangle SE;
        private Rectangle SW;
        private Rectangle W;
        private Rectangle N;
        private Rectangle E;
        private Rectangle S;
        private Rectangle SBorder;
        private Rectangle SEBorder;
        private Rectangle SWBorder;

        public Color color = Color.White;

        public MazeCell(MyGame game, Point position, Map map, int scaleCell)
        {
            this.game = game;
            this.map = map;
            this.position = position;
            this.scaleCell = scaleCell;
            scaleSizeCell.X = SizeCell.X * scaleCell;
            scaleSizeCell.Y = SizeCell.Y * scaleCell;
            isVisited = false;
            whiteTxt = game.Content.Load<Texture2D>("Characters/Shades/white");
            textureWall = new Rectangle(16, 144, 16, 16);
            textureFloor_1 = new Rectangle(16, 48, 48, 48);
            textureFloor_2 = new Rectangle(16, 80, 48, 16);
            textureFloor_3 = new Rectangle(64, 32, 16, 48);

            texture = game.Content.Load<Texture2D>("map/Tile Set");
            textureLeftBorder_2 = new Rectangle(96, 27, 4, 69);
            textureLeftBorder_1 = new Rectangle(96, 31, 4, 16);
            textureRightBorder_2 = new Rectangle(172, 27, 4, 69);
            textureRightBorder_1 = new Rectangle(172, 31, 4, 16);
            textureBottomBorder_1 = new Rectangle(191, 27, 16, 5);
            textureBottomBorder_2 = new Rectangle(96, 91, 80, 5);
            textureTopBorder = new Rectangle(96, 27, 80, 4);

            cellField = new Rectangle(position.X, position.Y, SizeCell.X * scaleCell, SizeCell.Y * scaleCell);

            leftBorderRect_1 = new Rectangle(position.X + wallShift.X * scaleCell,
                                             position.Y + wallShift.Y * scaleCell,
                                             textureLeftBorder_2.Width * scaleCell,
                                             textureLeftBorder_2.Height * scaleCell);
            leftBorderRect_2 = new Rectangle(position.X + wallShift.X * scaleCell,
                                             position.Y + wallShift.Y * scaleCell + leftBorderRect_1.Height,
                                             textureLeftBorder_2.Width * scaleCell,
                                             textureLeftBorder_2.Height * scaleCell);
            leftBorderRect_3 = new Rectangle(position.X + wallShift.X * scaleCell,
                                             position.Y,
                                             textureLeftBorder_1.Width * scaleCell,
                                             textureLeftBorder_1.Height * scaleCell);
            leftBorderRect_4 = new Rectangle(position.X + wallShift.X * scaleCell,
                                             position.Y + leftBorderRect_3.Height,
                                             textureLeftBorder_1.Width * scaleCell,
                                             textureLeftBorder_1.Height * scaleCell);
            leftBorderRect_5 = new Rectangle(position.X + wallShift.X * scaleCell,
                                             position.Y + wallShift.Y * scaleCell + leftBorderRect_1.Height + leftBorderRect_2.Height,
                                             textureLeftBorder_1.Width * scaleCell,
                                             textureLeftBorder_1.Height * scaleCell);
            leftBorderRect_6 = new Rectangle(position.X + wallShift.X * scaleCell,
                                             position.Y + wallShift.Y * scaleCell + leftBorderRect_1.Height + leftBorderRect_2.Height + leftBorderRect_5.Height,
                                             textureLeftBorder_1.Width * scaleCell,
                                             textureLeftBorder_1.Height * scaleCell);

            topBorderRect_1 = new Rectangle(position.X + wallShift.X * scaleCell,
                                            position.Y + wallShift.Y * scaleCell,
                                            textureBottomBorder_2.Width * scaleCell,
                                            textureBottomBorder_2.Height * scaleCell);
            topBorderRect_2 = new Rectangle(position.X + wallShift.X * scaleCell + topBorderRect_1.Width,
                                            position.Y + wallShift.Y * scaleCell,
                                            textureBottomBorder_2.Width * scaleCell,
                                            textureBottomBorder_2.Height * scaleCell);
            topBorderRect_3 = new Rectangle(position.X,
                                            position.Y + wallShift.Y * scaleCell,
                                            textureBottomBorder_1.Width * scaleCell,
                                            textureBottomBorder_1.Height * scaleCell);
            topBorderRect_4 = new Rectangle(position.X + topBorderRect_3.Width,
                                            position.Y + wallShift.Y * scaleCell,
                                            textureBottomBorder_1.Width * scaleCell,
                                            textureBottomBorder_1.Height * scaleCell);
            topBorderRect_5 = new Rectangle(position.X + wallShift.X * scaleCell + topBorderRect_1.Width + topBorderRect_2.Width - textureLeftBorder_1.Width * scaleCell,
                                            position.Y + wallShift.Y * scaleCell,
                                            textureBottomBorder_1.Width * scaleCell,
                                            textureBottomBorder_1.Height * scaleCell);
            topBorderRect_6 = new Rectangle(position.X + wallShift.X * scaleCell + topBorderRect_1.Width + topBorderRect_2.Width - textureLeftBorder_1.Width * scaleCell + topBorderRect_5.Width,
                                            position.Y + wallShift.Y * scaleCell,
                                            textureBottomBorder_1.Width * scaleCell,
                                            textureBottomBorder_1.Height * scaleCell);

            rightBorderRect_1 = new Rectangle(position.X + wallShift.X * scaleCell + topBorderRect_1.Width + topBorderRect_2.Width - textureRightBorder_2.Width * scaleCell,
                                              position.Y + wallShift.Y * scaleCell,
                                              textureRightBorder_2.Width * scaleCell,
                                              textureRightBorder_2.Height * scaleCell);
            rightBorderRect_2 = new Rectangle(position.X + wallShift.X * scaleCell + topBorderRect_1.Width + topBorderRect_2.Width - textureRightBorder_2.Width * scaleCell,
                                              position.Y + wallShift.Y * scaleCell + rightBorderRect_1.Height,
                                              textureRightBorder_2.Width * scaleCell,
                                              textureRightBorder_2.Height * scaleCell);
            rightBorderRect_3 = new Rectangle(position.X + wallShift.X * scaleCell + topBorderRect_1.Width + topBorderRect_2.Width - textureRightBorder_2.Width * scaleCell,
                                              position.Y,
                                              textureRightBorder_1.Width * scaleCell,
                                              textureRightBorder_1.Height * scaleCell);
            rightBorderRect_4 = new Rectangle(position.X + wallShift.X * scaleCell + topBorderRect_1.Width + topBorderRect_2.Width - textureRightBorder_2.Width * scaleCell,
                                              position.Y + rightBorderRect_3.Height,
                                              textureRightBorder_1.Width * scaleCell,
                                              textureRightBorder_1.Height * scaleCell);
            rightBorderRect_5 = new Rectangle(position.X + wallShift.X * scaleCell + topBorderRect_1.Width + topBorderRect_2.Width - textureRightBorder_2.Width * scaleCell,
                                              position.Y + wallShift.Y * scaleCell + rightBorderRect_1.Height + rightBorderRect_2.Height,
                                              textureRightBorder_1.Width * scaleCell,
                                              textureRightBorder_1.Height * scaleCell);
            rightBorderRect_6 = new Rectangle(position.X + wallShift.X * scaleCell + topBorderRect_1.Width + topBorderRect_2.Width - textureRightBorder_2.Width * scaleCell,
                                              position.Y + wallShift.Y * scaleCell + rightBorderRect_1.Height + rightBorderRect_2.Height + rightBorderRect_5.Height,
                                              textureRightBorder_1.Width * scaleCell,
                                              textureRightBorder_1.Height * scaleCell);

            bottomBorderRect_1 = new Rectangle(position.X + wallShift.X * scaleCell,
                                               position.Y + wallShift.Y * scaleCell + leftBorderRect_1.Height + leftBorderRect_2.Height,
                                               textureBottomBorder_2.Width * scaleCell,
                                               textureBottomBorder_2.Height * scaleCell);
            bottomBorderRect_2 = new Rectangle(position.X + wallShift.X * scaleCell + bottomBorderRect_1.Width,
                                               position.Y + wallShift.Y * scaleCell + leftBorderRect_1.Height + leftBorderRect_2.Height,
                                               textureBottomBorder_2.Width * scaleCell,
                                               textureBottomBorder_2.Height * scaleCell);
            bottomBorderRect_3 = new Rectangle(position.X,
                                               position.Y + wallShift.Y * scaleCell + leftBorderRect_1.Height + leftBorderRect_2.Height,
                                               textureBottomBorder_1.Width * scaleCell,
                                               textureBottomBorder_1.Height * scaleCell);
            bottomBorderRect_4 = new Rectangle(position.X + bottomBorderRect_3.Width,
                                              position.Y + wallShift.Y * scaleCell + leftBorderRect_1.Height + leftBorderRect_2.Height,
                                              textureBottomBorder_1.Width * scaleCell,
                                              textureBottomBorder_1.Height * scaleCell);
            bottomBorderRect_5 = new Rectangle(position.X + wallShift.X * scaleCell + bottomBorderRect_1.Width + bottomBorderRect_2.Width - textureLeftBorder_1.Width * scaleCell,
                                              position.Y + wallShift.Y * scaleCell + leftBorderRect_1.Height + leftBorderRect_2.Height,
                                              textureBottomBorder_1.Width * scaleCell,
                                              textureBottomBorder_1.Height * scaleCell);
            bottomBorderRect_6 = new Rectangle(position.X + wallShift.X * scaleCell + bottomBorderRect_1.Width + bottomBorderRect_2.Width - textureLeftBorder_1.Width * scaleCell + bottomBorderRect_5.Width,
                                             position.Y + wallShift.Y * scaleCell + leftBorderRect_1.Height + leftBorderRect_2.Height,
                                             textureBottomBorder_1.Width * scaleCell,
                                             textureBottomBorder_1.Height * scaleCell);

            NW = new Rectangle(position.X,
                               position.Y,
                               wallShift.X * scaleCell + textureLeftBorder_1.Width * scaleCell,
                               wallShift.Y * scaleCell + textureBottomBorder_1.Height * scaleCell);
            NE = new Rectangle(rightBorderRect_3.X,
                               position.Y,
                                wallShift.X * scaleCell + textureLeftBorder_1.Width * scaleCell,
                               wallShift.Y * scaleCell + textureBottomBorder_1.Height * scaleCell);
            SE = new Rectangle(rightBorderRect_5.X,
                               rightBorderRect_5.Y,
                                wallShift.X * scaleCell + textureLeftBorder_1.Width * scaleCell,
                               wallShift.Y * scaleCell + textureBottomBorder_1.Height * scaleCell);
            SEBorder = new Rectangle(rightBorderRect_5.X,
                                     rightBorderRect_5.Y + (wallShift.Y * scaleCell + textureBottomBorder_1.Height * scaleCell) / 2,
                                     wallShift.X * scaleCell + textureLeftBorder_1.Width * scaleCell,
                                     (wallShift.Y * scaleCell + textureBottomBorder_1.Height * scaleCell) / 2);
            SW = new Rectangle(position.X,
                               rightBorderRect_5.Y,
                               wallShift.X * scaleCell + textureLeftBorder_1.Width * scaleCell,
                               wallShift.Y * scaleCell + textureBottomBorder_1.Height * scaleCell);
            SWBorder = new Rectangle(position.X,
                                     rightBorderRect_5.Y + (wallShift.Y * scaleCell + textureBottomBorder_1.Height * scaleCell) / 2,
                                     wallShift.X * scaleCell + textureLeftBorder_1.Width * scaleCell,
                                     (wallShift.Y * scaleCell + textureBottomBorder_1.Height * scaleCell) / 2);

            W = new Rectangle(position.X,
                              topBorderRect_3.Y + topBorderRect_3.Height,
                              topBorderRect_3.Width + topBorderRect_4.Width,
                              leftBorderRect_1.Height + leftBorderRect_2.Height - topBorderRect_3.Height + (wallShift.Y * scaleCell + textureBottomBorder_1.Height * scaleCell) / 2);
            N = new Rectangle(leftBorderRect_3.X + textureLeftBorder_1.Width * scaleCell,
                              position.Y,
                              topBorderRect_1.Width + topBorderRect_2.Width - 2 * textureLeftBorder_1.Width * scaleCell,
                              leftBorderRect_3.Height + leftBorderRect_4.Height);
            E = new Rectangle(rightBorderRect_1.X,
                             rightBorderRect_1.Y + topBorderRect_5.Height,
                             topBorderRect_5.Width + topBorderRect_6.Width,
                             rightBorderRect_1.Height + rightBorderRect_2.Height - topBorderRect_5.Height + (wallShift.Y * scaleCell + textureBottomBorder_1.Height * scaleCell) / 2);
            S = new Rectangle(bottomBorderRect_1.X + textureLeftBorder_1.Width * scaleCell,
                              bottomBorderRect_1.Y,
                              bottomBorderRect_1.Width + bottomBorderRect_2.Width - 2 * textureLeftBorder_1.Width * scaleCell,
                              leftBorderRect_3.Height + leftBorderRect_4.Height);

            SBorder = new Rectangle(bottomBorderRect_1.X + textureLeftBorder_1.Width * scaleCell,
                                    bottomBorderRect_1.Y + (leftBorderRect_3.Height + leftBorderRect_4.Height) / 2,
                                    bottomBorderRect_1.Width + bottomBorderRect_2.Width - 2 * textureLeftBorder_1.Width * scaleCell,
                                    (leftBorderRect_3.Height + leftBorderRect_4.Height) / 2);

            int widthWall = (topBorderRect_1.Width + topBorderRect_2.Width - 2 * leftBorderRect_1.Width) / 10;
            wall1 = new Rectangle[2, 10];
            for (int j = 0; j < 2; j++)
                for (int i = 0; i < 10; i++)
                    wall1[j, i] = new Rectangle(topBorderRect_1.X + leftBorderRect_1.Width + i * widthWall,
                                              topBorderRect_1.Y + topBorderRect_1.Height + j * textureWall.Height * scaleCell,
                                              widthWall,
                                              textureWall.Height * scaleCell);

            wall2 = new Rectangle[2, 2];
            for (int j = 0; j < 2; j++)
                for (int i = 0; i < 2; i++)
                    wall2[j, i] = new Rectangle(position.X + i * textureWall.Width * scaleCell,
                                              topBorderRect_1.Y + topBorderRect_1.Height + j * textureWall.Height * scaleCell,
                                              textureWall.Width * scaleCell,
                                              textureWall.Height * scaleCell);
            wall3 = new Rectangle[2, 2];
            for (int j = 0; j < 2; j++)
                for (int i = 0; i < 2; i++)
                    wall3[j, i] = new Rectangle(wall1[0,9].X + wall1[0, 9].Width + i * textureWall.Width * scaleCell,
                                              topBorderRect_1.Y + topBorderRect_1.Height + j * textureWall.Height * scaleCell,
                                              textureWall.Width * scaleCell,
                                              textureWall.Height * scaleCell);

            int floorWidth = (topBorderRect_1.Width + topBorderRect_2.Width - 2 * leftBorderRect_1.Width) / 4;
            int floorHeight = (leftBorderRect_1.Height + leftBorderRect_2.Height - 2 * wall1[0,0].Height - bottomBorderRect_1.Height) / 3;

            floor1 = new Rectangle[3, 4];
            for (int j = 0; j < 3; j++)
                for (int i = 0; i < 4; i++)
                    floor1[j,i] = new Rectangle(leftBorderRect_1.X + leftBorderRect_1.Width + i * floorWidth,
                                                leftBorderRect_1.Y + topBorderRect_1.Height + 2 * wall1[0, 0].Height + j * floorHeight,
                                                floorWidth,
                                                floorHeight);

            floorHeight = (leftBorderRect_3.Height + leftBorderRect_4.Height + 2 * wall1[0,0].Height) / 2;

            floor2 = new Rectangle[2, 4];
            for (int j = 0; j < 2; j++)
                for (int i = 0; i < 4; i++)
                    floor2[j, i] = new Rectangle(leftBorderRect_1.X + leftBorderRect_1.Width + i * floorWidth,
                                                position.Y + j * floorHeight,
                                                floorWidth,
                                                floorHeight);

            floorWidth = topBorderRect_3.Width + topBorderRect_4.Width;
            floorHeight = (leftBorderRect_1.Height + leftBorderRect_2.Height - 2 * wall1[0, 0].Height - bottomBorderRect_1.Height) / 3;

            floor3 = new Rectangle[3];
            for (int i = 0; i < 3; i++)
                floor3[i] = new Rectangle(position.X,
                                          topBorderRect_3.Y + topBorderRect_3.Height + 2 * wall2[0,0].Height + i * floorHeight,
                                          floorWidth,
                                          floorHeight);

            floorWidth = topBorderRect_5.Width + topBorderRect_6.Width;
            floor4 = new Rectangle[3];
            for (int i = 0; i < 3; i++)
                floor4[i] = new Rectangle(topBorderRect_5.X,
                                          topBorderRect_5.Y + topBorderRect_3.Height + 2 * wall2[0, 0].Height + i * floorHeight,
                                          floorWidth,
                                          floorHeight);

            floorWidth = floor1[0, 0].Width;
            floorHeight = leftBorderRect_5.Height + leftBorderRect_6.Height;
            floor5 = new Rectangle[4];
            for (int i = 0; i < 4; i++)
                floor5[i] = new Rectangle(leftBorderRect_5.X + leftBorderRect_5.Width + i * floorWidth,
                                          leftBorderRect_5.Y,
                                          floorWidth,
                                          floorHeight);

            map.listBorders.Add(NW);
            map.listBorders.Add(NE);
            map.listBorders.Add(SEBorder);
            map.listBorders.Add(SWBorder);
            LeftBorder = true;
            TopBorder = true;
            RightBorder = true;
            BotBorder = true;

            isVisible = true;
        }

        protected void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (-position.X > Camera.positionCamera.X - Camera.zoomSizeWnd.X / 2 &&
                -position.X - scaleSizeCell.X < Camera.positionCamera.X + Camera.zoomSizeWnd.X / 2 &&
                -position.Y > Camera.positionCamera.Y - Camera.zoomSizeWnd.Y / 2 &&
                -position.Y - scaleSizeCell.Y < Camera.positionCamera.Y + Camera.zoomSizeWnd.Y / 2)
            {
                spriteBatch.Draw(whiteTxt, NW, null, Color.Black, 0, Vector2.Zero, SpriteEffects.None, 2 * GameFeature.zIndexStep);
                spriteBatch.Draw(whiteTxt, NE, null, Color.Black, 0, Vector2.Zero, SpriteEffects.None, 2 * GameFeature.zIndexStep);
                spriteBatch.Draw(whiteTxt, SE, null, Color.Black, 0, Vector2.Zero, SpriteEffects.None, botZindex);
                spriteBatch.Draw(whiteTxt, SW, null, Color.Black, 0, Vector2.Zero, SpriteEffects.None, botZindex);
                for (int j = 0; j < 3; j++)
                    for (int i = 0; i < 4; i++)
                        spriteBatch.Draw(texture, floor1[j, i], textureFloor_1, color, 0, Vector2.Zero, SpriteEffects.None, float.Epsilon);

                if (left)
                {
                    spriteBatch.Draw(whiteTxt, W, null, Color.Black, 0, Vector2.Zero, SpriteEffects.None, 2 * GameFeature.zIndexStep);
                    spriteBatch.Draw(texture, leftBorderRect_1, textureLeftBorder_2, Color.White, 0, Vector2.Zero, SpriteEffects.None, 4 * GameFeature.zIndexStep);
                    spriteBatch.Draw(texture, leftBorderRect_2, textureLeftBorder_2, Color.White, 0, Vector2.Zero, SpriteEffects.None, 4 * GameFeature.zIndexStep);
                }
                else
                {
                    spriteBatch.Draw(texture, topBorderRect_3, textureBottomBorder_1, Color.White, 0, Vector2.Zero, SpriteEffects.None, botZindex);
                    spriteBatch.Draw(texture, topBorderRect_4, textureBottomBorder_1, Color.White, 0, Vector2.Zero, SpriteEffects.None, botZindex);
                    spriteBatch.Draw(texture, bottomBorderRect_3, textureBottomBorder_1, Color.White, 0, Vector2.Zero, SpriteEffects.None, botZindex + GameFeature.zIndexStep);
                    spriteBatch.Draw(texture, bottomBorderRect_4, textureBottomBorder_1, Color.White, 0, Vector2.Zero, SpriteEffects.None, botZindex + GameFeature.zIndexStep);

                    for (int j = 0; j < 2; j++)
                        for (int i = 0; i < 2; i++)
                            spriteBatch.Draw(texture, wall2[j, i], textureWall, Color.White, 0, Vector2.Zero, SpriteEffects.None, 3 * GameFeature.zIndexStep);

                    for (int i = 0; i < 3; i++)
                        spriteBatch.Draw(texture, floor3[i], textureFloor_1, color, 0, Vector2.Zero, SpriteEffects.None, float.Epsilon);
                }

                if (top)
                {
                    spriteBatch.Draw(whiteTxt, N, null, Color.Black, 0, Vector2.Zero, SpriteEffects.None, 2 * GameFeature.zIndexStep);
                    spriteBatch.Draw(texture, topBorderRect_1, textureBottomBorder_2, Color.White, 0, Vector2.Zero, SpriteEffects.None, 3 * GameFeature.zIndexStep);
                    spriteBatch.Draw(texture, topBorderRect_2, textureBottomBorder_2, Color.White, 0, Vector2.Zero, SpriteEffects.None, 3 * GameFeature.zIndexStep);

                    for (int j = 0; j < 2; j++)
                        for (int i = 0; i < 10; i++)
                            spriteBatch.Draw(texture, wall1[j, i], textureWall, Color.White, 0, Vector2.Zero, SpriteEffects.None, 3 * GameFeature.zIndexStep);
                }
                else
                {
                    spriteBatch.Draw(texture, leftBorderRect_3, textureLeftBorder_1, Color.White, 0, Vector2.Zero, SpriteEffects.None, 3 * GameFeature.zIndexStep);
                    spriteBatch.Draw(texture, leftBorderRect_4, textureLeftBorder_1, Color.White, 0, Vector2.Zero, SpriteEffects.None, 3 * GameFeature.zIndexStep);
                    spriteBatch.Draw(texture, rightBorderRect_3, textureRightBorder_1, Color.White, 0, Vector2.Zero, SpriteEffects.None, 3 * GameFeature.zIndexStep);
                    spriteBatch.Draw(texture, rightBorderRect_4, textureRightBorder_1, Color.White, 0, Vector2.Zero, SpriteEffects.None, 3 * GameFeature.zIndexStep);

                    for (int j = 0; j < 2; j++)
                        for (int i = 0; i < 4; i++)
                            spriteBatch.Draw(texture, floor2[j, i], textureFloor_1, color, 0, Vector2.Zero, SpriteEffects.None, float.Epsilon);
                }

                if (right)
                {
                    spriteBatch.Draw(whiteTxt, E, null, Color.Black, 0, Vector2.Zero, SpriteEffects.None, 2 * GameFeature.zIndexStep);
                    spriteBatch.Draw(texture, rightBorderRect_1, textureRightBorder_2, Color.White, 0, Vector2.Zero, SpriteEffects.None, 4 * GameFeature.zIndexStep);
                    spriteBatch.Draw(texture, rightBorderRect_2, textureRightBorder_2, Color.White, 0, Vector2.Zero, SpriteEffects.None, 4 * GameFeature.zIndexStep);
                }
                else
                {
                    spriteBatch.Draw(texture, topBorderRect_5, textureBottomBorder_1, Color.White, 0, Vector2.Zero, SpriteEffects.None, botZindex);
                    spriteBatch.Draw(texture, topBorderRect_6, textureBottomBorder_1, Color.White, 0, Vector2.Zero, SpriteEffects.None, botZindex);
                    spriteBatch.Draw(texture, bottomBorderRect_5, textureBottomBorder_1, Color.White, 0, Vector2.Zero, SpriteEffects.None, botZindex + GameFeature.zIndexStep);
                    spriteBatch.Draw(texture, bottomBorderRect_6, textureBottomBorder_1, Color.White, 0, Vector2.Zero, SpriteEffects.None, botZindex + GameFeature.zIndexStep);
                    for (int j = 0; j < 2; j++)
                        for (int i = 0; i < 2; i++)
                            spriteBatch.Draw(texture, wall3[j, i], textureWall, Color.White, 0, Vector2.Zero, SpriteEffects.None, 3 * GameFeature.zIndexStep);

                    for (int i = 0; i < 3; i++)
                        spriteBatch.Draw(texture, floor4[i], textureFloor_1, color, 0, Vector2.Zero, SpriteEffects.None, float.Epsilon);
                }

                if (bot)
                {
                    spriteBatch.Draw(whiteTxt, S, null, Color.Black, 0, Vector2.Zero, SpriteEffects.None, botZindex);
                    spriteBatch.Draw(texture, bottomBorderRect_1, textureBottomBorder_2, Color.White, 0, Vector2.Zero, SpriteEffects.None, botZindex + GameFeature.zIndexStep);
                    spriteBatch.Draw(texture, bottomBorderRect_2, textureBottomBorder_2, Color.White, 0, Vector2.Zero, SpriteEffects.None, botZindex + GameFeature.zIndexStep);
                }
                else
                {
                    spriteBatch.Draw(texture, leftBorderRect_5, textureLeftBorder_1, Color.White, 0, Vector2.Zero, SpriteEffects.None, botZindex + 2 * GameFeature.zIndexStep);
                    spriteBatch.Draw(texture, leftBorderRect_6, textureLeftBorder_1, Color.White, 0, Vector2.Zero, SpriteEffects.None, botZindex + 2 * GameFeature.zIndexStep);
                    spriteBatch.Draw(texture, rightBorderRect_5, textureRightBorder_1, Color.White, 0, Vector2.Zero, SpriteEffects.None, botZindex + 2 * GameFeature.zIndexStep);
                    spriteBatch.Draw(texture, rightBorderRect_6, textureRightBorder_1, Color.White, 0, Vector2.Zero, SpriteEffects.None, botZindex + 2 *GameFeature.zIndexStep);

                    for (int i = 0; i < 4; i++)
                        spriteBatch.Draw(texture, floor5[i], textureFloor_1, color, 0, Vector2.Zero, SpriteEffects.None, float.Epsilon);
                }
            }
        }

        public void generateMob(int maxIndex)
        {
            if (GameFeature.rand.Next(10) < 4 && canSpawn)
            {
                if (maxIndex / 2 < Number)
                {
                    map.addListCharactersOnMap(new List<AbstractCharacter>()
                    {
                        new GolemGuardian(game, new Vector2(position.X + 70 * scaleCell, position.Y + 70 * scaleCell)),
                        new GolemGuardian(game, new Vector2(position.X + 70 * scaleCell, position.Y + 110 * scaleCell)),
                        new GolemGuardian(game, new Vector2(position.X + 110 * scaleCell, position.Y + 70 * scaleCell))
                    });
                }
                else
                {
                    map.addListCharactersOnMap(new List<AbstractCharacter>()
                    {
                        new Ghost(game, new Vector2(position.X + 70 * scaleCell, position.Y + 70 * scaleCell)),
                        new Ghost(game, new Vector2(position.X + 70 * scaleCell, position.Y + 110 * scaleCell)),
                        new Ghost(game, new Vector2(position.X + 110 * scaleCell, position.Y + 70 * scaleCell)),
                        new Ghost(game, new Vector2(position.X + 110 * scaleCell, position.Y + 110 * scaleCell)),
                    });
                }
            }
        }

        public void Remove()
        {
            isVisible = false;

            map.listBorders.Remove(NW);
            map.listBorders.Remove(NE);
            map.listBorders.Remove(SE);
            map.listBorders.Remove(SW);
            map.listBorders.Remove(W);
            map.listBorders.Remove(N);
            map.listBorders.Remove(E);
            map.listBorders.Remove(S);
            map.listBorders.Remove(SBorder);
            map.listBorders.Remove(SEBorder);
            map.listBorders.Remove(SWBorder);

            map = null;
            game = null;
        }

        public Point getPortalPosition(int i)
        {
            if (i==0)
            {
                return new Point(position.X + 30 * scaleCell,position.Y + 30 * scaleCell);
            }
            else return new Point(position.X + 90 * scaleCell,position.Y + 30 * scaleCell);
        }
    }
}
