﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Heroes_0._01
{
    public class Portal : InteractiveObject
    {
        public event EventHandler activate;

        private Map map;

        private Texture2D texturePortal;

        private SpriteEffects flip;

        private Rectangle portalField;

        private List<AbstractCharacter> listGuardians;

        private bool isactive = true;
        public bool isActive
        {
            get { return isactive; }
            set
            {
                isactive = value;
                if (value) color = Color.White;
                else color = new Color(30,30,30,255);
            }
        }

        private Description description;

        public bool Activated = false;
        private Color color = Color.White;

        private int minCount;

        public Portal(MyGame game, Map map, Point position, SpriteEffects flip, string info, int minCount) : base(game)
        {
            this.flip = flip;
            this.map = map;
            this.minCount = minCount;

            drawField.Location = position;

            texturePortal = game.Content.Load<Texture2D>("map/portal");

            drawField.Width = texturePortal.Width;
            drawField.Height = texturePortal.Height;

            portalField = new Rectangle(drawField.X + drawField.Width / 4, drawField.Y + drawField.Height/3, drawField.Width / 2, drawField.Height / 2);


            zIndex = GameFeature.getZIndex(drawField.Y + drawField.Height * 4 / 5);

            description = new Description(game, false);
            description.updateDescription(info, "", 0.25f, drawField, zIndex);

            isVisible = true;

            listGuardians = new List<AbstractCharacter>();

            mouseEnter += Portal_mouseEnter;
            mouseLeave += Portal_mouseLeave;

            game.updateGame += update;
        }

        private void update(GameTime gameTime)
        {
            if (map == null || !isActive) return;
             
            bool allInPortal = true;
            bool allDie = true;

            int count = 0;

            for (int i = 0; i < map.listHeroOnMap.Count; i++)
                if (map.listHeroOnMap[i].isAlive)
                {
                    allDie = false;
                    if (!map.listHeroOnMap[i].shade.Intersects(portalField))
                    {
                        allInPortal = false;
                        break;
                    }
                    else count++;
                }

            if (allInPortal && count >= minCount && !Activated && !allDie)
            {
                Activated = true;
                activate?.Invoke(this, null);
            }
        }

        private void Portal_mouseLeave(object sender, EventArgs e)
        {
            description.isVisible = false;
        }

        private void Portal_mouseEnter(object sender, EventArgs e)
        {
            description.isVisible = true;
        }

        public override void Remove()
        {
            base.Remove();
            map = null;
            game.updateGame -= update;
            listGuardians.Clear();
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texturePortal, drawField, null, color, 0, Vector2.Zero, flip, zIndex);
        }

        public void addGuardians(List<AbstractCharacter> newGuardians)
        {
            isActive = false;

            for (int i = 0; i<newGuardians.Count; i++)
            {
                newGuardians[i].died += checkUnlock;
                listGuardians.Add(newGuardians[i]);
            }
          
        }

        private void checkUnlock(object o, EventArgs e)
        {
            AbstractCharacter character = (AbstractCharacter)o;
            character.died -= checkUnlock;

            bool allDie = true;
            for (int i = 0; i < listGuardians.Count; i++)
                if (listGuardians[i].isAlive)
                {
                    allDie = false;
                    break;
                }

            if (allDie)
            {
                color = Color.White;
                isActive = true;
            }
        }

        public void Show()
        {
            isVisible = true;
            Activated = false;
            game.updateGame -= update;
            game.updateGame += update;
        }

        public void Hide()
        {
            isVisible = false;
            game.updateGame -= update;
        }
    }
}
