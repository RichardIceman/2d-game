﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class WorldMap : Map
    {
        public WorldMap(MyGame game, EventHandler mouseLeftButtonDown) : base(game, mouseLeftButtonDown) { }

        protected override void loadContent()
        {
            textureFon = game.Content.Load<Texture2D>("map/backgroundHD");
            song = game.Content.Load<Song>("Songs/148_Barovian_Castle");
            textureMap = game.Content.Load<Texture2D>("map/WorldMap");

            sizeMap.X = textureMap.Width;
            sizeMap.Y = textureMap.Height;

            scaleMap = 2.5f;
        }

        protected override void loadMap()
        {
            listBorders.AddRange(new List<Rectangle>
            {
                //Самый верх              
                new Rectangle(300, 0, 150, 410),
                new Rectangle(450, 0, 410, 150),
                new Rectangle(860, 50, 60, 150),
                new Rectangle(920, 100, 70, 150),
                new Rectangle(990, 150, 330, 170),
                new Rectangle(1320, 200, 240, 170),
                new Rectangle(1560, 240, 50, 170),
                new Rectangle(1610, 260, 50, 170),
                new Rectangle(1660, 250, 150, 170),
                new Rectangle(1810, 200, 150, 170),
                new Rectangle(1960, 250, 50, 170),
                new Rectangle(2010, 310, 200, 170),
                new Rectangle(2210, 350, 420, 170),
                new Rectangle(2630, 410, 60, 170),
                new Rectangle(2690, 450, 150, 170),
                new Rectangle(2840, 400, 170, 170),
                new Rectangle(3010, 450, 70, 170),
                new Rectangle(3080, 550, 70, 140),
                new Rectangle(3150, 570, 150, 150),
                new Rectangle(3300, 510, 280, 170),
                new Rectangle(3580, 460, 150, 170),
                new Rectangle(3730, 500, 630, 170),
                new Rectangle(4360, 500, 200, 900),
                new Rectangle(4260, 1200, 100, 300),
                new Rectangle(4160, 1250, 100, 300),
                new Rectangle(4060, 1350, 100, 300),
                new Rectangle(3950, 1400, 110, 1400), 

                //правый столб и вниз
                new Rectangle(3850, 2650, 100, 300),
                new Rectangle(3750, 2800, 100, 1300),
                new Rectangle(3700, 2900, 50, 700),
                new Rectangle(3550, 2980, 150, 620),
                new Rectangle(3450, 3050, 100, 470),
                new Rectangle(3250, 3100, 200, 380),
                new Rectangle(3150, 3150, 100, 250),
                new Rectangle(3050, 3200, 100, 100),

                //внутренне кольцо
                new Rectangle(3850, 1600, 100, 850),
                new Rectangle(3650, 1630, 200, 820),
                new Rectangle(3500, 1730, 150, 720),
                new Rectangle(3270, 1830, 230, 620),
                new Rectangle(2800, 1980, 470, 520),
                new Rectangle(2100, 2120, 700, 300),
                new Rectangle(1900, 2200, 200, 300),
                new Rectangle(1700, 2300, 200, 300),
                new Rectangle(1500, 2350, 200, 300),
                new Rectangle(1400, 2400, 100, 380),
                new Rectangle(1250, 2450, 150, 240),
                new Rectangle(1500, 2650, 100, 140),
                new Rectangle(1600, 2650, 100, 180),
                new Rectangle(1700, 2700, 100, 200),
                new Rectangle(1800, 2700, 150, 180),
                new Rectangle(1950, 2750, 70, 180),
                new Rectangle(2020, 2800, 280, 180),
                new Rectangle(2300, 2780, 180, 180),
                new Rectangle(2480, 2700, 200, 180),
                new Rectangle(2680, 2670, 50, 180),
                new Rectangle(2730, 2650, 70, 180),
                new Rectangle(2800, 2630, 50, 180),
                new Rectangle(2850, 2600, 50, 180),
                new Rectangle(2900, 2550, 100, 180),
                new Rectangle(3000, 2470, 120, 180),

                //НАД 2 ПОРТАЛОМ
                //верх с лева на право   
                new Rectangle(200, 300, 100, 400),
                new Rectangle(300, 530, 80, 1880),
                new Rectangle(150, 2150, 150, 300),
                new Rectangle(0, 2250, 150, 700),
                new Rectangle(150, 2750, 150, 300),
                new Rectangle(300, 2920, 350, 300),
                new Rectangle(650, 3000, 100, 300),
                new Rectangle(750, 3100, 200, 300),
                new Rectangle(950, 3150, 200, 300),
                new Rectangle(1150, 3300, 300, 300),
                new Rectangle(1450, 3450, 150, 300),
                new Rectangle(1600, 3500, 150, 300),
                new Rectangle(1750, 3620, 300, 300),
                new Rectangle(2050, 3720, 600, 300),
                new Rectangle(2650, 3850, 1100, 300),

                new Rectangle(380, 2270, 30, 120),
                new Rectangle(380, 600, 130, 1670),
                new Rectangle(510, 1650, 130, 600),
                new Rectangle(640, 1720, 300, 400),
                new Rectangle(940, 1800, 130, 140),
                new Rectangle(1070, 1800, 100, 100),

                new Rectangle(670, 150, 120, 60),
                new Rectangle(510, 620, 80, 700),
                new Rectangle(590, 720, 160, 560),
                new Rectangle(750, 780, 620, 500),
                new Rectangle(1270, 840, 340, 500),
                new Rectangle(1610, 920, 140, 550),
                new Rectangle(1750, 960, 100, 530),
                new Rectangle(1850, 1020, 80, 480),
                new Rectangle(1930, 1050, 370, 560),
                new Rectangle(2300, 1120, 130, 430),
                new Rectangle(2430, 1170, 280, 350),
                new Rectangle(2710, 1120, 380, 200),
                new Rectangle(2860, 1080, 40, 40),
                new Rectangle(3090, 1120, 80, 150),
                new Rectangle(3170, 1140, 100, 90),
                new Rectangle(3270, 1190, 40, 40),
                //низ справа на лево
                new Rectangle(2710, 1320, 400, 80),
                new Rectangle(2890, 1400, 40, 40),
                new Rectangle(2710, 1400, 180, 60),
                new Rectangle(2430, 1520, 80, 40),
                new Rectangle(2030, 1610, 80, 40),
                new Rectangle(1890, 1500, 40, 40),
                new Rectangle(1540, 1340, 70, 60),
                new Rectangle(1000, 1280, 90, 50),
                new Rectangle(670, 1280, 160, 100),           
            });


            new ManaPotion(game).Drop(this, new Point(2500, 900));
            new ManaPotion(game).Drop(this, new Point(2700, 900));

            addListCharactersOnMap(new List<AbstractCharacter>
            {
                new Ghost(game, new Vector2(1800, 700)),
                new Ghost(game, new Vector2(2000, 700)),

                new Ghost(game, new Vector2(3500, 900)),
                new Ghost (game, new Vector2(3700, 1000)),

                new Ghost(game, new Vector2(1700, 1600)),
                new Ghost (game, new Vector2(1900, 1700)),
                new Ghost(game, new Vector2(1700, 1800)),

                new Ghost(game, new Vector2(2200, 3100)),
                new Ghost (game, new Vector2(2500, 3100)),
                new Ghost(game, new Vector2(2200, 3300))
            });

            listPortals.Add(new Portal(game, this, new Point(3925, 495), SpriteEffects.FlipHorizontally, "Великий лабиринт", 1));
            listPortals.Add(new Portal(game, this, new Point(3400, 2200), SpriteEffects.FlipHorizontally, "Покои босса", 1));
        }
    }
}
