﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using Microsoft.Xna.Framework.Input;

namespace Heroes_0._01
{
    public enum TypeCharacter
    {
        Герой,
        Враг,
        Друг,
        Любой
    }

    public enum TypeAttack
    {
        Меч,
        Лук,
        Fireball,
        Fireskull
    }

    public abstract class AbstractCharacter : InteractiveObject
    {
        public event EventHandler died;
        public event EventHandler skillPointChange;
        public event EventHandler inventoryChanged;
        public event EventHandler statsChanged;
        public event EventHandler positionChanged;

        public Map currentMap;

        public float priceMult = 1f;

        public Vector2 sizeAvatar;

        public TypeCharacter typeCharacter;
        public TypeAttack typeAttack;
        protected TypeAttack defaultTypeAttack;
        protected ArmorType defaultArmorType;

        public Texture2D iconHead;
        public Texture2D iconSelectHead;
        public Rectangle shade;
        private Texture2D textureShade;


        private Rectangle visionRect;
        private Texture2D textureLight;
        public float defaultScaleVision;
        private float scalevision;
        private float tmpScaleVision;
        private float stepScaleVision = 0.25f;
        public float ScaleVision
        {
            get { return scalevision; }
            set
            {
                if (scalevision != value)
                {
                    stepScaleVision = Math.Abs(stepScaleVision);
                    tmpScaleVision = scalevision;
                    if (value < scalevision) stepScaleVision = -stepScaleVision;

                    scalevision = value;
                    game.updateGame += updateVision;
                }
            }
        }
        protected int shiftVision = 0;

        private Texture2D defaultEffect;

        protected Texture2D defaultShade;
        protected Texture2D selectShade;
        protected Texture2D mouseoverShade;


        protected int shiftShade_Y = 0;
        protected int shiftShade_X = 0;

        protected float shakePower = 3;

        public AbstractCharacter targetForAttack;
        protected int currentTime;
        protected int autoattackDelay = 500;
        private bool isautoattack = false;
        public bool isAutoattack
        {
            get { return isautoattack; }
            set
            {
                if (value != isautoattack)
                {
                    if (value)
                    {
                        currentTime = autoattackDelay;
                        game.updateGame += Autoattack;
                    }
                    else
                    {
                        game.updateGame -= Autoattack;
                    }
                    isautoattack = value;
                }
            }
        }

        public float indexVisibility = 1;
        private bool charactervisibility = true;
        public bool characterVisibile
        {
            get { return charactervisibility; }
            set
            {
                charactervisibility = value;
                if (value) indexVisibility = 1;
                else indexVisibility = 0.5f;

            }

        }

        public bool canMove = true;

        public Vector2 shift;
        private Vector2 newPosition;
        public Vector2 position;
        public Vector2 positionOnMap {
            get
            {
                return new Vector2(shade.X + shade.Width / 2, shade.Y + shade.Height / 2);
            }
        }

        private bool isSelected = false;
        private bool isMouseover = false;

        public bool isDamageImmune = false;
        public bool isAlive = true;
        private bool isMoving = false;
        public float moveSpeed = 5;
        protected int defaultMoveSpeed = 3;

        public float sizeCharacter = 0.7f;

        private float shadeZIndex = 3 * GameFeature.zIndexStep;
        protected int shiftForLMB = 20;
        protected int shadeWidth = 150;
        protected float sizeShade_Y = 0.3f;

        public ProgressBar manaBar;
        public ProgressBar healthBar;
        public ProgressBar expBar;
        protected int shiftHealthBar = 5;

        private int skillpoint;
        public int SkillPoint
        {
            get { return skillpoint; }
            set
            {
                skillpoint = value;
                skillPointChange?.Invoke(this, null);
                statsChanged?.Invoke(this, null);
            }
        }
        public int Lvl = 1;
        private int expPoint;
        public int ExpPoint
        {
            get
            {
                return expPoint;
            }
            set
            {
                if (isAlive)
                {
                    expPoint = value;
                    expBar.MaxValue = value;
                }
            }
        }

        protected int defaultRangeAttack = 100;
        protected int rangeAttack = 100;

        protected int defaultHealthPoint;

        private int healthPoint;
        public int MaxHealthPoint
        {
            get
            {
                return healthPoint;
            }
            set
            {
                if (isAlive)
                {
                    healthPoint = value;
                    healthBar.MaxValue = value;
                }
            }
        }

        protected int defaultManaPoint;

        private int manaPoint;
        public int MaxManaPoint
        {
            get
            {
                return manaPoint;
            }
            set
            {
                if (isAlive)
                {
                    manaPoint = value;
                    manaBar.MaxValue = value;
                }
            }
        }
        public int attack;
        public int defense;
        public int healPower;
        public int manaRegen;
        public int healthRegen;
        private int time;//для регена

        protected int defaultAttack = 5;
        protected int defaultDefense = 2;

        public string name;
        public string info;

        protected CharacterAnimation animMove;
        public CharacterAnimation animStand;
        protected CharacterAnimation animHurt;
        public CharacterAnimation animDie;
        public CharacterAnimation animAttack;

        public CharacterAnimation currentAnim;

        public AbstractSkill[] skills;

        public AbstractItem[] inventory;

        private Vector2 positionTxt;
        private Rectangle txtRect;
        private Texture2D txtTexture;
        private string text;
        private float sizeTxt = 0.22f;
        private int timeTxt;
        private bool isSaying = false;
        protected int shiftTxtBox_Y = 10;

        public AbstractCharacter(MyGame game, Vector2 position) : base(game)
        {
            this.game = game;
            this.position = position;

            drawField.X = (int)position.X;
            drawField.Y = (int)position.Y;

            shift = new Vector2(0, 0);
            newPosition = new Vector2(0, 0);

            defaultShade = game.Content.Load<Texture2D>("Characters/Shades/shade");
            mouseoverShade = game.Content.Load<Texture2D>("Characters/Shades/shadeY");
            textureLight = game.Content.Load<Texture2D>("Characters/Shades/noir");
            txtTexture = game.Content.Load<Texture2D>("Interface/textRect");

            txtRect = new Rectangle(0, 0, 320, 150);

            //defaultEffect = new Texture2D(game.GraphicsDevice, 1, 1);

            textureShade = defaultShade;

            mouseEnter += mouseOver;
            mouseLeave += lostMouseOver;
            isVisible = true;

            healthBar = new ProgressBar(game, 80, 10, false);
            healthBar.color = Color.Lime;
            healthBar.isVisible = true;
            manaBar = new ProgressBar(game, 100, 100, false);
            expBar = new ProgressBar(game, 100, 100, false);
            ExpPoint = GameFeature.lvlTable[Lvl - 1];
            expBar.Value = 0;

            inventory = new AbstractItem[GameFeature.inventorySize + GameFeature.equipementSize + GameFeature.countPotions];
            skills = new AbstractSkill[GameFeature.countSkills];
        }

        private void updateVision()
        {
            visionRect.Width = (int)(drawField.Width * tmpScaleVision * GameFeature.scaleEllipse_X);
            visionRect.Height = (int)(drawField.Height * tmpScaleVision * GameFeature.scaleEllipse_Y);
            visionRect.X = drawField.X + (drawField.Width - visionRect.Width) / 2;
            visionRect.Y = drawField.Y + (drawField.Height - visionRect.Height) / 2 + shiftVision;
        }

        private void updateVision(GameTime gameTime)
        {
            if (stepScaleVision > 0 && tmpScaleVision <= scalevision ||
                stepScaleVision < 0 && tmpScaleVision >= scalevision)
            {
                tmpScaleVision += stepScaleVision;
                updateVision();
            }
            else
            {
                tmpScaleVision = scalevision;
                updateVision();
                game.updateGame -= updateVision;
            }
        }

        protected virtual void Autoattack(GameTime gameTime)
        {
            if (targetForAttack != null && !targetForAttack.characterVisibile)
            {
                isAutoattack = false;
                return;
            }

            currentTime += gameTime.ElapsedGameTime.Milliseconds;
            if (currentTime >= autoattackDelay)
            {
                currentTime -= autoattackDelay;
                Attack(targetForAttack);
                if (targetForAttack != null && !targetForAttack.isAlive) targetForAttack = null;
            }
        }

        protected virtual void ComplateLoad(string icon, string iconSelect)
        {
            iconHead = game.Content.Load<Texture2D>(icon);
            iconSelectHead = game.Content.Load<Texture2D>(iconSelect);

            animAttack.animationEnd += animEnd;
            animHurt.animationEnd += animEnd;
            animStand.Play();       

            healthBar.X = drawField.X + (drawField.Width - healthBar.Width) / 2;
            healthBar.Y = drawField.Y - healthBar.Height - shiftHealthBar;
            shade = new Rectangle((int)(drawField.X + (drawField.Width - shadeWidth * sizeCharacter) / 2) + shiftShade_X, (int)(drawField.Y + drawField.Height * 0.85 + shiftShade_Y), (int)(shadeWidth * sizeCharacter), (int)(drawField.Height * sizeShade_Y * sizeCharacter));
            zIndex = GameFeature.getZIndex(shade.Y + shade.Height/2);

            healthBar.zIndex = zIndex;

            loadParametrs();
        }

        protected void drawVision(GameTime gameTime, SpriteBatch spriteBatch)
        {
          spriteBatch.Draw(textureLight, visionRect, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 1);
        }

        protected void animEnd(object o, EventArgs e)
        {
           if (isVisible) animStand.Play();
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(textureShade, shade, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, shadeZIndex);
        }

        public void Stand()
        {
            if (isMoving) game.updateGame -= Move;
        }

        public void mouseOver(object o, EventArgs e)
        {
            if (isSelected || isMouseover) return;
            textureShade = mouseoverShade;
            isMouseover = true;
        }

        public void lostMouseOver(object o, EventArgs e)
        {
            if (isSelected || !isMouseover) return;
            textureShade = defaultShade;
            isMouseover = false;
        }

        public void lostFocus()
        {
            if (!isSelected) return;
            textureShade = defaultShade;
            isSelected = false;
        }

        public void getFocus()
        {
            if (isSelected) return;
            textureShade = selectShade;
            isSelected = true;
        }

        public void Attack(AbstractCharacter target)
        {
            if (target == null || !isAlive) return;
            targetForAttack = target;
           
            Vector2 distance = target.positionOnMap - positionOnMap;
            if (distance.X > 0) shift.X = 1;
            else shift.X = -1;
            if (GameFeature.inEllipse(distance, rangeAttack))
            {

                if (animAttack.Play())
                {
                    if (inventory[29] != null && (inventory[29] as AbstractWeapon).typeAttack != defaultTypeAttack) Say("Я не умею этим\nпользоваться!", TypeCharacter.Герой);
                    Vector2 startPosition = new Vector2(drawField.X + drawField.Width / 2, drawField.Y + drawField.Height / 2);
                    if (!characterVisibile) characterVisibile = true;
                    switch (typeAttack)
                    {
                        case TypeAttack.Меч:
                            target?.getDamage(this);

                            if (inventory[29] != null && (inventory[29] as AbstractWeapon).typeAttack == defaultTypeAttack)
                            {
                                SpriteEffects flip = SpriteEffects.None;
                                sideAnimation side = sideAnimation.Front;
                                if (distance.X < 0) flip = SpriteEffects.FlipHorizontally;
                                if (distance.Y > 0) side = sideAnimation.Back;

                                new SwordHitEffect(game, target, side, flip).Play();
                            }
                            else
                            {
                                Camera.Shake(0.2f, shakePower);
                            }
                            break;
                        case TypeAttack.Лук:
                            if (inventory[30] != null)
                            {
                                AbstractSecondWeapon secWeapon = inventory[30] as AbstractSecondWeapon;
                                if (secWeapon.secondWeaponType == SecondWeaponType.Колчан)
                                {
                                    Arrow arrow = new Arrow(game, startPosition, target, this);
                                }
                                else
                                {
                                    Say($"Мне нужен колчан,\nа не это!", TypeCharacter.Герой);
                                    isAutoattack = false;
                                }
                            }
                            else
                            {
                                Say("Нет стрел!", TypeCharacter.Герой);
                                isAutoattack = false;
                            }

                            break;
                        case TypeAttack.Fireball:
                            Fireball fireball = new Fireball(game, startPosition, target, this);
                            break;
                        case TypeAttack.Fireskull:
                            Fireskull fireskull = new Fireskull(game, startPosition, target, this);
                            break;
                    }
                }
            }
            else
            {
                Say("Слишком далеко.", TypeCharacter.Герой);
                moveTo(target.positionOnMap.ToPoint());
            }
            if (!isAutoattack) targetForAttack = null; 
        }

        public void Resurrect()
        {
            if (!isAlive)
            {
                ScaleVision = defaultScaleVision;
                isAlive = true;
                animStand.Play();
            }
        }

        public void getExp(float exp)
        {
            if (expBar.Value + exp >= expBar.MaxValue)
            {
                float tmpExp = expBar.Value + exp - expBar.MaxValue;
                expBar.Value += exp;
                Lvl++;
                SkillPoint++;
                if (Lvl <= GameFeature.lvlTable.Count)
                {
                    new LvlUp(game, this, sideAnimation.Front, SpriteEffects.None).Play();
                    expBar.MaxValue = GameFeature.lvlTable[Lvl - 1];
                    expBar.Value = 0;
                    getExp(tmpExp);
                }
            }
            else
            {
                expBar.Value += exp;
            }
        }

        public virtual void getDamage(AbstractCharacter character)
        {         
            getDamage(GameFeature.calculateDamage(character.attack, defense));
        }

        public virtual void getDamage(float value)
        {
            if (isDamageImmune) return;
            healthBar.Value -= value;
            if (healthBar.Value / healthBar.MaxValue <= 0.2)
            {
                switch (GameFeature.rand.Next(2))
                {
                    case 0:
                        Say("Мне нужно\nлечение!", TypeCharacter.Герой);
                        break;

                    case 1:
                        Say("Я сильно ранен!", TypeCharacter.Герой);
                        break;
                }
            }
            if (healthBar.Value > 0)
                animHurt.Play();
            else if (isAlive)
            {
                Die();
            }
        }

        public void Die()
        {
            Say("AAAAaaaaa...", TypeCharacter.Герой);
            isAlive = false;
            ScaleVision = 3;
            animDie.Play();
            died?.Invoke(this, null);
        }

        public virtual void moveTo(Point _newPosition)
        {
            if (!isAlive || !canMove) return;

            newPosition.X = _newPosition.X - drawField.Width / 2;
            newPosition.Y = _newPosition.Y - drawField.Height + shiftForLMB;

            if (Math.Abs(position.X - newPosition.X) > Math.Abs(position.Y - newPosition.Y))
            {
                shift.X = moveSpeed * (newPosition.X - position.X) / Math.Abs(position.X - newPosition.X);
                shift.Y = moveSpeed * (newPosition.Y - position.Y) / Math.Abs(position.X - newPosition.X);
            }
            else
            {
                shift.X = moveSpeed * (newPosition.X - position.X) / Math.Abs(position.Y - newPosition.Y);
                shift.Y = moveSpeed * (newPosition.Y - position.Y) / Math.Abs(position.Y - newPosition.Y);
            }
            if (animMove.Play())
            {
                if (isMoving) game.updateGame -= Move;
                isMoving = true;
                game.updateGame += Move;
            }
        }

        protected void Move(GameTime gameTime)
        {
            if (!canMove || position.X + moveSpeed / 2 >= newPosition.X && position.X - moveSpeed / 2 <= newPosition.X &&
               position.Y + moveSpeed / 2 >= newPosition.Y && position.Y - moveSpeed / 2 <= newPosition.Y)
            {
                stopMoving();
                return;
            }

            updatePosition(shift);
            intersects();                  
        }

        protected virtual bool intersects()
        {
            for (int i = 0; i < currentMap?.listBorders.Count; ++i)
                if (currentMap.listBorders[i].Intersects(shade))
                {
                    updatePosition(-shift);
                    targetForAttack = null;
                    stopMoving();
                    return false;
                }
            return true;
        }

        private void stopMoving()
        {
            isMoving = false;
            game.updateGame -= Move;
            if (isVisible) animStand.Play();
        }

        public void setPosition(Vector2 newPosition)
        {
            stopMoving();
            position = newPosition;

            drawField.X = (int)position.X;
            drawField.Y = (int)position.Y;

            visionRect.X = drawField.X + (drawField.Width - visionRect.Width) / 2;
            visionRect.Y = drawField.Y + (drawField.Height - visionRect.Height) / 2 + shiftVision;

            healthBar.X = drawField.X + (drawField.Width - healthBar.Width) / 2;
            healthBar.Y = drawField.Y - healthBar.Height - shiftHealthBar;

            shade = new Rectangle((int)(drawField.X + (drawField.Width - shadeWidth * sizeCharacter) / 2) + shiftShade_X, (int)(drawField.Y + drawField.Height * 0.85 + shiftShade_Y), (int)(shadeWidth * sizeCharacter), (int)(drawField.Height * sizeShade_Y * sizeCharacter));

            zIndex = GameFeature.getZIndex(shade.Y + shade.Height / 2);

            healthBar.zIndex = zIndex;

            positionChanged?.Invoke(this, null);
        }

        public void updatePosition(Vector2 shift)
        {
            position.X += shift.X;
            position.Y += shift.Y;

            drawField.X = (int)position.X;
            drawField.Y = (int)position.Y;

            shade.X = drawField.X + (drawField.Width - shade.Width) / 2 + shiftShade_X;
            shade.Y = (int)(drawField.Y + drawField.Height * 0.85 + shiftShade_Y);

            zIndex = GameFeature.getZIndex(shade.Y + shade.Height / 2);

            visionRect.X = drawField.X + (drawField.Width - visionRect.Width) / 2;
            visionRect.Y = drawField.Y + (drawField.Height - visionRect.Height) / 2 + shiftVision;

            healthBar.X = drawField.X + (drawField.Width - healthBar.Width) / 2;
            healthBar.Y = drawField.Y - healthBar.Height - shiftHealthBar;
            healthBar.zIndex = zIndex;

            positionChanged?.Invoke(this, null);
        }

        private void drawTxt(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(txtTexture, txtRect, null, Color.White * 0.7f, 0, Vector2.Zero, SpriteEffects.None, zIndex);
            spriteBatch.DrawString(GameFeature.Font, currentTxt, positionTxt, Color.White, 0, Vector2.Zero, sizeTxt, SpriteEffects.None, zIndex + GameFeature.zIndexStep);
        }

        private string currentTxt;
        private int currentSymbol;

        private void updateTxt(GameTime gameTime)
        {
            timeTxt -= gameTime.ElapsedGameTime.Milliseconds;
            if (timeTxt <= 0)
            {
                game.updateGame -= updateTxt;
                game.drawScaleInterface -= drawTxt;
                isSaying = false;
            }

            if (currentTxt.Length < text.Length)
            {
                currentTxt += text[currentSymbol];
                currentSymbol++;
            }
    
            txtRect.X = (int)positionOnMap.X;
            txtRect.Y = (int)positionOnMap.Y - drawField.Height - txtRect.Height + shiftTxtBox_Y;
            positionTxt.X = txtRect.X + 20;
            positionTxt.Y = txtRect.Y + 30;
        }

        public void Say(string _text, TypeCharacter typeCharacter)
        {
            if (isAlive && (this.typeCharacter == typeCharacter || typeCharacter == TypeCharacter.Любой))
            {
                if (isSaying)
                {
                    if (text != _text)
                    {
                        text = _text;
                        currentSymbol = 0;
                        currentTxt = "";
                    }
                    timeTxt = 2000;
                    return;
                }

                isSaying = true;
                text = _text;

                currentSymbol = 0;
                currentTxt = "";

                txtRect.X = (int)positionOnMap.X;
                txtRect.Y = (int)positionOnMap.Y - drawField.Height - txtRect.Height + shiftTxtBox_Y;
                positionTxt.X = txtRect.X + 20;
                positionTxt.Y = txtRect.Y + 30;

                timeTxt = 2000;

                game.updateGame += updateTxt;
                game.drawScaleInterface += drawTxt;
            }
        }

        public void onInventoryChanged()
        {
            inventoryChanged?.Invoke(this, null);
        }

        public void recalculateStats()
        {
            game.updateGame -= regen;

            Vector2 newShift = shift / moveSpeed;

            attack = defaultAttack;
            defense = defaultDefense;
            healthPoint = defaultHealthPoint;
            manaPoint = defaultManaPoint;
            rangeAttack = defaultRangeAttack;
            healPower = 0;
            manaRegen = 0;
            healthRegen = 0;
            moveSpeed = defaultMoveSpeed;

            if (typeCharacter == TypeCharacter.Герой)
            {
                if (inventory[29] == null) //проверяем есть ли в руках оружие
                {
                    rangeAttack = 60;
                    typeAttack = TypeAttack.Меч;
                }
                else
                {
                    AbstractWeapon weapon = inventory[29] as AbstractWeapon;
                    if (weapon.typeAttack == defaultTypeAttack)
                    {
                        attack += weapon.Attack;
                        rangeAttack = weapon.rangeAttack;
                        typeAttack = weapon.typeAttack;
                    }
                    else
                    {
                        rangeAttack = 60;
                        typeAttack = TypeAttack.Меч;
                    }
                }
            }


            for (int i = GameFeature.inventorySize; i < GameFeature.inventorySize + GameFeature.equipementSize; i++)
            {
                if (inventory[i] != null && i != 29) //скипаем оружие
                {
                    AbstractEquipment equipeItem = inventory[i] as AbstractEquipment;
                    attack += equipeItem.Attack;
                    defense += equipeItem.Defense;
                    healthPoint += equipeItem.HealthPoint;
                    manaPoint += equipeItem.ManaPoint;
                    healPower += equipeItem.HealPower;
                    manaRegen += equipeItem.ManaRegen;
                    healthRegen += equipeItem.HealthRegen;
                    moveSpeed += equipeItem.MoveSpeed;
                }
            }

            for (int i = 0; i < GameFeature.countSkills; i++)
                if (skills[i] != null && skills[i].typeSkill == TypeSkill.Passive)
                {
                    attack += skills[i].Attack;
                    defense += skills[i].Defense;
                    healthPoint += skills[i].HealthPoint;
                    manaPoint += skills[i].ManaPoint;
                    healPower += skills[i].HealPower;
                    manaRegen += skills[i].ManaRegen;
                    healthRegen += skills[i].HealthRegen;
                    moveSpeed += skills[i].MoveSpeed;
                }

            if (manaRegen > 0 || healthRegen > 0) game.updateGame += regen;

            MaxManaPoint = manaPoint;
            MaxHealthPoint = healthPoint;

            if (inventory[25] != null) //штраф за неправильный тип доспехов
            {
                AbstractArmor armor = inventory[25] as AbstractArmor;
                if (defaultArmorType != armor.armorType) moveSpeed /= 2;
            }

            if (moveSpeed < 0) moveSpeed = 0;

            shift = newShift * moveSpeed;

            for (int i = 0; i < GameFeature.countSkills; i++)
                skills[i]?.updadeSkill();

            statsChanged?.Invoke(this, null);
        }

        private void regen(GameTime gameTime)
        {
            time += gameTime.ElapsedGameTime.Milliseconds;
            if (time >= 1000)
            {
                time -= 1000;
                if (isAlive)
                {
                    manaBar.Value += manaRegen;
                    healthBar.Value += healthRegen;
                }
            }
        }

        public override void Remove()
        {
            base.Remove();

            for (int i = 0; i < GameFeature.countSkills; i++)
                if (skills[i] != null) skills[i].Remove();

            isAutoattack = false;

            animAttack.animationEnd -= animEnd;
            animHurt.animationEnd -= animEnd;

            healthBar.Remove();
            expBar.Remove();
            manaBar.Remove();
            animAttack.Remove();
            animDie.Remove();
            animHurt.Remove();
            animMove.Remove();
            animStand.Remove();
            currentAnim = null;
            currentMap = null;
            targetForAttack = null;
        }

        public virtual void Hide()
        {
            isVisible = false;
            targetForAttack = null;
            animAttack.isVisible = false;
            animDie.isVisible = false;
            animHurt.isVisible = false;
            animMove.isVisible = false;
            animStand.isVisible = false;
            healthBar.isVisible = false;
        }

        public virtual void Show()
        {
            isVisible = true;
            currentAnim = null;
            if (isAlive)
            {
                animStand.Play();
            }
            else
            {
                currentAnim = animDie;
                animDie.isVisible = true;
            }
            healthBar.isVisible = true;
        }

        protected void clearInventory()
        {
            for (int i = 0; i < GameFeature.inventorySize + GameFeature.equipementSize + GameFeature.countPotions; i++)
                if (inventory[i] != null)
                {
                    inventory[i].Remove();
                    inventory[i] = null;
                }
        }

        protected virtual void loadParametrs()
        {
            isAlive = true;
            recalculateStats();

            healthBar.Value = healthBar.MaxValue;
            manaBar.Value = manaBar.MaxValue;
        }

        public void Restart()
        {
            Lvl = 1;

            Resurrect();
            skillpoint = 1;
            expBar.MaxValue = GameFeature.lvlTable[0];
            expBar.Value = 0;

            for (int i = 0; i < GameFeature.countSkills; i++)
                skills[i].Lvl = 0;
            
            clearInventory();
            loadParametrs();
        }
    }
}
