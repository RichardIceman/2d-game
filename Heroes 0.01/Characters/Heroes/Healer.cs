﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Healer : AbstractHero
    {
        public Healer(MyGame game, Vector2 position) : base(game, position)
        {
            animAttack = new Attack(game, this, "Characters/Healer/healer_attack");
            animDie = new Die(game, this, "Characters/Healer/healer_die");
            animHurt = new Hurt(game, this, "Characters/Healer/healer_hurt");
            animMove = new Move(game, this, "Characters/Healer/healer_run");
            animStand = new Stand(game, this, "Characters/Healer/healer_stand");
            ComplateLoad("Characters/Healer/HealerHead", "Characters/Healer/selectHealerHead");

            name = "Даэрон";
            info = "Роль: Жрец\nОружие: Посох\nДоспехи: Ткань";
        }

        protected override void loadParametrs()
        {
            ScaleVision = 14;
            defaultScaleVision = 14;

            defaultHealthPoint = 60;
            defaultManaPoint = 60;
            defaultAttack = 1;
            defaultDefense = 1;

            defaultTypeAttack = TypeAttack.Fireball;
            defaultArmorType = ArmorType.Ткань;

            skills[0] = new MassHeal(this);
            skills[1] = new MassResurrection(this);
            skills[2] = new MagePassive(this);

            new Wand_1(game).Equipe(this);
            new MageArmor_1(game).Equipe(this);
            new Belt_1(game).Equipe(this);
            new Boots_1(game).Equipe(this);

            base.loadParametrs();
        }
    }
}
