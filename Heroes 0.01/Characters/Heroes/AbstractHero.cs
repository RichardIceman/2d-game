﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public abstract class AbstractHero : AbstractCharacter
    {
        public AbstractHero(MyGame game, Vector2 position) : base(game, position)
        {
            typeCharacter = TypeCharacter.Герой;
            selectShade = game.Content.Load<Texture2D>("Characters/Shades/shadeG");

            game.drawLight += drawVision;

            priceMult = 0.5f;

          //  isDamageImmune = true;

            ScaleVision = 15;
            SkillPoint = 1;
           
            isAutoattack = true;
        }

        protected override bool intersects()
        {
            for (int i = 0; i < currentMap?.listItems.Count; ++i)
                if (currentMap.listItems[i].shadeField.Intersects(shade))
                    currentMap.listItems[i].Pickup(this);
            return base.intersects();
        }

        public override void Hide()
        {
            game.drawLight -= drawVision;
            base.Hide();
        }

        public override void Show()
        {
            game.drawLight -= drawVision;
            game.drawLight += drawVision;
            base.Show();
        }
    }
}
