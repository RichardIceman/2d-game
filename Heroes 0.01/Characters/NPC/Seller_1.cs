﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{ 
    public class Seller_1 : AbstractNPC
    {
        public Seller_1(MyGame game, Vector2 position) : base(game, position)
        {
            animAttack = new Attack(game, this, "Characters/Sellers/seller_1");
            animDie = new Die(game, this, "Characters/Sellers/seller_1");
            animHurt = new Hurt(game, this, "Characters/Sellers/seller_1");
            animMove = new Move(game, this, "Characters/Sellers/seller_1");
            animStand = new Stand(game, this, "Characters/Sellers/seller_1");

            shiftShade_Y = -20;
            shiftShade_X = -110;

            sizeShade_Y = 0.2f;

            animAttack.frameHeight = 400;
            animDie.frameHeight = 400;
            animStand.frameHeight = 400;
            animHurt.frameHeight = 400;
            animMove.frameHeight = 400;
            animStand.frameHeight = 400;

            ComplateLoad("Characters/Sellers/seller_1_head", "Characters/Sellers/seller_1_head");

            shiftShade_X = -30;

            animAttack.delay = 150;
            animDie.delay = 150;
            animStand.delay = 150;
            animHurt.delay = 150;
            animMove.delay = 150;
            animStand.delay = 150;

            animAttack.maxFrame = 7;
            animDie.maxFrame = 7;
            animStand.maxFrame = 7;
            animHurt.maxFrame = 7;
            animMove.maxFrame = 7;
            animStand.maxFrame = 7;

            new LightArmor_1(game).Pickup(this);
            new Belt_1(game).Pickup(this);
            new Boots_1(game).Pickup(this);
            new Book_1(game).Pickup(this);
            new MageArmor_1(game).Pickup(this);


            new OldNecklace(game).Pickup(this);

            for (int i = 0; i < 12; i++)
            {
                if (i < 3) new ResurrectScroll(game).Pickup(this);
                new HealthPotion(game).Pickup(this);
                new ManaPotion(game).Pickup(this);
            }

            name = "Флавий Эфти";
            info = "Роль: Танк\nОружие: Меч\nДоспехи: Тяжелые";
        }
    }
}
