﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public abstract class AbstractNPC : AbstractCharacter
    {
        public List<AbstractItem> extraInventory;

        public AbstractNPC(MyGame game, Vector2 position) : base(game, position)
        {
            extraInventory = new List<AbstractItem>();

            typeCharacter = TypeCharacter.Друг;
            selectShade = game.Content.Load<Texture2D>("Characters/Shades/shadeG");

            game.drawLight += drawVision;

            isDamageImmune = true;
            SkillPoint = 1;

            canMove = false;

            healthBar.isVisible = false;
        }

        protected override void loadParametrs()
        {
            defaultHealthPoint = 600;

            defaultAttack = 1;
            defaultDefense = 1000;

            base.loadParametrs();
        }

        public override void Show()
        {
            base.Show();
            healthBar.isVisible = false;
        }
    }
}
