﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Heroes_0._01
{
    public class Ghost : AbstractEnemy
    {
        private bool reveal = false;

        private int rangeReveal = 600;

        public Ghost(MyGame game, Vector2 position) : base(game, position)
        {
            animAttack = new Attack(game, this, "Characters/Ghost/ghost-shriek");
            animDie = new Die(game, this, "Characters/Ghost/ghost-vanish");
            animHurt = new Hurt(game, this, "Characters/Ghost/ghost-idle");
            animMove = new Move(game, this, "Characters/Ghost/ghost-idle");
            animStand = new Stand(game, this, "Characters/Ghost/ghost-idle");

            animAttack.maxFrame = 4;
            animDie.maxFrame = 7;
            animStand.maxFrame = 7;
            animHurt.maxFrame = 7;
            animMove.maxFrame = 7;
            animStand.maxFrame = 7;

            animAttack.isReverse = true;
            animDie.isReverse = true;
            animHurt.isReverse = true;
            animMove.isReverse = true;
            animStand.isReverse = true;
        

            ComplateLoad("Characters/Ghost/GhostHead", "Characters/Ghost/GhostHead");

            name = "Призрак";
        }

        protected override void loadParametrs()
        {
            defaultHealthPoint = 80;
            defaultManaPoint = 60;
            defaultAttack = 5;
            defaultDefense = 5;

            defaultTypeAttack = TypeAttack.Fireskull;
            typeAttack = TypeAttack.Fireskull;

            defaultRangeAttack = 400;

            countEXP = 70;

            rangeAgr = 300;

            base.loadParametrs();
        }

        public override void Show()
        {
            if (!reveal)
            {
                Hide();
                game.updateGame += inRange;
            }
            else
            {
                base.Show();
            }
        }

        public override void Hide()
        {
            game.updateGame -= inRange;
            base.Hide();
        }

        public override void Remove()
        {
            game.updateGame -= inRange;
            base.Remove();
        }

        private void inRange(GameTime gameTime)
        {
            if (currentMap == null)
            {
                game.updateGame -= inRange;
                return;
            }

            for (int i = 0; i < currentMap.listHeroOnMap.Count; i++)
            {
                Vector2 distance = currentMap.listHeroOnMap[i].positionOnMap - positionOnMap;
                if (GameFeature.inEllipse(distance, rangeReveal))
                {
                    reveal = true;
                    GhostReveal effect = new GhostReveal(game, this, sideAnimation.Front, Microsoft.Xna.Framework.Graphics.SpriteEffects.None);
                    effect.Play();
                    effect.animationEnd += Effect_animationEnd;
                    game.updateGame -= inRange;
                    break;
                }
             }
        }

        private void Effect_animationEnd(object sender, EventArgs e)
        {
            if (currentMap != null && currentMap.isVisible) Show();
        }
    }
}
