﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public abstract class AbstractEnemy : AbstractCharacter
    {
        protected List<Damage> dmgFromCharacter;
        protected float rangeAgr = 450;
        protected float rangeEXP = 1000;
        protected float countEXP = 100;
        protected int countPoints = 50;

        protected int countMoney = 6;
        protected int random = 5;

        private bool isagressive = true;
        public bool isAgressive
        {
            get { return isagressive; }
            set
            {
                if (isagressive != value)
                {
                    isagressive = value;
                    if (value)
                    {
                        game.updateGame -= brain;
                        isAutoattack = false;
                        game.updateGame += brain;
                    }
                    else
                    {
                        game.updateGame -= brain;
                        isAutoattack = false;
                    }
                }
            }
        }

        public AbstractEnemy(MyGame game, Vector2 position) : base(game, position)
        {
            dmgFromCharacter = new List<Damage>();
            typeCharacter = TypeCharacter.Враг;
            selectShade = game.Content.Load<Texture2D>("Characters/Shades/shadeR");
            healthBar.color = Color.OrangeRed;

            healthBar.Value = MaxHealthPoint;

            autoattackDelay = 750;

            died += drop;
            game.updateGame += brain;
        }

        protected override void ComplateLoad(string icon, string iconSelect)
        {
            base.ComplateLoad(icon, iconSelect);
            animDie.animationEnd += AnimDie_animationEnd;
        }

        private void AnimDie_animationEnd(object sender, EventArgs e)
        {
            animDie.animationEnd -= AnimDie_animationEnd;
            Remove();
        }

        public override void Remove()
        {
            base.Remove();

            died -= drop;
            game.updateGame -= brain;
            dmgFromCharacter.Clear();
        }

        protected virtual void drop(object o, EventArgs e)
        {
            clearInventory();

            GameFeature.score += countPoints;
            List<AbstractCharacter> charactersEXP = new List<AbstractCharacter>();
            for (int i = 0; i< currentMap.listHeroOnMap.Count; i++)
            {
                Vector2 distance = positionOnMap - currentMap.listHeroOnMap[i].positionOnMap;
                if (currentMap.listHeroOnMap[i].typeCharacter == TypeCharacter.Герой && currentMap.listHeroOnMap[i].isAlive && GameFeature.inEllipse(distance, rangeEXP))
                    charactersEXP.Add(currentMap.listHeroOnMap[i]);
            }

            float exp = countEXP / charactersEXP.Count;

            for (int i = 0; i < charactersEXP.Count; i++)
            {
                charactersEXP[i].getExp(exp);
            }

            dropList();
        }

        protected virtual void dropList()
        {
            int rnd = GameFeature.rand.Next(3);


            switch (rnd)
            {
                case 0:
                    new ManaPotion(game).Drop(currentMap, getRandPosition());
                    break;
                case 1:
                    new HealthPotion(game).Drop(currentMap, getRandPosition());
                    break;
                case 2:
                    new ManaPotion(game).Drop(currentMap, getRandPosition());
                    new HealthPotion(game).Drop(currentMap, getRandPosition());
                    break;
            }

            Money money = new Money(game);
            money.count = countMoney + GameFeature.rand.Next(random);

            money.Drop(currentMap, getRandPosition());
        }


        protected Point getRandPosition()
        {
            Vector2 rndShift;
            Vector2 range = shade.Size.ToVector2() * 0.8f;

            rndShift.X = GameFeature.rand.Next((int)range.X / 2) - range.X;
            rndShift.Y = GameFeature.rand.Next((int)range.Y) - range.Y/2;

            return (positionOnMap + rndShift).ToPoint();
        }

        protected override void Autoattack(GameTime gameTime)
        {
            if (targetForAttack == null || !targetForAttack.characterVisibile)
            {
                isAutoattack = false;

                game.updateGame += brain;
                return;
            }

            currentTime += gameTime.ElapsedGameTime.Milliseconds;
            if (currentTime >= autoattackDelay)
            {
                currentTime -= autoattackDelay;
                Attack(targetForAttack);
                if (targetForAttack != null && !targetForAttack.isAlive)
                {
                    int index = findCharacterInList(targetForAttack);
                    if (index != -1) dmgFromCharacter.Remove(dmgFromCharacter[index]);
                    targetForAttack = null;
                    findMaxDmg();
                }
            }
        }

        private int findCharacterInList(AbstractCharacter character)
        {
            int index = -1;
            for (int i = 0; i < dmgFromCharacter.Count; i++)
            {
                if (dmgFromCharacter[i].character == character)
                {
                    index = i;
                    break;
                }
            }
            return index;
        }


        protected void brain(GameTime gameTime)
        {
            if (currentMap == null)
            {
                game.updateGame -= brain;
                return;
            }

            if (!isAlive) return;

            float minRadius = 1;
            targetForAttack = null;
            for (int i = 0; i < currentMap.listHeroOnMap.Count; i++)
            {
                Vector2 distance = positionOnMap - currentMap.listHeroOnMap[i].positionOnMap;
                float radius = GameFeature.radiusEllipse(distance, rangeAgr);

                if (minRadius >= radius && currentMap.listHeroOnMap[i].characterVisibile && currentMap.listHeroOnMap[i].isAlive)
                {
                    targetForAttack = currentMap.listHeroOnMap[i];
                }
            }

            if (targetForAttack != null)
            {
                isAutoattack = true;
                game.updateGame -= brain;
            }
        }

        public void updateListDmg(AbstractCharacter character, int dmg)
        {
            if (isAlive)
            {
                int index = findCharacterInList(character);
                if (index == -1) dmgFromCharacter.Add(new Damage(character, dmg));
                else dmgFromCharacter[index].damage += dmg;

                isagressive = true;

                findMaxDmg();
            }
        }

        public void findMaxDmg()
        {
            int maxDmg = 0;
            for (int i = 0; i < dmgFromCharacter.Count; i++)
            {
                if (dmgFromCharacter[i].damage >= maxDmg)
                {
                    maxDmg = dmgFromCharacter[i].damage;
                    targetForAttack = dmgFromCharacter[i].character;
                }
            }

            if (!isAutoattack && targetForAttack != null)
            {
                game.updateGame -= brain;
                isAutoattack = true;
            }
        }

        public override void getDamage(AbstractCharacter character)
        {
            base.getDamage(character);
            if (!isDamageImmune) updateListDmg(character, GameFeature.calculateDamage(character.attack, defense));
        }

        public override void Hide()
        {
            base.Hide();
            game.updateGame -= brain;
        }

        public override void Show()
        {
            base.Show();

            if (isAgressive)
            {
                game.updateGame -= brain;
                game.updateGame += brain;
            }
        }
    }
}
