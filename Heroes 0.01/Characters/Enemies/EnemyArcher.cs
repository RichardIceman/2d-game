﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{ 
    public class EnemyArcher : AbstractEnemy
    {
        public EnemyArcher(MyGame game, Vector2 position) : base (game, position)
        {
            animAttack = new Attack(game, this, "Characters/Archer/archer_attack");
            animDie = new Die(game, this, "Characters/Archer/archer_die");
            animHurt = new Hurt(game, this, "Characters/Archer/archer_hurt");
            animMove = new Move(game, this, "Characters/Archer/archer_run");
            animStand = new Stand(game, this, "Characters/Archer/archer_stand");
            ComplateLoad("Characters/Archer/ArcherHead", "Characters/Archer/selectArcherHead");

            name = "Лучник";
        }

        protected override void loadParametrs()
        {
            defaultHealthPoint = 50;
            defaultManaPoint = 50;
            defaultAttack = 1;
            defaultDefense = 1;

            typeAttack = TypeAttack.Лук;
            defaultTypeAttack = TypeAttack.Лук;
            defaultArmorType = ArmorType.Легкая;

            new Bow_1(game).Equipe(this);
            new Quiver_1(game).Equipe(this);
            new LightArmor_1(game).Equipe(this);
            new Belt_1(game).Equipe(this);
            new Boots_1(game).Equipe(this);

            base.loadParametrs();
        }
    }
}
