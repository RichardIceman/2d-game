﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Heroes_0._01
{
    public class GhostBoss : AbstractEnemy
    {
        private bool reveal = false;

        private int time = 0;
        private int delay = 15000;

        private int rangeReveal = 800;

        public GhostBoss(MyGame game, Vector2 position) : base(game, position)
        {
            animAttack = new Attack(game, this, "Characters/GhostBoss/ghostBoss_stand");
            animDie = new Die(game, this, "Characters/GhostBoss/ghostBoss_die");
            animHurt = new Hurt(game, this, "Characters/GhostBoss/ghostBoss_stand");
            animMove = new Move(game, this, "Characters/GhostBoss/ghostBoss_run");
            animStand = new Stand(game, this, "Characters/GhostBoss/ghostBoss_stand");

            animAttack.maxFrame = 6;
            animDie.maxFrame = 6;
            animStand.maxFrame = 6;
            animHurt.maxFrame = 6;
            animMove.maxFrame = 6;
            animStand.maxFrame = 6;

            animHurt.canSkip = true;

            sizeCharacter = 1.5f;

            shiftShade_Y = -30;

            ComplateLoad("Characters/GhostBoss/GhostBossHead", "Characters/GhostBoss/GhostBossHead");

            name = "Темный дух Джу";
        }

        protected override void dropList()
        {
            base.dropList();

            new GhostHeart(game).Drop(currentMap, getRandPosition());

            if (GameFeature.rand.Next(2) == 1) new ResurrectScroll(game).Drop(currentMap, getRandPosition());
        }

        public override void Hide()
        {
            game.updateGame -= spawnMob;
            game.updateGame -= inRange;
            base.Hide();
        }

        protected void spawnMob(GameTime gameTime)
        {
            if (healthBar.Value / MaxHealthPoint > 0.3f)
            {
                time += gameTime.ElapsedGameTime.Milliseconds;

                if (time >= delay)
                {
                    time -= delay;

                    Ghost newMob = new Ghost(game, Vector2.Zero);

                    Vector2 shift = new Vector2(GameFeature.rand.Next(100) + 20, GameFeature.rand.Next(100) + 20);

                    newMob.setPosition(positionOnMap - shift);

                    currentMap.addCharacterOnMap(newMob);
                }
            }
        }

        protected override void loadParametrs()
        {
            defaultHealthPoint = 1000;
            defaultManaPoint = 60;
            defaultAttack = 7;
            defaultDefense = 5;

            defaultRangeAttack = 400;

            autoattackDelay = 1000;

            defaultTypeAttack = TypeAttack.Fireskull;
            typeAttack = TypeAttack.Fireskull;

            countPoints = 1000;
            rangeAgr = 300;
            rangeEXP = 2000;

            countEXP = 1500;

            Lvl = 3;

            base.loadParametrs();
        }

        public override void Show()
        {
            if (!reveal)
            {
                Hide();
                game.updateGame += inRange;
            }
            else
            {
                game.updateGame -= spawnMob;
                game.updateGame += spawnMob;
                base.Show();
            }
        }

        public override void Remove()
        {
            game.updateGame -= inRange;
            game.updateGame -= spawnMob;
            base.Remove();
        }

        private void inRange(GameTime gameTime)
        {
            for (int i = 0; i < currentMap.listHeroOnMap.Count; i++)
            {
                Vector2 distance = currentMap.listHeroOnMap[i].positionOnMap - positionOnMap;
                if (GameFeature.inEllipse(distance, rangeReveal))
                {
                    reveal = true;
                    GhostBossReveal effect = new GhostBossReveal(game, this, sideAnimation.Front, Microsoft.Xna.Framework.Graphics.SpriteEffects.None);
                    effect.Play();
                    effect.animationEnd += Effect_animationEnd;
                    Say("Кто потревожил\nмой покой?!", TypeCharacter.Враг);
                    game.updateGame -= inRange;
                    game.updateGame += spawnMob;
                    Camera.Shake(3, 20);
                    break;
                }
             }
        }

        private void Effect_animationEnd(object sender, EventArgs e)
        {
            if (currentMap.isVisible) Show();
        }
    }
}
