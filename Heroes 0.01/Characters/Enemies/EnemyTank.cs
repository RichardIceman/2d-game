﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class EnemyTank : AbstractEnemy
    {
        public EnemyTank(MyGame game, Vector2 position) : base(game, position)
        {
            animAttack = new Attack(game, this, "Characters/Tank/tank_attack");
            animDie = new Die(game, this, "Characters/Tank/tank_die");
            animHurt = new Hurt(game, this, "Characters/Tank/tank_hurt");
            animMove = new Move(game, this, "Characters/Tank/tank_run");
            animStand = new Stand(game, this, "Characters/Tank/tank_stand");
            ComplateLoad("Characters/Tank/TankHead", "Characters/Tank/selectTankHead");

            name = "Рыцарь";
        }

        protected override void loadParametrs()
        {
            defaultHealthPoint = 70;
            defaultManaPoint = 50;
            defaultAttack = 1;
            defaultDefense = 1;

            typeAttack = TypeAttack.Меч;
            defaultTypeAttack = TypeAttack.Меч;
            defaultArmorType = ArmorType.Тяжелая;

            new Sword_1(game).Equipe(this);
            new Shield_1(game).Equipe(this);
            new HeavyArmor_1(game).Equipe(this);
            new Belt_1(game).Equipe(this);
            new Boots_1(game).Equipe(this);

            new ManaPotion(game).Pickup(this);

            base.loadParametrs();
        }
    }
}
