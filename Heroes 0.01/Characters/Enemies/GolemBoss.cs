﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Heroes_0._01
{
    public class GolemBoss : AbstractEnemy
    {
        private int time = 0;
        private int delay = 7000;

        private bool isRage = false;

        private Vector2 spawnPoint;

        private List<IceWall> listIceWall;

        public GolemBoss(MyGame game, Vector2 position) : base(game, position)
        {
            animAttack = new Attack(game, this, "Characters/GolemBoss/golem_jump");
            animDie = new Die(game, this, "Characters/GolemBoss/golem_die");
            animHurt = new Hurt(game, this, "Characters/GolemBoss/golem_stand");
            animMove = new Move(game, this, "Characters/GolemBoss/golem_run");
            animStand = new Stand(game, this, "Characters/GolemBoss/golem_stand");

            spawnPoint = position;

            animAttack.maxFrame = 6;
            animDie.maxFrame = 6;
            animStand.maxFrame = 6;
            animHurt.maxFrame = 6;
            animMove.maxFrame = 6;
            animStand.maxFrame = 6;

            sizeCharacter = 1.5f;

            shiftShade_Y = -50;

            shakePower = 7;

            listIceWall = new List<IceWall>();

            ComplateLoad("Characters/GolemBoss/GolemHead", "Characters/GolemBoss/GolemHead");

            name = "Глыба";
        }

        protected override void dropList()
        {
            base.dropList();

            new GolemHeart(game).Drop(currentMap, getRandPosition());
            new Handle(game).Drop(currentMap, getRandPosition());

            if (GameFeature.rand.Next(2)==1) new ResurrectScroll(game).Drop(currentMap, getRandPosition());
        }

        public override void Hide()
        {
            game.updateGame -= distanceAttack;
            base.Hide();
        }

        protected void distanceAttack(GameTime gameTime)
        {
            if (isAutoattack)
            {
                time += gameTime.ElapsedGameTime.Milliseconds;

                if (time >= delay)
                {
                    time -= delay;


                    if (healthBar.Value > healthBar.MaxValue * 0.5f)
                    {
                        AbstractCharacter farthestHero = null;

                        Vector2 distance;

                        float range;
                        float maxRange = 0;

                        for (int i = 0; i < currentMap.listHeroOnMap.Count; i++)
                        {
                            if (currentMap.listHeroOnMap[i].isAlive)
                            {
                                distance = positionOnMap - currentMap.listHeroOnMap[i].positionOnMap;

                                range = (float)Math.Sqrt(distance.X * distance.X + distance.Y * distance.Y);

                                if (range > maxRange)
                                {
                                    maxRange = range;
                                    farthestHero = currentMap.listHeroOnMap[i];
                                }
                            }
                        }

                        if (farthestHero != null)
                        {
                            listIceWall.Clear();
                            listIceWall.Add(new IceWall(game, this, Microsoft.Xna.Framework.Graphics.SpriteEffects.None, farthestHero.positionOnMap));
                        }
                    }
                    else //впадает в ярость
                    {
                        if (!isRage)
                        {
                            isRage = true;
                            Say("Вы начинаете мне\nнадоедать!", TypeCharacter.Враг);
                            delay = 5000;
                        }

                        listIceWall.Clear();
                        for (int i = 0; i < currentMap.listHeroOnMap.Count; i++)
                            if (currentMap.listHeroOnMap[i].isAlive)
                                listIceWall.Add(new IceWall(game, this, Microsoft.Xna.Framework.Graphics.SpriteEffects.None, currentMap.listHeroOnMap[i].positionOnMap));
                    }
                }
            }
        }

        protected override void loadParametrs()
        {
            defaultHealthPoint = 2000;
            defaultManaPoint = 60;
            defaultAttack = 15;
            defaultDefense = 7;

            defaultRangeAttack = 200;

            autoattackDelay = 1200;

            defaultMoveSpeed = 5;

            isRage = false;

            defaultTypeAttack = TypeAttack.Меч;
            typeAttack = TypeAttack.Меч;

            countPoints = 1500;
            rangeAgr = 300;
            rangeEXP = 2000;

            countMoney = 500;
            random = 100;

            countEXP = 3000;

            base.loadParametrs();
        }

        public override void Show()
        {
            game.updateGame -= distanceAttack;
            game.updateGame += distanceAttack;
            base.Show();
        }

        public override void Remove()
        {
            game.updateGame -= distanceAttack;

            for (int i = 0; i< listIceWall.Count; i++)
                listIceWall[i].Remove();

            listIceWall.Clear();
            base.Remove();
        }

        private void Effect_animationEnd(object sender, EventArgs e)
        {
            if (currentMap.isVisible) Show();
        }
    }
}
