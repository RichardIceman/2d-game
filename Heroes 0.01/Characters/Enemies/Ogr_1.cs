﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{ 
    public class Ogr_1 : AbstractEnemy
    {
        public Ogr_1(MyGame game, Vector2 position) : base (game, position)
        {
            animAttack = new Attack(game, this, "Characters/Ogr_1/ogr_1_attack");
            animDie = new Die(game, this, "Characters/Ogr_1/ogr_1_die");
            animHurt = new Hurt(game, this, "Characters/Ogr_1/ogr_1_hurt");
            animMove = new Move(game, this, "Characters/Ogr_1/ogr_1_run");
            animStand = new Stand(game, this, "Characters/Ogr_1/ogr_1_stand");

            typeCharacter = TypeCharacter.Враг;

            animAttack.maxFrame = 7;
            animDie.maxFrame = 7;
            animStand.maxFrame = 7;
            animHurt.maxFrame = 7;
            animMove.maxFrame = 7;
            animStand.maxFrame = 7;

            sizeCharacter = 1.2f;
            sizeShade_Y = 0.15f;
            shadeWidth = 120;
            shiftShade_X = -10;
            shiftShade_Y = 10;
            shiftHealthBar = -80;

            ComplateLoad("Characters/Ogr_1/ogr_1_head", "Characters/Ogr_1/ogr_1_head");

            isDamageImmune = true;

            isAgressive = false;

            name = "Грог";
        }

        protected override void loadParametrs()
        {
            defaultHealthPoint = 2000;
            defaultManaPoint = 50;
            defaultAttack = 40;
            defaultDefense = 25;

            typeAttack = TypeAttack.Меч;
            defaultTypeAttack = TypeAttack.Меч;

            base.loadParametrs();
        }
    }
}
