﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Heroes_0._01
{
    public class DemonBoss : AbstractEnemy
    {
        private bool reveal = false;

        private FireWall firewall;

        private int time_1 = 0;
        private int delay_1 = 6000;

        private int time_2 = 0;
        private int delay_2 = 8000;

        private bool isBlind = false;

        private int rangeReveal = 800;

        private List<Meteorit> listMeteorits;

        public DemonBoss(MyGame game, Vector2 position) : base(game, position)
        {
            animAttack = new Attack(game, this, "Characters/Demon/demon-attack-no-breath");
            animDie = new Die(game, this, "Characters/Demon/demon-idle");
            animHurt = new Hurt(game, this, "Characters/Demon/demon-idle");
            animMove = new Move(game, this, "Characters/Demon/demon-idle");
            animStand = new Stand(game, this, "Characters/Demon/demon-idle");

            animAttack.maxFrame = 8;
            animDie.maxFrame = 6;
            animStand.maxFrame = 6;
            animHurt.maxFrame = 6;
            animMove.maxFrame = 6;
            animStand.maxFrame = 6;

            animAttack.isReverse = true;
            animDie.isReverse = true;
            animStand.isReverse = true;
            animHurt.isReverse = true;
            animMove.isReverse = true;
            animStand.isReverse = true;

            shiftHealthBar = -70;

            sizeCharacter = 2.2f;

            shiftShade_Y = -50;
            sizeShade_Y = 0.12f;
            shadeWidth = 100;

            shiftTxtBox_Y = 150;

            listMeteorits = new List<Meteorit>();

            ComplateLoad("Characters/Demon/demonHead", "Characters/Demon/demonHead");

            name = "Харон";

            died += DemonBoss_died;
        }

        protected override void dropList()
        {
            base.dropList();

            new DemonHeart(game).Drop(currentMap, getRandPosition());
            new Blade(game).Drop(currentMap, getRandPosition());

            if (GameFeature.rand.Next(2) == 1) new ResurrectScroll(game).Drop(currentMap, getRandPosition());
        }

        private void DemonBoss_died(object sender, EventArgs e)
        {
            died -= DemonBoss_died;

            for (int i = 0; i < currentMap.listHeroOnMap.Count; i++)
               if (currentMap.listHeroOnMap[i].isAlive)
                    currentMap.listHeroOnMap[i].ScaleVision = currentMap.listHeroOnMap[i].defaultScaleVision;
        }

        public override void Hide()
        {
            game.updateGame -= massAttack;
            game.updateGame -= inRange;
            base.Hide();
        }

        protected void massAttack(GameTime gameTime)
        {
            if (isAutoattack)
            {
                time_1 += gameTime.ElapsedGameTime.Milliseconds;
                time_2 += gameTime.ElapsedGameTime.Milliseconds;

                if (time_1 >= delay_1) //огонь
                {
                    time_1 -= delay_1;

                    if (!isDamageImmune)
                    {
                        SpriteEffects flip = SpriteEffects.None;
                        if (shift.X < 0) flip = SpriteEffects.FlipHorizontally;

                        firewall = new FireWall(game, this, flip, positionOnMap);
                    }
                }

                if (time_2 >= delay_2)//метеорит
                {
                    time_2 -= delay_2;

                    AbstractCharacter farthestHero = null;

                    Vector2 distance;

                    float range;
                    float maxRange = 0;

                    for (int i = 0; i < currentMap.listHeroOnMap.Count; i++)
                    {
                        if (currentMap.listHeroOnMap[i].isAlive)
                        {
                            distance = positionOnMap - currentMap.listHeroOnMap[i].positionOnMap;

                            range = (float)Math.Sqrt(distance.X * distance.X + distance.Y * distance.Y);

                            if (range > maxRange)
                            {
                                maxRange = range;
                                farthestHero = currentMap.listHeroOnMap[i];
                            }
                        }
                    }

                    if (farthestHero != null)
                    {
                        listMeteorits.Clear();
                        listMeteorits.Add(new Meteorit(game, this, SpriteEffects.None, farthestHero.positionOnMap));
                    }
                }
            }

            if (healthBar.Value <= healthBar.MaxValue * 0.2f && !isBlind)
            {
                isBlind = true;

                Say("Да наступит...\nТьма!!!", TypeCharacter.Враг);

                for (int i = 0; i < currentMap.listHeroOnMap.Count; i++)
                    if (currentMap.listHeroOnMap[i].isAlive) currentMap.listHeroOnMap[i].ScaleVision = 4;
            }
        }

        protected override void loadParametrs()
        {
            defaultHealthPoint = 4000;
            defaultManaPoint = 60;
            defaultAttack = 5;
            defaultDefense = 5;

            defaultRangeAttack = 500;

            autoattackDelay = 1200;

            defaultMoveSpeed = 5;

            Lvl = 10;

            defaultTypeAttack = TypeAttack.Fireskull;
            typeAttack = TypeAttack.Fireskull;

            countPoints = 2500;
            rangeAgr = 300;
            rangeEXP = 2000;

            countEXP = 3000;

            base.loadParametrs();
        }

        public override void Show()
        {
            if (!reveal)
            {
                Hide();
                game.updateGame += inRange;
            }
            else
            {
                game.updateGame -= massAttack;
                game.updateGame += massAttack;
                base.Show();
            }
        }

        public override void Remove()
        {
            game.updateGame -= inRange;
            game.updateGame -= massAttack;
            firewall?.Remove();
            firewall = null;

            for (int i = 0; i < listMeteorits.Count; i++)
                listMeteorits[i].Remove();

            listMeteorits.Clear();

            base.Remove();
        }

        private void inRange(GameTime gameTime)
        {
            for (int i = 0; i < currentMap.listHeroOnMap.Count; i++)
            {
                Vector2 distance = currentMap.listHeroOnMap[i].positionOnMap - positionOnMap;
                if (GameFeature.inEllipse(distance, rangeReveal))
                {
                    reveal = true;
                    GhostBossReveal effect = new GhostBossReveal(game, this, sideAnimation.Front, Microsoft.Xna.Framework.Graphics.SpriteEffects.None);
                    effect.Play();
                    effect.animationEnd += Effect_animationEnd;
                    Say("Зря вы сюда\nпришли!", TypeCharacter.Враг);
                    game.updateGame -= inRange;
                    game.updateGame += massAttack;
                    Camera.Shake(3, 20);
                    break;
                }
             }
        }

        private void Effect_animationEnd(object sender, EventArgs e)
        {
            if (currentMap.isVisible) Show();
        }
    }
}
