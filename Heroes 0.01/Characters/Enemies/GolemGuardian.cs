﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Heroes_0._01
{
    public class GolemGuardian : AbstractEnemy
    {

        public GolemGuardian(MyGame game, Vector2 position) : base(game, position)
        {
            animAttack = new Attack(game, this, "Characters/GolemBoss/golem_jump");
            animDie = new Die(game, this, "Characters/GolemBoss/golem_die");
            animHurt = new Hurt(game, this, "Characters/GolemBoss/golem_stand");
            animMove = new Move(game, this, "Characters/GolemBoss/golem_run");
            animStand = new Stand(game, this, "Characters/GolemBoss/golem_stand");

            animAttack.maxFrame = 6;
            animDie.maxFrame = 6;
            animStand.maxFrame = 6;
            animHurt.maxFrame = 6;
            animMove.maxFrame = 6;
            animStand.maxFrame = 6;

            sizeCharacter = 0.8f;

            shiftShade_Y = -10;

            shakePower = 7;

            ComplateLoad("Characters/GolemBoss/GolemHead", "Characters/GolemBoss/GolemHead");

            name = "Осколок";
        }

        protected override void loadParametrs()
        {
            defaultHealthPoint = 200;
            defaultManaPoint = 60;
            defaultAttack = 10;
            defaultDefense = 8;

            defaultRangeAttack = 200;

            autoattackDelay = 1200;

            defaultMoveSpeed = 3;

            Lvl = 4;

            defaultTypeAttack = TypeAttack.Меч;
            typeAttack = TypeAttack.Меч;

            countPoints = 150;
            rangeAgr = 300;
            rangeEXP = 2000;

            countEXP = 150;

            base.loadParametrs();
        }

        private void Effect_animationEnd(object sender, EventArgs e)
        {
            if (currentMap.isVisible) Show();
        }
    }
}
