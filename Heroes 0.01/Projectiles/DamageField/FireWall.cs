﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Heroes_0._01
{
    public class FireWall : AbstractDamageField
    {
        private bool previouseCanMove;

        public FireWall(MyGame _game, AbstractCharacter _character, SpriteEffects _flip, Vector2 _position) : base(_game, _character, _flip, _position)
        {
            previouseCanMove = _character.canMove;
            _character.canMove = false;
            damage = 30;
        }

        protected override void effect()
        {
            character.canMove = previouseCanMove;

            if (this.flip == SpriteEffects.FlipHorizontally) character.shift.X = -1;
            else character.shift.X = 1;

            character.animAttack.Play();

            float zIndex = GameFeature.getZIndex(fieldRect.Y + fieldRect.Height);

            Point position = new Point(fieldRect.X - fieldRect.Width / 6, fieldRect.Y + fieldRect.Height);

            SpriteEffects flip = SpriteEffects.None;
            if (this.flip == flip)
            {
                flip = SpriteEffects.FlipHorizontally;
                position.X = fieldRect.X;
            }

            animation = new FireBreath(game, zIndex, position, flip);
            animation.Play();

            for (int i = 0; i < character.currentMap.listHeroOnMap.Count; i++)
                if (character.currentMap.listHeroOnMap[i].isAlive && fieldRect.Intersects(character.currentMap.listHeroOnMap[i].shade))
                    character.currentMap.listHeroOnMap[i].getDamage(GameFeature.calculateDamage(damage, character.currentMap.listHeroOnMap[i].defense));
        }

        protected override void load()
        {
            width = 400;
            height = 200;

            speed = new Vector2(2, 2);

            fieldType = FieldType.rectangle;
        }
    }
}
