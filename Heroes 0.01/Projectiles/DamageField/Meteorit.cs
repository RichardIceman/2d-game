﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Heroes_0._01
{
    public class Meteorit : AbstractDamageField
    {
        public Meteorit(MyGame _game, AbstractCharacter _character, SpriteEffects _flip, Vector2 _position) : base(_game, _character, _flip, _position)
        {
            damage = 50;
        }

        protected override void effect()
        {
            Point position = new Point(fieldRect.X, fieldRect.Y + fieldRect.Height * 4 / 5);

            float zIndex = GameFeature.getZIndex(fieldRect.Y + fieldRect.Height / 2);

            animation = new MeteoritEffect(game, zIndex, position, SpriteEffects.None);
            animation.Play();

            for (int i = 0; i < character.currentMap.listHeroOnMap.Count; i++)
            {
                Vector2 distance = fieldRect.Location.ToVector2() + fieldRect.Size.ToVector2() / 2 - character.currentMap.listHeroOnMap[i].positionOnMap;

                if (character.currentMap.listHeroOnMap[i].isAlive && GameFeature.inEllipse(distance, (height + character.currentMap.listHeroOnMap[i].shade.Height) / 2))
                {
                    character.currentMap.listHeroOnMap[i].getDamage(GameFeature.calculateDamage(damage, character.currentMap.listHeroOnMap[i].defense));

                    sideAnimation side = sideAnimation.Front;
                    if (character.currentMap.listHeroOnMap[i].zIndex > zIndex) side = sideAnimation.Back;

                    new FireballHitEffect(game, character.currentMap.listHeroOnMap[i], side, SpriteEffects.None).Play();
                }
            }
        }

        protected override void load()
        {
            width = 300;
            height = 300;

            speed = new Vector2(2, 2);

            fieldType = FieldType.ellipse;
        }
    }
}
