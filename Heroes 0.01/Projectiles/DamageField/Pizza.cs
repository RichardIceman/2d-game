﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Heroes_0._01
{
    class Pizza : AbstractDamageField
    {
        private float safeAngle = (float)Math.PI * 3 / 4;

        public Pizza(MyGame _game, AbstractCharacter _character, SpriteEffects _flip, Vector2 _position, float _angle) : base(_game, _character, _flip, _position)
        {
            damage = 70;
            angle = _angle;

            safeAngle -= angle;

            if (safeAngle <= 0) safeAngle += 2 * (float)Math.PI;
        }

        protected override void effect()
        {
            Point position = fieldRect.Location;

            animation = new PizzaAnimation(game, position, angle);
            animation.animationEnd += Animation_animationEnd;     
            animation.Play();

            for (int i = 0; i < character.currentMap.listHeroOnMap.Count; i++)
            {
                Vector2 distance = character.currentMap.listHeroOnMap[i].positionOnMap - position.ToVector2();


                float angle_character = (float)Math.Atan(Math.Abs(distance.Y / distance.X));

                if (distance.X < 0 && distance.Y < 0) angle_character = (float)Math.PI - angle_character;
                else if (distance.X < 0 && distance.Y >= 0) angle_character += (float)Math.PI;
                else if (distance.X >= 0 && distance.Y >= 0) angle_character = 2 * (float)Math.PI - angle_character;

                if (!(angle_character <= safeAngle && angle_character >= safeAngle - Math.PI/4))
                    character.currentMap.listHeroOnMap[i].getDamage(damage);
            }
        }

        protected override void load()
        {
            width = 6000;
            height = 6000 / GameFeature.scaleEllipse_Y;

            speed = new Vector2(50, 50 / GameFeature.scaleEllipse_Y);

            texture = game.Content.Load<Texture2D>("Effect/pizza");

            origin = new Vector2(texture.Width / 2, texture.Height / 2);

            fieldType = FieldType.ellipse;
        }
    }
}
