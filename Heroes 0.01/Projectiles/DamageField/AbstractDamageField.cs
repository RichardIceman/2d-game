﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{

    public enum FieldType
    {
        ellipse,
        rectangle
    }

    public abstract class AbstractDamageField
    {
        public event EventHandler end;

        protected AbstractCharacter character;
        protected MyGame game;
        protected FieldType fieldType;
        protected SpriteEffects flip;

        protected Vector2 origin = Vector2.Zero;
        protected float angle = 0;

        public AbstractAnimations animation;

        protected int damage = 0;

        protected Vector2 speed;

        private int time;

        protected Rectangle fieldRect;
        protected Rectangle loadFieldRect;

        protected float width = 50;
        protected float height = 50;

        private float loadWidth;
        private float loadHeight;

        protected Texture2D texture;

        protected Color color = Color.Red * 0.4f;

        protected float zIndex = GameFeature.zIndexStep;
        protected float zIndexLoad = 2 * GameFeature.zIndexStep;

        public AbstractDamageField(MyGame _game, AbstractCharacter _character, SpriteEffects _flip, Vector2 _position)
        {
            character = _character;
            game = _game;
            flip = _flip;

            load();

            width *= GameFeature.scaleEllipse_X;
            height *= GameFeature.scaleEllipse_Y;

            speed.X = speed.X * GameFeature.scaleEllipse_X;
            speed.Y = speed.Y * GameFeature.scaleEllipse_Y;

            if (fieldType == FieldType.rectangle)
            {
                if (texture == null) texture = game.Content.Load<Texture2D>("Characters/Shades/white");

                if (flip == SpriteEffects.None)
                {
                    fieldRect = new Rectangle((int)_position.X, (int)(_position.Y - height / 2), (int)width, (int)height);
                    loadFieldRect = new Rectangle((int)_position.X, (int)(_position.Y - height / 2), 1, (int)height);
                }
                else
                {
                    fieldRect = new Rectangle((int)(_position.X - width), (int)(_position.Y - height / 2), (int)width, (int)height);
                    loadFieldRect = new Rectangle((int)(_position.X - width), (int)(_position.Y - height / 2), 1, (int)height);
                }
            }
            else //эллипс
            {
                if (texture == null) texture = game.Content.Load<Texture2D>("Characters/Shades/light");

                if (origin == Vector2.Zero)
                {
                    fieldRect = new Rectangle((int)(_position.X - width / 2), (int)(_position.Y - height / 2), (int)width, (int)height);
                    loadFieldRect = new Rectangle((int)(_position.X - width / 2), (int)(_position.Y - height / 2), 1, 1);
                }
                else
                {
                    fieldRect = new Rectangle((int)_position.X, (int)_position.Y, (int)width, (int)height);
                    loadFieldRect = new Rectangle((int)_position.X, (int)_position.Y, 1, 1);
                }
            }

            game.drawGameObject += draw;
            game.updateGame += update;
        }

        protected void Animation_animationEnd(object sender, EventArgs e)
        {
            animation.animationEnd -= Animation_animationEnd;
            end?.Invoke(this, null);
        }

        protected void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, fieldRect, null, color, angle, origin, SpriteEffects.None, zIndex);
            spriteBatch.Draw(texture, loadFieldRect, null, color, angle, origin, SpriteEffects.None, zIndexLoad);
        }

        protected void update(GameTime gameTime)
        {
            time += gameTime.ElapsedGameTime.Milliseconds;

            if (loadWidth < width)
            {
                if (fieldType == FieldType.rectangle)
                {
                    if (flip == SpriteEffects.None)
                    {
                        loadWidth += speed.X;
                        loadFieldRect.Width = (int)loadWidth;
                    }
                    else
                    {
                        loadWidth += speed.X;
                        loadFieldRect.Width = (int)loadWidth;

                        loadFieldRect.X = fieldRect.X + fieldRect.Width - loadFieldRect.Width;
                    }
                }
                else //эллипс
                {
                    loadWidth += speed.X;
                    loadHeight += speed.Y;

                    loadFieldRect.Width = (int)loadWidth;
                    loadFieldRect.Height = (int)loadHeight;

                    if (origin == Vector2.Zero)
                    {
                        loadFieldRect.X = fieldRect.X + (fieldRect.Width - loadFieldRect.Width) / 2;
                        loadFieldRect.Y = fieldRect.Y + (fieldRect.Height - loadFieldRect.Height) / 2;
                    }
                }
            }
            else //конец загрузки
            {
                if (character.isAlive) effect();

                Remove();
            }
        }

        protected abstract void load();
        protected abstract void effect();

        public virtual void Remove()
        {
            character = null;
            game.updateGame -= update;
            game.drawGameObject -= draw;
        }
    }
}
