﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Arrow : AbstractProjectile
    {
        public Arrow(MyGame game, Vector2 startPosition, AbstractCharacter target, AbstractCharacter character) : base (game, startPosition, target, character)
        {
            speed = 25;
        }

        protected override void load()
        {
            width = 100;
            height = 50;
            shiftY = -20;
            flyEffect = new ArrowEffect(game, this);
        }

        protected override void hit()
        {
            sideAnimation side = sideAnimation.Front;
            SpriteEffects flip = SpriteEffects.None;
            if (shift.Y > 0) side = sideAnimation.Back;
            if (shift.X < 0) flip = SpriteEffects.FlipHorizontally;

            new ArrowHitEffect(game, target, side, flip).Play();
            base.hit();
        }
    }
}
