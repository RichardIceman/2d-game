﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Fireball : AbstractProjectile
    {
        public Fireball(MyGame game, Vector2 startPosition, AbstractCharacter target, AbstractCharacter character) : base(game, startPosition, target, character)
        {
            speed = 10; 
        }

        protected override void load()
        {
            width = 80;
            height = 80;
            shiftY = -30;
            flyEffect = new FireballEffect(game, this);
        }

        protected override void hit()
        {
            sideAnimation side = sideAnimation.Front;
            SpriteEffects flip = SpriteEffects.None;
            if (shift.Y > 0) side = sideAnimation.Back;
            if (shift.X < 0) flip = SpriteEffects.FlipHorizontally;

            new FireballHitEffect(game, target, side, flip).Play();
            base.hit();
        }
    }
}
