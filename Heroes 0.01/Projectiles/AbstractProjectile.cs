﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public abstract class AbstractProjectile
    {
        protected MyGame game;

        public Rectangle drawField;

        protected AbstractProjectileEffect flyEffect;

        protected AbstractCharacter target;
        protected AbstractCharacter character;

        public SpriteEffects flip = SpriteEffects.None;

        protected Vector2 shift;
        private Vector2 position;
        private Vector2 targetPosition;
        public Vector2 origin;
        public float zIndex = 0;

        protected int width = 50;
        protected int height = 10;

        protected int shiftY = 0;

        protected float speed = 1;

        public float angle = 0;

        public AbstractProjectile(MyGame game, Vector2 startPosition , AbstractCharacter target, AbstractCharacter character)
        {
            this.game = game;
            this.target = target;
            this.character = character;

            load();

            position.X = startPosition.X;
            position.Y = startPosition.Y + shiftY;

            drawField = new Rectangle((int)position.X, (int)position.Y, width, height);
            origin = new Vector2(drawField.Width, drawField.Height);

            getShift();

         

            game.updateGame += update;
            flyEffect.Play();
        }

        protected abstract void load();

        private void update(GameTime gameTime)
        {
            Vector2 newTargetPosition = new Vector2(target.drawField.X + target.drawField.Width / 2 - drawField.Width/2,
                                                    target.drawField.Y + target.drawField.Height / 2 + shiftY);

            if (targetPosition != newTargetPosition) getShift();

            if (position.X + speed > targetPosition.X && position.X - speed < targetPosition.X &&
                position.Y + speed > targetPosition.Y && position.Y - speed < targetPosition.Y)
            {
                hit();
                return;
            }

            position.X += speed * shift.X;
            position.Y += speed * shift.Y;

            drawField.X = (int)position.X + width / 2;
            drawField.Y = (int)position.Y + height / 2;

            zIndex = GameFeature.getZIndex(position.Y + target.drawField.Height / 2 - shiftY);
        }

        protected virtual void hit()
        {
            game.updateGame -= update;
            flyEffect.Remove();

            target.getDamage(character);

            character = null;
            game = null;
            target = null;
        }

        protected bool isReverse = false;

        private void getShift()
        {
            targetPosition = new Vector2(target.drawField.X + target.drawField.Width / 2 - drawField.Width/2, target.drawField.Y + target.drawField.Height / 2 + shiftY);
            Vector2 distance = targetPosition - position;

            if (Math.Abs(distance.X) >= Math.Abs(distance.Y))
            {
                shift.X = distance.X / Math.Abs(distance.X);
                shift.Y = distance.Y / Math.Abs(distance.X);
            }
            else
            {
                shift.X = distance.X / Math.Abs(distance.Y);
                shift.Y = distance.Y / Math.Abs(distance.Y);
            }

            angle = (float)(Math.Atan(Math.Abs(shift.Y / shift.X)));

            if (!isReverse)
            {

                flip = SpriteEffects.None;
                if (shift.X >= 0 && shift.Y <= 0)
                {
                    angle = -angle;
                }
                else if (shift.X < 0 && shift.Y < 0)
                {
                    flip = SpriteEffects.FlipHorizontally;
                }
                else if (shift.X < 0 && shift.Y > 0)
                {
                    angle = -angle;
                    flip = SpriteEffects.FlipHorizontally;
                }
            }
            else
            {
                flip = SpriteEffects.FlipHorizontally;
                if (shift.X >= 0 && shift.Y <= 0)
                {
                    angle = -angle;
                }
                else if (shift.X < 0 && shift.Y < 0)
                {
                    flip = SpriteEffects.None;
                }
                else if (shift.X < 0 && shift.Y > 0)
                {
                    angle = -angle;
                    flip = SpriteEffects.None;
                }
            }
        }       
    }
}
