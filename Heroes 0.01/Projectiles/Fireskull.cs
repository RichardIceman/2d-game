﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Fireskull : AbstractProjectile
    {
        public Fireskull(MyGame game, Vector2 startPosition, AbstractCharacter target, AbstractCharacter character) : base(game, startPosition, target, character)
        {
            speed = 10;
        }

        protected override void load()
        {
            width = 60;
            height = 60;
            shiftY = -30;
            isReverse = true;
            flyEffect = new FireskullEffect(game, this);
        }

        protected override void hit()
        {
            sideAnimation side = sideAnimation.Front;
            SpriteEffects flip = SpriteEffects.None;
            if (shift.Y > 0) side = sideAnimation.Back;
            if (shift.X < 0) flip = SpriteEffects.FlipHorizontally;

            new FireskullHitEffect(game, target, side, flip).Play();
            base.hit();
        }
    }
}
