﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class DampingScreen
    {
        public event EventHandler animationEnd;

        private float dampingScale;
        private int dampingSpeed = 20;
        private float dampingStep = 0.025f;

        private int time;

        private MyGame game;

        private Texture2D texture;
        private Rectangle screenRect;

        private Color color;
        private Color defaultColor = Color.Black;

        public DampingScreen(MyGame game)
        {
            this.game = game;
            texture = game.Content.Load<Texture2D>("Characters/Shades/white");
            screenRect = new Rectangle(Point.Zero, Camera.sizeWnd.ToPoint());
        }

        private void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, screenRect, null, color, 0, Vector2.Zero, SpriteEffects.None, 1);
        }

        private void update(GameTime gameTime)
        {
            time += gameTime.ElapsedGameTime.Milliseconds;

            if (time >= dampingSpeed)
            {
                time -= dampingSpeed;

                dampingScale += dampingStep;
                if (dampingScale > 1)
                {
                    Stop();
                    animationEnd?.Invoke(this, null);
                    return;
                }
                if (dampingScale < 0)
                {
                    Stop();
                    animationEnd?.Invoke(this, null);
                    return;
                }
                color = defaultColor * dampingScale;
            }
        }

        public void Play()
        {
            dampingStep = Math.Abs(dampingStep);
            dampingScale = 0;
            color = defaultColor * dampingScale;

            Stop();

            game.updateInterface += update;
            game.drawInterface += draw;
        }

        public void PlayRevers()
        {
            dampingStep = -Math.Abs(dampingStep);
            dampingScale = 1;
            color = defaultColor * dampingScale;

            Stop();

            game.updateInterface += update;
            game.drawInterface += draw;
        }

        public void Stop()
        {
            game.updateInterface -= update;
            game.drawInterface -= draw;
        }
    }
}
