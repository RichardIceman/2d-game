﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class PauseEffect
    {
        private float dampingScale;
        private int dampingSpeed = 20;
        private float dampingStep = 0.025f;
        private float maxScale = 0.5f;

        private int time;

        private MyGame game;

        private Texture2D texture;

        private Rectangle screenRect;


        private Color defaultColor = Color.DimGray;
        private Color color;

        private float zIndex = 0.5f;

        public PauseEffect(MyGame game)
        {
            this.game = game;
            texture = game.Content.Load<Texture2D>("Characters/Shades/white");

            screenRect = new Rectangle(Point.Zero, Camera.sizeWnd.ToPoint());
        }

        private void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, screenRect, null, color, 0, Vector2.Zero, SpriteEffects.None, zIndex);
        }

        private void update(GameTime gameTime)
        {
            time += gameTime.ElapsedGameTime.Milliseconds;

            if (time >= dampingSpeed)
            {
                time -= dampingSpeed;

                dampingScale += dampingStep;

                if (dampingScale >= maxScale) 
                {
                    game.updateInterface -= update;
                    return;
                }
                else if (dampingScale <= 0)
                {
                    game.drawInterface -= draw;
                    game.updateInterface -= update;
                    return;
                }
                
                color = defaultColor * dampingScale;
            }
        }

        public void Play()
        {
            dampingStep = Math.Abs(dampingStep);

            dampingScale = dampingStep;

            color = defaultColor * dampingScale;

            game.drawInterface -= draw;
            game.updateInterface -= update;
            game.drawInterface += draw;
            game.updateInterface += update;
        }

        public void Stop()
        {
            dampingStep = -Math.Abs(dampingStep);

            dampingScale = maxScale + dampingStep;

            game.updateInterface -= update;
            game.updateInterface += update;

            color = defaultColor * dampingScale;
        }
    }
}
