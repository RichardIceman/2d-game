﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace Heroes_0._01
{
    public class FallArrowEffect : AbstractAnimations
    {
        public FallArrowEffect(MyGame game, float zIndex, Point position, SpriteEffects flip) : base(game)
        {
            maxframe = 7;
            drawField.Location = position;
            drawField.Width = 75;
            drawField.Height = 150;
            delay = 80;
            this.zIndex = zIndex;
            this.flip = flip;
            loadAnimation("Effect/arrowspell");

            game.Content.Load<SoundEffect>("Songs/arrowFall").Play(0.4f,0.5f,0);

            isVisible = true;
        }

        public void updatePosition(Point newPosition)
        {
            drawField.Location = newPosition;
        }

        protected override void onAnimationEnd()
        {
            base.onAnimationEnd();
            Remove();
        }
    }
}
