﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class StampAnimation : AbstractAnimations
    {
        public StampAnimation(MyGame game, Point position) : base(game)
        {
            maxframe = 14;
            drawField.Location = position;
            drawField.Width = 300;
            drawField.Height = 450;
            delay = 40;

            zIndex = GameFeature.zIndexStep;

            loadAnimation("Effect/stamp");
        }

        protected override void Update(GameTime gameTime)
        {
            currentTime += gameTime.ElapsedGameTime.Milliseconds;
            if (currentTime >= delay)
            {
                currentFrame++;
                if (currentFrame >= maxframe)
                    currentFrame = 0;
                

                currentTime -= delay;
                frame.X = currentFrame * frameWidth;
            }
        }
    }
}
