﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    class GhostBossReveal : AbstractEffect
    {
        public GhostBossReveal(MyGame game, AbstractCharacter character, sideAnimation side, SpriteEffects flip) : base(game, character, side, flip)
        {
            width = 400;
            height = 400;
            shiftY = 30;
            maxframe = 6;
            delay = 100;
 
            loadEffect("Characters/Ghost/ghost-appears", "Songs/Arcane_AttackF2");
        }
    }
}
