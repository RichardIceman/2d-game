﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace Heroes_0._01
{
    public enum sideAnimation
    {
        Back,
        Front
    }


    public class AbstractEffect : AbstractAnimations
    {
        protected AbstractCharacter character;
        protected sideAnimation side;
        protected int width = 50;
        protected int height = 50;
        protected int shiftX = 0;
        public int shiftY = 0;

        protected Point position;
        protected SoundEffect sound;
        protected float pitch = 0;

        protected float volume = 1;

        public AbstractEffect(MyGame game, AbstractCharacter character, sideAnimation side, SpriteEffects flip): base(game)
        {
            this.character = character;
            this.side = side;
            this.flip = flip;  
        }

        protected virtual void loadEffect(string pathAnim, string pathSound)
        {
            sound = game.Content.Load<SoundEffect>(pathSound);

            drawField = new Rectangle(0, 0, width, height);
            position = character.drawField.Location;
            if (side == sideAnimation.Front) zIndex = character.zIndex + GameFeature.zIndexStep;
            else zIndex = character.zIndex - GameFeature.zIndexStep;

            updatePosition();

            loadAnimation(pathAnim);
        }

        protected void updatePosition()
        {
            drawField.X = character.shade.X + (character.shade.Width - drawField.Width) / 2 + shiftX;
            drawField.Y = character.shade.Y - drawField.Height + (int)(drawField.Height - character.sizeAvatar.Y) / 2 + shiftY;
        }

        public override bool Play()
        {
            updatePosition();
            sound.Play(volume, pitch, 0);
            return base.Play();
        }

        protected override void Update(GameTime gameTime)
        {
            if (position != character.drawField.Location)
            {
                position = character.drawField.Location;
                if (side == sideAnimation.Front) zIndex = character.zIndex + GameFeature.zIndexStep;
                else zIndex = character.zIndex - GameFeature.zIndexStep;

                updatePosition();
            }
            base.Update(gameTime);
        }

        protected override void onAnimationEnd()
        {
            Remove();
            base.onAnimationEnd();
        }

        protected override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, drawField, frame, color, 0, Vector2.Zero, flip, zIndex);
        }

        public override void Remove()
        {
            character = null;
            base.Remove();
        }

    }
}
