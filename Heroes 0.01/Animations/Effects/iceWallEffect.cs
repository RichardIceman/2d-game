﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace Heroes_0._01
{
    public class IceWallEffect : AbstractAnimations
    {
        public IceWallEffect(MyGame game, float zIndex, Point position, SpriteEffects flip) : base(game)
        {
            maxframe = 14;

            drawField.Width = 300;
            drawField.Height = 300;

            drawField.X = position.X;
            drawField.Y = position.Y - drawField.Height;

            delay = 30;

            this.zIndex = zIndex;
            this.flip = flip;

            loadAnimation("Effect/iceWall");            

            game.Content.Load<SoundEffect>("Songs/arrowFall").Play();

            Camera.Shake(1, 30);

            isVisible = true;
        }

        protected override void onAnimationEnd()
        {
            base.onAnimationEnd();
            Remove();
        }
    }
}
