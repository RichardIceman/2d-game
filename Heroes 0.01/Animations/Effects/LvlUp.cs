﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    class LvlUp : AbstractEffect
    {
        public LvlUp(MyGame game, AbstractCharacter character, sideAnimation side, SpriteEffects flip) : base(game, character, side, flip)
        {
            width = 250;
            height = 250;
            shiftY = 50;
            maxframe = 11;
            delay = 80;
            flip = SpriteEffects.FlipVertically;
            loadEffect("Effect/resurection", "Songs/Arcane_AttackF2");
        }
    }
}
