﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    class Protection : AbstractEffect
    {
        public Protection(MyGame game, AbstractCharacter character, sideAnimation side, SpriteEffects flip) : base(game, character, side, flip)
        {
            width = 250;
            height = 250;
            shiftY = 40;
            maxframe = 20;
            delay = 50;
            canSkip = true;
            color = Color.MediumPurple;
            loadEffect("Effect/resistance", "Songs/Arcane_AttackF2");
        }

        protected override void Update(GameTime gameTime)
        {
            if (position != character.drawField.Location)
            {
                position = character.drawField.Location;
                if (side == sideAnimation.Front) zIndex = character.zIndex + GameFeature.zIndexStep;
                else zIndex = character.zIndex - GameFeature.zIndexStep;

                updatePosition();
            }

            currentTime += gameTime.ElapsedGameTime.Milliseconds;
            if (currentTime >= delay)
            {
                currentTime -= delay;
                frame.X = currentFrame * frameWidth;
                currentFrame = (currentFrame + 1) % maxframe;
            }
        }
    }
}
