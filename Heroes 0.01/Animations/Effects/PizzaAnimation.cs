﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class PizzaAnimation : AbstractAnimations
    {
        private int duration = 500;
        private int time;

        public PizzaAnimation(MyGame game, Point position, float _angle) : base(game)
        {
            maxframe = 1;
            drawField.Width = 6000;
            drawField.Height = 6000;

            drawField.Location = position;

            angle = _angle;

            delay = 40;

            zIndex = GameFeature.zIndexStep;

            loadAnimation("Effect/pizza");

            Camera.Shake(0.5f, 100);

            origin = new Vector2(texture.Width / 2, texture.Height / 2);
        }

        protected override void Update(GameTime gameTime)
        {
            currentTime += gameTime.ElapsedGameTime.Milliseconds;
            time += gameTime.ElapsedGameTime.Milliseconds;
            if (time > duration)
            {
                onAnimationEnd();
                return;
            }

            if (currentTime >= delay)
            {
                currentFrame++;
                if (currentFrame >= maxframe)
                    currentFrame = 0;
                

                currentTime -= delay;
                frame.X = currentFrame * frameWidth;
            }

            float kef = (float)GameFeature.rand.Next(40, 80) / 100;

            color = Color.Black * kef;
        }
    }
}
