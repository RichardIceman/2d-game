﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    class Heal : AbstractEffect
    {
        public Heal(MyGame game, AbstractCharacter character, sideAnimation side, SpriteEffects flip) : base(game, character, side, flip)
        {
            width = 200;
            height = 200;
            shiftY = 30;
            maxframe = 20;
            delay = 30;
            loadEffect("Effect/heal", "Songs/Arcane_AttackF2");
        }
    }
}
