﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace Heroes_0._01
{
    public class MeteoritEffect : AbstractAnimations
    {
        private Texture2D light;
        private Rectangle lightRect;
        private int time;
        private int diametrLight = 500;
        private int shiftLight_y = 100;

        public MeteoritEffect(MyGame game, float zIndex, Point position, SpriteEffects flip) : base(game)
        {
            maxframe = 18;

            drawField.Width = 300;
            drawField.Height = 300;

            drawField.X = position.X;
            drawField.Y = position.Y - drawField.Height;

            delay = 60;

            this.zIndex = zIndex;
            this.flip = flip;

            loadAnimation("Effect/cometa");

            light = game.Content.Load<Texture2D>("Characters/Shades/noir");

            lightRect = new Rectangle(drawField.X + (drawField.Width - diametrLight) / 2,
                                      drawField.Y + (drawField.Height - diametrLight) / 2 + shiftLight_y,
                                      (int)(diametrLight * GameFeature.scaleEllipse_X),
                                      (int)(diametrLight * GameFeature.scaleEllipse_Y));

            game.Content.Load<SoundEffect>("Songs/Flame_Impact2").Play();

            Camera.Shake(1, 30);

            isVisible = true;
        }

        private void drawLight(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(light, lightRect, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 1);
        }

        private void updateLight(GameTime gameTime)
        {
            time += gameTime.ElapsedGameTime.Milliseconds;
            if (time >= delay)
            {
                time -= delay;

                int rnd_x = 10 - GameFeature.rand.Next(20);
                int rnd_y = 10 - GameFeature.rand.Next(20);

                lightRect = new Rectangle(drawField.X + (drawField.Width - diametrLight) / 2 + rnd_x,
                                          drawField.Y + (drawField.Height - diametrLight) / 2 + rnd_y + shiftLight_y,
                                          (int)(diametrLight * GameFeature.scaleEllipse_X + rnd_x * 2),
                                          (int)(diametrLight * GameFeature.scaleEllipse_Y + rnd_y * 2));
            }
        }

        public override bool Play()
        {
            game.drawLight -= drawLight;
            game.updateGame -= updateLight;

            game.drawLight += drawLight;
            game.updateGame += updateLight;
            return base.Play();
        }

        public override bool Stop()
        {
            game.drawLight -= drawLight;
            game.updateGame -= updateLight;
            return base.Stop();
        }

        protected override void onAnimationEnd()
        {
            game.drawLight -= drawLight;
            game.updateGame -= updateLight;
            base.onAnimationEnd();
            Remove();
        }

        public override void Remove()
        {
            if (game != null)
            {
                game.drawLight -= drawLight;
                game.updateGame -= updateLight;
            }
            base.Remove();
        }
    }
}
