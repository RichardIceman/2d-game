﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    class SwordHitEffect : AbstractEffect
    {
        public SwordHitEffect(MyGame game, AbstractCharacter character, sideAnimation side, SpriteEffects flip) : base(game, character, side, flip)
        {
            width = 75;
            height = 100;
            maxframe = 10;
            delay = 100;
            shiftY = -10;
            Camera.Shake(0.2f, 5);
            volume = 0.5f;
            loadEffect("Effect/swordhit", "Songs/sword-unsheathe");
        }
    }
}
