﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class RainOfArrowsAnimation
    {
        private MyGame game;

        private Random rand;

        private SpriteEffects flip;
        private int diametr_x;
        private int diametr_y;

        private Vector2 shift = new Vector2(-40,80);
        private Vector2 position;

        public int delaySpawn = 200;
        private int timeSpawn;

        private int time;
        private int updateDelay = 30;

        private int flyFrame = 4;

        private bool isStop = false;

        private List<FallArrowEffect> listArrowsEffect;
        private List<Vector2> listPositionArrows;
        private List<float> listEndPositionArrows;

        private float zIndex = 5 * GameFeature.zIndexStep;

        public RainOfArrowsAnimation(MyGame game)
        {
            this.game = game;

            rand = new Random(DateTime.Now.Millisecond);

            listPositionArrows = new List<Vector2>();
            listArrowsEffect = new List<FallArrowEffect>();
            listEndPositionArrows = new List<float>();
        }

        private void update(GameTime gameTime)
        {
            timeSpawn += gameTime.ElapsedGameTime.Milliseconds;
            time += gameTime.ElapsedGameTime.Milliseconds;

            if (timeSpawn >= delaySpawn && !isStop)
            {
                timeSpawn -= delaySpawn;

                Vector2 positionNewArrow = new Vector2(position.X + rand.Next(diametr_x*2/3) - diametr_x / 2 - shift.X * flyFrame,
                                                       position.Y + rand.Next(diametr_y * 2/3) - diametr_y / 2 - shift.Y * flyFrame);

                float zIndex = GameFeature.getZIndex(positionNewArrow.Y + shift.Y * flyFrame + 150);

                FallArrowEffect newFallArrow = new FallArrowEffect(game, zIndex, positionNewArrow.ToPoint(), flip);
                newFallArrow.animationEnd += NewFallArrow_animationEnd;

                listEndPositionArrows.Add(positionNewArrow.Y + shift.Y * flyFrame);
                listArrowsEffect.Add(newFallArrow);
                listPositionArrows.Add(positionNewArrow);
            }

            if (time >= updateDelay)
            {
                time -= updateDelay;

                for (int i = listArrowsEffect.Count - 1; i >= 0; i--)
                {
                    if (listPositionArrows[i].Y < listEndPositionArrows[i])
                    { 
                        listPositionArrows[i] += shift;
                        listArrowsEffect[i].updatePosition(listPositionArrows[i].ToPoint());
                    }
                }
            }
        }

        private void NewFallArrow_animationEnd(object sender, EventArgs e)
        {
            FallArrowEffect effect = sender as FallArrowEffect;

            int i = listArrowsEffect.IndexOf(effect);

            if (i > 0)
            {
                listArrowsEffect.RemoveAt(i);
                listPositionArrows.RemoveAt(i);
            }

            if (listArrowsEffect.Count == 0 && isStop)
            {
                game.updateGame -= update;
            }
        }

        public void Play(Vector2 position, SpriteEffects flip, int diametr)
        {
            if (flip == SpriteEffects.FlipHorizontally)
                shift.X = Math.Abs(shift.X);
            else
                shift.X = -Math.Abs(shift.X);

            listEndPositionArrows.Clear();
            listArrowsEffect.Clear();
            listPositionArrows.Clear();

            isStop = false;

            diametr_x = (int)(diametr * GameFeature.scaleEllipse_X);
            diametr_y = (int)(diametr * GameFeature.scaleEllipse_Y);

            timeSpawn = 0;
            position.Y -= shift.Y;

            this.flip = flip;
            this.position = position;

            game.updateGame -= update;
            game.updateGame += update;
        }

        public void Stop()
        {
            isStop = true;
        }
    }
}
