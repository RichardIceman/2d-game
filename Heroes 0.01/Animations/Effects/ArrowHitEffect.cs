﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class ArrowHitEffect : AbstractEffect
    {
        public ArrowHitEffect(MyGame game, AbstractCharacter character, sideAnimation side, SpriteEffects flip) : base(game, character, side, flip)
        {
            width = 150;
            height = 150;
            shiftY = 0;
            maxframe = 7;
            delay = 30;
            pitch = 1;
            Camera.Shake(0.2f, 5);
            loadEffect("Effect/arrowhit", "Songs/Arcane_ImpactF2");
        }
    }
}
