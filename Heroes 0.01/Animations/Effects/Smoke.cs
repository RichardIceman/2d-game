﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    class Smoke : AbstractEffect
    {
        public Smoke(MyGame game, AbstractCharacter character, sideAnimation side, SpriteEffects flip) : base(game, character, side, flip)
        {
            width = 300;
            height = 300;
            shiftY = 40;
            maxframe = 14;
            delay = 60;
            loadEffect("Effect/smoke", "Songs/Arcane_AttackF2");
        }
    }
}
