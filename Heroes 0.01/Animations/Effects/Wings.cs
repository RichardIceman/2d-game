﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    class Wings : AbstractEffect
    {
        public Wings(MyGame game, AbstractCharacter character, sideAnimation side, SpriteEffects flip) : base(game, character, side, flip)
        {
            width = 200;
            height = 200;
            shiftY = 0;
            maxframe = 10;
            delay = 60;
            loadEffect("Effect/resurrectionWings", "Songs/Arcane_AttackF2");
        }
    }
}
