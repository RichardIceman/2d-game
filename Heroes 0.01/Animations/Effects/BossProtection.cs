﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class  BossProtection : AbstractEffect
    {
        public BossProtection(MyGame game, AbstractCharacter character, sideAnimation side, SpriteEffects flip) : base(game, character, side, flip)
        {
            width = 400;
            height = 400;
            shiftY = 120;
            maxframe = 20;
            delay = 50;
            canSkip = true;
            loadEffect("Effect/resistance", "Songs/Arcane_AttackF2");
        }

        protected override void Update(GameTime gameTime)
        {
            if (position != character.drawField.Location)
            {
                position = character.drawField.Location;
                if (side == sideAnimation.Front) zIndex = character.zIndex + GameFeature.zIndexStep;
                else zIndex = character.zIndex - GameFeature.zIndexStep;

                updatePosition();
            }

            currentTime += gameTime.ElapsedGameTime.Milliseconds;
            if (currentTime >= delay)
            {
                currentTime -= delay;
                frame.X = currentFrame * frameWidth;
                currentFrame = (currentFrame + 1) % maxframe;
            }
        }
    }
}
