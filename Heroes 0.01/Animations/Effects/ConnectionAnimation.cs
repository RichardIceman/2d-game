﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Heroes_0._01
{
    public class ConnectionAnimation : AbstractAnimations
    {
        private AbstractCharacter character_1;
        private AbstractCharacter character_2;

        private Vector2 distance;

        public ConnectionAnimation(MyGame game, AbstractCharacter _character_1, AbstractCharacter _character_2) : base(game)
        {
            character_1 = _character_1;
            character_2 = _character_2;

            drawField.Height = 40;

            maxframe = 8;     

            loadAnimation("Effect/lightning_0");

            origin = new Vector2(frame.Width / 2, frame.Height / 2);

            zIndex = GameFeature.zIndexStep;
        }

        protected override void Update(GameTime gameTime)
        {
            currentTime += gameTime.ElapsedGameTime.Milliseconds;
            if (currentTime >= delay)
            {
                currentTime -= delay;
                frame.X = currentFrame * frameWidth;
                currentFrame = (currentFrame + 1) % maxframe;
            }

            if (character_1 != null && character_2 != null)
                distance = character_1.positionOnMap - character_2.positionOnMap;
            else return;

            drawField.Width = (int)Math.Sqrt(distance.X * distance.X + distance.Y * distance.Y);

            drawField.X = (int)(character_1.positionOnMap.X - distance.X / 2);
            drawField.Y = (int)(character_1.positionOnMap.Y - distance.Y / 2);

            angle = (float)(Math.Atan(Math.Abs(distance.Y / distance.X)));
            if (distance.X >= 0 && distance.Y <= 0)
            {
                angle = -angle;
            }
            else if (distance.X < 0 && distance.Y < 0)
            {
                flip = SpriteEffects.FlipHorizontally;
            }
            else if (distance.X < 0 && distance.Y > 0)
            {
                angle = -angle;
                flip = SpriteEffects.FlipHorizontally;
            }

        }

        public override void Remove()
        {
            character_1 = null;
            character_2 = null;
            base.Remove();
        }
    }
}
