﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    class FireskullHitEffect : AbstractEffect
    {
        public FireskullHitEffect(MyGame game, AbstractCharacter character, sideAnimation side, SpriteEffects flip) : base(game, character, side, flip)
        {
            width = 200;
            height = 200;
            shiftY = 0;
            maxframe = 16;
            delay = 30;
            color = Color.Purple;
            Camera.Shake(0.2f, 5);
            loadEffect("Effect/fireballHit", "Songs/Flame_Impact2");
        }
    }
}
