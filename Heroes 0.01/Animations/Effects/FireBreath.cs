﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace Heroes_0._01
{
    public class FireBreath : AbstractAnimations
    {
        public FireBreath(MyGame game, float zIndex, Point position, SpriteEffects flip) : base(game)
        {
            maxframe = 5;

            drawField.Width = 500;
            drawField.Height = 300;

            drawField.X = position.X;
            drawField.Y = position.Y - drawField.Height;

            delay = 120;

            this.zIndex = zIndex;
            this.flip = flip;

            loadAnimation("Effect/breath");            

            game.Content.Load<SoundEffect>("Songs/Flame_Impact2").Play();

            Camera.Shake(1, 50);

            isVisible = true;
        }

        protected override void onAnimationEnd()
        {
            base.onAnimationEnd();
            Remove();
        }
    }
}
