﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class AbstractAnimations
    {
        public event EventHandler animationEnd;

        protected MyGame game;
        protected SpriteEffects flip;
        protected Vector2 origin = Vector2.Zero;

        protected Texture2D texture;
        protected Rectangle frame;
        protected Rectangle drawField;

        private bool isvisible = false;
        public bool isVisible
        {
            get { return isvisible; }
            set
            {
                if (value != isvisible)
                {
                    if (value)
                    {
                        game.drawGameObject -= Draw;
                        game.drawGameObject += Draw;
                        game.updateGame -= Update;
                        game.updateGame += Update;
                    }
                    else
                    {
                        game.drawGameObject -= Draw;
                        game.updateGame -= Update;
                    }

                    isvisible = value;
                }
            }
        }

        public bool canSkip = false;
        public bool isEnd = true;

        public int frameWidth = 1;
        public int frameHeight = 212;

        public int delay = 90;
        protected int currentTime = 0;
        protected int currentFrame = 0;
        protected int maxframe = 5;

        public int maxFrame
        {
            set
            {
                maxframe = value;
                frameWidth = texture.Width / value;
                frame.Width = frameWidth;
            }
        }

        protected float zIndex;
        protected float angle = 0;
        public Color color = Color.White;

        public AbstractAnimations(MyGame game)
        {
            this.game = game;
        }

        protected virtual void loadAnimation(string path)
        {
            texture = game.Content.Load<Texture2D>(path);
            frameWidth = texture.Width / maxframe;
            frame = new Rectangle(0, 0, frameWidth, texture.Height);       
        }

        protected virtual void Update(GameTime gameTime)
        {
            currentTime += gameTime.ElapsedGameTime.Milliseconds;
            if (currentTime>=delay)
            {
                currentFrame++;
                if (currentFrame >= maxframe)
                    onAnimationEnd();

                currentTime -= delay;
                frame.X = currentFrame * frameWidth;
            }
        }

        protected virtual void onAnimationEnd()
        {
            isEnd = true;
            Stop();
            animationEndEvent();
        }

        protected void animationEndEvent()
        {
            animationEnd?.Invoke(this, null);
        }

        protected virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, drawField, frame, color, angle, origin, flip, zIndex);
        }

        public virtual bool Play()
        {
            isEnd = false;
            frame.X = 0;
            currentFrame = 0;
            isVisible = true;
            return true;
        }

        public virtual bool Stop()
        {
            if (canSkip || isEnd)
            {
                isVisible = false;
                return true;
            }
            return false;
        }

        public virtual void Remove()
        {
            isVisible = false;
            game = null;
        }
    }
}
