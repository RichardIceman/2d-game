﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Attack : CharacterAnimation
    {
        public Attack(MyGame game, AbstractCharacter character, string path): base (game, character)
        {
            frameWidth = 213;
            loadAnimation(path);
        }
    }
}
