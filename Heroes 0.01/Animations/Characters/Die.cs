﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Die : CharacterAnimation
    {
        public Die(MyGame game, AbstractCharacter character, string path): base (game, character)
        {
            loadAnimation(path);
            canSkip = false;
        }

        protected override void Update(GameTime gameTime)
        {
            currentTime += gameTime.ElapsedGameTime.Milliseconds;
            if (currentTime >= delay)
            {
                currentTime -= delay;
                frame.X = currentFrame * frameWidth;
                if (currentFrame < maxframe - 1) currentFrame++;
                else if (!isEnd)
                {
                    isEnd = true;
                    animationEndEvent();
                }
            }
        }

        public override bool Play()
        {
            AbstractAnimations temp = character.currentAnim;
            bool canskipTmp = temp.canSkip;
            temp.canSkip = true;
            bool result = base.Play();
            temp.canSkip = canskipTmp;
            return result;
        }
    }
}
