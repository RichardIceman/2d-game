﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Heroes_0._01
{
    public class CharacterAnimation : AbstractAnimations
    {
        protected AbstractCharacter character;

        public bool isReverse = false;

        public CharacterAnimation(MyGame game,AbstractCharacter character) : base(game)
        {
            this.character = character;
        }

        public override bool Play()
        {
            if (character == null) return false;

            if (!isReverse)
            {
                if (character.shift.X >= 0) flip = SpriteEffects.None;
                else flip = SpriteEffects.FlipHorizontally;
            }
            else
            {
                if (character.shift.X >= 0) flip = SpriteEffects.FlipHorizontally;
                else flip = SpriteEffects.None;
            }

            if (character.currentAnim == this || character.currentAnim != null && !character.currentAnim.Stop()) return false;

            character.Stand();
            character.currentAnim = this;
            character.sizeAvatar.X = frameWidth * character.sizeCharacter;
            character.sizeAvatar.Y = frameHeight * character.sizeCharacter;
            character.drawField.Width = (int)(character.sizeAvatar.X);
            character.drawField.Height = (int)(character.sizeAvatar.Y);

            return base.Play();
        }

        protected override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, character.drawField, frame, Color.White * character.indexVisibility, 0, Vector2.Zero, flip, character.zIndex);
        }

        public override void Remove()
        {
            character = null;
            base.Remove();
        }
    }
}
