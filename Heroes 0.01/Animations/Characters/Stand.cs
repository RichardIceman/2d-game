﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Stand : CharacterAnimation
    {
        public Stand(MyGame game, AbstractCharacter character, string path) : base (game, character)
        {
            canSkip = true;
            loadAnimation(path);
        }

        protected override void Update(GameTime gameTime)
        {
            currentTime += gameTime.ElapsedGameTime.Milliseconds;
            if (currentTime >= delay)
            {
                currentTime -= delay;
                frame.X = currentFrame * frameWidth;
                currentFrame = (currentFrame + 1) % maxframe;
            }
        }
    }
}
