﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Hurt : CharacterAnimation
    {
        public Hurt(MyGame game, AbstractCharacter character, string path): base (game, character)
        {
            frameWidth = 232;
            canSkip = true;
            loadAnimation(path);
        }
    }
}
