﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Heroes_0._01
{
    public abstract class AbstractProjectileEffect : AbstractAnimations
    {
        private AbstractProjectile projectile;

        public AbstractProjectileEffect(MyGame game, AbstractProjectile projectile):base(game)
        {
            this.projectile = projectile;
            canSkip = true;
        }

        protected override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, projectile.drawField, frame, Color.White, projectile.angle, projectile.origin, projectile.flip, projectile.zIndex);
        }

        protected override void Update(GameTime gameTime)
        {
            currentTime += gameTime.ElapsedGameTime.Milliseconds;
            if (currentTime >= delay)
            {
                currentFrame++;
                if (currentFrame >= maxframe)
                    currentFrame = 0;

                currentTime -= delay;
                frame.X = currentFrame * frameWidth;
            }
        }
    }
}
