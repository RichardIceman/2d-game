﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    class FireskullEffect : AbstractProjectileEffect
    {
        public FireskullEffect(MyGame game, AbstractProjectile projectile) : base(game, projectile)
        {
            maxframe = 8;
            loadAnimation("Projectile/fire-skull");
        }
    }
}
