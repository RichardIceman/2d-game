﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class ArrowEffect : AbstractProjectileEffect
    {
        public ArrowEffect(MyGame game, AbstractProjectile projectile) : base(game, projectile)
        {
            maxframe = 1;
            loadAnimation("Projectile/Arrow");
        }
    }
}
