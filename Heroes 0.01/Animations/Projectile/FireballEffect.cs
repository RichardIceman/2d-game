﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    class FireballEffect : AbstractProjectileEffect
    {
        public FireballEffect(MyGame game, AbstractProjectile projectile) : base(game, projectile)
        {
            maxframe = 6;
            loadAnimation("Projectile/fireball");
        }
    }
}
