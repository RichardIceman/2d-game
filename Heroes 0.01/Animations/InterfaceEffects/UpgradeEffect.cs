﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Heroes_0._01
{
    public class UpgradeEffect : AbstractInterfaceEffect
    {
        public UpgradeEffect(MyGame game, float zIndex, Point position) : base(game, zIndex, position)
        {
            drawField.Width = (int)(200 * Camera.scaleWnd);
            drawField.Height = (int)(200 * Camera.scaleWnd);
            delay = 50;
            maxframe = 18;
            loadAnimation("Effect/upgradeskill");
        }
    }
}
