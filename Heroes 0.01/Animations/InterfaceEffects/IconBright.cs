﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Heroes_0._01
{
    public class IconBright : AbstractInterfaceEffect
    {
        public IconBright(MyGame game, float zIndex, Point position, Point size) : base(game, zIndex, position)
        {
            drawField.Width = size.X;
            drawField.Height = size.Y;
            maxframe = 10;
            loadAnimation("Effect/brightIcon");
        }

        protected override void Update(GameTime gameTime)
        {
            currentTime += gameTime.ElapsedGameTime.Milliseconds;
            if (currentTime >= delay)
            {
                currentTime -= delay;
                frame.X = currentFrame * frameWidth;
                currentFrame = (currentFrame + 1) % maxframe;
            }
        }
    }
}
