﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public abstract class AbstractInterfaceEffect : AbstractAnimations
    {
        public AbstractInterfaceEffect(MyGame game, float zIndex, Point position) : base(game)
        {
            canSkip = true;
            this.zIndex = zIndex;
            drawField.Location = position;
        }

        public override bool Play()
        {
            Stop();
            isEnd = false;
            frame.X = 0;
            currentFrame = 0;
            game.updateInterface += Update;
            game.drawInterface += Draw;
            return true;
        }

        public override bool Stop()
        {
            if (canSkip || isEnd)
            {
                game.drawInterface -= Draw;
                game.updateInterface -= Update;
                return true;
            }
            return false;
        }

        protected override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, drawField, frame, Color.White, 0, Vector2.Zero, flip, zIndex);
        }
    }
}
