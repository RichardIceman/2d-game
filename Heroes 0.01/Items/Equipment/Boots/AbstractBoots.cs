﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public abstract class AbstractBoots : AbstractEquipment
    {
        public AbstractBoots(MyGame game) : base(game)
        {
            type = ItemType.Ботинки;
        }
    }
}
