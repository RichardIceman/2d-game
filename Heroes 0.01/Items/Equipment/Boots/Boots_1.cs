﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Boots_1 : AbstractBoots
    {
        public Boots_1(MyGame game) : base(game)
        {
            MoveSpeed = 2;

            Cost = 5;

            icon = game.Content.Load<Texture2D>("Items/Equipment/Boots");
            name = "Кожаные сапоги";
            description = $"Повышают скорость\nперемещения на {MoveSpeed}";
        }
    }
}
