﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Sword_1 : AbstractWeapon
    {
        public Sword_1(MyGame game) : base(game)
        {
            typeAttack = TypeAttack.Меч;

            Attack = 7;
            rangeAttack = 100;

            Cost = 10;

            icon = game.Content.Load<Texture2D>("Items/Equipment/sword");
            name = "Меч рыцаря";
            description = $"Меч выкованный\nлучшими кузнецами.\nАтака: {Attack}";
        }
    }
}
