﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Wand_1 : AbstractWeapon
    {
        public Wand_1(MyGame game) : base(game)
        {
            typeAttack = TypeAttack.Fireball;

            Attack = 8;
            rangeAttack = 350;

            Cost = 10;

            icon = game.Content.Load<Texture2D>("Items/Equipment/wand");
            name = "Магический посох";
            description = $"Загадочный посох,\nисточающий странную\nауру.\nАтака: {Attack}";
        }
    }
}
