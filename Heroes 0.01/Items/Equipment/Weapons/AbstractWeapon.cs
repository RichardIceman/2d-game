﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public abstract class AbstractWeapon : AbstractEquipment
    {
        public TypeAttack typeAttack;
        public int rangeAttack;

        public AbstractWeapon(MyGame game) : base(game)
        {
            type = ItemType.ОсновноеОружие;
        }
    }
}
