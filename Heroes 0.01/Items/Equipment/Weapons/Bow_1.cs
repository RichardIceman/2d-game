﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Bow_1 : AbstractWeapon
    {
        public Bow_1(MyGame game) : base(game)
        {
            typeAttack = TypeAttack.Лук;

            Attack = 10;
            rangeAttack = 550;

            Cost = 10;

            icon = game.Content.Load<Texture2D>("Items/Equipment/bow");
            name = "Лук охотника";
            description = $"Отличный лук для\nохоты.\nАтака: {Attack}";
        }
    }
}
