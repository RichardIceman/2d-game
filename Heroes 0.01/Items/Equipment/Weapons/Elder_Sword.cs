﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Elder_Sword : AbstractWeapon
    {
        public Elder_Sword(MyGame game) : base(game)
        {
            typeAttack = TypeAttack.Меч;

            Attack = 30;
            rangeAttack = 120;

            Cost = 1200;

            icon = game.Content.Load<Texture2D>("Items/Equipment/sword");
            name = "Древний меч эльфов";
            description = $"Давно забытый меч.\nАтака: {Attack}\n";
        }
    }
}
