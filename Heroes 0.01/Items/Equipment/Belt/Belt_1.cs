﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Belt_1 : AbstractBelt
    {
        public Belt_1(MyGame game) : base(game)
        {
            Size = 1;

            Cost = 10;

            icon = game.Content.Load<Texture2D>("Items/Equipment/belt");
            name = "Кожаный пояс";
            description = $"Сумок для зелий: {Size}";

        }
    }
}
