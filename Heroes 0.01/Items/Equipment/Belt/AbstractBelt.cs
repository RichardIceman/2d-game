﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public abstract class AbstractBelt : AbstractEquipment
    {
        public int Size = 0;

        public AbstractBelt(MyGame game) : base(game)
        {
            type = ItemType.Пояс;
        }
    }
}
