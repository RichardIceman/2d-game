﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class LightArmor_1 : AbstractArmor
    {
        public LightArmor_1(MyGame game) : base(game)
        {
            armorType = ArmorType.Легкая;

            Cost = 18;

            Defense = 4;

            icon = game.Content.Load<Texture2D>("Items/Equipment/leather");
            name = "Кожаный нагрудник";
            description = $"Легкий нагрудник\nиз прочной дубленой\nкожи.\nЗащита: {Defense}";
        }
    }
}
