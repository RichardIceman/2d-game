﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public enum ArmorType
    {
        Легкая,
        Тяжелая,
        Ткань
    }

    public class AbstractArmor : AbstractEquipment
    {
        public ArmorType armorType;

        public AbstractArmor(MyGame game) : base(game)
        {
            type = ItemType.Доспех;
        }
    }
}
