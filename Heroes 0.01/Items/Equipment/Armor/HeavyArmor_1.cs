﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class HeavyArmor_1 : AbstractArmor
    {
        public HeavyArmor_1(MyGame game) : base(game)
        {
            armorType = ArmorType.Тяжелая;

            Cost = 24;

            Defense = 5;
            HealthPoint = 30;

            icon = game.Content.Load<Texture2D>("Items/Equipment/armor");
            name = "Стальной нагрудник";
            description = $"Тяжелый нагрудник\nиз прочной стали.\nЗащита: {Defense}\nПовышение здоровья: {HealthPoint}";
        }
    }
}
