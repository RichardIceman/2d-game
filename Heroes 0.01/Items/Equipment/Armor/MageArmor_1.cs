﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class MageArmor_1 : AbstractArmor
    {
        public MageArmor_1(MyGame game) : base(game)
        {
            armorType = ArmorType.Ткань;

            Cost = 15;

            Defense = 3;
            ManaPoint = 20;

            icon = game.Content.Load<Texture2D>("Items/Equipment/3");
            name = "Мантия жреца";
            description = $"Магическая мантия\nиз рунической ткани.\nЗащита: {Defense}\nПовышение маны: {ManaPoint}";
        }
    }
}
