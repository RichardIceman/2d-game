﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Quiver_1 : AbstractSecondWeapon
    {
        public Quiver_1(MyGame game) : base(game)
        {
            secondWeaponType = SecondWeaponType.Колчан;

            Cost = 6;

            icon = game.Content.Load<Texture2D>("Items/Equipment/quiver");
            name = "Колчан охотника";
            description = $"Волшебный колчан\nс бесконечными\nстрелами.";
        }
    }
}
