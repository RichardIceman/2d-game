﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public enum SecondWeaponType
    {
        Щит,
        Книга,
        Колчан
    }

    public abstract class AbstractSecondWeapon : AbstractEquipment
    {
        public SecondWeaponType secondWeaponType;

        public AbstractSecondWeapon(MyGame game) : base(game)
        {
            type = ItemType.ВторостепенноеОружие;
        }
    }
}
