﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Book_1 : AbstractSecondWeapon
    {
        public Book_1(MyGame game) : base(game)
        {
            secondWeaponType = SecondWeaponType.Книга;

            HealPower = 5;

            Cost = 12;

            icon = game.Content.Load<Texture2D>("Items/Equipment/book2");
            name = "Древний манускрипт";
            description = $"В нем скрыты многие\nтайны.\nПовышение силы\nлечения: {HealPower}";
        }
    }
}
