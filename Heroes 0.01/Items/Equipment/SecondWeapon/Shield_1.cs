﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Shield_1 : AbstractSecondWeapon
    {
        public Shield_1(MyGame game) : base(game)
        {
            secondWeaponType = SecondWeaponType.Щит;

            Defense = 2;

            Cost = 8;

            icon = game.Content.Load<Texture2D>("Items/Equipment/shield_semi");
            name = "Прочный щит";
            description = $"Хороший деревянный\nщит с металическими\nвставками.\nЗащита: {Defense}";
        }
    }
}
