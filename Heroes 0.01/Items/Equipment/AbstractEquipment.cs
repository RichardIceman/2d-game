﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public abstract class AbstractEquipment : AbstractItem
    {
        public int HealthPoint = 0;
        public int ManaPoint = 0;
        public int Attack = 0;
        public int Defense = 0;
        public int HealPower = 0;
        public int ManaRegen = 0;
        public int HealthRegen = 0;
        public int MoveSpeed = 0;

        public AbstractEquipment(MyGame game) : base(game)
        {

        }

        public AbstractItem Equipe(AbstractCharacter character)
        {
            this.character = character;

            isVisible = false;

            AbstractItem item = null;

            switch(type)
            {
                case ItemType.Шлем:
                    item = character.inventory[24];
                    character.inventory[24] = this;
                    break;
                case ItemType.Доспех:
                    item = character.inventory[25];
                    character.inventory[25] = this;
                    break;
                case ItemType.Пояс:
                    item = character.inventory[26];
                    character.inventory[26] = this;
                    break;
                case ItemType.Ботинки:
                    item = character.inventory[27];
                    character.inventory[27] = this;
                    break;
                case ItemType.Ожерелье:
                    item = character.inventory[28];
                    character.inventory[28] = this;
                    break;
                case ItemType.ОсновноеОружие:
                    item = character.inventory[29];
                    character.inventory[29] = this;
                    break;
                case ItemType.ВторостепенноеОружие:
                    item = character.inventory[30];
                    character.inventory[30] = this;
                    break;
            }
            return item;
        }
    }
}
