﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class OldNecklace : AbstractItem
    {
        public OldNecklace(MyGame game) : base(game)
        {
            type = ItemType.Ожерелье;

            dropItemWidth = 40;
            dropItemHeight = 40;

            icon = game.Content.Load<Texture2D>("Items/Artefacts/ожерелье_без_камней");
            name = "Древнее ожерелье";
            description = "Кажется, в нем не\nхватает нескольких\nкамней.";

            Cost = 66666;
        }
    }
}
