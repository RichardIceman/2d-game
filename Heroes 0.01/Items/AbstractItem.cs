﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{

    public enum ItemType
    {
        Артефакт,
        Любой,
        Зелье,
        Шлем,
        Ожерелье,
        Доспех,
        Пояс,
        Ботинки,
        ОсновноеОружие,
        ВторостепенноеОружие
    }

    public abstract class AbstractItem : InteractiveObject
    {
        public ItemType type;

        public int itemLvl = 1;

        public int count = 1;
        public int maxCount = 1;

        public AbstractCharacter character;
        private Map map;

        public Texture2D icon;

        protected Texture2D shade;
        protected Texture2D defaultShade;
        protected Texture2D mouseoverShade;
        public Rectangle shadeField;
        protected int dropItemWidth = 60;
        protected int dropItemHeight = 60;
        private bool isMouseover;

        private Vector2 position;
        protected int maxAmp = 10;
        private float amp = 0;
        protected float ampSpd = 0.5f;

        private int cost;
        public int Cost
        {
            get
            {
                if (character != null) return (int)(cost * character.priceMult);

                return cost;
            }
            set
            {
                cost = value;
            }
        }
        public string name;
        public string description;

        private Description descriptionField;

        public AbstractItem(MyGame game) : base(game)
        {      
            defaultShade = game.Content.Load<Texture2D>("Characters/Shades/shade");
            mouseoverShade = game.Content.Load<Texture2D>("Characters/Shades/shadeY");

            descriptionField = new Description(game, false);
        }

        protected override void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(shade, shadeField, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 0.09f);
            spriteBatch.Draw(icon, drawField, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, zIndex);
        }

        private void update(GameTime gameTime)
        {
            amp += ampSpd;

            if (amp >= maxAmp || amp <= 0)
                ampSpd = -ampSpd;

            drawField.Y = (int)(position.Y - amp);
        }

        public virtual void Pickup(AbstractCharacter character)
        {
            int index = -1;
            for (int i = 0; i < 24; i++)
                if (character.inventory[i] != null)
                {
                    if (character.inventory[i].type == ItemType.Зелье && type == ItemType.Зелье && character.inventory[i].itemLvl == itemLvl)
                    {
                        StackableItem potion = character.inventory[i] as StackableItem;

                        StackableItem thisPotion = this as StackableItem;

                        if (potion.count == potion.maxCount || thisPotion.stackType != potion.stackType) continue;

                        if (thisPotion.stackType == potion.stackType && potion.count + count > potion.maxCount)
                        {
                            count = potion.count + count - potion.maxCount;
                            potion.count = maxCount;                                               
                            continue;
                        }

                        character.inventory[i].count += count;
                        descriptionField.isVisible = false;

                        addItemInInventory(character);
                        return;
                    }
                }
                else if (index == -1) index = i;

            if (index == -1)
            {
                character.Say("Недостаточно\nместа.", TypeCharacter.Герой);
            }
            else
            {
                character.inventory[index] = this;
                addItemInInventory(character);
            }
        }

        private void addItemInInventory(AbstractCharacter character)
        {          
            this.character = character;
            if (map != null) map.listItems.Remove(this);

            game.updateGame -= update;

            mouseEnter -= mouseOver;
            mouseLeave -= lostMouseOver;

            descriptionField.isVisible = false;
            descriptionField.isInterface = true;
            descriptionField.updateDescription(name, description, 0.17f, drawField, zIndex);

            isVisible = false;

            character.onInventoryChanged();
        }

        public void Drop(Map map, Point position)
        {
            this.map = map;
            character = null;

            shade = defaultShade;

            if (!map.listItems.Contains(this)) map.listItems.Add(this);

            position.X = position.X + dropItemWidth / 2;
            position.Y = position.Y - dropItemHeight;

            drawField = new Rectangle(position.X, position.Y, dropItemWidth, dropItemHeight);

            shadeField.Width = (int)(dropItemWidth * 0.7);
            shadeField.Height = (int)(dropItemHeight * 0.3);

            this.position = position.ToVector2();

            shadeField.X = position.X + (dropItemWidth - shadeField.Width) / 2;
            shadeField.Y = (int)(position.Y + dropItemHeight * 0.85);       

            zIndex = GameFeature.getZIndex(position.Y + dropItemHeight);

            descriptionField.isInterface = false;
            descriptionField.updateDescription(name, description, 0.17f, drawField, zIndex);

            game.updateGame -= update;
            game.updateGame += update;

            mouseEnter -= mouseOver;
            mouseLeave -= lostMouseOver;
            mouseEnter += mouseOver;
            mouseLeave += lostMouseOver;

            isVisible = true;
        }

        public override void Remove()
        {
            base.Remove();
            game.updateGame -= update;
            mouseEnter -= mouseOver;
            mouseLeave -= lostMouseOver;

            count = 0;

            descriptionField.isVisible = false;
            descriptionField = null;

            if (map != null)
            {
                map.listItems.Remove(this);
                map = null;
            }

            if (character != null)
            {

                for (int i = 0; i < character.inventory.Count(); i++)
                    if (character.inventory[i] == this)
                    {
                        character.inventory[i] = null;
                        break;
                    }

                character.onInventoryChanged();
                character = null;
            }
        }

        public void mouseOver(object o, EventArgs e)
        {
            if (isMouseover) return;

            descriptionField.isVisible = true;

            shade = mouseoverShade;
            isMouseover = true;
        }

        public void lostMouseOver(object o, EventArgs e)
        {
            if (!isMouseover) return;

            descriptionField.isVisible = false;

            shade = defaultShade;
            isMouseover = false;
        }
    }
}
