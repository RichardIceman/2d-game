﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class ResurrectScroll : StackableItem
    {
        public ResurrectScroll(MyGame game) : base(game)
        {
            icon = game.Content.Load<Texture2D>("Items/Potions/scroll");
            name = "Свиток воскрешения";
            description = "Воскрешает ближайших\nсоюзных юнитов\nвосстанавливая им 20%\nздоровья.\nРадиус действия: 500";
            stackType = StackType.Свиток;

            maxCount = 3;

            dropItemWidth = 50;
            dropItemHeight = 50;

            Cost = 50;
        }

        protected override void Effect()
        {
            if (character == null) return;

            int rangeCast = 500;
            character.animAttack.Play();
            character.Say("Герои не умирают!", TypeCharacter.Герой);
            for (int i = 0; i < character.currentMap.listHeroOnMap.Count; i++)
            {
                Vector2 distance = character.positionOnMap - character.currentMap.listHeroOnMap[i].positionOnMap;
                if (character.currentMap.listHeroOnMap[i] != character &&
                    !character.currentMap.listHeroOnMap[i].isAlive &&
                    GameFeature.inEllipse(distance, rangeCast))
                {
                    new Wings(character.game, character.currentMap.listHeroOnMap[i], sideAnimation.Back, SpriteEffects.None).Play();
                    character.currentMap.listHeroOnMap[i].healthBar.Value = character.currentMap.listHeroOnMap[i].healthBar.MaxValue / 5;
                    character.currentMap.listHeroOnMap[i].Resurrect();
                }
            }
        }


        public override StackableItem getNewItem()
        {
            ResurrectScroll newPotion = new ResurrectScroll(game);
            newPotion.character = character;

            return newPotion;
        }
    }
}
