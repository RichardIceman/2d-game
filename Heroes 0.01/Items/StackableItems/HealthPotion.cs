﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class HealthPotion : StackableItem
    {
        public HealthPotion(MyGame game) : base(game)
        {
            icon = game.Content.Load<Texture2D>("Items/Potions/potions (5)");
            name = "Зелье здоровья";
            description = "Восстанавливает 10 О3";
            stackType = StackType.Зулье_здоровья;
            
            Cost = 10;
        }

        protected override void Effect()
        {
            HealthPotion test1 = new HealthPotion(game);
            HealthPotion test2 = new HealthPotion(game);

            character.healthBar.Value += 10;
            new Heal(game, character, sideAnimation.Front, SpriteEffects.None).Play();
        }

        public override StackableItem getNewItem()
        {
            HealthPotion newPotion = new HealthPotion(game);
            newPotion.character = character;

            return newPotion;
        }
    }
}
