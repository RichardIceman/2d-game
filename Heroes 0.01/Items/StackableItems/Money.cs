﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Money : StackableItem
    {
        public Money(MyGame game) : base(game)
        {
            name = "Золото";
            description = "Мешочек с золотом";
            icon = game.Content.Load<Texture2D>("Items/мешочек-с-золотом-png-2");

            Cost = 1;
            stackType = StackType.Золото;

            maxCount = int.MaxValue;
        }

        public override StackableItem getNewItem()
        {
            Money money = new Money(game);
            money.character = character;

            return money;
        }

        protected override void Effect() { }

        public override void Use() { }

        public override void Pickup(AbstractCharacter character)
        {
            GameFeature.Money += count;
            Remove();
        }
    }
}
