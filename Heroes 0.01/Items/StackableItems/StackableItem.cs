﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public enum StackType
    {
        Зелье_маны,
        Зулье_здоровья,
        Золото,
        Свиток
    }

    public abstract class StackableItem : AbstractItem
    {
        public StackType stackType;

        public StackableItem(MyGame game): base(game)
        {
            type = ItemType.Зелье;
            maxCount = 12;
        }

        public virtual void Use()
        {
            if (character != null && character.isAlive)
            {
                Effect();
                count--;
                character.onInventoryChanged();
                if (count <= 0) Remove();
            }
        }

        protected abstract void Effect();
        public abstract StackableItem getNewItem();
    }
}
