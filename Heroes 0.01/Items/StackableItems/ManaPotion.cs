﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class ManaPotion : StackableItem
    {
        public ManaPotion(MyGame game) : base(game)
        {
            icon = game.Content.Load<Texture2D>("Items/Potions/potions (1)");
            name = "Зелье маны";
            description = "Восстанавливает 20 ОМ";
            stackType = StackType.Зелье_маны;

            Cost = 5;
        }

        protected override void Effect()
        {
            character.manaBar.Value += 20;
            new ManaRestoreEffect(game, character, sideAnimation.Front, SpriteEffects.None).Play();
        }


        public override StackableItem getNewItem()
        {
            ManaPotion newPotion = new ManaPotion(game);
            newPotion.character = character;

            return newPotion;
        }
    }
}
