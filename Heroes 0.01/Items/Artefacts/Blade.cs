﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Blade : StackableItem
    {
        public Blade(MyGame game) : base(game)
        {
            type = ItemType.Любой;

            icon = game.Content.Load<Texture2D>("Items/Artefacts/лезвие");
            name = "Лезвие древнего меча";
            description = "Даже спустя столько лет,\nоно все еще острое.";

            Cost = 500;
        }

        public override StackableItem getNewItem()
        {
            return new Blade(game);
        }

        protected override void Effect()
        {
            if (character != null)
            {

            }
        }
    }
}
