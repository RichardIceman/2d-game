﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class Handle : StackableItem
    {
        public Handle(MyGame game) : base(game)
        {
            type = ItemType.Любой;

            dropItemWidth = 40;
            dropItemHeight = 40;

            icon = game.Content.Load<Texture2D>("Items/Artefacts/рукоять");
            name = "Рукоять меча";
            description = "Очень удобная рукоять\nдревнего меча.";

            Cost = 500;
        }

        public override StackableItem getNewItem()
        {
            return new Handle(game);
        }

        protected override void Effect()
        {
            if (character != null)
            {
                for (int i = 0; i < GameFeature.inventorySize; i++)
                    if (character.inventory[i].name == "Лезвие древнего меча")
                    {
                        Remove();
                        character.inventory[i].Remove();

                    }
            }
        }
    }
}
