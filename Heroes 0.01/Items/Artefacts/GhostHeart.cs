﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class GhostHeart : AbstractItem
    {
        public GhostHeart(MyGame game) : base(game)
        {
            type = ItemType.Артефакт;

            dropItemWidth = 40;
            dropItemHeight = 40;

            icon = game.Content.Load<Texture2D>("Items/Artefacts/shpinel-chernaja");
            name = "Сердце Джу";
            description = "Загадочный камень. Будто\nбесчисленное множество душ\nзаточены в нем.";

            Cost = 1000;
        }
    }
}
