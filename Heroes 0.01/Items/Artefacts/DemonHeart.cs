﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class DemonHeart : AbstractItem
    {
        public DemonHeart(MyGame game) : base(game)
        {
            type = ItemType.Артефакт;

            dropItemWidth = 40;
            dropItemHeight = 40;

            icon = game.Content.Load<Texture2D>("Items/Artefacts/clipart-rubin");
            name = "Сердце Ахерона";
            description = "Загадочный камень. Внутри\nпылает неистовое пламя.\nКажется он может усилить\nчто угодно.";

            Cost = 1000;
        }
    }
}
