﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_0._01
{
    public class GolemHeart : AbstractItem
    {
        public GolemHeart(MyGame game) : base(game)
        {
            type = ItemType.Артефакт;

            dropItemWidth = 40;
            dropItemHeight = 40;

            icon = game.Content.Load<Texture2D>("Items/Artefacts/sapphire");
            name = "Сердце Глыбы";
            description = "Загадочный камень. Кажется\nв нем заточена сила, способная\nсдержать что угодно.";

            Cost = 1000;
        }
    }
}
